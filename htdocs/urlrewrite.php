<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#/pay/([0-9]+)/#",
		"RULE" => "SCORE_ID=\$1",
		"ID" => "",
		"PATH" => "/pay/index.php",
	),
	array(
		"CONDITION" => "#^/api/.*#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/api/api.php",
	),
	array(
		"CONDITION" => "#^/create/([0-9]+)/($|index\\.php|\\?.*\\=.*)#",
		"RULE" => "ID=\$1",
		"ID" => "",
		"PATH" => "/create/index.php",
	),
    array(
        "CONDITION" => "#^/create/([0-9a-zA-Z\-\_]+)/($|index\\.php|\\?.*\\=.*)#",
        "RULE" => "ID=\$1",
        "ID" => "",
        "PATH" => "/create/index.php",
    ),
    array(
        "CONDITION" => "#^/create/($|index\\.php|\\?.*\\=.*)#",
        "RULE" => "",
        "ID" => "",
        "PATH" => "/create/index.php",
    ),
	array(
		"CONDITION" => "#^/auth/#",
		"RULE" => "",
		"ID" => "diamis:auth",
		"PATH" => "/auth/index.php",
	),

    array(
        "CONDITION" => "#/packages/([0-9]+)/($|index\\.php|\\?.*\\=.*)#",
        "RULE" => "ID=\$1",
        "ID" => "",
        "PATH" => "/packages/index.php",
    ),

    array(
        'CONDITION' => "#/review/([a-z]+)/([0-9]+)/($|index\\.php|\\?.*\\=.*)#",
        "RULE" => "TYPE=\$1&ID=\$2",
        'PATH' => '/review/index.php'
    ),

    // Профиль
    array(
        'CONDITION' => "#/profile/ads/([a-z]+)/($|index\\.php|\\?.*\\=.*)#",
        "RULE" => "TYPE=\$1",
        'PATH' => '/profile/ads/index.php'
    ),
    array(
        'CONDITION' => "#/profile/message/([0-9]+)/($|index\\.php|\\?.*\\=.*)#",
        "RULE" => "ID=\$1",
        'PATH' => '/profile/message/index.php'
    ),


    // Объявления
    array(
        "CONDITION" => "#^/map/($|index\\.php|\\?.*\\=.*)#",
        "RULE" => "TEMPLATE=MAP",
        "ID" => "",
        "PATH" => "/catalog/index.php",
    ),
    array(
        "CONDITION" => "#^/([a-z0-9\-]+)/([a-z0-9\-]+)/([a-z0-9\-]+)/([a-z0-9\-\_]+)/($|index\\.php|\\?.*\\=.*)#",
        "RULE" => "REGION_CODE=\$1&SECTION_CODE=\$2&TYPE_CODE=\$3&ELEMENT_CODE=\$4",
        "ID" => "",
        "PATH" => "/catalog/index.php",
    ),
    array(
        "CONDITION" => "#^/([a-z0-9\-]+)/([a-z0-9\-]+)/([a-z0-9\-]+)/($|index\\.php|\\?.*\\=.*)#",
        "RULE" => "REGION_CODE=\$1&SECTION_CODE=\$2&TYPE_CODE=\$3",
        "ID" => "",
        "PATH" => "/catalog/index.php",
    ),
    array(
        "CONDITION" => "#^/([a-z0-9\-]+)/([a-z0-9\-]+)/($|index\\.php|\\?.*\\=.*)#",
        "RULE" => "REGION_CODE=\$1&SECTION_CODE=$2",
        "ID" => "",
        "PATH" => "/catalog/index.php",
    ),
    array(
        "CONDITION" => "#^/([a-z0-9\-]+)/($|index\\.php|\\?.*\\=.*)#",
        "RULE" => "REGION_CODE=\$1",
        "ID" => "",
        "PATH" => "/catalog/index.php",
    ),
);

?>