<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Персональный раздел");
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.register", 
	".default", 
	array(
		"USER_PROPERTY_NAME" => "",
		"SEF_MODE" => "Y",
		"SHOW_FIELDS" => array(
			0 => "EMAIL",
			1 => "NAME",
			2 => "PERSONAL_PHONE",
		),
		"REQUIRED_FIELDS" => array(
			0 => "EMAIL",
			1 => "PERSONAL_PHONE",
		),
		"AUTH" => "N",
		"USE_BACKURL" => "Y",
		"SUCCESS_PAGE" => "/profile/register-info.php",
		"SET_TITLE" => "Y",
		"USER_PROPERTY" => array(
		),
		"SEF_FOLDER" => "/",
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>