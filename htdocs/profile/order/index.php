<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мои Заказы - ты мне я тебе");
?>
<div class="section">
    <div class="content">
        <?
        $APPLICATION->IncludeComponent(
            'bitrix:menu',
            'profile',
            array(
                "ROOT_MENU_TYPE" => "profile",
                "MAX_LEVEL" => "1",
                "USE_EXT" => "Y"
            )
        );
        ?>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>