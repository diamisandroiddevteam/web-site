<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мои Сообщения - ты мне я тебе");
?>
<div class="section">
    <div class="content">
        <?
        $APPLICATION->IncludeComponent(
            'bitrix:menu',
            'profile',
            array(
                "ROOT_MENU_TYPE" => "profile",
                "MAX_LEVEL" => "1",
                "USE_EXT" => "Y"
            )
        );
        ?>
        <?
        $APPLICATION->IncludeComponent(
            'diamis:chat',
            '',
            array()
        );
        ?>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>