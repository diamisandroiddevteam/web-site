<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


use Bitrix\Main\Loader;

use Diamis\Ads\TypeTable;

Loader::includeModule('diamis.ads');

$category_id = [
   21
];
$data = [
    'Запчасти',
    'Аксессуары ',
    'GPS-навигаторы ',
    'Автокосметика и автохимия',
    'Аудио- и видеотехника',
    'Багажники и фаркопы',
    'Инструменты ',
    'Прицепы ',
    'Противоугонные устройства ',
    'Тюнинг ',
    'Шины, диски и колёса ',
    'Экипировка ',
];

try{


    foreach($category_id as $id)
    {

        foreach ($data as $i=>$item)
        {
            $item = trim($item);

            if(!strlen($item))
                throw new Exception('Значение пустое');

            $res = TypeTable::getList(array('filter'=>array('CATEGORY_ID'=>$id, 'NAME'=>$item), 'limit'=>1));
            if($res->getSelectedRowsCount())
                throw new Exception('Тип <b>'.$item.'</b> для категории '. $id .' уже создан');

            TypeTable::add(array(
                'CATEGORY_ID' => $id,
                'NAME' => $item,
                'ACTIVE' => true,
                'SORT' => $i
            ));
        }

    }



} catch (Exception $e) {
    echo $e->getMessage();
}