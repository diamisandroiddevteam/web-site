<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


use Bitrix\Main\Loader;
use Diamis\Ads\FieldsTable;
use SimpleExcel\SimpleExcel;


Loader::includeModule('diamis.ads');


require_once($_SERVER["DOCUMENT_ROOT"].'/tools/SimpleExcel/SimpleExcel.php');

$excel = new SimpleExcel('csv');
$excel->parser->loadFile('fields.csv');

$rows = $excel->parser->getField();
foreach($rows as $key=>$row)
{
    $data = explode(';', implode(' ', $row));
    $fName = trim($data[0]);
    $fType = trim($data[1]) ? trim($data[1]) : 'string';

    if($fName && $fType)
    {
        $db = FieldsTable::getList(array(
            'select' => array('ID'),
            'filter' => array(
                'NAME' => $fName,
                'TYPE' => $fType
            )
        ))->fetch();

        if(!$db['ID'])
        {
            $add = FieldsTable::add(array(
                'ACTIVE' => true,
                'NAME' => $fName,
                'TYPE' => $fType,
                'SORT' => $key.'0'
            ));
            if(!$add->isSuccess()) {
                p($add->getErrorMessages(), 1);
            };
        }
    }
}
echo 'WORK!!!';