<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/../vendor/autoload.php");

global $DB;

use \Bitrix\Main\Loader;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use \Diamis\Ads\Base;
use \Diamis\Ads\TypeTable;
use \Diamis\Ads\FieldsTable;
use \Diamis\Ads\CategoryTable;
use \Diamis\Ads\RelationsTable;


Loader::includeModule('diamis.ads');

$types = array();
$dbTypes = TypeTable::getList();
foreach($dbTypes as $type)
{
    $types[] = $type;
}


$fields = array();
$dbFields = FieldsTable::getList();
foreach ($dbFields as $field)
{
    $fields[] = $field;
}

$Category = CategoryTable::getList(array(
    'select' => array(
        'ID',
        'CODE',
        'PARENT',
        'ACTIVE',
        'NAME',
        'TYPE_ID'  =>'\Diamis\Ads\TypeTable:CATEGORY.ID',
        'TYPE_NAME'=>'\Diamis\Ads\TypeTable:CATEGORY.NAME'
    )
))->fetchAll();
$trees = Base::tree($Category);

$key = 1;
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$sheet->setCellValue('A'.$key, 'Category');
$sheet->setCellValue('B'.$key, 'CategoryChild');
$sheet->setCellValue('C'.$key, 'Type');
$sheet->setCellValue('D'.$key, 'FieldID');
$sheet->setCellValue('E'.$key, 'FieldName');



foreach($trees as $tree)
{
    $id = $tree['ID'];
    $name = trim($tree['NAME']);

    if(strlen($name)):
        $key ++;
        $sheet->setCellValue('A'.$key, $name);

        foreach($tree['CHILD'] as $tr)
        {
            $subId = $tr['ID'];
            $subName = trim($tr['NAME']);

            if(strlen($subName)):
                $sheet->setCellValue('A'.$key, $name);
                $sheet->setCellValue('B'.$key, $subName);
                if($tr['TYPE_NAME']) $sheet->setCellValue('C'.$key, $tr['TYPE_NAME']);
                $sheet->setCellValue('D'.$key, 48);
                $sheet->setCellValue('E'.$key, 'Цена');

                foreach($fields as $field)
                {

                }

                $key +=10;
            endif;
        }
    endif;
}


$writer = new Xlsx($spreadsheet);
$writer->save('diamis.ads.relations.xlsx');
p($fields);