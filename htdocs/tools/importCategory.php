<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


use Bitrix\Main\Loader;
use Diamis\Ads\CategoryTable;
use Diamis\Ads\TypeTable;

Loader::includeModule('diamis.ads');


function tree($tree)
{
	$data = array();
	$level = array(-1,-1,-1,-1,-1);
    $row = 0;
	while($row<count($tree))
	{
		$col1 = trim($tree[$row][0]);
		$col2 = trim($tree[$row][1]);
		$col3 = trim($tree[$row][2]);
		$col4 = trim($tree[$row][3]);
		$col5 = trim($tree[$row][4]);
		

		if($col1) {
			$level[0] ++;
            if($level[1]>=0) $level[1] = -1;
			$data[$level[0]]['name'] = $col1;
		} else if($col2) {
			$level[1] ++;
            if($level[2]>=0) $level[2] = -1;
			$data[$level[0]]['child'][$level[1]]['name'] = $col2;
		} else if($col3) {
			$level[2] ++;
			if($level[3]>=0) $level[3] = -1;
			$data[$level[0]]['child'][$level[1]]['child'][$level[2]]['name'] = $col3;
		} else if($col4) {
//			$level[3] ++;
//			$data[$level[0]]['child'][$level[1]]['child'][$level[2]]['child'][$level[3]]['name'] = $col4;
		}

		$row++;
	}

	return $data;
}

function search($array, $search, $parent = false) 
{
	$i = 0;
	$id = null;
	$search = mb_strtolower(trim($search));
	while($i<count($array))
	{
		$name = mb_strtolower(trim($array[$i]['NAME']));
		if($name==$search && $array[$i]['PARENT']==0) {
			$id = $array[$i]['ID'];
			break;
		} else if($name==$search && $parent==$array[$i]['PARENT']) {
			$id = $array[$i]['ID'];
			break;
		}
		$i++;
	}

	return $id;
}


// Чтение файла
include_once($_SERVER["DOCUMENT_ROOT"]."/tools/XLSXReader.php");
$xlsx = new XLSXReader($_SERVER["DOCUMENT_ROOT"].'/tools/category.xlsx');
$sheetNames = $xlsx->getSheetNames();

foreach($sheetNames as $sheetName)
{
	$sheet = $xlsx->getSheet($sheetName);
	$data = $sheet->getData();
	$result = tree($data, 0);
}




$isJson = isset($_REQUEST['json']) ? true : false;
$json = array(
	'status'  => 'success',
	'message' => array()
);



$category = CategoryTable::getList(array())->fetchAll();
foreach($result as $cgtKey=>$cgtData)
{
    // поиск и запись Категории
    $ctgID = search($category, $cgtData['name']);
    if(!$ctgID && $cgtData['name']) {
        $db = CategoryTable::add(array('NAME'   => $cgtData['name'],'ACTIVE' => true,'PARENT' => 0,'SORT'	 => intval($cgtKey)));
        $ctgID = $db->getID();
    }

    foreach($cgtData['child'] as $subKey=>$subData)
    {
        // поиск и запись ПодКатегории
        $subID = search($category, $subData['name']);
        if(!$subID && $subData['name']) {
            $db = CategoryTable::add(array('NAME'   => $subData['name'],'ACTIVE' => true,'PARENT' => $ctgID,'SORT'	 => intval($subKey)));
            $subID = $db->getID();
        }

        foreach($subData['child'] as $typeKey=>$typeData)
        {
            $db = TypeTable::getList(array('filter'=>array('NAME'=>$typeData['name'],'CATEGORY'=>$subID),'limit'=>1))->fetch();
            if(!$db && $typeData['name'])
            {
                TypeTable::add(array(
                    'ACTIVE'    => true,
                    'CATEGORY'  => $subID,
                    'NAME' => $typeData['name'],
                    'SORT' => $typeKey
                ));
            }
        }
    }
}