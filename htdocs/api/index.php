<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

include_once $_SERVER['DOCUMENT_ROOT'].'/local/php_interface/config.php';
define("API_INIT", true);

global $APPLICATION;

$uri = mb_strtolower(str_replace('/api/', '', $APPLICATION->GetCurPage()));
$box = explode('/', $uri);


if(!$box[2]) {
    $box[2] = 'index';
}


try {
    $section = '/api/'.$box[0].'/api-' . $box[1] . '/';
    $method = !empty($box[3]) ? $box[2] . '/' . $box[3] . '.php' : $box[2] . '.php';
    $file_init = $_SERVER["DOCUMENT_ROOT"] . $section . $method;

    // обнуляем данные
    $count= 0;
    $data = null;
    if(is_file($file_init)) {
        if($_REQUEST['data']) $data = $_REQUEST['data'];

        include $file_init;

        $arRes = [
            'status'  => 'success',
            'message' => $arResult ? $arResult : null
        ];

        if($count>0) $arRes['count'] = (int) $count;

        echo json_encode($arRes);
    } else {
        throw new Exception('Not found API: ' . $box[0]);
    }
} catch (Exception $e) {
    $msg = $e->getMessage();
    $arRes = [
        'status' => 'error',
        'message' => $msg
    ];
    echo json_encode($arRes);
}
die();