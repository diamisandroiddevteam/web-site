<?php
// init core 1C-Bitrix
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION;


define('SECRET_KEY', '28e336acfdg6c9423d946fdgd19c6a2632');


$resutl = array();
try {
    // Удаляем из url /api/ и / на конце
    $uri = str_replace('/api/', '', $APPLICATION->GetCurPage());
    if(mb_substr($uri, mb_strlen($uri)-1, mb_strlen($uri))==='/')
        $uri = (mb_substr($uri, 0, mb_strlen($uri)-1));


    $path = explode('/', $uri);
    $controller = ucfirst(strip_tags($path[1]));
    $action = strip_tags($path[2]);

    $params = $_REQUEST;

    if( file_exists("{$_SERVER["DOCUMENT_ROOT"]}/api/{$path[0]}/{$controller}Class.php") ) {
        include_once "{$_SERVER["DOCUMENT_ROOT"]}/api/{$path[0]}/MainInterface.php";
        include_once "{$_SERVER["DOCUMENT_ROOT"]}/api/{$path[0]}/{$controller}Class.php";
    } else {
        throw new Exception('Controller is invalid.');
    }


    $classes = 'Diamis\Api\\'.$controller;
    $controller = new $classes($params);

    if(!$action) {
        $action = 'init';
    }

    if( method_exists($controller, $action) === false ) {
        throw new Exception('method "'.$action.'" is invalid.');
    }

    $result['status'] = true;
    $result['result'] = $controller->$action();

} catch ( Exception $e) {
    $result['status'] = false;
    $result['error']  = $e->getMessage();
}


echo json_encode($result);
exit();