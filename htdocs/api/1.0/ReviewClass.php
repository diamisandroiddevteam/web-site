<?php
namespace Diamis\Api;


//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader;
use \Diamis\Ads\ReviewTable;

Loader::includeModule('diamis.ads');

class Review implements MainInterface
{
    protected $params = array();


    public function __construct($params)
    {
        $this->params = $params;
    }


    public function init()
    {
        // TODO: Implement init() method.
    }


    public function create()
    {
        return ReviewTable::create($this->params);
    }
}