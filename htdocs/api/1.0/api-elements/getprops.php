<?php
if(!defined('API_INIT')) die('Access closed');


$IBLOCK_ID = 1;
$props = CIBlockProperty::GetList(
    array(
        "sort" => "asc",
        "name" => "asc"
    ),
    array(
        "ACTIVE" => "Y",
        "IBLOCK_ID" => $IBLOCK_ID
    )
);

$arResult = null;
while ($prop = $props->Fetch())
{
    $arResult[] = array(
        'id'   => $prop['ID'],
        'user_type' => $prop['USER_TYPE'],
        'property_type' => $prop['PROPERTY_TYPE'],
        'name' => $prop['NAME'],
        'code' => $prop['CODE'],
        'active' => $prop['ACTIVE'],
        'required' => $prop['IS_REQUIRED']
    );
}

?>