<?php
if(!defined('API_INIT')) die('Access closed');


// поля для заполнения
$fields = array();


if(empty($data['iblock_id'])) throw new Exception('no field found: iblock_id');
else $fields['IBLOCK_ID'] = (int) $data['iblock_id'];

if(empty($data['section_id'])) throw new Exception('no field found: section_id');
else $fields['SECTION_ID'] = $data['section_id'];

if(empty($data['name'])) throw new Exception('no field found: name');
else $fields['NAME'] = $data['name'];


$props = array();
$query_props = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $fields['IBLOCK_ID']));
while($prop = $query_props->Fetch())
{
    if($prop['IS_REQUIRED']=='Y' and empty($data[$prop['CODE']]))
        throw new Exception('Required properties are not filled: '.$prop['CODE']);

    /*
    PROPERTY_TYPE - по типу свойства:
        S - строка
        N - число
        L - список
        F - файл
        G - привязка к разделу
        E - привязка к элементу
     */
    if($prop['PROPERTY_TYPE']=='L' and !empty($data[$prop['CODE']])):

        $query_enum = CIBlockPropertyEnum::GetList(array(), array('PROPERTY_ID'=>$prop['ID']));
        if(!$query_enum->SelectedRowsCount()) {

            $props[$prop['ID']] = is_array($data[$prop['CODE']]) ? $data[$prop['CODE']] : array('VALUE'=>$data[$prop['CODE']]);
        } else {

            $enums = array();
            while ($enum = $query_enum->Fetch()) {
                $enums[$enum['ID']] = true;
            }

            if(is_numeric($data[$prop['CODE']]))
            {
                $props[$prop['ID']] = array(
                    'VALUE' => $data[$prop['CODE']]
                );
            }
            elseif(is_array($data[$prop['CODE']]))
            {
                foreach($data[$prop['CODE']] as $id)
                    if(isset($enums[$id])) $props[$prop['ID']][] = $id;
            }
        }
    elseif($prop['PROPERTY_TYPE']=='S' and $prop['USER_TYPE']=='HTML' and !empty($data[$prop['CODE']])):

        $props[$prop['ID']][0]['TEXT'] = $data[$prop['CODE']];
        $props[$prop['ID']][0]['TYPE'] = $prop['DEFAULT_VALUE']['TYPE'];
    else:

        if(!empty($data[$prop['CODE']]))
            $props[$prop['ID']] = $data[$prop['CODE']];
    endif;
}

$fields['PROPERTY_VALUES'] = $props;


$el = new CIBlockElement;

echo '<pre>';
print_r($fields);
echo '</pre>';