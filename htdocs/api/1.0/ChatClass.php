<?php
namespace Diamis\Api;

use Bitrix\Main\Loader;
use Diamis\Chat\Base as ChatBase;
use Exception;


Loader::includeModule('diamis.chat');


class Chat implements MainInterface {

    public function __construct($params)
    {
        parent::__construct($params);
    }

    public function init()
    {

    }

    /**
     * Получаем список созданных чатов в которых участвует пользователь
     * author - ID пользователя
     */
    public function getChat()
    {
        $author = intval($this->params['author']);
        return ChatBase::getChat($author);
    }


    /**
     * Получить список сообщений выбранного чата
     *
     * Данные:
     * type - тип чата (по умолчанию 1 - для объявления)
     * entity_id - идентификатор группы чата (берется значение ID объявления для типа 1)
     * to - id пользователя для которого адрисовано сообщение
     * from - id пользователя
     */
    public function getMessages() {
        $entity_id = intval($this->params['entity_id']);
        $to = intval($this->params['to']);
        $from = intval($this->params['from']);
        $type = intval($this->params['type']);

        return ChatBase::getMessages($entity_id, $to, $from, $type);
    }

    /**
     * Отправка сообщения
     * Если чат с указанным type и entity_id не существет, то система автоматически создает данный чат
     *
     * Данные:
     * type - тип чата (по умолчанию 1 - для объявления)
     * entity_id - идентификатор группы чата (берется значение ID объявления для типа 1)
     * to - id пользователя для которого адрисовано сообщение
     * from - id пользователя
     * content - сообщение
     */
    public function setMessage() {
        $entity_id = intval($this->params['entity_id']);
        $to = intval($this->params['to']);
        $from = intval($this->params['from']);
        $type = intval($this->params['type']);
        $content = trim(strip_tags($this->params['content']));

        return ChatBase::setMessage($entity_id, $to, $from, $content, $type);
    }




    /**
     * Проверяем на наличе пользователя в черном списке
     * user_id - Id пользователя для поиска
     * author - владелец списка
     *
     * Если user_id найден в списке то возращает дату добавления
     */
    public function isBlackList() {
        $user_id = intval($this->params['user_id']);
        $author = intval($this->params['author']);

        return ChatBase::isBlackList($user_id, $author);
    }


    /**
     * Возвращает содержимое Черного Списка
     * author - владелец списка
     *
     */
    public function getBlackList() {
        $author = intval($this->params['author']);
        return ChatBase::getBlackList($author);
    }
}