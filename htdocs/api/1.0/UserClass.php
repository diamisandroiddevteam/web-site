<?php
namespace Diamis\Api;

use Diamis\Api\MainInterface;
use Exception;


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

class User implements MainInterface {

    protected $user_id = null;

    protected $data = null;

    protected $params = null;

    function __construct($prop) {
        global $USER;

        $this->data = $prop;

        $this->params = \CUser::GetList($USER->GetID())->Fetch();
        $this->user_id = $this->params['id'];
    }

    public function init()
    {

    }



    /** Запрос: /api/1.0/User/getProp/?id=1
     *
     * Возвращает значение пользователя
     */
    public function getProp(){
        if(!$this->user_id) {
            throw new Exception("id пользователя не передано");
        }

        return \CUser::GetByID($this->user_id);
    }


    public function update(){


        $update = array();

        foreach($this->data as $key=>$val){
            $isUserFields = substr($key,0,3)=='UF_' ? true : false;
            if($isUserFields) {
                $update[$key] = strip_tags(trim($val));
            }

            if($key==='NAME') $update[$key] = strip_tags(trim($val));
            if($key==='LAST_NAME') $update[$key] = strip_tags(trim($val));
        };

        if(count($update) and $this->params['id']) {
            $user = new \CUser;
            $user->Update($this->params['id'], $update);
            if($user->LAST_ERROR){
                throw new Exception("Error: ".$user->LAST_ERROR);
            }
        }

        return $this->params['id'];
    }
}