<?php
namespace Diamis\Api;

use Bitrix\Main\Loader;
use Diamis\Api\MainInterface;
use Diamis\Ads\CategoryTable;
use Diamis\Ads\Base as AdsBase;
use Exception;


Loader::includeModule('diamis.ads');

class Categories implements MainInterface {

    private $id;

    private $type;

    private $action;


    public function __construct($params)
    {
        $this->id = intval($params['id']);
        $this->type = intval($params['type']);
        $this->action = intval($params['action']);
    }


    /**
     * Запрос: /api/1.0/categories/
     *
     * Аналогичен запросу: /api/1.0/categories/getAll/
     */
    public function init(){
        return self::getAll();
    }


    private function filter() {
        $arFilter = array();
        if($this->action)
            $arFilter['ACTIVE'] = true;

        return $arFilter;
    }


    /**
     * Запрос: /api/1.0/categories/getAll/
     *
     * Возвращает все Категории сайта
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    public function getAll()
    {
        $Categories = array();
        $dbCategory = CategoryTable::getList(array(
            'filter'=> $this->filter(),
            'cache' => array('ttl' => 3600)
        ));

        while($Category = $dbCategory->fetch()):
            // Получаем путь до изображения
            if($Category['FILE_ID']>0) {
                $Category['FILE_PATH'] = \CFile::GetPath($Category['FILE_ID']);
            }

            $Categories[] = $Category;
        endwhile;

        return $Categories;
    }


    /**
     * Запрос: /api/1.0/categories/getTree/
     *
     * Возвращаеть массив категорий с учетом вложенности
     * @return array
     */
    public function getTree()
    {
        $Categories = $this->getAll();
        return AdsBase::tree($Categories);
    }


    /**
     * Запрос: /api/1.0/categories/getFields/
     *
     * В запросе так же обязательно необходимо указать:
     * id - Индентификатор категории
     * type - Идентификатор типа объявления
     * Пример: /api/1.0/categories/getFields/?id=2&type=2
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */

    public function getFields()
    {
        if(!$this->id) throw new Exception('Variable "id" not found.');
        if(!$this->type) throw new Exception('Variable "type" not found.');

        $fields = AdsBase::getFields($this->id, $this->type);

        return $fields;
    }
}
