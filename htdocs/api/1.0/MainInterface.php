<?php
namespace Diamis\Api;


interface MainInterface{

    public function __construct($params);

    public function init();
}