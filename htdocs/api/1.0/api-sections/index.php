<?php
if(!defined('API_INIT')) die('Access closed');

CModule::IncludeModule("iblock");

$arResult = [];
$arSort = [
    "DEPTH_LEVEL" => "ASC"
];
$arFilter = [
    'ACTIVE' => 'Y',
    'GLOBAL_ACTIVE' => 'Y',
    'IBLOCK_ID' => API_CATALOG_ID
];
$arSelect = [
    'ID',
    'NAME',
    'CODE',
    'SORT',
    'ACTIVE',
    'IBLOCK_ID',
    'IBLOCK_SECTION_ID',
    'DEPTH_LEVEL',
];
$lists = null;
$dbSct = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
while($sct = $dbSct->Fetch()) {
    switch($sct['DEPTH_LEVEL'])
    {
        case 3:
            $key = 0;
            $parentID = null;
            $parentSubID = null;
            while(!$parentID)
            {
                if(empty($arResult[$key]['ITEMS']) || count($arResult[$key]['ITEMS'])==0)
                {
                    $key ++;
                }
                else
                {
                    $parentSubID = array_search($sct['IBLOCK_SECTION_ID'], array_column($arResult[$key]['ITEMS']), 'ID');
                    if($parentSubID) {
                        $parentID = array_search($parentSubID, array_column($arResult,'ID'));
                        $arResult[$parentID]['ITEMS'][$parentSubID]['ITEMS'][] = $sct;
                        break;
                    }
                }
            }
            break;
        case 2:
            $parentID = array_search($sct['IBLOCK_SECTION_ID'], array_column($arResult,'ID'));
            $arResult[$parentID]['ITEMS'][] = $sct;
            break;
        default:
            $arResult[] = $sct;
            break;
    }
}
$count = count($arResult);
?>