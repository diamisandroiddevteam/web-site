<?php
namespace Diamis\Api;

use Bitrix\Main\Loader;
use Diamis\Api\MainInterface;
use Diamis\Ads\AdsTable;
use Diamis\Ads\FavoritesTable;
use Diamis\Ads\Base as AdsBase;
use Exception;


Loader::includeModule('diamis.ads');

class Element implements MainInterface {

    private $params;

    public function __construct($params)
    {
        $this->params = $params;
    }



    public function init(){

    }

    /**
     * Выборка элементов по заданным параметрам
     *
     * Запрос: /api/1.0/Element/getFilter/
     *
     * Массив для выборки элементов вида:
     * array(
     *   filter => array(
     *       'ACTIVE' => true    # Принимает два значение (true - объявление прошло модерацию, false - объявление на модерации)
     *       'CATEGORY_ID' => 1  # ID категории
     *       'TYPE_ID' => 1      # ID типа
     *    )
     *   offset => 5        позиция начала выборки (по умолчанию: 0)
     *   limit  => 10       кол-во выбранных элементов (по умолчанию: 10)
     *   expire => true     выводить элементы у которых срок публикации не истех (по умолчанию: true)
     * );
     */
    public function getFilter() {
        $filter = $this->params['filter'];
        $pageSzie = intval($this->params['limit']) ? intval($this->params['limit']) : 10;
        $offset = intval($this->params['offset']) ? intval($this->params['offset']) : 5;

        $expire = $this->params['expire'] ? $this->params['expire'] : true;

        return AdsTable::getElements($filter, $expire, $pageSzie, $offset);
    }



    /**
     * Пример массива для создания объявления:
     *
     * <pre>
     * Array(
     *   [category] => 2
     *   [type] => 2
     *   [name] => Название объявления
     *   [price] => 100 000
     *   [files] => Array(153)
     *   [text] => Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
     *   [1] => Array(
     *      [2018] => 2018
     *   )
     *   [3] => 1
     *   [4] => 2
     *   [phone] => Array( +7 (333) 333-33-33 )
     *   [email] => Array( test@test.ru )
     *   [location] => Array(
     *       [name] => Санкт-Петергбург, ул. Марата, д.75
     *       [lat] => 45.041297082261,
     *       [lng] => 38.991626328124
     *   )
     *  [user] => 1
     * );
     * </pre>
     *
     * !!! Обратите внимание что переменная files (изображение) передает id файла уже загруженного на сервер.
     *
     * Для загрузки изображения необходимо передать запрос "Content-Type: multipart/form-data;"
     * со значение файла в переменной "files" по адрессу: /api/1.0/UploadHandler/
     *
     * Возвращает массив, где result[0][id] - содержит id файла загруженного на сервер
     *
     */
    public function create()
    {
        return AdsTable::create($this->params);
    }


    /**
     * Запрос для получения объявления
     * Запрос: /api/1.0/Element/getID/?id=2
     * @return array|string
     */
    public function getID()
    {
        return AdsTable::getElementID(intval($this->params['id']));
    }


    /**
     * Добавление/Удаление в избранное
     * Запрос: /api/1.0/Element/updateFavorite/?id=2&user_id=1
     */
    public function updateFavorite()
    {
        $id = intval($this->params['id']);
        $user_id = intval($this->params['user_id']);

        return FavoritesTable::updateFavorite($id, $user_id);
    }
}