<?php
namespace Diamis\Api;


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


class UploadHandler implements MainInterface{

    protected $options;

    protected $image_objects = array();

    protected $tmp_dir = '/upload/tmp/';

    public function __construct($params) {
        // параметры
        $this->options = array(
            'upload_dir' => $_SERVER['DOCUMENT_ROOT'] . $this->tmp_dir,
            'upload_url' => $this->get_full_upload_url() . $this->tmp_dir,
            'param_name' => 'files',
            'preview_width' => 200,
            'preview_height' => 120,
        );

    }


    protected function get_server_var($id) {
        return isset($_SERVER[$id]) ? $_SERVER[$id] : '';
    }


    public function init()
    {
        $result = null;
        switch ($this->get_server_var('REQUEST_METHOD')) {
            case 'PATCH':
            case 'PUT':
            case 'POST':
                $result = $this->post();
                break;
            case 'DELETE':
                $result = $this->delete();
                break;
            default:
                $this->header('HTTP/1.1 405 Method Not Allowed');
        }

        return $result;
    }


    public function post() {
        $upload = isset($_FILES[$this->options['param_name']]) ? $_FILES[$this->options['param_name']] : null;


        // Parse the Content-Disposition header, if available:
        $file_name = $this->get_server_var('HTTP_CONTENT_DISPOSITION') ?
            rawurldecode(preg_replace(
                '/(^[^"]+")|("$)/',
                '',
                $this->get_server_var('HTTP_CONTENT_DISPOSITION')
            )) : null;


        // Parse the Content-Range header, which has the following form:
        // Content-Range: bytes 0-524287/2000000
        $content_range = $this->get_server_var('HTTP_CONTENT_RANGE') ?
            preg_split('/[^0-9]+/', $this->get_server_var('HTTP_CONTENT_RANGE')) : null;
        $size =  $content_range ? $content_range[3] : null;
        $files = array();

        if ($upload && is_array($upload['tmp_name'])) {
            // param_name is an array identifier like "files[]",
            // $_FILES is a multi-dimensional array:
            foreach ($upload['tmp_name'] as $index => $value) {
                $files[] = $this->handle_file_upload(
                    $upload['tmp_name'][$index],
                    $file_name ? $file_name : $upload['name'][$index],
                    $size ? $size : $upload['size'][$index],
                    $upload['type'][$index],
                    $upload['error'][$index],
                    $index,
                    $content_range
                );
            }
        } else {
            // param_name is a single object identifier like "file",
            // $_FILES is a one-dimensional array:
            $files[] = $this->handle_file_upload(
                isset($upload['tmp_name']) ? $upload['tmp_name'] : null,
                $file_name ? $file_name : (isset($upload['name']) ? $upload['name'] : null),
                $size ? $size : (isset($upload['size']) ? $upload['size'] : $this->get_server_var('CONTENT_LENGTH')),
                isset($upload['type']) ? $upload['type'] : $this->get_server_var('CONTENT_TYPE'),
                isset($upload['error']) ? $upload['error'] : null,
                null,
                $content_range
            );
        }

//        $resultAdd = $this->generate_response(
//            array($this->options['param_name'] => $files)
//        );


        return $files;
    }

    protected function get_full_upload_url() {
        $https = !empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'on') === 0 ||
            !empty($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            strcasecmp($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') === 0;
        return
            ($https ? 'https://' : 'http://').
            (!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'].'@' : '').
            (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'].
                ($https && $_SERVER['SERVER_PORT'] === 443 ||
                $_SERVER['SERVER_PORT'] === 80 ? '' : ':'.$_SERVER['SERVER_PORT'])));
        //.substr($_SERVER['SCRIPT_NAME'],0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
    }






    protected function body($str) {
        echo $str;
    }

    protected function header($str) {
        header($str);
    }

    protected function send_content_type_header() {
        $this->header('Vary: Accept');
        if (strpos($this->get_server_var('HTTP_ACCEPT'), 'application/json') !== false) {
            $this->header('Content-type: application/json');
        } else {
            $this->header('Content-type: text/plain');
        }
    }

    protected function send_access_control_headers() {
        $this->header('Access-Control-Allow-Origin: '.$this->options['access_control_allow_origin']);
        $this->header('Access-Control-Allow-Credentials: '
            .($this->options['access_control_allow_credentials'] ? 'true' : 'false'));
        $this->header('Access-Control-Allow-Methods: '
            .implode(', ', $this->options['access_control_allow_methods']));
        $this->header('Access-Control-Allow-Headers: '
            .implode(', ', $this->options['access_control_allow_headers']));
    }

    public function head() {
        $this->header('Pragma: no-cache');
        $this->header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->header('Content-Disposition: inline; filename="files.json"');
        // Prevent Internet Explorer from MIME-sniffing the content-type:
        $this->header('X-Content-Type-Options: nosniff');
        if ($this->options['access_control_allow_origin']) {
            $this->send_access_control_headers();
        }
        $this->send_content_type_header();
    }

    protected function generate_response($content, $print_response = true) {
        if ($print_response) {
            $json = json_encode($content);
            $this->head();
            $this->body($json);
        }
        return $content;
    }




    public function get($print_response = true) {
        if ($print_response && $this->get_query_param('download')) {
            return $this->download();
        }
        $file_name = $this->get_file_name_param();
        if ($file_name) {
            $response = array(
                $this->get_singular_param_name() => $this->get_file_object($file_name)
            );
        } else {
            $response = array(
                $this->options['param_name'] => $this->get_file_objects()
            );
        }
        return $this->generate_response($response, $print_response);
    }

    protected function download() {
        switch ($this->options['download_via_php']) {
            case 1:
                $redirect_header = null;
                break;
            case 2:
                $redirect_header = 'X-Sendfile';
                break;
            case 3:
                $redirect_header = 'X-Accel-Redirect';
                break;
            default:
                return $this->header('HTTP/1.1 403 Forbidden');
        }
        $file_name = $this->get_file_name_param();
        if (!$this->is_valid_file_object($file_name)) {
            return $this->header('HTTP/1.1 404 Not Found');
        }
        if ($redirect_header) {
            return $this->header(
                $redirect_header.': '.$this->get_download_url(
                    $file_name,
                    $this->get_version_param(),
                    true
                )
            );
        }
        $file_path = $this->get_upload_path($file_name, $this->get_version_param());
        // Prevent browsers from MIME-sniffing the content-type:
        $this->header('X-Content-Type-Options: nosniff');
        if (!preg_match($this->options['inline_file_types'], $file_name)) {
            $this->header('Content-Type: application/octet-stream');
            $this->header('Content-Disposition: attachment; filename="'.$file_name.'"');
        } else {
            $this->header('Content-Type: '.$this->get_file_type($file_path));
            $this->header('Content-Disposition: inline; filename="'.$file_name.'"');
        }
        $this->header('Content-Length: '.$this->get_file_size($file_path));
        $this->header('Last-Modified: '.gmdate('D, d M Y H:i:s T', filemtime($file_path)));
        $this->readfile($file_path);
    }

    protected function get_query_param($id) {
        return @$_GET[$id];
    }

    protected function get_file_name_param() {
        $name = $this->get_singular_param_name();
        return $this->basename(stripslashes($this->get_query_param($name)));
    }

    protected function get_singular_param_name() {
        return substr($this->options['param_name'], 0, -1);
    }

    protected function get_file_object($file_name) {
        if ($this->is_valid_file_object($file_name)) {
            $file = new \stdClass();
            $file->name = $file_name;
            $file->size = $this->get_file_size(
                $this->get_upload_path($file_name)
            );
            $file->url = $this->get_download_url($file->name);
            foreach ($this->options['image_versions'] as $version => $options) {
                if (!empty($version)) {
                    if (is_file($this->get_upload_path($file_name, $version))) {
                        $file->{$version.'Url'} = $this->get_download_url(
                            $file->name,
                            $version
                        );
                    }
                }
            }
            $this->set_additional_file_properties($file);
            return $file;
        }
        return null;
    }

    protected function get_file_objects($iteration_method = 'get_file_object') {
        $upload_dir = $this->get_upload_path();
        if (!is_dir($upload_dir)) {
            return array();
        }
        return array_values(array_filter(array_map(
            array($this, $iteration_method),
            scandir($upload_dir)
        )));
    }

    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error,  $index = null, $content_range = null) {
        $file = new \stdClass();
        $uploaded = array();
        $errorText = "";
        $fileElementName = $this->options['param_name'];
        // проверка на ошибку и т.д.
        if($error){
            switch($error) {
                case '1':
                    $errorText = 'Размер принятого файла превысил максимально допустимый размер';
                    break;
                case '2':
                    $errorText = 'Размер загружаемого файла превысил максимально допустимый размер';
                    break;
                case '3':
                    $errorText = 'Загружаемый файл был получен только частично';
                    break;
                case '4':
                    $errorText = 'Файл не был загружен';
                    break;
                case '6':
                    $errorText = 'Отсутствует временная папка';
                    break;
                case '7':
                    $errorText = 'Не удалось записать файл на диск';
                    break;
                case '8':
                    $errorText = 'PHP-расширение остановило загрузку файла';
                    break;
                case '999':
                default:
                    $errorText = 'Неизвестная ошибка';
            }
            //
            $file->error = $errorText;
            $file->name = $name;
            $file->size = $size;
            $file->type = $type;
        }elseif(empty($uploaded_file) || $uploaded_file == 'none'){
            $errorText = 'Файл не был загружен';
            //
            $file->error = $errorText;
            $file->name = $name;
            $file->size = $size;
            $file->type = $type;
        }else{
            // формируем файл и т.д.
            $uploaded = array("name"=>$name, "tmp_name"=>$uploaded_file, "type" => $type, "size" => $size, "error" => $error);
            //
            $arIMAGE = array_merge($uploaded, array(
                "del" => ${$fileElementName."_del"},
                "MODULE_ID" => "diamis.ads",
            ));
            $res = \CFile::CheckFile($arIMAGE, 0, false, 'jpg,jpeg,gif,png'); // нах bmp  //5242880
            if (strlen($res) > 0) {
                $errorText = $res;
            }
            list($width, $height) = getimagesize($uploaded_file);
            /*if(intval($width)>2500){
                $error = $error." Ширина изображения не должна превышать 2500 пикселей ";
            }
            if(intval($height)>2500){
                $error = $error." Высота изображения не должна превышать 2500 пикселей ";
            }*/
            /*if(intval($width)<450){
                $error = $error." Ширина изображения должна быть не менее 450 пикселей ";
            }
            if(intval($height)<350){
                $error = $error." Высота изображения должна быть не менее 350 пикселей ";
            }*/
            //AddMessage2Log("\n".var_export(getimagesize($_FILES[$fileElementName]['tmp_name']), true). " \n \r\n ", "getimagesize");
            //AddMessage2Log("\n".var_export($width, true). " \n \r\n ", "width");
            //AddMessage2Log("\n".var_export($height, true). " \n \r\n ", "height");

            if (strlen($errorText) == 0){
                //
                $img_id = \CFile::SaveFile($arIMAGE, "diamis.ads");
                if(intval($width)>800 || intval($height)>600) {
                    $img_id = $this->reSizeFile($img_id);
                }
                $arFile = \CFile::GetFileArray($img_id);
                //
                // AddMessage2Log("\n".var_export($arFile, true). " \n \r\n ", "arFile");
                //for security reason, we force to remove all uploaded file
                @unlink($uploaded_file);
                /// Формируем превью
                $arFileTmp = \CFile::ResizeImageGet(
                    $img_id,
                    array("width" => $this->options['preview_width'], "height" => $this->options['preview_height']),
                    BX_RESIZE_IMAGE_PROPORTIONAL,
                    true, ''
                );
                // AddMessage2Log("\n".var_export($arFileTmp, true). " \n \r\n ", "arFileTmp");
                // тут формируем массив полей и т.д.
                // $file->error = $errorText;
                $file->id = $img_id;
                $file->name = $name;
                $file->size = $size;
                $file->type = $type;
                $file->url = $arFileTmp['src'];
                $file->width = $arFileTmp['width'];
                $file->height = $arFileTmp['height'];
                $file->src = $arFile['SRC'];
                $file->arr = array(
                    "error" => $errorText,
                    "id" => $img_id,
                    "src" => $arFile['SRC'],
                    "width" => $arFile['WIDTH'],
                    "height" => $arFile['HEIGHT'],
                    "preview" => array(
                        "src" => $arFileTmp['src'],
                        "width" => $arFileTmp['width'],
                        "height" => $arFileTmp['height'],
                    ));
            }else{
                $file->error = $errorText;
                $file->name = $name;
                $file->size = $size;
                $file->type = $type;
            }
        }
        return $file;
    }


    protected function reSizeFile($img_id){
        $imgResize = $img_id;
        $fileResize = \CFile::ResizeImageGet(
            $img_id,
            array("width" => 800, "height" => 600),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true, ''
        );
        $arFileID = \CFile::GetFileArray($img_id);
        // !copy($_SERVER["DOCUMENT_ROOT"].$fileResize['src'], $_SERVER["DOCUMENT_ROOT"].$arFileID['SRC']) && !move_uploaded_file($_SERVER["DOCUMENT_ROOT"].$fileResize['src'], $_SERVER["DOCUMENT_ROOT"].$arFileID['SRC'])
        if(!copy($_SERVER["DOCUMENT_ROOT"].$fileResize['src'], $_SERVER["DOCUMENT_ROOT"].$arFileID['SRC'])){
            // не меняем $DB
        }else{
            // меняем $DB
            $setArr = array("HEIGHT"=>$fileResize['height'],"WIDTH"=>$fileResize['width'],"FILE_SIZE"=>$fileResize['size']);
            foreach ($setArr as $s=>$sv) {
                $SET[] = "BF.`".$s."` = '".$sv."'";
            }
            global $DB;
            $DB->StartTransaction();
            $SQL = "UPDATE `b_file` as BF SET ".(count($SET)>0 ? implode(", ",$SET):"")." WHERE BF.`ID` =  ".$img_id."";
            $dbResult = $DB->Query($SQL, true);
            $DB->Commit();
        }
        return $imgResize;
    }

}