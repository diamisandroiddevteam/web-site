<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Демонстрационная версия продукта «1С-Битрикс: Управление сайтом»");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Главная страница");

use Bitrix\Main\Loader;
use Diamis\GeoIp\GeoBase;

Loader::includeModule('diamis.geoip');

$city = GeoBase::getCityIp($_SERVER['REMOTE_ADDR']);

?>
<div class="section-slider">
    <div class="content">
        <div class="offer offertop">
            <span class="offer-title">Найди</span>
            <span class="offer-dot"></span>
            <span class="offer-title">Продай</span>
        </div>
        <div class="offer offerselect">
            <span class="offer-title">Обменяй</span>
        </div>
        <div class="offer offer-desc">
            Все самые интересные объявления и услуги в вашей
            досягаемости. Заклюайте договора не выходя из дома!
            Обменивайтесь ненужными товарами на нужные.
            Пользуйтесь услугами в своем доме, районе, крае, по всей стране!
        </div>
        <a href="/create/" class="btn offer-btn">Подать объявление</a>
    </div>
</div>
<?$APPLICATION->IncludeComponent(
    "diamis:ads.filter",
    "",
    array(
        'SHOW_CATEGORY_LIST' => 'Y',
        'HISTORY' => 'N',
        'FILTER_NAME' => 'arCategoryFilter',
    )
);
?>
<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH.'/include/page-plus.php',array());?>
<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH.'/include/page-count.php',array());?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"revicew", 
	array(
		"COMPONENT_TEMPLATE" => "revicew",
		"IBLOCK_TYPE" => "review",
		"IBLOCK_ID" => "2",
		"NEWS_COUNT" => "10",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"STRICT_SECTION_CHECK" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>
<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH.'/include/page-mobile.php',array());?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>