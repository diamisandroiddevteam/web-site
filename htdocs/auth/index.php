<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<div class="section">
	<div class="content">
	<?$APPLICATION->IncludeComponent(
		"diamis:auth",
		".default",
		array(
			"COMPONENT_TEMPLATE" => ".default",
			"QUERY_AJAX" => "Y",
			"SOCIAL_AUTH" => "Y",
			"SOCIAL_BACK_URL" => "/",
			"PAGE_FORGOT" => "",
			"PAGE_WELCOME" => "",
			"SEF_MODE" => "N",
			"SEF_FOLDER" => "/auth/"
		),
		false
	);?>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>