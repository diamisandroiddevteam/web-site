<?php


use Phinx\Migration\AbstractMigration;

class CreateDiamisChat extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other distructive changes will result in an error when trying to
     * rollback the migration.
     */
    public function change()
    {
        $table = $this->table('diamis_chat');
        $table->addColumn('name','string', ['limit'=>100]) // Название чата
            ->addColumn('type', 'integer', ['default'=>1]) // Тип Чата
            ->addColumn('entity_id', 'integer') // Тип Чата
            ->addIndex('type', 'entity_id')
            ->save();
    }
}
