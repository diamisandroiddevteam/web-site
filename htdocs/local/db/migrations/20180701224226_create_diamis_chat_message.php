<?php


use Phinx\Migration\AbstractMigration;

class CreateDiamisChatMessage extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other distructive changes will result in an error when trying to
     * rollback the migration.
     */
    public function change()
    {
        $table = $this->table('diamis_chat_message');
        $table->addColumn('chat_id', 'integer')
            ->addColumn('to_id', 'integer')
            ->addColumn('from_id', 'integer')
            ->addColumn('content_id', 'integer')
            ->addColumn('date_created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('date_update', 'datetime', ['null' => true])
            ->addIndex(['chat_id', 'to_id', 'from_id', 'content_id'])
            ->save();
    }
}
