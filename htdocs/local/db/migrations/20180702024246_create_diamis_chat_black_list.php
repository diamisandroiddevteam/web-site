<?php


use Phinx\Migration\AbstractMigration;

class CreateDiamisChatBlackList extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other distructive changes will result in an error when trying to
     * rollback the migration.
     */
    public function change()
    {
        $table = $this->table('diamis_chat_black_list');
        $table->addColumn('author','integer')
            ->addColumn('user_id', 'integer')
            ->addColumn('date_created', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->save();
    }
}
