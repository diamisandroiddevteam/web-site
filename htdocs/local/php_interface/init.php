<?php

// TEMP
function p($obj, $stop = false) {
    global $USER;
//    if($USER->IsAdmin()) {
        echo '<pre>';print_r($obj);echo'</pre>';
//    }
    if($stop) die('stop');
}

/** Выводит / возвращает Dump SQL запроса Bitrix D7 и старого ядра */
if( !function_exists('_dumpSQL') ){

    function _dumpSQL(callable $callback, array $params = [])
    {
        $result = [];
        if($params['user'] == 'all' || $GLOBALS['USER']-> isAdmin() ){

            $backtrace = array_shift( debug_backtrace() );
            $result['FILE'] = $backtrace['line'].": ".$backtrace['file'];
            $con = \Bitrix\Main\Application::getConnection();
            $GLOBALS['DB']-> sqlTracker = $con-> startTracker();
            $res = call_user_func($callback);
            $con-> stopTracker();
            foreach(is_array($res) ? $res : [$res] as $key => $obj){
                $sql = null;
                if( is_a($obj, '\CDBResult') ){
                    $sql = $obj-> DB-> sqlTracker-> current()-> getSql();

                }else if( is_a($obj, '\Bitrix\Main\DB\Result') ){

                    $sql = $obj -> getTrackerQuery()-> getSql();
                }
                if( !empty($sql) ){
                    $result['SQL'][$key] = $sql;
                    $dump[] = $key." - ".get_class($obj)." -> \n".$sql;
                }
            }

            if($params['show'] !== false){
                $separator = "\n".str_pad("", strlen($result['FILE']), "=")."\n";
                die(
                    '<pre>'
                    .$result['FILE']
                    .$separator
                    .implode($separator, $dump)
                    .'</pre>'
                );
            }
        }
        return $result;
    }
}


// TEMP
if($_SERVER['HTTP_X_REAL_IP']=='127.0.0.1')
{
    $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_REAL_IP'] = '62.183.22.4';
}

// Connect Vendor
require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';


$url = $_SERVER['REQUEST_URI'];
$isAdminPanel = false;
if(stripos($url, 'admin/')!==false){
    $isAdminPanel = true;
}

$includePaths = [
    'config' => true,           // Общие настройки системы
    'function.init' => true,    // Вспомогательные функции
    'auth' => true,
    'section' => true,
    'geoip' => true,
];

foreach($includePaths as $path=>$action) {
    $dir = $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/' . $path .'.php';
    if(!$isAdminPanel && $action && is_file($dir)) {
        require_once $dir;
    }
}