<?
// добавляем обработчик события при регистрации
AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserRegisterHandler");
AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserRegisterHandler");
function OnBeforeUserRegisterHandler(&$arFields)
{
    if(!empty($arFields["EMAIL"]))
        $arFields["LOGIN"] = $arFields["EMAIL"];
    else if(!empty($arFields["PERSONAL_PHONE"]))
        $arFields["LOGIN"] = preg_replace("/[^0-9]/", '', $arFields["PERSONAL_PHONE"]);

    return $arFields;
}
