<?php

/* HTML select
* id     - attribute ID container
* class  - attribute Class container
* title  -
* label  - placeholder
* select - массив выбранных данных
* name   - attribute name input
* items  - array(id, title, name, value, checked, class)
*/
function htmlSelect($array)
{
    ?>
    <div<?=($array['id'] ? ' id="'.$array['id'].'"' : '');?> class="select <?=($array['id'] ? $array['class'] : '');?>">
        <? if($array['title']):?>
            <div class="select-title"><?=$array['title'];?></div>
        <? endif;?>
        <div class="select-content">
            <i class="select-icon"></i>
            <div class="select-placeholder"><?=$array['label'];?></div>
            <div class="select-result"></div>
        </div>
        <div class="select-lists">
            <i class="select-lst-icon"></i>
            <div class="select-find"><input type="text" class="select-find-input"></div>
            <div class="select-lst-container">
                <ul class="select-lst-items">
                    <? foreach ($array['items'] as $item):?>
                        <li data-for="<?=$item['id'];?>" class="select-item<?if($item['checked']):?> select-active-item<?endif;?>">
                            <span><?=$item['title'];?></span>
                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="select-check">
            <? foreach ($array['items'] as $item):?>
                <input
                        name="<?=$item['name'];?>"
                        type="checkbox"
                        id="<?=$item['id'];?>"
                        value="<?=$item['value'];?>"
                        <?if($item['checked']):?>checked="checked"<?endif;?>
                >
            <? endforeach; ?>
        </div>
    </div>
    <?
}


function htmlInput($array){
    ?>
    <div class="field-input">
        <div class="field-label">
            <label><?=$array['title'];?></label>
        </div>
        <div class="field-string">
            <input name="<?=$array['name'];?>" type="<?=$array['type'] ? $array['type'] : 'text';?>" autocomplete="off" value="<?=$array['value'];?>">
        </div>
        <? /*
        <div class="field-tools">
            <span class="field-tools__edit">Рудактировать</span>
        </div> */
        ?>
    </div>
    <?
}

function htmlInputMin($array){
    ?>
    <div class="form-input">
        <input name="<?=$array['name'];?>" type="<?=$array['type'] ? $array['type'] : 'text';?>" class="input-tag" value="<?=$array['value'];?>">
    </div>
    <?
}

function htmlPass($array) {
    ?>
    <div class="field-input field-input__password">
        <div class="field-label">
            <label><?=$array['title'];?></label>
        </div>
        <div class="field-string">
            <input maxlength="50" name="<?=$array['name'];?>" type="password" autocomplete="off" value="">
        </div>
    </div>
    <?
}

function htmlCheck($array) {
    ?>
    <div class="field-input field-input__checked js-checked-tools-box<?if($array['checked']):?> active<?endif;?>">
        <div class="field-label"><label><?=$array['title'];?></label></div>
        <div class="field-tools">
            <label for="check_<?=$array['id'];?>"><span class="field-tools__checked"></span></label>
            <input id="check_<?=$array['id'];?>" type="checkbox" class="js-checked-tools" autocomplete="off"<?if($array['checked']):?> checked<?endif;?>>
            <input id="check_<?=$array['id'];?>_on" type="checkbox" class="js-checked-tools" name="<?=$array['name'];?>" autocomplete="off"<?if($array['checked']):?> checked<?endif;?> value="1">
            <input id="check_<?=$array['id'];?>_off" type="checkbox" class="js-checked-tools" name="<?=$array['name'];?>" autocomplete="off" value="0">
        </div>
    </div>
    <?
}


function htmlSelectSlider()
{
    ?>
    <div class="select select-slider">
        <div class="select-content">
            <i class="select-icon"></i>
            <div class="select-placeholder"></div>
            <div class="select-result"></div>
        </div>
        <div class="select-lists">
            <i class="select-lst-icon"></i>
            <div class="slc-slider-wrap">
                <div class="slc-slider"></div>
            </div>
        </div>
    </div>
    <?
}

/* HTML input[type=file]
 * name - атрибут name
 * conf - количество выводимых полей
 */
function htmlFilePhoto($conf=array('count'=>1)) {
    $i = 0;
    while($i<$conf['count'])
    {
        $i ++;
        ?>
        <div class="field-file">
            <input name="<?=$conf['name'];?>" type="file" accept="image/jpeg,image/png,image/gif">
            <div class="field-file--preview"></div>
            <div class="field-file--action">Добавить фото</div>
        </div>
        <?
    }
}