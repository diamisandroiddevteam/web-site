<?php
use Bitrix\Main\Loader;
use Diamis\GeoIp\GeoBase;

if (Loader::includeModule('diamis.geoip')):
    if(!isset($_SERVER['HTTP_X_REAL_IP']))
        $_SERVER['HTTP_X_REAL_IP'] = $_SERVER['REMOTE_ADDR'];

    $CityIP = GeoBase::getCityIp($_SERVER['HTTP_X_REAL_IP']);
    $GLOBALS['CITY_IP'] = $CityIP;

endif;