<?
use \Bitrix\Main\Loader;
use \Bitrix\Iblock\SectionTable;


// Get Category
if (Loader::includeModule('iblock')):
    $query = SectionTable::getList(array(
        'select' => array('ID','NAME','CODE','IBLOCK_SECTION_ID','DEPTH_LEVEL','PICTURE'),
        'filter' => array(
            'ACTIVE' => 'Y',
            'GLOBAL_ACTIVE' => 'Y',
            'IBLOCK_ID' => CATEGORY_IBLOCK_ID
        )
    ));

    while($res = $query->fetch()):
        if($res['PICTURE']>0)
            $res['PICTURE'] = CFile::GetPath($res['PICTURE']);

            $GLOBALS['CATEGORY'][$res['ID']] = $res;
    endwhile;
endif;

