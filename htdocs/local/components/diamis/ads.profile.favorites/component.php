<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


use \Bitrix\Main\Loader;
use \Diamis\Ads\AdsTable;
use \Diamis\Ads\FavoritesTable;


if (!Loader::includeModule('diamis.ads')) return;

global $DB, $USER;


$arFavorites = FavoritesTable::getFavorites();
$arItems = AdsTable::getElements(array('ID'=> $arFavorites));

$arResult['FAVORITES'] = $arFavorites;
$arResult['ITEMS'] = $arItems;


$this->IncludeComponentTemplate($componentPage);