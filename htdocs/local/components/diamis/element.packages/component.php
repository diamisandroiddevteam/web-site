<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();

use Bitrix\Main\ArgumentException;
use \Bitrix\Main\Loader;
use \Diamis\Ads\Base as AdsBase;
use \Diamis\Ads\AdsTable as AdsElement;
use \Diamis\Ads\CategoryTable as AdsCategory;
use \Diamis\Ads\TypeTable as AdsType;
use \Diamis\Ads\PackagesTable as AdsPackage;




if (!Loader::includeModule('diamis.ads')) return;

$ID = $arParams['ID'];

if($ID && $Elements = AdsElement::getElementID($ID))
{
    $arResult = $Elements;

    try {

        $category = AdsCategory::getList(array(
            'select' => array('NAME', 'CODE'),
            'filter' => array('ID'=> intval($Elements['CATEGORY_ID'])),
            'limit' => 1
        ))->fetch();

        $arResult['CATEGORY_NAME'] = $category['NAME'];
        $arResult['CATEGORY_CODE'] = $category['CODE'];


        $category = AdsType::getList(array(
            'select' => array('NAME', 'CODE','ACTIVE', 'SORT'),
            'filter' => array('ID'=> intval($Elements['TYPE_ID'])),
            'limit' => 1
        ))->fetch();

        $arResult['TYPE_NAME'] = $category['NAME'];
        $arResult['TYPE_CODE'] = $category['CODE'];
        $arResult['TYPE_SORT'] = $category['SORT'];
        $arResult['TYPE_ACTIVE'] = $category['ACTIVE'];

        $arResult['PACKAGES'] = AdsPackage::getPackagesCategory($Elements['CATEGORY_ID']);

    } catch (ArgumentException $e) {

    }

    $arResult['TITLE'] = 'Проверка объявления';
    $this->IncludeComponentTemplate($componentPage);
}