<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();
?>

<div class="ads-nav-container">
    <a href="/create/?id=<?=$arParams['ID'];?>" class="ads-nav-container--back"></a>
    <span class="ads-nav-container--title"><?=$arResult['TITLE'];?></span>
</div>
<div class="ads-fields">
    <? if($arResult['CATEGORY_NAME']): ?>
    <div class="ads-field">
        <div class="ads-field-label">Категория:</div>
        <div class="ads-field-result"><?=$arResult['CATEGORY_NAME'];?></div>
    </div>
    <? endif; ?>
    <? if($arResult['TYPE_NAME']): ?>
    <div class="ads-field">
        <div class="ads-field-label">Тип объявления:</div>
        <div class="ads-field-result"><?=$arResult['TYPE_NAME'];?></div>
    </div>
    <? endif; ?>
    <? if($arResult['NAME']): ?>
    <div class="ads-field">
        <div class="ads-field-label">Название объявления:</div>
        <div class="ads-field-result"><b><?=$arResult['NAME'];?></b></div>
    </div>
    <? endif; ?>
    <? if($arResult['FILES']): ?>
        <div class="ads-field">
            <div class="ads-field-label">Фотографии:</div>
            <div class="ads-field-result">
                <div class="package-image">
                    <? foreach($arResult['FILES'] as $image): ?>
                        <div class="package-image-item">
                            <img src="<?=$image;?>" class="package-image-item-tag" />
                        </div>
                    <? endforeach; ?>
                    <div class="package-clear"></div>
                </div>
            </div>
        </div>
    <?
    endif;

    if($arResult['~TEXT']):
        ?>
        <div class="ads-field">
            <div class="ads-field-label">Описание объявления:</div>
            <div class="ads-field-result"><?=$arResult['~TEXT'];?></div>
        </div>
        <?
    endif;

    ?>
    <div class="ads-field-title">Параметры</div>
    <?
    foreach($arResult['PROPERTY'] as $value):
        //TODO Настроить вывод для слайдера
        if($value['VALUE'] && $value['TYPE']!=='slider'):
        ?>
        <div class="ads-field">
            <div class="ads-field-label"><?=$value['NAME'];?>:</div>
            <div class="ads-field-result"><?=$value['VALUE'];?></div>
        </div>
        <?
        endif;
    endforeach;
    ?>
    <div class="ads-field-title">Контактная информация</div>
    <?
    if(count($arResult['CONTACTS']['PHONE'])):
        ?>
        <div class="ads-field">
            <div class="ads-field-label">Телефон:</div>
            <? foreach($arResult['CONTACTS']['PHONE'] as $phone):
                ?><div class="ads-field-result"><?=$phone;?></div><?
            endforeach; ?>
        </div>
        <?
    endif;

    if(count($arResult['CONTACTS']['EMAIL'])):
        ?>
        <div class="ads-field">
            <div class="ads-field-label">E-Mail:</div>
            <?
            foreach($arResult['CONTACTS']['EMAIL'] as $email):
                ?><div class="ads-field-result"><?=$email;?></div><?
            endforeach;
            ?>
        </div>
        <?
    endif;

    if(count($arResult['LOCATION'])):
        ?>
        <div class="ads-field">
            <div class="ads-field-label">Адрес:</div>
            <?
            $lat = null;
            $lng = null;
            foreach($arResult['LOCATION'] as $location):
                $lat = $location['LAT'];
                $lng = $location['LNG'];
                ?><div class="ads-field-result"><p><?=$location['VALUE'];?></p></div><?
            endforeach; ?>
            <div class="ads-field-result">
                <div id="package-map" style="width:100%; height: 200px;"></div>
            </div>
        </div>
        <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script type="text/javascript">
        $('#package-map').mapsAdd({
            'latMaps' : '<?=$lat;?>',
            'lngMaps' : '<?=$lng;?>',
            'markerDraggable': false
        });
        </script>
        <?
    endif;


    if(count($arResult['PACKAGES'])):
        ?>
        <div class="ads-field-title">Способ размещения</div>
        <form action="/pay/" method="get">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="ID" value="<?=$arParams['ID'];?>">
            <div class="packages">
            <?
            $action = false;
            foreach($arResult['PACKAGES'] as $package):
                ?>
                <label for="packages-item-<?=$package['PACKAGE_ID'];?>" class="packages-item<?if(!$action):?> active<?endif;?>" >
                    <input id="packages-item-<?=$package['PACKAGE_ID'];?>" class="packages-item-input" type="radio" name="PACKAGE_ID" value="<?=$package['PACKAGE_ID'];?>" <?if(!$action):?>checked<?endif;?> >
                    <div class="packages-head">
                        <div class="packages-title"><?=$package['NAME'];?></div>
                        <div class="packages-desc"><?=$package['TEXT'];?></div>
                    </div>
                    <div class="packages-body">
                        <ul class="packages-list">
                        <? foreach($package['SETTINGS'] as $setting): ?>

                            <li class="packages-list-item">
                                <span class="packages-list-title"><?=$setting['NAME'];?></span>
                                <span class="packages-list-text"><?=$setting['TEXT'];?></span>
                            </li>

                        <? endforeach; ?>
                        </ul>
                    </div>
                    <? if($package['BONUS']>0): ?>
                        <div class="packages-bonus">
                            + бонус <?=$package['BONUS'];?>
                        </div>
                    <? endif; ?>
                    <? if($package['PRICE']>0): ?>
                        <div class="packages-price">
                            <?=$package['PRICE'];?>
                            <span>a</span>
                        </div>
                    <? endif; ?>
                </label>
                <?
                $action = true;
            endforeach;
            ?>
            </div>
            <div class="packages-btn">
                <input class="btn" type="submit" value="Подтвердить">
            </div>
        </form>
        <?php
    endif;
    ?>
</div>