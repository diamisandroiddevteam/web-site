<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();

use \Bitrix\Main\Loader;
use \Diamis\Ads\ReviewTable;
use \Diamis\Ads\Base as AdsBase;


global $USER, $APPLICATION;


$ajax = $_REQUEST['ajax']==='y' ? true : false;
$arResult['ID'] = intval($arParams['ID']);
$arResult['PAGE_SIZE'] = intval($arParams['PAGE_SIZE']) ? intval($arParams['PAGE_SIZE']) : 12;
$arResult['OFFSET'] = intval($arParams['OFFSET']);
$arResult['TYPE'] = strip_tags($arParams['TYPE']);

if($ajax)
{
    $APPLICATION->RestartBuffer();

}


$arItems = array();
$arFilter = array(
    'ACTIVE' => true,
    'ADS_ID' => $arResult['ID']
);


$nav = new \Bitrix\Main\UI\PageNavigation("page");
$nav->allowAllRecords(true)
    ->setPageSize($arResult['PAGE_SIZE'])
    ->initFromUri();

$n = 0;
$db = ReviewTable::getList(array(
    'select' => array('*'),
    'filter' => $arFilter,
    'order' => array('ID'=>'DESC'),
    'offset' => ($offset = $nav->getOffset()),
    'limit'  => (($limit = $nav->getLimit()) > 0 ? $limit + 1 : 0)
));
while($item = $db->fetch()) {
    $n++;
    if ($limit > 0 && $n > $limit) break;

    $arUser = CUser::GetByID($item['USER_ID'])->Fetch();



    $item['DATE'] = $item['DATE_CREATE']->format('d.m.Y h:i');
    $item['USER_NAME'] = strlen($arUser['NAME'] .' '. $arUser['LAST_NAME']) ? $arUser['NAME'] .' '. $arUser['LAST_NAME'] : 'Пользователь';

    $arItems[] = $item;
}
$nav->setRecordCount($offset + $n);
$arResult['NAV'] = $nav;

$data = AdsBase::tree($arItems);
$arResult['ITEMS'] = $data;

$arResult['AJAX'] = $ajax;

$this->IncludeComponentTemplate();