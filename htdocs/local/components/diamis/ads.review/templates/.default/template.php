<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();

global $USER;

function reviewShow($items)
{
    foreach($items as $item):
        ?>
        <li class="content-list__item content-list__item_comment js-comment" data-parent="<?=$item['ID'];?>">
            <div class="comment js-form">
                <div class="comment__head">
                    <a href="#" class="user-info user-info_inline">
                        <img src="<?=SITE_TEMPLATE_PATH;?>/images/no_user.jpg" class="user-info__image" />
                        <span class="user-info__nickname"><?=$item['USER_NAME'];?></span>
                    </a>
                    <time class="comment__date-time"><?=$item['DATE'];?></time>
                </div>
                <div class="comment__message"><?=$item['TEXT'];?></div>
                <div class="comment__footer">
                    <a href="#" class="comment__footer-link js-form_btn" data-review="<?=$item['ID'];?>">Ответить</a>
                </div>
                <div class="comment__reply-form js-form_wrapper"></div>
            </div>
            <?
            if(count($item['CHILD'])):
                ?><ul class="content-list content-list_nested-comments"><?
                reviewShow($item['CHILD']);
                ?></ul><?
            endif;
            ?>
        </li>
        <?
    endforeach;
}

?>
<div class="comments-section">
    <div class="comments-section__head-title">Комментарии</div>
    <ul class="content-list content-list_comments">
        <? reviewShow($arResult['ITEMS']) ?>
    </ul>
    <div class="comment-form js-form">
        <div class="comment-form__title js-form_btn" data-review="0">Написать комментарий</div>
        <div class="comment-form_wrapper js-form_wrapper">
            <form class="form form_comment-message" action="#">
                <div class="form__error"></div>
                <div class="form__true"></div>
                <?=bitrix_sessid_post()?>
                <input type="hidden" name="element" value="<?=$arResult['ID'];?>" >
                <input type="hidden" name="review" value="0" >
                <input type="text" name="bot" style="display: none;" value="" >
                <div class="from__editor__textarea_wrap">
                    <textarea class="from__editor__textarea" name="text" cols="30" rows="10"></textarea>
                </div>
                <div class="form__buttons form__buttons_comment">
                    <button class="btn btn-radius btn-rempty" name="save">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</div>
<? if($arResult['AJAX']) die(); ?>