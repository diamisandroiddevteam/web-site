<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();


use \Bitrix\Main\Loader;
use \Diamis\Ads\Base as AdsBase;
use \Diamis\Ads\CategoryTable;
use \Diamis\Ads\TypeTable;

use \Diamis\GeoIp\GeoBase;


if (!Loader::includeModule('diamis.ads')) return;
if (!Loader::includeModule('diamis.geoip')) return;


global $USER, $APPLICATION;


$ajax = $_REQUEST['ajax']==='y' ? true : false;
$arFilter = $_REQUEST['filter']; // Получаем параметры формы фильтр
$fields = array();
$arFieldValues = array();

if($_REQUEST['show']=='map')
{
    unset($_REQUEST['show']);
    $url = http_build_query($_REQUEST);
    LocalRedirect('/map/?'.$url);
}


if(empty($arFilter['radius'])) $arFilter['radius'] = 60;
if(!empty($arFilter['region'])) $_SESSION['ADS_FILTER_REGION'] = $arFilter['region'];
else $arFilter['region'] = $_SESSION['ADS_FILTER_REGION'];

// Выводить сладер категорий
$arResult['SHOW_CATEGORY_LIST'] = $arParams['SHOW_CATEGORY_LIST'] ? $arParams['SHOW_CATEGORY_LIST'] : 'N';
$filterName = $arParams['FILTER_NAME'] ? $arParams['FILTER_NAME'] : 'arCatalogFilter';


global $$filterName;

$requestURL = Bitrix\Main\Context::getCurrent()->getRequest()->getRequestedPage();
$requestURL = str_replace('index.php', '', $requestURL);



// Формируем списко Района
$arFieldValues['region'] = array(
    'id' => 'filter[region]',
    'type' => 'select',
    'placeholder' => 'Город',
    'values' => array()
);

$region_code = strip_tags($_REQUEST['REGION_CODE']);
$regions = GeoBase::getCity();
if(empty($region_code)) {
    $region_code = $GLOBALS['CITY_IP']['CITY_CODE'];
}

foreach($regions as $region)
{
    if($region['COUNTRY']=='Россия')
    {
        $arFieldValues['region']['values'][] = array(
            'name' => $region['CITY'],
            'value' => $region['ID'],
        );

        $arResult['REGION_LIST'][] = $region;
        if(
            (!$arFilter['region'] && $region['CITY_CODE']===$region_code)
            ||
            ( $arFilter['region'] && array_search($region['ID'], $arFilter['region'])!==false)
        ) {
            $arResult['CURRENT_REGION'] = $region;
            $arFieldValues['region']['currentValue'] = $region['ID'];
        }
    }
}


// Формируем список Категория
$arFieldValues['category'] = array(
    'id' => 'filter[category]',
    'type' => 'select',
    'placeholder' => 'Категория',
    'values' => array()
);

$category_code = strip_tags($_REQUEST['SECTION_CODE']);
$dbCategory = CategoryTable::getList(array('filter'=>array('ACTIVE'=>1)));
while($category = $dbCategory->fetch())
{
    $arFieldValues['category']['values'][] = array(
        'name' => $category['NAME'],
        'value' => $category['ID'],
    );

    // ключем делаем ID категории для более быстрого поиска
    $arResult['CATEGORY_LIST'][$category['ID']] = $category;
    if(
        (!$arFilter['category'] && $category['CODE']===$category_code)
        ||
        ( $arFilter['category'] && array_search($category['ID'], $arFilter['category'])!==false)
    ) {
        $arResult['CURRENT_CATEGORY'] = $category;
        $arFieldValues['category']['currentValue'] = $category['ID'];
    }
}


$arResult['CATEGORY_TREE'] = AdsBase::tree($arResult['CATEGORY_LIST']);





// Получаем доступный список типа объялвения
// для данной категории
if($arResult['CURRENT_CATEGORY']['ID']):
    $type_code = strip_tags($_REQUEST['TYPE_CODE']);
    $types = AdsBase::getTypes($arResult['CURRENT_CATEGORY']['ID']);
    if(count($types)):

        $fields['type'] = array(
            'id' => 'filter[type]',
            'type' => 'select',
            'placeholder' => 'Тип Объявления',
            'values' => array()
        );

        foreach($types[$arResult['CURRENT_CATEGORY']['ID']] as $item){
            $fields['type']['values'][] = array(
                'name' => $item['NAME'],
                'value' => $item['ID']
            );
            if(
                (!$arFilter['type'] && $item['CODE']===$type_code)
                ||
                ( $arFilter['type'] && array_search($item['ID'], $arFilter['type']))
            ) {
                $arResult["CURRENT_TYPE"] = $item;
                $fields['type']['currentValue'] = $item['ID'];
            }
        }
    endif;
endif;



// Задаем возможный радиус
$arFieldValues['radius'] = array(
    'id' => 'filter[radius]',
    'type' => 'slider',
    'placeholder' => 'Радиус, км.',
    'currentValue' => array(intval($arFilter['radius'])),
    'values' => array(
        array(
            'max' => 300,
            'name' => 'до',
            'value'=> 60
        )
    )
);




// Обрабатываем AJAX запрос
if($ajax)
{
    $APPLICATION->RestartBuffer();


    $arHistory = array();
    if($arResult['CURRENT_REGION']['CITY_CODE']) $arHistory[] = $arResult['CURRENT_REGION']['CITY_CODE'];
    if($arResult['CURRENT_CATEGORY']['CODE']) $arHistory[] = $arResult['CURRENT_CATEGORY']['CODE'];
    if($arResult['CURRENT_TYPE']['CODE']) $arHistory[] = $arResult['CURRENT_TYPE']['CODE'];

    $json = array(
        'title'   => $arResult['CURRENT_CATEGORY']['NAME'] ? $arResult['CURRENT_CATEGORY']['NAME'] : null,
        'fields'  => array()
    );


    $arProps = AdsBase::getFields($arResult['CURRENT_CATEGORY']['ID'], $arResult["CURRENT_TYPE"]['ID']);
    foreach($arProps as $item) {
        $key = count($fields['props']);
        $fields['props'][$key] = array(
            'id' => 'filter['.$item['ID'].']',
            'type' => $item['FIELD_TYPE'],
            'placeholder' => $item['FIELD_NAME'],
            'values' => array()
        );

        if($arFilter[$item['ID']] && !is_array($arFilter[$item['ID']])) {
            $fields['props'][$key]['currentValue'] = $value['VALUE'];
        }

        foreach($item['FIELD_VALUES'] as $value) {
            $fields['props'][$key]['values'][] = array(
                'name' => $value['NAME'],
                'value' => $value['VALUE'],
            );
            if($arFilter[$item['ID']][$value['VALUE']] && $item['FIELD_TYPE']!=='slider')
            {
                $fields['props'][$key]['currentValue'] = $value['VALUE'];
            }
        }
    }


    if(count($arHistory)) {
        $json['action'] = '/'.implode('/', $arHistory).'/';
    }

    if($arParams['HISTORY']=='Y' && count($arHistory)){
        $json['history'] = '/'.implode('/', $arHistory).'/';
    }


    $json['fields'] = $fields;
    die(json_encode($json));
}

if(empty($arFilter['region'])) $arFilter['region'] = $arResult['CURRENT_REGION']['ID'];
if(empty($arFilter['category'])) $arFilter['category'] = $arResult['CURRENT_CATEGORY']['ID'];
if(empty($arFilter['type'])) $arFilter['type'] = $arResult['CURRENT_TYPE']['ID'];

$arResult['DATA_FILTER'] = AdsBase::genFilter($arFilter);


$$filterName = $arResult['DATA_FILTER'];

$arResult['FIELDS'] = $arFieldValues;
$this->IncludeComponentTemplate();