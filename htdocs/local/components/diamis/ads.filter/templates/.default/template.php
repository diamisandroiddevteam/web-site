<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

use \Diamis\Ads\DomElement as AdsDomElement;

if($arResult['SHOW_CATEGORY_LIST']==='Y'):
?>
<div class="content">
    <div class="scroll js-find-category-list">
        <div class="scroll-arrow scroll-arrow-left ctg-arrow"></div>
        <div class="scroll-list ctg-list">
            <?
            foreach($arResult['CATEGORY_TREE'] as $item):
                $region = '';
                if(count($arResult['CURRENT_REGION'])) $region = $arResult['CURRENT_REGION']['CITY_CODE'].'/';
                ?>
                <div class="scroll-item ctg-item">
                    <a href="/<?=$region.$item['CODE'];?>/" class="scroll-link ctg-link">
                        <img src="<?=$item['FILE_PATH'];?>" alt="<?=$item['NAME'];?>" class="ctg-icon">
                        <span><?=$item['NAME'];?></span>
                    </a>
                </div>
                <?
            endforeach;
            ?>
        </div>
        <div class="scroll-arrow scroll-arrow-right ctg-arrow"></div>
    </div>
</div>
<script>
$('.js-find-category-list .ctg-list').slick({
    infinite: false,
    slidesToShow: 7,
    slidesToScroll: 3,
    prevArrow: $('.js-find-category-list .scroll-arrow-left'),
    nextArrow: $('.js-find-category-list .scroll-arrow-right'),
    responsive: [
        {
            breakpoint: 900,
            settings: {
                slidesToShow: 6,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 640,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }
    ]
});
</script>
<?
endif;
?>
<div class="content">
    <div class="find-form">
        <form action="#" method="get" id="filter">
            <?=bitrix_sessid_post()?>
            <div class="find flexbox">
                <div class="find-item">
                    <?=AdsDomElement::get($arResult['FIELDS']['category']);?>
                </div>
                <div class="find-item">
                    <?=AdsDomElement::get($arResult['FIELDS']['region']);?>
                </div>
                <div class="find-item">
                    <?
                    htmlSelect(array(
                        'label'=>'Район',
                        'items'=>array()
                    ));
                    ?>
                </div>
                <div class="find-item">
                    <?=AdsDomElement::get($arResult['FIELDS']['radius']);?>
                </div>
            </div>
            <div class="find-property js-find-fields"></div>
            <div class="find">
                <button name="show" value="list" class="btn btn-rfull">Найти</button>
                <button name="show" value="map" class="btn btn-rempty">Показать на карте</button>
            </div>
        </form>
    </div>
    <script type="text/javascript">
    var dFilter = new window.DFilter();
    dFilter.init('filter');
    </script>
</div>