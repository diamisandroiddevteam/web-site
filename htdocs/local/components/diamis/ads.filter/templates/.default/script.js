(function(window) {

    window.DFilter = function(){
        this.form = null;
        this.fields = null;
        this.timerId = null;
        this.htmlform = new window.DHtmlForm();
    };


    window.DFilter.prototype.init = function(formID) {
        var _this = this;

        this.form = document.getElementById(formID);
        this.fields = $(this.form).find('.js-find-fields');

        $(this.form).on('click', function(){
            if(_this.timerId) clearTimeout(_this.timerId);
        });

        $(this.form).on('change', function(event){
            var name = $(event.target).attr('name');
            if(name) {
                if(~name.indexOf('type')) _this.timeout();
                if(~name.indexOf('category')) _this.timeout();
                if(~name.indexOf('region')) _this.timeout();
            }
        });

        _this.update();
    };




    window.DFilter.prototype.update = function() {
        var action = $(this.form).attr('action'),
            data = $(this.form).serializeArray(),
            _this = this;

        data.push({name: 'ajax', value: 'y'});
        data.push({name: 'type', value: 'fields'});
        $.ajax({
            url: action ? action : window.location.href,
            data: data,
            dataType: 'json',
            type: 'post',
            success: function(result) {
                if(result.action)
                    $(_this.form).attr('action', result.action);

                if(result.history)
                    window.history.pushState(null, result.title, result.history);

                if(result.fields) _this.htmlFields(result.fields);
            }
        })
    };


    window.DFilter.prototype.htmlFields = function(data) {

        this.fields.html('');
        if(typeof data!=="undefined") {
            this.fields.css({
                width: '100%',
                marginLeft: '-5px',
                marginRight: '-5px',
            });
        } else {
            this.fields.css({margin: '0'});
        }


        if(data.type) {
            var itemType = $('<div/>'),
                htmlType = this.htmlform.getField(data.type);

            itemType.addClass('find-item find-item-min');
            itemType.html(htmlType.html);
            itemType.appendTo(this.fields);
        }

        if(data.props) {
            for(var i=0; i<data.props.length; i++)
            {
                var itemType = $('<div/>'),
                    htmlType = this.htmlform.getField(data.props[i]);

                itemType.addClass('find-item find-item-min');
                itemType.html(htmlType.html);
                itemType.appendTo(this.fields);
            }
        }
    };


    window.DFilter.prototype.timeout = function() {
        var _this = this;

        if(_this.timerId) clearTimeout(_this.timerId);
        _this.timerId = setTimeout(function(){ _this.update(); }, 1300);
    };

    window.DFilter.prototype.i3 = function() {};
    window.DFilter.prototype.i4 = function() {};
    window.DFilter.prototype.i5 = function() {};

})(window);