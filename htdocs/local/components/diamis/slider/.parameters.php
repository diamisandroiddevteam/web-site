<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
    return;

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock=array();
$rsIBlock = CIBlock::GetList(
    Array("SORT" => "ASC"),
    Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y")
);
while($arr=$rsIBlock->Fetch())
{
    $arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}


$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("D_IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("D_IBLOCK"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y",
            "ADDITIONAL_VALUES" => "Y",
        ),
        'LIMIT' => array(
            "PARENT" => "BASE",
            'NAME' => GetMessage("D_LIMIT"),
            "TYPE" => "STRING",
            "DEFAULT" => 10
        ),
        'SHOW' => array(
            "PARENT" => "BASE",
            'NAME' => GetMessage("D_SHOW"),
            "TYPE" => "STRING",
            "DEFAULT" => 2
        ),
        'SCROLL' => array(
            "PARENT" => "BASE",
            'NAME' => GetMessage("D_SCROLL"),
            "TYPE" => "STRING",
            "DEFAULT" => 1
        ),
        'RESPONSIVE' => array(
            "PARENT" => "BASE",
            'NAME' => GetMessage("D_RESPONSIVE"),
            "TYPE" => "TEXT",
            "DEFAULT" => "[
                {
                    breakpoint: 1000,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]"
        )
    )
);