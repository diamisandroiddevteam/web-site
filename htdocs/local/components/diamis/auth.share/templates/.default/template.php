<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arClass = array(
    'vk' => 'CSocServVKontakte',
    'ok' => 'CSocServOdnoklassniki',
    'fb' => 'CSocServFacebook',
    'google' => 'CSocServGooglePlusOAuth',
    'twitter' => 'CSocServTwitter',
);

?>
<div class="user-cshare-title">Сохранить в соц.сети:</div>
<div class="user-cshare">
    <? foreach($arResult['SHARE'] as $key=>$item):?>
        <div class="social_item">
            <a href="<?=$item['URL'];?>" onclick="return false;" class="js-share social_link <?=$arClass[$key] ? $arClass[$key] : 'social_link_'.$key;?>"></a>
        </div>
    <? endforeach; ?>
</div>
<script>
$('.js-share').on('click', function(){
    var url = $(this).attr('href');
    window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
})
</script>