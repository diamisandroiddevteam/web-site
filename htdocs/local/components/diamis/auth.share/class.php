<?
use Bitrix\Main;
use \Bitrix\Main\Loader;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();



class AuthShare extends \CBitrixComponent
{
    private $url;
    private $title;
    private $description;
    private $image;
    private $via;           // ссылка на Twitter-аккаунт (например официальный канал сайта)


    public function setVia($via) {
        if($via) $this->via = urlencode($via);
    }


    public function setUrl($url) {
        if($url) $this->url = urlencode($url);
    }

    public function setTitle($title) {
        if($title) $this->title = urlencode($title);
    }

    public function setDescription($description) {
        if($description) $this->description = urlencode($description);
    }

    public function setImage($image) {
        if($image) $this->image = urlencode($image);
    }


    /** Документация http://vk.com/pages?oid=-17680044&p=Sharing_External_Pages
     * @return array
     */
    public function getVk(){
        return array(
            'url' => 'https://vk.com/share.php',
            'options' => array(
                'url' => $this->url,
                'title' => $this->title,
                'description' => $this->description,
                'image' => $this->image,
            )
        );
    }




    public function getFb(){
        return $this->getFacebook();
    }

    public function getFacebook(){
        return array(
            'url' => 'https://www.facebook.com/sharer.php',
            'options' => array(
                'u'=> $this->url,
            )
        );
    }


    public function getGoogle(){
        return array(
            'url' => 'https://plus.google.com/share',
            'options' => array(
                'url'=> $this->url,
            )
        );
    }


    public function getLinkedIn(){
        return array(
            'url' => 'https://www.linkedin.com/shareArticle',
            'options' => array(
                'url'=> $this->url,
            )
        );
    }


    public function getTwitter(){
        return array(
            'url' => 'https://twitter.com/share',
            'options' => array(
                'url'=> $this->url,
                'text'=> $this->description,
                'via'=> $this->via,
            )
        );
    }


    public function getInstagramTEST(){
        return array(
            'url' => 'https://twitter.com/share',
            'options' => array(
                'url'=> $this->url,
                'text'=> $this->description,
                'via'=> $this->via,
            )
        );
    }



    // TODO Необходимо разобраться с запросом
    public function getOkTEST(){
        return array(
            'url' => 'https://connect.ok.ru/dk',
            'options' => array(
                'st.cmd' => 'WidgetShare',
                'st.shareUrl'=> $this->url,
                'st.title'=> $this->title,
                'st.description'=> $this->description,
                'st.imageUrl'=> $this->image,
            )
        );
    }


    public function getUrl($data)
    {
        $params = [];
        foreach($data['options'] as $key=>$val) {
            if(!empty($val)) {
                $params[] = $key.'='.$val;
            }
        };

        return $data['url'] . '?' . implode('&', $params);
    }
}