<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $DB;


$arShareSelect = array();
$arShareDefault = array(
    "vk",
    "fb",
    'google',
    "twitter"
);


$requestURL = ($_SERVER['HTTPS'] ? 'https' : 'http' ).'://'.$_SERVER['HTTP_HOST'].Bitrix\Main\Context::getCurrent()->getRequest()->getRequestedPage();
$requestURL = str_replace('index.php', '', $requestURL);


$page = strip_tags($arParams["PAGE_URL"]) ? $arParams["PAGE_URL"] : $requestURL;
$title =strip_tags($arParams["PAGE_TITLE"]);
$description =strip_tags($arParams["PAGE_DESCRIPTION"]);
$image =strip_tags($arParams["PAGE_IMAGE"]);


$this->setUrl($page);
$this->setTitle($title);
$this->setDescription($description);
$this->setImage($image);

$arShare = $arParams['HANDLERS'] ? $arParams['HANDLERS'] : $arShareDefault;


foreach($arParams['HANDLERS'] as $keyShare)
{
    $action = 'get'.ucfirst(strtolower($keyShare));
    if(method_exists($this, $action))
    {
        $data = $this->$action();
        $arResult['SHARE'][$keyShare]['CLASS'] = 'share-' . $keyShare;
        $arResult['SHARE'][$keyShare]['URL'] = $this->getUrl($data);
    }
}

$this->IncludeComponentTemplate();