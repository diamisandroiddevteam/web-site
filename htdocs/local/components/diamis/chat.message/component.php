<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();

global $USER;

use \Bitrix\Main\Loader;
use \Diamis\Chat\Base as ChatBase;

if (!Loader::includeModule('diamis.chat')) return;

$ID = intval($arParams['ID']);

$requestURL = Bitrix\Main\Context::getCurrent()->getRequest()->getRequestedPage();
$requestURL = str_replace('index.php', '', $requestURL);

$Element = ChatBase::getElement($ID);
$arResult['ELEMENT'] = $Element;

$UserTo = $Element['USER_ID'];
$UserFrom = $USER->GetID();

$text = trim(strip_tags($_REQUEST['content']));
if($_REQUEST['submit']==='save' && strlen($text))
{
    $result = ChatBase::setMessage($ID, $UserTo, $UserFrom, $text);
    if($result) {
        LocalRedirect($requestURL);
    }
}

$messages = ChatBase::getMessages($ID, $UserTo, $UserFrom);
$arGroup = array();
foreach($messages as $item)
{
    $day = $item['date_created']->format('d.m.Y');
    $name = $day;
    if($day===date('d.m.Y')) $name = 'Сегодня';
    if($day===date('d.m.Y', strtotime('-1 day'))) $name = 'Вчера';

    $arGroup[$day]['name'] = $name;
    $arGroup[$day]['items'][] = $item;
}

$arResult['MESSAGES'] = $messages;
$arResult['GROUP'] = $arGroup;
$arResult['USER_ID'] = $UserFrom;


$arResult['BLACK_LIST'] = ChatBase::isBlackList($UserTo, $UserFrom);


$this->IncludeComponentTemplate();