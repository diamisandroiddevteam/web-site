<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();
?>
<div class="office-container">
    <div class="title office-title">Мои сообщения</div>
    <div class="messenger">
        <div class="messenger-controls">

        </div>
        <div class="messenger-product">
            <div class="messenger-product--view">
                <img src="<?=array_shift($arResult['ELEMENT']['FILES']);?>" alt="<?=$arResult['ELEMENT']['NAME'];?>">
            </div>
            <div class="messenger-product--context">
                <a href="<?=$arResult['ELEMENT']['DETAIL_PAGE'];?>" target="_blank" class="messenger-product--title">Название объявления</a>
                <? if($arResult['ELEMENT']['PRICE']):?>
                    <div class="messenger-product--price">
                        <?=$arResult['ELEMENT']['PRICE']['PRICE'];?>
                        <span>a</span>
                    </div>
                <? endif;?>
                <div class="messenger-product--info">
                    <? foreach($arResult['ELEMENT']['LOCATION'] as $item):?>
                        <div class="messenger-product--location"><?=trim($item['VALUE']);?></div>
                    <? endforeach;?>
                    <div class="messenger-product--phone">+7 (900) 549-36-65</div>
                </div>
            </div>
        </div>
        <div class="messenger-container">
            <div class="messenger-container--wrapper">
                <?
                foreach($arResult['GROUP'] as $group):
                    ?>
                    <div class="messenger-datetime"><?=$group['name'];?></div>
                    <?
                    foreach($group['items'] as $item):
                        $date = $item['date_update'] ? $item['date_update']->format('i:s') : $item['date_created']->format('i:s');
                        ?>
                        <div class="messenger-message<?if($item['to_id']===$arResult['USER_ID']):?> messenger-message--main<?endif;?>">
                            <div class="messenger-message--content">
                                <div class="messenger-message--datetime"><?=$date;?></div>
                                <div class="messenger-message--text"><?=$item['content_value'];?></div>
                            </div>
                        </div>
                        <?
                    endforeach;
                endforeach;
                ?>
            </div>
        </div>
        <div class="messenger-reply">
            <? if($arResult['BLACK_LIST']):?>

            <? else:?>
            <form action="#" method="post">
                <div class="messenger-reply--container">
                    <span class="messenger-reply--image"></span>
                    <input type="file" class="messenger-reply--image-input">
                    <div class="messenger-reply--message">
                        <textarea name="content" id="" cols="30" rows="10" placeholder="Написать сообщение"></textarea>
                    </div>
                </div>
                <button name="submit" value="save" class="messenger-reply--send">Отправить</button>
            </form>
            <? endif;?>
        </div>
    </div>
</div>
