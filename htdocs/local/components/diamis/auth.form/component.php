<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();


global $USER, $APPLICATION;


use Bitrix\Main\Loader;
use Diamis\Ads\Base as AdsBase;


if (!Loader::includeModule('diamis.ads')) return;
if (!Loader::IncludeModule('socialservices')) return;


$componentPage = $_REQUEST['id'] ? strip_tags($_REQUEST['id']) : 'auth';
$ajax = $_REQUEST['ajax'];



// Соц Сети
if(isset($_REQUEST["auth_service_error"]) && $_REQUEST["auth_service_error"] <> ''){

}
elseif($_REQUEST["auth_service_id"]) {
//    $oAuthManager = new CSocServAuthManager;
//    $oAuthManager->Authorize($_REQUEST["auth_service_id"]);
//    LocalRedirect('/');
}



if($ajax) {
    $APPLICATION->RestartBuffer();
    $arResult['AJAX'] = true;
} else {
//    LocalRedirect('/');
}

if($_REQUEST['form'])
{
    $jsonData = array(
        'status' => false,
        'message' => null
    );

    // Авторизация
    if(
        $_REQUEST['form']==='auth'
        && isset($_REQUEST['LOGIN'])
        && strlen($_REQUEST['PASS'])
    ) {
        $login = trim($_REQUEST['LOGIN']);
        $password = $_REQUEST['PASS'];

        $isAuth = $USER->Login($login, $password);
        if($isAuth===true) {
            $jsonData['status'] = true;
            LocalRedirect($APPLICATION->GetCurPage());
        }
        else {
            $jsonData['message'] = "Логин или Пароль указан неверно.<br/>";
        }
    }


    // REGISTRATIONS
    if($_REQUEST['form']==='reg')
    {
        if(!is_array($arParams["SHOW_FIELDS"]))	$arParams["SHOW_FIELDS"] = array();
        if(!is_array($arParams["REQUIRED_FIELDS"]))	$arParams["REQUIRED_FIELDS"] = array();


        $arResult["VALUES"] = array();
        $arResult["ERRORS"] = array();

        $arResult["SHOW_FIELDS"] = array_merge($arDefaultFields, $arParams["SHOW_FIELDS"]);
        $arResult["REQUIRED_FIELDS"] = array_merge($arDefaultFields, $arParams["REQUIRED_FIELDS"]);

        $arResult["EMAIL_REQUIRED"] = (COption::GetOptionString("main", "new_user_email_required", "Y") <> "N");  // E-mail является обязательным полем
        // Запрашивать подтверждение регистрации по E-mail
        $arResult["USE_EMAIL_CONFIRMATION"] = (COption::GetOptionString("main", "new_user_registration_email_confirmation", "N") == "Y" && $arResult["EMAIL_REQUIRED"]? "Y" : "N");
        $arResult["USE_CAPTCHA"] = $arParams["USE_CAPTCHA"] == "Y" ? "Y" : "N";


        // Проверяем наличие пароля
        if(!empty($_REQUEST['PASS'][0]) || !empty($_REQUEST['PASS'][1])) {
            if($_REQUEST['PASS'][0]!==$_REQUEST['PASS'][1]) {
                $arResult['ERRORS'][] = "Пароли не совпадают<br/>";
            }else{
                $arResult['VALUES']['PASSWORD'] = $_REQUEST['PASS'][0];
            }
        } else {
            $arResult['ERRORS'][] = "Пароль не указан<br/>";
        }

        $arResult['VALUES']['PERSONAL_PHONE'] = $_REQUEST['PHONE'] = preg_replace('/[^0-9]/', '', (string) $_REQUEST['PHONE']);
        $arResult['VALUES']['LOGIN'] =  $arResult['VALUES']['EMAIL'] = $_REQUEST['EMAIL'];

        // Присваиваем группу по умолчанию
        $def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
        if($def_group !== "")
            $arResult['VALUES']["GROUP_ID"] = explode(",", $def_group);


        if(strlen($arResult["VALUES"]["EMAIL"]) > 0 && COption::GetOptionString("main", "new_user_email_uniq_check", "N") === "Y")
        {
            $res = CUser::GetList($b, $o, array("=EMAIL" => $arResult["VALUES"]["EMAIL"]));
            if($res->Fetch())
                $arResult["ERRORS"][] = "Пользователь с таким e-mail (".$arResult["VALUES"]["EMAIL"].") уже существует.<br/>";
        }

        if(!count($arResult["ERRORS"]))
        {
            $bConfirmReq = COption::GetOptionString("main", "new_user_registration_email_confirmation", "N") == "Y";

            $arResult['VALUES']["CHECKWORD"] = randString(8);
            $arResult['VALUES']["~CHECKWORD_TIME"] = $DB->CurrentTimeFunction();
            $arResult['VALUES']["ACTIVE"] = $bConfirmReq ? "N": "Y";
            $arResult['VALUES']["CONFIRM_CODE"] = $bConfirmReq ? randString(8): "";
            $arResult['VALUES']["LID"] = SITE_ID;

            $arResult['VALUES']["USER_IP"] = $_SERVER["REMOTE_ADDR"];
            $arResult['VALUES']["USER_HOST"] = @gethostbyaddr($REMOTE_ADDR);

            $bOk = true;

            $events = GetModuleEvents("main", "OnBeforeUserRegister");
            while($arEvent = $events->Fetch())
            {
                if(ExecuteModuleEventEx($arEvent, array(&$arResult['VALUES'])) === false)
                {
                    if($err = $APPLICATION->GetException())
                        $arResult['ERRORS'][] = $err->GetString();

                    $bOk = false;
                    break;
                }
            }
            $ID = null;
            if ($bOk)
            {
                $user = new CUser();
                $ID = $user->Add($arResult["VALUES"]);
            }

            $arResult["USER_ID"] = $ID;
            if (intval($ID) > 0)
            {
                $jsonData['status'] = true;
                $jsonData['message'] = "Спасибо за регистрация. На почту " .$arResult['VALUES']['EMAIL']." отправлено письмо<br/>";

                // authorize user
                if ($arParams["AUTH"] == "Y" && $arResult["VALUES"]["ACTIVE"] == "Y") {
                    if (!$arAuthResult = $USER->Login($arResult["VALUES"]["LOGIN"], $arResult["VALUES"]["PASSWORD"]))
                        $arResult["ERRORS"][] = $arAuthResult;
                }

                $arEventFields = $arResult['VALUES'];
                unset($arEventFields["PASSWORD"]);
                unset($arEventFields["CONFIRM_PASSWORD"]);

                $event = new CEvent;
                $event->SendImmediate("NEW_USER", SITE_ID, $arEventFields);
                if($bConfirmReq)
                    $event->SendImmediate("NEW_USER_CONFIRM", SITE_ID, $arEventFields);

                $arResult['VALUES']["USER_ID"] = $ID;
            }else{
                $arResult["ERRORS"][] = $user->LAST_ERROR;
            }
        }
    }


    if(count($arResult["ERRORS"])) {
        $jsonData['status'] = false;
        $jsonData['message'] = $arResult["ERRORS"];
    }


    if($ajax) {
        echo json_encode($jsonData);
        die();
    }
}
else
{

    $arResult['FORM_AUTH'] = $this->getFormAuth();
    $arResult['FORM_REG']  = $this->getFormReg();
}






$this->IncludeComponentTemplate($componentPage);