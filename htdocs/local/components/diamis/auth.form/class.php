<?
use Bitrix\Main;
use \Bitrix\Main\Loader;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


class Form extends \CBitrixComponent
{

    public static function getFormAuthSoc() {
        CModule::IncludeModule('socialservices');
        $oAuthManager = new CSocServAuthManager;
        $arSocServices = $oAuthManager->GetActiveAuthServices('Facebook');

        ob_start();
        ?>
        <div class="social_items">
            <div class="social_item"><a href="#" <?=$arSocServices['VKontakte']['FORM_HTML']['ON_CLICK']?> class="social_link social_link__vk"></a></div>
            <div class="social_item"><a href="#" <?=$arSocServices['Odnoklassniki']['FORM_HTML']['ON_CLICK']?> class="social_link social_link__odnoklasniki"></a></div>
            <div class="social_item"><a href="#" <?=$arSocServices['Facebook']['FORM_HTML']['ON_CLICK']?> class="social_link social_link__facebook"></a></div>
            <div class="social_item"><a href="#" <?=$arSocServices['GooglePlusOAuth']['FORM_HTML']['ON_CLICK']?> class="social_link social_link__google"></a></div>
        </div>
        <?
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }


    public static function getFormAuth(){

        global $APPLICATION;

        ob_start();
        ?>
        <form id="form-auth" action="/registration.php" data-form="ajax" method="post">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="ajax" value="y" >
            <input type="hidden" name="form" value="auth" >
            <div class="scroll_item__title">Авторизация</div>
            <div class="form-error-message"></div>
            <div class="form-true-message"></div>
            <div class="scroll_item__row">
                <div class="input">
                    <div class="input-placeholder">Логин</div>
                    <input name="LOGIN" type="text" data-required="notnull" class="input-tag">
                </div>
            </div>
            <div class="scroll_item__row">
                <div class="input">
                    <div class="input-placeholder">Пароль</div>
                    <input name="PASS" type="password" data-required="notnull" class="input-tag">
                </div>
                <div class="scroll__right">
                    <a href="#" class="scroll_link scroll_link__help">Забыли пароль?</a>
                </div>
            </div>
            <div class="scroll_item__row">
                <button name="button" class="btn btn-rempty" value="auth">Войти</button>
            </div>
            <div class="scroll_item__row">
                <div class="social social_entry">
                    <div class="social_header">или</div>
                    <?$APPLICATION->IncludeComponent(
                        "diamis:auth.social",
                        ".default",
                        array(
                            'BACKURL' => '/'
                        )
                    );?>
                    <div class="social_footer">Войти через социальную сеть</div>
                </div>
            </div>
            <div class="scroll_item__row">
                <div class="scroll__center">
                    <a href="#" data-scroll-target="reg" class="scroll_link scroll_link__big">Зарегистрироваться</a>
                </div>
            </div>
        </form>
        <?
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }


    public static function getFormReg() {

        global $APPLICATION;

        ob_start();
        ?>
        <form id="form-reg" action="/registration.php" data-form="ajax" method="post">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="ajax" value="y" >
            <input type="hidden" name="form" value="reg" >
            <div class="scroll_item__title">Регистрация</div>
            <div class="form-error-message"></div>
            <div class="form-true-message"></div>
            <div class="scroll_item__row">
                <div class="input">
                    <div class="input-placeholder">Ваше Имя</div>
                    <input name="NAME" type="text" class="input-tag">
                </div>
            </div>
            <div class="scroll_item__row">
                <div class="input">
                    <div class="input-placeholder">Ваш E-Mail</div>
                    <input name="EMAIL" type="text" data-required="notnull" class="input-tag">
                </div>
            </div>
            <div class="scroll_item__row">
                <div class="input">
                    <div class="input-placeholder">Ваш Телефон</div>
                    <input name="PHONE" type="text" data-required="notnull" data-mask="phone" class="input-tag">
                </div>
            </div>
            <div class="scroll_item__row">
                <div class="input">
                    <div class="input-placeholder">Пароль</div>
                    <input name="PASS[0]" type="password" data-required="notnull" class="input-tag">
                </div>
            </div>
            <div class="scroll_item__row">
                <div class="input">
                    <div class="input-placeholder">Повторите Пароль</div>
                    <input name="PASS[1]" type="password" data-required="notnull" class="input-tag">
                </div>
            </div>
            <div class="scroll_item__row">
                <button name="button" class="btn btn-rempty" value="reg">Зарегистрироваться</button>
            </div>
            <div class="scroll_item__row">
                <div class="social social_entry">
                    <div class="social_header">или</div>
                    <?$APPLICATION->IncludeComponent(
                        "diamis:auth.social",
                        ".default",
                        array(
                            'BACKURL' => '/'
                        )
                    );?>
                    <div class="social_footer">Нажимая "Зарегистрироваться" или выбирая социальную сеть. Вы принимаете условия
                        <a href="#">пользовательского соглашения</a></div>
                </div>
            </div>
            <div class="scroll_item__row">
                <div class="scroll__center">
                    <a href="#" data-scroll-target="entry" class="scroll_link scroll_link__big">Авторизация</a>
                </div>
            </div>
        </form>
        <?
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }
}