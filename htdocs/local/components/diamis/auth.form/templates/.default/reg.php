<?php

use Bitrix\Main\Page\Asset;

if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();

?>
<div class="window">
    <div class="window_content">
        <div data-scroll="scroll_item" class="scroll">
            <div data-scroll-id="reg" class="scroll_item show">
                <?=$arResult['FORM_REG'];?>
            </div>
            <div data-scroll-id="entry" class="scroll_item">
                <?=$arResult['FORM_AUTH'];?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    if(!!window.DScroll) {
        var dscroll = new window.DScroll();
        dscroll.init();
    }
    if(!!window.DForm) {
        new window.DForm('form-auth');
        new window.DForm('form-reg');
    }
</script>