<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


use Diamis\Ads\Base;

?>
<div class="office">
    <?
    $APPLICATION->IncludeComponent(
        'bitrix:menu',
        'profile',
        array(
            "ROOT_MENU_TYPE" => "profile",
            "MAX_LEVEL" => "1",
            "USE_EXT" => "Y"
        )
    );
    ?>
    <div class="office-container">
        <div class="office-ctabs">
            <a href="/profile/ads/" class="office-ctabs-item<?if($arResult['TYPE']==='list'):?> active<?endif;?>">Активные</a>
            <a href="/profile/ads/moder/" class="office-ctabs-item<?if($arResult['TYPE']==='moder'):?> active<?endif;?>">На модерации</a>
            <a href="/profile/ads/archive/" class="office-ctabs-item<?if($arResult['TYPE']==='archive'):?> active<?endif;?>">Архив</a>
            <a href="/profile/ads/dell/" class="office-ctabs-item<?if($arResult['TYPE']==='dell'):?> active<?endif;?>">Удаленные</a>
        </div>
        <div class="office-catalog">
            <?
            foreach($arResult['ITEMS'] as $item):
                $image = array_shift($item['FILES']);
                $text = Base::crop($item['TEXT'], 160);
                $text = Base::concludeTag($text);

                ?>
                <div class="order">
                    <div class="order-container">
                        <div class="order-status">Статус: <?=$arResult['STATUS'];?></div>
                        <div class="order-image">
                            <img src="<?=$image;?>" alt="<?=$item['NAME'];?>" style="width:100%;">
                        </div>
                        <div class="order-info">
                            <div class="order-title"><?=$item['NAME'];?></div>
                            <?if($item['PRICE']):?>
                                <div class="order-price">
                                    <?=$item['PRICE'][key($item['PRICE'])]['PRICE'];?>
                                    <span>a</span>
                                </div>
                            <?endif;?>
                            <div class="order-desc"><?=$text;?></div>
                            <div class="product-card-stat" style="bottom:0;">
                                <div class="product-cstat-label">Размещено: <?=$item['DATE_CREATE']->format("d.m.Y");?></div>
                                <div class="product-cstat-label">
                                    <span class="product-cstat-update"></span>
                                    <?=$item['DATE_UPDATE']->format("d.m.Y  h:i");?>
                                </div>
                                <div class="product-cstat-label">
                                    <span class="product-cstat-view"></span>
                                    234
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="order-ads-tools">
                        <div class="order-tools-list">
                            <a href="#" class="order-tools">Обмен (2)</a>
                            <a href="#" class="order-tools">Сообщения</a>
                        </div>
                        <div class="order-tools-list">
                            <a href="/create/<?=$item['ID'];?>/" class="order-tools">Редактировать</a>
                            <a href="#" class="order-tools">Удалить</a>
                        </div>
                    </div>
                </div>
                <?
            endforeach;
            ?>
        </div>
        <?
        $APPLICATION->IncludeComponent(
            "bitrix:main.pagenavigation",
            "",
            array(
                "NAV_OBJECT" => $arResult['NAV'],
                "SEF_MODE" => "N",
                "SHOW_COUNT" => "N",
            ),
            false
        );
        ?>
    </div>
</div>