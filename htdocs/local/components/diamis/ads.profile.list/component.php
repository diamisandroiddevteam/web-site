<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


use \Bitrix\Main\Loader;
use \Diamis\Ads\AdsTable;
use \Diamis\Ads\FavoritesTable;


if (!Loader::includeModule('diamis.ads')) return;


global $USER;


$arResult['TYPE'] = $_REQUEST['TYPE'] ? strip_tags($_REQUEST['TYPE']) : 'list';
$arResult['STATUS'] = 'Идут показы';
$arResult['PAGE_SIZE'] = ($pageSize = intval($arParams['PAGE_SIZE']) ? $pageSize : 10);


$arFilter = array(
    'USER_ID' => $USER->GetID()
);


switch ($arResult['TYPE']) {
    case 'moder':
        $arResult['STATUS'] = 'На модерации';
        $arFilter['ACTIVE'] = false;
        break;
    case 'archive':
        $arResult['STATUS'] = 'Архив';
        $arFilter['<DATE_EXPIRE'] = date('Y-m-d h:i:s');
        break;

    default:
        $arFilter['ACTIVE'] = true;
        break;
}




// Постраничная навигация без COUNT
// Возможна только прямая навигация.
// Принцип работы: разработчик выбирает чуть больше записей, чтобы определить, возможно ли показать "далее",
// а в своем цикле по записям разработчик определяет границу цикла.
$nav = new \Bitrix\Main\UI\PageNavigation("page");
$nav->allowAllRecords(true)
    ->setPageSize($arResult['PAGE_SIZE'])
    ->initFromUri();

$dbAds = AdsTable::getList(array(
    'select' => array('ID'),
    'filter' => $arFilter,
    'offset' => ($offset = $nav->getOffset()),
    'limit'  => (($limit = $nav->getLimit()) > 0 ? $limit + 1 : 0)
));

$n = 0;
$arAdsId = array();
while($item = $dbAds->fetch())
{
    $n++;
    if($limit > 0 && $n > $limit) break;

    $arAdsId[] = $item['ID'];
}


$nav->setRecordCount($offset + $n);


$filter = array('ID' => $arAdsId);
$arResult['ITEMS'] = AdsTable::getElements($filter);
$arResult['NAV'] = $nav;



$this->IncludeComponentTemplate($componentPage);