<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();

use \Bitrix\Main\Loader;
use \Diamis\Ads\AdsTable;


if (!Loader::includeModule('diamis.ads')) return;


$filterName = $arParams['FILTER_NAME'] ? $arParams['FILTER_NAME'] : 'arCatalogFilter';
global $$filterName;

unset($$filterName['filter']['sort']);
$arResult['DATA_FILTER'] = $$filterName;

$arResult['ITEMS'] = AdsTable::getElements($arResult['DATA_FILTER']['filter'], $arResult['DATA_FILTER']['select'], true);
$arLocation = array();
foreach($arResult['ITEMS'] as $key=>$item)
{
    $date = $item['DATE_CREATE']->format("d.m.Y");
    if($date==date('d.m.Y')){
        $date = 'Сегодня в ' . $item['DATE_CREATE']->format("h:i");
    }elseif($date==date('d.m.Y', strtotime('last day'))){
        $date = 'Вчера в ' . $item['DATE_CREATE']->format("h:i");
    }

    $k = 0;
    $arProp = array();
    while($k<2)
    {
        if(empty($item['PROPERTY'][key($item['PROPERTY'])])) break;
        $arProp[] = $item['PROPERTY'][key($item['PROPERTY'])];
        next($item['PROPERTY']);
        $k ++;
    }


    $locations = $item['LOCATION'];
    foreach($locations as $local) {
        $arLocation[] = array(
            'KEY' => $key,
            'ELEMENT_ID' => $item['ID'],
            'DATE_UPDATE' => $item['DATE_UPDATE']->format("d.m.Y h:i"),
            'DATE_CREATE' => $date,
            'PROPERTY' => $arProp,
            'VIEW' => $item['VIEW'],
            'NAME' => $item['NAME'],
            'URL' => $item['URL'].$item['CODE'].'_'.$item['ID'].'/',
            'IMAGE' => array_shift($item['FILES']),
            'PRICE' => count($item['PRICE']) ? array_shift($item['PRICE']) : false,
            'CATEGORY_NAME' => $item['CATEGORY_NAME'],
            'LAT' => $local['LAT'],
            'LNG' => $local['LNG']
        );
    }
}
$arResult['LOCATION'] = $arLocation;


$this->IncludeComponentTemplate();