<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();

?>
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<div class="map">
    <div class="map-list">
        <div class="btn-tabs">
            <span class="btn-tabs-item select">На карте</span>
            <span class="btn-tabs-item" onclick="$('button[value=list]').click();">Список</span>
        </div>
        <div class="map-title">Найдено объявлений: <?=count($arResult['ITEMS']);?></div>
        <div id="maps-list" class="map-catalog"></div>
    </div>
    <div id="maps-page" class="map-container"></div>
</div>
<script type="text/javascript">
var dmap = new window.DMap($('#maps-page'));
dmap.setMarker('<?=json_encode($arResult['LOCATION']);?>');
dmap.init();
</script>