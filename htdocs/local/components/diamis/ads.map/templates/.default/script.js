(function(window){


    window.DMap = function(el) {
        this.el = $(el);
        this.ymap = null;
        this.location = {
            type: 'FeatureCollection',
            features: null
        };
    };


    window.DMap.prototype.init = function() {
        var _this = this;

        _this.el.mapsAdd({
            markerShow: false,
            arrMarker: _this.location
        });
    };

    window.DMap.prototype.setMarker = function(data){
        var _this = this,
            boxList = document.getElementById('maps-list'),
            data = JSON.parse(data),
            features = [];


        $.each(data, function(i, item) {

            if(boxList) {
                var ads = document.createElement('div');
                ads.className = 'map-item';
                ads.innerHTML = _this.getItemList(i, item);
                boxList.appendChild(ads);
            }
            features.push(_this.getItemMap(i, item) );
        });

        this.location.features = features;
    };


    /**
     * Формирует список объявлений
     * @param i
     * @param item
     * @returns {string}
     */
    window.DMap.prototype.getItemList = function(i, item) {
        var prop = '';
        $.each(item.PROPERTY, function(j, property){
            prop +=
                '<div class="map-item__row">' +
                '   <div class="map-item__label">- ' +  property.NAME.toLowerCase() + ':</div>' +
                '   <div class="map-item__result">' + property.VALUE + '</div>' +
                '</div>';
        });

        return '' +
        '<div class="map-item__images">' +
'           <img class="map-item__image" src="' + item.IMAGE + '" alt="' + item.NAME + '"> ' +
        '</div>' +
        '<div class="map-item__info">' +
        '   <a href="'+item.URL+'" class="map-item__title" href="">' + item.NAME + '</a> ' +
        '   ' + prop
            +
            (
                (typeof item.PRICE.PRICE !== "undefined" )
                    ? '<div class="map-item__price"><span>Цена: </span><span>' + item.PRICE.PRICE + '</span><span class="price">a</span></div>'
                    : ''
            )
            +
        '</div>';
    };



    /**
     * Объект для отображения на карте
     * @param i
     * @param item
     * @returns {{type: string, id: *, geometry: {type: string, coordinates: *[]}, properties: {balloonContentHeader: *, balloonContentBody: string, balloonContentFooter: string, clusterCaption: *, hintContent: string}}}
     */
    window.DMap.prototype.getItemMap = function(i, item){
        var prop = '';
        $.each(item.PROPERTY, function(j, property){
            prop +=
                '<div class="ymap-widget-prop">' +
                '   <div class="ymap-widget-prop-label">- ' +  property.NAME.toLowerCase() + ':</div>' +
                '   <div class="ymap-widget-prop-result">' + property.VALUE + '</div>' +
                '</div>';
        });

        return {
            type: 'Feature',
            id: i,
            geometry: {
                type: 'Point',
                coordinates: [item.LAT, item.LNG]
            },
            properties: {
                balloonContentHeader: item.NAME,
                balloonContentBody: '' +
                '<div class="ymap-widget ymap-widget-tooltip-content">' +
                '   <div class="ymap-widget-tooltip-image">' +
                '       <img src="' + item.IMAGE + '" alt="' + item.NAME + '"> ' +
                '   </div>' +
                '   <div class="ymap-widget-tooltip-info">' +
                '       <div class="point-name">Категория: ' + item.CATEGORY_NAME + '</div>' +
                '       <div class="product-cstat-label">' + item.DATE_CREATE + '</div>' +
                '       <div class="product-cstat-label">' +
                '           <span class="product-cstat-update"></span>' + item.DATE_UPDATE +
                '       </div>' +
                '       <div class="product-cstat-label">' +
                '           <span class="product-cstat-view"></span>' + item.VIEW +
                '       </div>' +
                '       ' + prop +
                '   </div>' +
                '</div>',
                balloonContentFooter: '<a href="'+item.URL+'" target="_blank" class="point-site">Подробнее</a>'+
                (
                    (typeof item.PRICE.PRICE !== "undefined" )
                        ? '<div class="point-price"><span>Цена:</span><span>' + item.PRICE.PRICE + '</span><span class="price">a</span></div>'
                        : ''
                ),
                clusterCaption: item.NAME,
                hintContent: 'hintContent'
            }
        }
    }
})(window);
