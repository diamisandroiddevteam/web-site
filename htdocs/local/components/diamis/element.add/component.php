<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();

use \Bitrix\Main\Loader;
use \Diamis\Ads\Base as AdsBase;
use \Diamis\Ads\User as AdsUser;
use \Diamis\Ads\DomElement as AdsDomElement;
use \Diamis\Ads\CategoryTable;


if (!Loader::includeModule('diamis.ads')) return;


global $USER, $APPLICATION;

$AJAX = $_REQUEST['ajax'];
$TYPE = $_REQUEST['type'];
$ACTION = $_REQUEST['action'];

// Получаем Post параметры
$FIELDS = $_REQUEST['field'];

$USER_ID = $USER->GetID();
$USER_FIELDS = array();
if($USER->IsAuthorized()) {
    $PROP = AdsUser::getProp(array($USER_ID));
    $USER_FIELDS = $PROP[$USER_ID];
    $USER_GROUPS = CUser::GetUserGroup($USER_ID); // массив групп, в которых состоит пользователь
}


// Получаем ID объявления при редактировании
$arParams['ID'] = intval($_REQUEST["ID"]);
$arParams['SEF_TYPE'] = $arParams['SEF_TYPE'] ? $arParams['SEF_TYPE'] : 'ELEMENT_CODE';


$page404 = false;
$UserAuthor = ($arParams['ID'] ? AdsUser::inAuthor($arParams['ID']) : false); // Является пользователь автором объявления или нет
$UserAccess = false;  // Разрешен пользователю доступ на редактирование или нет
$arSefType = array(
    "ELEMENT_ID" => "ID",
    "ELEMENT_CODE" => "CODE"
);

$arResult["ERRORS"] = array();   // Массив сообщений об ошибке
$arResult["MESSAGE"] = array();  // Массив сообщений об ошибке
$arResult["DATE_CREATE"] = ConvertTimeStamp(time(), "FULL");
$arResult["DATE_UPDATE"] = ConvertTimeStamp(time(), "FULL");


if($UserAuthor && $arParams['ID']) {
    $UserAccess = true;
};

// вывод сообщения об отказе доступа
if(!$UserAccess && $arParams["ID"]) {
    ShowError("У вас нет прав для редактирования данного объявления");
    return;
}
if($arCategory[$i]['FILE_ID']>0) {
    $arCategory[$i]['FILE_PATH'] = \CFile::GetPath($arCategory[$i]['FILE_ID']);
}

$arCategory = array();
try {
    // Получаем список категорий
    $dbCategory = CategoryTable::getList(array(
        'cache' => array('ttl' => 3600)
    ));

    foreach($dbCategory as $Category):
        // Получаем путь до изображения
        if($Category['FILE_ID']>0) {
            $Category['FILE_PATH'] = \CFile::GetPath($Category['FILE_ID']);
        }

        $Category['NAME'] = trim($Category['NAME'], "\t\n\r\0\x0B");
        $Category['CREATE_LINK'] = $arParams["SEF_FOLDER"].$Category[$arSefType[$arParams['SEF_TYPE']]].'/';

        $arCategory[] = $Category;
    endforeach;


    // Save WebForm method POST
    if($AJAX && $_REQUEST['submit']) {
        $data = array(); // массив полей для заполнения
        $result = array(
            'status' => true,
            'result' => array()
        );

        $APPLICATION->RestartBuffer();

        if($arParams['ID']) {
            $result = \Diamis\Ads\AdsTable::updateElement($arParams['ID'], $FIELDS);
        }
        else{
            $result = \Diamis\Ads\AdsTable::create($FIELDS);
        }

        echo json_encode($result);
        die();
    }

} catch (\Bitrix\Main\ArgumentException $e) {
    ShowError($e->getMessage());
    return;
}



// Если успользуем ЧПУ
if($arParams["SEF_MODE"] == "Y")
{
    $requestURL = Bitrix\Main\Context::getCurrent()->getRequest()->getRequestedPage();
    $requestURL = str_replace('index.php', '', $requestURL);

    $folder = str_replace("\\", "/", $arParams["SEF_FOLDER"]);
    if ($folder != "/")
        $folder = "/".trim($folder, "/ \t\n\r\0\x0B")."/";


    $currentPageUrl = substr($requestURL, strlen($folder));

    $elementEdit = intval(str_replace('/','', $currentPageUrl));
    if($elementEdit>0):
        $element = \Diamis\Ads\AdsTable::getElementID($elementEdit);
        $arResult['ELEMENT'] = $element;
        $arResult["CURRENT_TYPE_ID"] = $element['TYPE_ID'];
        $componentPage = 'element';
        $i = 0;
        $length = count($arCategory);
        while($i<$length)
        {
            $item = $arCategory[$i];
            if($item['ID']===$element['CATEGORY_ID']) {
                $arResult["CURRENT_CATEGORY"]['ID'] = $item['ID'];
                $arResult["CURRENT_CATEGORY"]['PARENT_ID'] = $item['PARENT'] ? $item['PARENT'] : $item['ID'];
            }
            $i++;
        }

    // Проверяем на существование категории
    elseif(strlen($currentPageUrl)):
        $i = 0;
        $length = count($arCategory);
        while($i<$length)
        {
            $item = $arCategory[$i][$arSefType[$arParams['SEF_TYPE']]];
            if($item === $currentPageUrl || $item.'/' == $currentPageUrl)
            {
                $componentPage = 'element';
                $arResult["CURRENT_CATEGORY"]['ID'] = $arCategory[$i]['ID'];
                $arResult["CURRENT_CATEGORY"]['PARENT_ID'] = $arCategory[$i]['PARENT']
                    ? $arCategory[$i]['PARENT']
                    : $arCategory[$i]['ID'];

                break;
            }
            $i++;
        }
    else:
        $arResult["CATEGORIES"] = AdsBase::tree($arCategory);
        $componentPage = 'category';
    endif;

    if(!strlen($componentPage))
        $page404 = true;
}
else
{
    ShowError("Необходимо подключить ЧПУ компонента");
    return;
}
// TheEnd ЧПУ




if($AJAX)
{
    $Type = intval(array_shift($FIELDS['type']));
    $Category = intval(array_shift($FIELDS['category']));
    $ParentID = intval($FIELDS['category_parent']) ? intval($FIELDS['category_parent']) : $Category;


    $arResult["CURRENT_TYPE"] = $Type;
    $arResult["CURRENT_CATEGORY"]['ID'] = $Category;
    $arResult["CURRENT_CATEGORY"]['PARENT_ID'] = $ParentID;
}




if($componentPage=='element')
{
    try
    {
        $arResult['CATEGORY_PARENT'] = $this->getParentCategory($arCategory);
        $arType = AdsBase::getTypes(array(
            $arResult["CURRENT_CATEGORY"]['ID'],
            $arResult["CURRENT_CATEGORY"]['PARENT_ID']
        ));

        // Если у категории отстуствует тип то
        // значение категорий берется у его родителя
        $arResult['CATEGORY_TYPE'] = $arType[$arResult["CURRENT_CATEGORY"]['ID']]
            ? $arType[$arResult["CURRENT_CATEGORY"]['ID']]
            : $arType[$arResult["CURRENT_CATEGORY"]['PARENT_ID']];

        $arResult['FIELDS']['CATEGORY'] = $this->getFieldCategory($arCategory, $arResult["CURRENT_CATEGORY"]['PARENT_ID'], $arResult["CURRENT_CATEGORY"]['ID']);
        $arResult['FIELDS']['TYPE'] = $this->getFieldType($arResult["CURRENT_TYPE"], $arResult["CURRENT_TYPE"]);

        $arFieldsContact = $this->getFieldContact();

        $arProps = AdsBase::getFields($arResult["CURRENT_CATEGORY"]['ID'], $arResult["CURRENT_TYPE"]);

        $arProps = $this->getFieldProp($arProps);

        $arResult['LAT'] = ($GLOBALS['CITY_IP']['LAT'] ? $GLOBALS['CITY_IP']['LAT'] : '55.751430');
        $arResult['LNG'] = ($GLOBALS['CITY_IP']['LNG'] ? $GLOBALS['CITY_IP']['LNG'] : '37.618832');
        if(count($arResult['ELEMENT']['LOCATION'])) {
            $location = array_shift($arResult['ELEMENT']['LOCATION']);
            $arResult['LAT'] = $location['LAT'];
            $arResult['LNG'] = $location['LNG'];
        }

        if($AJAX)
        {
            $APPLICATION->RestartBuffer();
            $result = array_merge(
                array(
                    $arResult['FIELDS']['CATEGORY'],
                    $arResult['FIELDS']['TYPE'],
                ),
                $arProps,
                $arFieldsContact
            );


            $jsonData = array(
                'status' => true,
                'result' => $result
            );
            die(json_encode($jsonData));
        }
    }
    catch (\Bitrix\Main\ArgumentException $e)
    {
        die(json_encode($e->getMessage()));
    }
}



$this->IncludeComponentTemplate($componentPage);