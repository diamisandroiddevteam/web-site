(function(window) {

    window.DElementAdd = function(){
        this.form = $('#form-ads-add');
        this.htmlform = new window.DHtmlForm();
    };


    window.DElementAdd.prototype.init = function(){
        var _this = this;

        this.start();
        this.form.on('submit', function(event){
            event.preventDefault();
            _this.send();
        });
    };

    window.DElementAdd.prototype.start = function(){
        var _this = this;
        // this.changeFiles();
        $.ajax({
            url:  window.location.href,
            type:'post',
            data: this.getFormData(this.form),
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(result){
                if (result.status)
                {
                    var fields = _this.fields(result.result);
                    _this.updateFormAdd(fields);
                }

            }
        });
    };

    window.DElementAdd.prototype.updateFormAdd = function(data){
        var category = false,
            type = false,
            Section = {
                main: $(this.form).find('#section-main'),
                prop: $(this.form).find('#section-property')
            };

        Section.prop.html('');

        if(typeof data.main!=="undefined") {
            $.each(data.main, function(i, item){
                var htmlField;

                // Заменяем если элемент найден
                if(htmlField = $('.' + item.id)) {
                    htmlField.replaceWith( item.html );
                }


                if(item.data.id==='field[category]' && item.data.currentValue) category = true;
                if(item.data.id==='field[type]' && item.data.currentValue) type = true;
            });
        }



        if(typeof data.prop!=="undefined") {

            $('<div/>').addClass('form-group').html('<div class="title office-title">Параметры<div/>').appendTo(Section.prop);

            $.each(data.prop, function(i, item){
                var group = $('<div/>'),
                    field = $('<div/>'),
                    title = item.data.title;

                if(item.data.required)
                    title += '<span class="form-field--important"></span>';

                group.addClass('form-group');
                field.addClass('form-field');

                $('<div/>').addClass('form-field--label').html( title + ':').appendTo(field);
                $('<div/>').addClass('form-field--param').html( item.html ).appendTo(field);

                field.appendTo(group);
                group.appendTo(Section.prop);
            });

        }


        if(category && type)
        {
            $('#section-main').removeClass('section-disable');
            $('#section-contact').removeClass('section-disable');
            $('#section-save').removeClass('section-disable');
        }
        else
        {
            $('#section-main').addClass('section-disable');
            $('#section-contact').addClass('section-disable');
            $('#section-save').addClass('section-disable');
        }

    };

    window.DElementAdd.prototype.changeFiles = function(){
        if(!this.form) return false;

        $(this.form).find('[type=file]').on('change', function(evt){
            var input = evt.target,
                image = $(input).parent().find('.field-file--preview');

            if(input.files && input.files[0]) {
                var reader = new FileReader(),
                    rotate = $('<span/>'),
                    dell = $('<span/>');

                rotate.addClass('field-file-rotate');
                rotate.attr('title', '90');

                dell.addClass('field-file-dell');
                dell.attr('title', '90');

                reader.onload = function(e){
                    $(image).css({
                        width: '102%',
                        height: '102%',
                        background: 'url('+e.target.result+') no-repeat',
                        backgroundSize: 'cover'
                    });

                    retate.appendTo(image);
                    dell.appendTo(image);
                };
                reader.readAsDataURL(input.files[0]);
            }
        });
    };

    window.DElementAdd.prototype.change = function(container, callback=function(){}){
        let box = container ? container : this.form,
            items;

        items = box.find('[name]');
        items.on('change', (event)=> {

            let el = event.target;
            callback(el);

        });
    };

    window.DElementAdd.prototype.getFormData = function() {
        var fd = new FormData,
            data = $(this.form).serializeArray(),
            inputFiles = $(this.form).find('[type=file]');

        fd.append('ajax', 'y');

        if(inputFiles.length) {
            for(let i=0; i<inputFiles.length; i++) {

                let files = inputFiles[i].files;
                for(let j=0; j<files.length; j++) {

                    fd.append(inputFiles[i].name, files[j]);
                }
            }
        }

        for(let i=0; i<data.length; i++) {
            if(data[i].value) {
                fd.append(data[i].name, data[i].value);
            }
        }

        return fd;
    };


    // Генерируем список полей
    // в виде DOM и групируем
    window.DElementAdd.prototype.fields = function(fields){
        var result = {};
        if(!fields) return result;

        for(let i=0; i<=fields.length; i++)
        {
            if(typeof fields[i]==="undefined") break;

            let field = this.htmlform.getField(fields[i]);
            if(field)
            {
                if(typeof result[fields[i].group]==="undefined")
                    result[fields[i].group] = [];

                // Прикрепляем обработчик события при изменении
                if(fields[i].updatesForm) {
                    this.change(field.html, ()=>{ this.start(this.form); });
                }

                result[fields[i].group].push({
                    id: field.id,
                    data: fields[i],
                    html: field.html
                });
            }
        }
        return result;
    };


    window.DElementAdd.prototype.send = function(){
        var action = $(this.form).attr('action') ? $(this.form).attr('action') : window.location.href,
            type = $(this.form).attr('method') ? $(this.form).attr('method') : 'post',
            data = this.getFormData(this.form),
            _this = this;

        data.append('submit', 'send');
        $.ajax({
            url: action,
            type: type,
            data: data,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(data) {
                let status = data.status;
                if(!status) _this.showError(data.result);
                if(data.redirect) window.location.href = data.redirect;
            },
            error: function(data){
                console.log('error', data);
            }
        });
    };


    window.DElementAdd.prototype.showError = function(errors){
        if(!errors) return false;

        var stop = false;
        for(let i=0; i<errors.length; i++) {
            let input = $(errors[i].field);
            if(input.length)
            {
                var box = $(input).parents('.form-field--param'),
                    container = box.children();

                container.addClass('field-error');
                if(!stop && container.length) {
                    stop = true;
                    $('html, body').animate({
                        scrollTop: ($(container).offset().top - 100)
                    }, 800);
                }
            }

        }
    }


})(window);