<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

?>
<div class="ads-add">
    <div class="category-list-title">Выберите категорию:</div>
    <div class="category-list">
        <?
        foreach($arResult['CATEGORIES'] as $item):
            ?>
            <div class="category-item">
                <a href="<?=$item['CREATE_LINK'];?>" class="category-link">
                    <div style="background: url(<?=$item['FILE_PATH'];?>) center no-repeat; background-size: contain;" class="category-icon"></div>
                    <div class="category-title"><?=$item['NAME'];?></div>
                </a>
                <div class="category-sub-list">
                    <?
                    foreach($item['CHILD'] as $child):
                        ?>
                        <a href="<?=$child['CREATE_LINK'];?>" class="category-sub-link"><?=$child['NAME'];?></a>
                        <?
                    endforeach;
                    ?>
                </div>
                <? if(count($item['CHILD'])>15):?>
                    <a class="category-sub-btn">показать еще</a>
                <? endif;?>
            </div>
            <?
        endforeach;
        ?>
        <script>
        $('.category-sub-btn').on('click', function(){
            var list = $(this).parent().find('.category-sub-list');

            if(!list.hasClass('show')) {
                list.addClass('show');
                $(this).html('скрыть');
            } else {
                list.removeClass('show');
                $(this).html('показать еще');
            }
        });
        </script>
    </div>
</div>