<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Page\Asset;
use \Diamis\Ads\Base as AdsBase;
use \Diamis\Ads\DomElement as AdsDomElement;


Asset::getInstance()->addJs($templateFolder.'/jQuery-File-Upload-9.21.0/js/vendor/jquery.ui.widget.js');
Asset::getInstance()->addJs($templateFolder.'/jQuery-File-Upload-9.21.0/js/jquery.iframe-transport.js');
Asset::getInstance()->addJs($templateFolder.'/jQuery-File-Upload-9.21.0/js/jquery.fileupload.js');


$title = $arParams['ID']
    ? 'Редактировать объявление'
    : 'Подать объявление';


?>
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<div class="title office-title"><?=$title;?></div>
<div class="ads-nav-container">
    <a href="<?=$arParams['SEF_FOLDER'];?>" title="изменить категорию" class="ads-nav-container--icon">
        <img src="<?=$arResult['CATEGORY_PARENT']['FILE_PATH'];?>" alt="<?=$arResult['CATEGORY_PARENT']['NAME'];?>" />
    </a>
    <div class="ads-nav-container--title"><?=$arResult['CATEGORY_PARENT']['NAME'];?></div>
</div>
<div class="form">
    <form id="form-ads-add" data-form="ads-add" class="form-ads-add" method="post" enctype="multipart/form-data">
        <?=bitrix_sessid_post()?>
        <?if(intval($arParams["ID"])>0):?>
            <input type="hidden" name="field[id]" value="<?=$arParams["ID"]?>" />
        <?endif;?>
        <input type="hidden" name="field[category_parent]" value="<?=$arResult['CURRENT_CATEGORY']['PARENT_ID'];?>">
<!--        <input type="hidden" name="field[location][lat]" value="">-->
<!--        <input type="hidden" name="field[location][lng]" value="">-->
        <div id="section-category">
            <div class="form-group">
                <div class="form-field">
                    <div class="form-field--label">
                        Категория
                        <span class="form-field--important"></span>:
                    </div>
                    <div class="form-field--param">
                        <?=AdsDomElement::get($arResult['FIELDS']['CATEGORY']);?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-field">
                    <div class="form-field--label">
                        Тип Объявления
                        <span class="form-field--important"></span>:
                    </div>
                    <div class="form-field--param">
                        <?=AdsDomElement::get($arResult['FIELDS']['TYPE']);?>
                    </div>
                </div>
            </div>
        </div>
        <div id="section-main" <?if(!$arResult["CURRENT_TYPE"]):?>class="section-disable"<?endif;?>>
            <div class="form-group">
                <div class="form-field">
                    <div class="form-field--label">
                        Название
                        <span class="form-field--important"></span>:
                    </div>
                    <div class="form-field--param">
                        <div class="form-field-input">
                            <input id="field_name" name="field[name]" type="text" class="form-field-input-tag" value="<?=$arResult['ELEMENT']['NAME'];?>"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-field">
                    <div class="form-field--label">
                        Цена:
                    </div>
                    <div class="form-field--param">
                        <div class="form-field-input form-field-input-price">
                            <input id="field_price" name="field[price]" type="text" data-mask="int" class="form-field-input-tag" value="<?=$arResult['ELEMENT']['PRICE'][key($arResult['ELEMENT']['PRICE'])]['PRICE'];?>"/>
                        </div>
                        <div class="form-field--price">a</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-field">
                    <div class="form-field--label">
                        Фотографии
                        <span class="form-field--important"></span>:
                    </div>
                    <div class="form-field--param">
                        <?
                        if($arResult['ELEMENT']['FILES']):
                            foreach($arResult['ELEMENT']['FILES'] as $fileID=>$src):
                            ?>
                            <div class="files-list">
                                <div class="files-item">
                                    <a href="<?=$src;?>" class="files-item-link">
                                        <img src="<?=$src;?>" class="files-item-image">
                                    </a>
                                    <input type="hidden" name="field[files][]" value="<?=$fileID;?>">
                                </div>
                            </div>
                            <?
                            endforeach;
                        endif;
                        ?>
                        <div class="files-input">
                            <div class="files-list" id="files"></div>
                            <label class="fileinput-button">
                                <span>Добавить фотографию</span>
                                <input id="fileupload" type="file" name="files[]" multiple="" />
                            </label>
                            <div class="files-clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-field">
                    <div class="form-field--label">
                        Описание
                        <span class="form-field--important"></span>:
                    </div>
                    <div class="form-field--param">
                        <div class="form-field-textarea">
                            <textarea id="field_text" name="field[text]" cols="30" rows="10" class="form-field-textarea-tag"><?=$arResult['ELEMENT']['TEXT'];?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="section-property"></div>
        <div id="section-contact" <?if(!$arResult["CURRENT_TYPE"]):?>class="section-disable"<?endif;?>>
            <div class="form-group">
                <div class="title office-title">Контакты</div>
            </div>
            <div class="form-group">
                <div class="form-field">
                    <div class="form-field--label">Телефон<span class="form-field--important"></span>:</div>
                    <div class="form-field--param">
                        <?
                        if($arResult['ELEMENT']['CONTACTS']['PHONE']):
                            foreach($arResult['ELEMENT']['CONTACTS']['PHONE'] as $phoneID=>$phone):
                                $phone = AdsBase::formatPhone($phone);
                                ?>
                                <div class="form-field-input form-field-input--min-line">
                                    <input id="field_phone" name="field[phone][<?=$phoneID;?>]" type="text" class="input-tag" data-mask="phone" value="<?=$phone;?>">
                                </div>
                                <?
                            endforeach;
                        else:
                            ?>
                            <div class="form-field-input form-field-input--min-line">
                                <input id="field_phone" name="field[phone][]" type="text" class="input-tag" data-mask="phone">
                            </div>
                            <?
                        endif;
                        ?>
    <!--                    <a href="#" class="add-phone">Добавить телефон</a>-->
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-field">
                    <div class="form-field--label">E-Mail<span class="form-field--important"></span>:</div>
                    <div class="form-field--param">
                        <?
                        if($arResult['ELEMENT']['CONTACTS']['EMAIL']):
                            foreach($arResult['ELEMENT']['CONTACTS']['EMAIL'] as $emailID=>$email):
                                ?>
                                <div class="form-field-input form-field-input--min-line">
                                    <input id="field_email" name="field[email][<?=$emailID;?>]" type="text" class="input-tag" data-mask="email" value="<?=$email;?>">
                                </div>
                                <?
                            endforeach;
                        else:
                            ?>
                            <div class="form-field-input form-field-input--min-line">
                                <input id="field_email" name="field[email][]" type="text" class="input-tag" data-mask="email">
                            </div>
                            <?
                        endif;
                        ?>
                    </div>
                    <div class="form-field--param form-field--param-up">
                        <?
                        $arConf = array(
                            'id'=>'field[email][notice]',
                            'type' => 'boolean',
                            'title' => 'Не присылать уведомления от покупателей',
                            'currentValue' => false
                        );
                        echo AdsDomElement::get($arConf);
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-field">
                    <div class="form-field--label">Адрес:</div>
                    <div class="form-field--param">
                        <div class="form-field-input form-field-input-map">
                            <input data-search="maps-location" name="field[location][name]" type="text" id="form-field-input-map" class="form-field-input-tag input-tag" value=""/>
                        </div>
                        <div id="maps-location" data-kart="maps-location" style="width:100%; height: 320px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="section-save"  <?if(!$arResult["CURRENT_TYPE"]):?>class="section-disable"<?endif;?>>
            <div class="form-group">
                <button name="submit" value="send" class="form-button"><?=$title;?></button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
$(function () {
    $('#maps-location').mapsAdd({
        'latMaps' : '<?=$arResult['LAT'];?>',
        'lngMaps' : '<?=$arResult['LNG'];?>',
        'markerShow' : true,
        'nameMarkerLat' : 'field[location][lat]',
        'nameMarkerLng' : 'field[location][lng]',
        'searchHint' : true,
    });

    var url = '/api/1.0/UploadHandler/';
    $('#fileupload').fileupload({
        dataType: 'json',
        multipart: true,
        sequentialUploads: true,
        previewMaxWidth: 200,
        previewMaxHeight: 160,
        url: url,
        add: function (e, data) {
            var loading = $('<div/>');
            var item = $('<div/>');

            loading.addClass('cssload-loader');
            item.addClass('files-item');

            loading.html('<div class="cssload-inner cssload-one"></div><div class="cssload-inner cssload-two"></div><div class="cssload-inner cssload-three"></div>');

            loading.appendTo(item);
            item.appendTo('#files');

            data.submit();
        },
        done: function (e, data) {
            var result = data.result.result;
            $.each(result, function (index, file) {
                var items = $('#files .files-item');
                var lastItem = items.get(items.length - 1);
                // TODO Добавить поворот и удаление
                /*
                '<!--span class="files-item-dell">dell</span-->' +
                '<!--span class="files-item-rotate">rotate</span-->' +
                */
                $(lastItem).html('' +
                    '<a class="files-item-link" href="' + file.src + '">' +
                    '<img class="files-item-image" src="' + file.url + '"/>' +
                    '</a>' +
                    '<input type="hidden" name="field[files][]" value="' + file.id + '"/>');
            });
        },
        progressall: function (e, data) {

        }
    });
    $('body').on('click', '.files-item-rotate', function(){
        var parent = $(this).parent(),
            imageID = parent.find('input').val();


    });

    var dElementAdd = new window.DElementAdd();
    dElementAdd.init();
});
</script>