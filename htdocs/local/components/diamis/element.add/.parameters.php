<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();



$arComponentParameters = array(
    "GROUPS" => Array(
        "COMMENT" => array("NAME" => 'test'),
    ),
    "PARAMETERS" => array(
        "ID" => array(
            "NAME" => "Переменная содержащая ID объявления",
            "DEFAULT" => '$_REQUEST["ID"]',
        ),
        "SEF_MODE" => array(),
        "SEF_TYPE" => array(
            "NAME" => 'Страница добавления элемента',
            "TYPE" => 'list',
            "DEFAULT" => "#ELEMENT_ID#",
            "VALUES" => array(
                "ELEMENT_ID" => 'ID Элемента',
                "ELEMENT_CODE" => 'CODE Элемента',
            ),
        )
    )
);