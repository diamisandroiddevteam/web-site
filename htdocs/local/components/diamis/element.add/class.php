<?
use Bitrix\Main;
use \Bitrix\Main\Loader;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!Loader::includeModule('diamis.ads')) return;

class Element extends \CBitrixComponent
{

    public $reques = array(
    );


    /**
     *
     */
    public function getParentCategory($items) {
        $i = 0;
        $result = array();
        while($i<count($items))
        {
            if($items[$i]['ID']==$this->arResult['CURRENT_CATEGORY']['PARENT_ID']){
                $result = $items[$i];
                break;
            }
            $i ++;
        }

        return $result;
    }





    public function getFieldContact()
    {
        return array(
            array(
                'code' => 'phone',
                'id' => 'field[phone][]',
                'title' => 'Телефон',
                'group' => 'contact',
                'required' => true,
                'type' => 'number',
                'currentValue' => ''
            ),
            array(
                'code' => 'email',
                'id' => 'field[email][]',
                'title' => 'E-Mail',
                'group' => 'contact',
                'required' => true,
                'type' => 'number',
                'currentValue' => ''
            ),
            array(
                'code' => 'location',
                'id' => 'field[location]',
                'title' => 'Адрес',
                'group' => 'contact',
                'required' => false,
                'type' => 'number',
                'currentValue' => ''
            )
        );
    }

    /*
     * Формируем сассив поля Категория 2-го уровня для передачи его в javaScript
     */
    public function getFieldCategory($items, $parentID, $current = array())
    {
        $result = array();
        $disable = count($items) ? false : true;

//        $result = $this->getArrayFields($items);
        foreach($items as $item)
        {
            if($item['PARENT']==$parentID)
            {
                $result[] = array(
                    'id' => 'category_' . $item['ID'],
                    'name' => $item['NAME'],
                    'value' => $item['ID']
                );
            }
        }

        return array(
            'id'            => 'field[category]',
            'title'         => 'Категория',
            'group'         => 'main',
            'required'      => true,
            'updatesForm'   => true,
            'inDisable'     => $disable,
            'currentValue'  => $current,
            'type'          => 'select',
            'values'        => $result
        );
    }


    /*
     * Формируем массив поля Тип для передачи его в javaScript
     */
    public function getFieldType($current, $currentid=null)
    {
        $disable = count($this->arResult['CATEGORY_TYPE']) ? false : true;
        $result =  array(
            'id'            => 'field[type]',
            'type'          => 'select',
            'group'         => 'main',
            'required'      => true,
            'updatesForm'   => true,
            'inDisable'     => $disable,
            'values'        => $this->getArrayFields($this->arResult['CATEGORY_TYPE'])
        );

        if($currentid) $result['currentValue'] = $currentid;

        return $result;
    }



    public function getFieldProp($items)
    {
        $multiType = array(
            'select' => 'multiselect',
            'radio'  => 'checked',
        );

        $result = array();

        $key = 0;
        foreach($items as $item)
        {
            $type =  $item['FIELD_TYPE'];
            if($item['FIELD_MULTY'] && $multiType[$item['FIELD_TYPE']])
                $type = $multiType[$item['FIELD_TYPE']];


            $result[$key] = array(
                'id'            => 'field['.$item['FIELD_ID'].']',
                'type'          => $type,
                'title'         => $item['FIELD_NAME'],
                'group'         => 'prop',
                'hint'          => $item['FIELD_HINT'],
                'required'      => $item['REQUIRED']
            );

            if($item['FIELD_TYPE']=='select'
            || $item['FIELD_TYPE']=='slider'
            || $item['FIELD_TYPE']=='checked'
            ) {
                $result[$key]['values'] = $this->getArrayFields($item['FIELD_VALUES']);
            }
            $key++;
        }


        return $result;
    }



    public function getArrayFields($items){

        $result = array();
        foreach($items as $item)
        {
            $result[] =  array(
                'id' => 'type_'.$item['ID'],
                'name' => $item['NAME'],
                'value' => $item['VALUE']
            );
        }
        return $result;
    }
}