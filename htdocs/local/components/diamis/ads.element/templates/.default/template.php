<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs($templateFolder.'/sliderPro/jquery.sliderPro.min.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/libs/d-review.js");


$element = $arResult['ELEMENT'];
$image = null;
$arSlider = array();
foreach($element['FILES'] as $file){
    if(!$image) $image = ($_SERVER['HTTPS'] ? 'https' : 'http' ).'://'.$_SERVER['HTTP_HOST'].$file;
    $arSlider[0] .= "<div class='sp-slide'><img class='sp-image' src='".$file."' /></div>";
    $arSlider[1] .= "<img class='sp-thumbnail' src='".$file."' />";
}


$date = $element['DATE_CREATE']->format("d.m.Y");
if($date==date('d.m.Y')){
    $date = 'Сегодня в ' . $element['DATE_CREATE']->format("h:i");
}elseif($date==date('d.m.Y', strtotime('last day'))){
    $date = 'Вчера в ' . $element['DATE_CREATE']->format("h:i");
}



$loc = $element['LOCATION'][key($element['LOCATION'])];
$element['DISTANCE'] = Diamis\Ads\Base::getDistance($loc['LAT'], $loc['LNG'], $GLOBALS['FILTER_LAT'], $GLOBALS['FILTER_LNG']);


$distance = 0;
$distPrefix = 'м';
$exDistance = explode(',',$element['DISTANCE']);
if(!$exDistance[0]) {
    $distPrefix = 'м';
    $distance = $exDistance[1];
}else {
    $distPrefix = 'км';
    $distance = $exDistance[0];
}

$favorite = '';
if(array_search($element['ID'], $arParams['FAVORITES'])!==false
    && array_search($element['ID'], $arParams['FAVORITES'])!==null)
    $favorite = ' favorite-active';


$location = $element['LOCATION'][key($element['LOCATION'])];
$lat = $location['LAT'];
$lng = $location['LNG'];

?>
<div class="section">
    <div class="content">
        <div class="crumbs">
            <?
            foreach($arResult['CRUMBS'] as $key=>$item):
                ?>
                <div class="crumbs-item">
                    <a href="<?=$item['link'];?>" class="crumbs-link"><?=$item['name'];?></a>
                </div>
                <?
                if($key<count($arResult['CRUMBS'])-1):
                    ?>
                    <div class="crumbs-item"><span>/</span></div>
                    <?
                endif;
            endforeach;
            ?>
        </div>
        <div class="product-card">
            <div class="product-card-main">
                <div class="product-card-title"><?=$element['NAME'];?></div>
                <div class="product-card-price">
                    <span class="product-cprice">12 000</span>
                    <span class="product-cprice-icon">a</span>
                </div>
                <div id="slider-carousel" class="slider-pro">
                    <div class="slider-arrow">
                        <div class="slider-arrow-up"></div>
                        <div class="slider-arrow-down"></div>
                    </div>
                    <div class="sp-slides">
                        <div class="sp-slides"><?=$arSlider[0];?></div>
                        <div class="sp-thumbnails"><?=$arSlider[1];?></div>
                    </div>
                </div>
                <div class="product-card-stat">
                    <div class="product-cstat-label">Размещено: <?=$date;?></div>
                    <div class="product-cstat-label">
                        <span class="product-cstat-update"></span>
                        <?=$element['DATE_UPDATE']->format("d.m.Y");?>
                    </div>
                    <div class="product-cstat-label">
                        <span class="product-cstat-view"></span>
                        <?=$element['VIEW'];?>
                    </div>
                </div>
            </div>
            <script>
            $(function(){
                var dslider = new window.DSlider();
                dslider.init('slider-carousel');
            });
            </script>

            <div class="product-card-tools">
                <div class="product-card-favorite js-favorite<?if($arResult['FAVORITES']):?> favorite-active<?endif;?>" data-favorite="<?=$element['ID'];?>">
                    <span></span>
                    Добавить в избранное
                </div>
                <div class="product-card-print">
                    <span></span>
                    Печать
                </div>
                <div class="user-card">
                    <div class="user-card-info">
                        <div class="user-cinfo-image"></div>
                        <div class="user-cinfo-wrap">
                            <div class="user-cinfo-title">Продавец 1</div>
                            <div class="user-cinfo-phone js-show-contact" data-id="<?=$element['ID'];?>" data-type="phone">Показать телефон</div>
                        </div>
                        <div class="user-card-rating">
                            <div class="user-crating-label">Рейтинг: 4.5</div>
                            <div class="user-crating-label">Проголосовало: 200 чел.</div>
                        </div>
                    </div>
                    <a href="#" class="user-card-count">Все объявления продавца (3)</a>
                    <div class="user-card-btn">
                        <a href="/profile/message/<?=$element['ID'];?>/" class="btn btn-radius btn-blue">Написать продавцу</a>
<!--                        <div class="btn btn-radius btn-rempty">Бартер</div>-->
                    </div>
                </div>
                <div class="user-card-share">
                    <?$APPLICATION->IncludeComponent(
                            "diamis:auth.share",
                        "",
                        Array(
                            "HANDLERS" => array("vk","fb",'google',"twitter",'ok'),
                            "PAGE_TITLE" => $element['NAME'],
                            "PAGE_DESCRIPTION" => $element['TEXT'],
                            "PAGE_IMAGE" => $image,
                        )
                    );?>
                    <?
                    // TODO Реализовать функцианала подписаться и пожаловаться
                    /*
                    <div class="product-card-subscribe">Подписаться на похожие объявления</div>
                    <div class="product-card-complain">Пожаловаться</div>
                    */
                    ?>
                </div>
            </div>
        </div>
        <div class="product-card-info">
            <div class="product-cinfo-property">
                <span class="product-property-filed">Категория</span>
                <span class="product-property-value"><?=$element['CATEGORY_NAME'];?></span>
            </div>
            <div class="product-cart-text"><?=$element['~TEXT'];?></div>
        </div>
        <div class="product-map">
            <div class="product-map-title">Местонахождение на карте</div>
            <div class="product-map-wrap">
                <div id="element-map" style="width:100%; height: 300px;"></div>
            </div>
            <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
            <script type="text/javascript">
                $('#element-map').mapsAdd({
                    'latMaps' : '<?=$lat;?>',
                    'lngMaps' : '<?=$lng;?>',
                    'markerDraggable': false
                });
            </script>
        </div>
        <div class="product-tabs">
            <div class="product-tabs-item active" data-tabs="tabs-property">Параметры объявления</div>
            <div class="product-tabs-item" data-tabs="tabs-form">Отзывы</div>
        </div>
        <div class="product-tabs-table">
            <div id="tabs-property" class="product-tabs-table-item">
                <?foreach($element['PROPERTY'] as $item):?>
                <div class="product-property">
                    <span class="product-property-filed"><?=$item['NAME'];?>: </span>
                    <span class="product-property-value"><?=$item['VALUE'];?></span>
                </div>
                <?endforeach;?>
            </div>
            <div id="tabs-form" class="product-tabs-table-item">

            </div>
        </div>
        <?
        global $arFilterSimilar;

        $arFilterSimilar = array(
            'select' => array(
                'PRICE_PRICE' => 'PRICE.PRICE'
            ),
            'filter' => array(
                '!ID' => $element['ID'],
                'CATEGORY_ID' => $element['CATEGORY_ID'],
                'TYPE_ID' => $element['TYPE_ID'],
//                '<=DISTANCE' => 300,
//                '>=PRICE_PRICE' => ($element['PRICE'][key($element['PRICE'])]['PRICE'] + $element['PRICE'][key($element['PRICE'])]['PRICE'] * .15),
//                '<=PRICE_PRICE' => ($element['PRICE'][key($element['PRICE'])]['PRICE'] - $element['PRICE'][key($element['PRICE'])]['PRICE'] * .10)
            )
        );

        $APPLICATION->IncludeComponent(
            "diamis:ads.sections",
            "similar",
            array(
                'FILTER_NAME' => 'arFilterSimilar',
                'PAGE_SIZE' => 4
            )
        );?>
    </div>
</div>
<script>
$(function(){
    var contactShoe = new window.DShowElementContact();
    contactShoe.init('js-show-contact');

    if(!!window.DReview) {
        var dreview = new window.DReview('/review/ads/<?=$element['ID'];?>/', $('#tabs-form'));
        $('.product-tabs-item').click(function(){
            var tab = $(this).data('tabs');

            $('.product-tabs-item').removeClass('active');
            $('.product-tabs-table-item').css('display', 'none');

            $(this).addClass('active');
            $('#' + tab).css('display','block');

            if(tab==='tabs-form') dreview.init();
        });
    }
});
</script>