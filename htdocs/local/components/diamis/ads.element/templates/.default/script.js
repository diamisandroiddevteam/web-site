(function(window){

    // Подключение слайдера
    window.DSlider = function (){
        this.el = null;
        this.up = null;
        this.down = null;
        this.slider = null;
    };

    window.DSlider.prototype.init = function(id){
        this.el = $('#'+id);
        if(this.el)
        {
            this.up = this.el.find('.slider-arrow-up');
            this.down = this.el.find('.slider-arrow-down');
            this.start();
        }
    };


    window.DSlider.prototype.start = function(){
        this.slider = this.el.sliderPro({
            arrows: true,
            autoplay: false,
            autoScaleLayers: false,
            width: '100%',
            height: 400,
            thumbnailArrows: false,
            thumbnailWidth: 170,
            thumbnailsPosition: 'right',
            breakpoints: {
                768: {
                    thumbnailsPosition: 'bottom',
                    thumbnailWidth: 150,
                    thumbnailHeight: 100
                }
            },
        });

        var width = $(window).width(),
            slider = this.el.data('sliderPro');

        if(width<768) {
            this.slider.sliderPro('thumbnailsPosition','bottom');
            $(window).resize(()=>{
                let w =  $(window).width();
                if(w>=768) this.slider.sliderPro('thumbnailsPosition','right');
            });
        }

        this.up.on('click', ()=>{
            slider.previousSlide();
        });

        this.down.on('click', ()=>{
            slider.nextSlide();
        });
    };




    // Вывод контакта в виде картинки
    window.DShowElementContact = function(){
        this.el = null;
    };

    window.DShowElementContact.prototype.init = function(className) {
        this.el = $('.'+className);
        this.el.on('click', function(){
            var el = $(this),
                id = $(this).data('id'),
                type = $(this).data('type');

            if(id) {
                $.ajax({
                    url: window.location.href,
                    method: 'post',
                    data: {
                        id: id,
                        ajax: 'y',
                        type: type
                    },
                    dataType: 'json',
                    success: function(result){
                        el.html('<img src="'+result+'" />');
                        console.log(result);
                    }
                })
            }
            return false;
        });
    }


    // Отобразить блок отзывы
})(window);