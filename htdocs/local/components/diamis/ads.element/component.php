<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


use \Bitrix\Main\Loader;

use \Diamis\Ads\Base as AdsBase;
use \Diamis\Ads\CategoryTable;
use \Diamis\Ads\TypeTable;
use \Diamis\Ads\AdsTable;
use \Diamis\Ads\FavoritesTable;


if (!Loader::includeModule('diamis.ads')) return;

global $DB, $USER, $APPLICATION;


$ajax    = $_REQUEST['ajax']=='y' ? true: false;
$region_code    = strip_tags($_REQUEST['REGION_CODE']);

$error404 = false;
$errorMessage = null;

$ID = intval($arParams['ID']);

if(!$ID || !$element = AdsTable::getElementID($ID)) {
    $error404 = true;
    $errorMessage[] = "Элемент не найден";
}

$arResult['TITLE'] = $element['NAME'];
$arResult['ELEMENT'] = $element;


$requestURL = Bitrix\Main\Context::getCurrent()->getRequest()->getRequestedPage();
$requestURL = str_replace('index.php', '', $requestURL);


$arResult['URL'] = $requestURL;

// Проверяем на существование данного региона
if(!isset($GLOBALS['CITY'][$region_code])){
    $error404 = true;
    $errorMessage[] = "Не верно указан город.";
};

if($ajax)
{
    $APPLICATION->RestartBuffer();
    switch ($_REQUEST['type'])
    {
        case 'phone':
            $im = imagecreatetruecolor(160, 20);
            $text = AdsBase::formatPhone($element['CONTACTS']['PHONE'][key($element['CONTACTS']['PHONE'])]);

            $font  = imageloadfont($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH. '/fonts/Roboto-Bold.ttf');
            $black = imagecolorallocate($im, 0, 0, 0);
            imagecolortransparent($im, $black);

            $text_color = imagecolorallocate($im, 51, 51, 51);

            imagestring($im, ($font ? $font : 4), 0, 0, $text , $text_color);

            ob_start();
            imagepng($im);
            $outputBuffer = ob_get_contents();
            ob_clean();

            $base64Image = base64_encode($outputBuffer);
            imagedestroy($im);

            echo json_encode('data:image/jpg;base64,'.$base64Image);
            break;
    }
    die();
}




// хлебные крошки
$link = array();
if(isset($GLOBALS['CITY'][$region_code])) {
    $link[] = $GLOBALS['CITY'][$region_code]['CITY_CODE'];
    $arResult['CRUMBS'][] = array(
        'name' => $GLOBALS['CITY'][$region_code]['CITY'],
        'code' => $GLOBALS['CITY'][$region_code]['CITY_CODE'],
        'link' => '/'.implode('/', $link).'/'
    );
}
if($arResult['ELEMENT']['CATEGORY_CODE']) {
    $link[] = $arResult['ELEMENT']['CATEGORY_CODE'];
    $arResult['CRUMBS'][] = array(
        'name' => $arResult['ELEMENT']['CATEGORY_NAME'],
        'code' => $arResult['ELEMENT']['CATEGORY_CODE'],
        'link' => '/'.implode('/', $link).'/'
    );
}
if($arResult['ELEMENT']['TYPE_CODE']) {
    $link[] = $arResult['ELEMENT']['TYPE_CODE'];
    $arResult['CRUMBS'][] = array(
        'name' => $arResult['ELEMENT']['TYPE_NAME'],
        'code' => $arResult['ELEMENT']['TYPE_CODE'],
        'link' => '/'.implode('/', $link).'/'
    );
}


$view = intval($arResult['ELEMENT']['VIEW'])+1;
AdsTable::update($ID, array('VIEW'=>$view));


// Получаем список избранного
if($USER->IsAuthorized()):

    $sql = "SELECT `ELEMENT_ID` FROM `".FavoritesTable::getTableName()."` WHERE `ELEMENT_ID`='".$ID."' AND `USER_ID`='".$USER->GetID()."'";
    $results = $DB->Query($sql);
    $item = $results->Fetch();
    $arResult['FAVORITES'] = $item['ELEMENT_ID'];

endif;



$this->IncludeComponentTemplate();