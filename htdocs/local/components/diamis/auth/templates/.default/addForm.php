<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();


if(!$arResult['AJAX']):
    ?><div class="box-shadow box-shadow--max"><?
endif;
?>
    <form id="form-reg" action="<?=Bitrix\Main\Context::getCurrent()->getRequest()->getRequestedPage();?>" data-form="ajax" method="post">
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="ajax" value="<?=$arResult['AJAX'];?>" >
        <input type="hidden" name="type" value="<?=$arResult['TYPE'];?>" >
        <input type="hidden" name="backurl" value="<?=$arResult['BACK_URL'];?>" >
        <input type="hidden" name="form" value="reg" >
        <div class="scroll_item__title">Регистрация</div>
        <div class="form-error-message">
            <?if(!$arResult['RESULT']['status']):
                echo $arResult['RESULT']['message'];
            endif;?>
        </div>
        <div class="form-true-message"></div>
        <div class="scroll_item__row">
            <div class="input">
                <div class="input-placeholder">Ваше Имя</div>
                <input name="NAME" type="text" class="input-tag">
            </div>
        </div>
        <div class="scroll_item__row">
            <div class="input">
                <div class="input-placeholder">Ваш E-Mail</div>
                <input name="EMAIL" type="text" data-required="notnull" class="input-tag">
            </div>
        </div>
        <div class="scroll_item__row">
            <div class="input">
                <div class="input-placeholder">Ваш Телефон</div>
                <input name="PHONE" type="text" data-required="notnull" data-mask="phone" class="input-tag">
            </div>
        </div>
        <div class="scroll_item__row">
            <div class="input">
                <div class="input-placeholder">Пароль</div>
                <input name="PASS[0]" type="password" data-required="notnull" class="input-tag">
            </div>
        </div>
        <div class="scroll_item__row">
            <div class="input">
                <div class="input-placeholder">Повторите Пароль</div>
                <input name="PASS[1]" type="password" data-required="notnull" class="input-tag">
            </div>
        </div>
        <div class="scroll_item__row" style="text-align: center;">
            <input type="submit" name="submit" class="btn btn-rempty" value="Зарегистрироваться" />
        </div>
        <div class="scroll_item__row">
            <div class="social social_entry">
                <div class="social_header">или</div>
                <?php
                if($arResult['SOCIAL_AUTH']):
                    $APPLICATION->IncludeComponent(
                        "diamis:auth.social",
                        ".default",
                        array(
                            'BACKURL' => $arParams['SOCIAL_BACK_URL']
                        )
                    );
                endif;
                ?>
                <div class="social_footer">Нажимая "Зарегистрироваться" или выбирая социальную сеть. Вы принимаете условия
                    <a href="#">пользовательского соглашения</a></div>
            </div>
        </div>
        <div class="scroll_item__row">
            <div class="scroll__center">
                <a href="<?=$arResult['URL_AUTH'];?>" data-scroll-target="entry" class="scroll_link scroll_link__big">Авторизация</a>
            </div>
        </div>
    </form>
<?php
if(!$arResult['AJAX']):
    ?></div><?
endif;