<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();
?>
<div class="window">
    <div class="window_content">
        <div data-scroll="scroll_item" class="scroll">
            <?
            $show = false;
            foreach($arResult['FORM'] as $key=>$form):
                ?>
                <div data-scroll-id="<?=$key;?>" class="scroll_item <?if(!$show): $show = true;?>show<?endif;?>">
                    <?= $form;?>
                </div>
                <?
            endforeach;
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    if(!!window.DScroll) {
        var dscroll = new window.DScroll();
        dscroll.init();

    }

    if(!!window.DForm) {
        new window.DForm('form-auth');
        new window.DForm('form-reg');
    }
</script>