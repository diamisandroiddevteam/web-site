<?php

if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();

$APPLICATION->IncludeComponent(
    "bitrix:system.auth.changepasswd",
    "",
    Array(
        "LOGIN" => $arParams["CONFIRM_LOGIN"],
        "USER_ID" => $arParams["CONFIRM_USER_ID"],
        "CONFIRM_CODE" => $arParams["CONFIRM_CODE"]
    )
);