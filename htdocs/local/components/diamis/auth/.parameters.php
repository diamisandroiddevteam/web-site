<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();



$arComponentParameters = array(
    "GROUPS" => Array(
        "COMMENT" => array("NAME" => 'test'),
    ),
    "PARAMETERS" => array(
        "SEF_MODE" => array(),
        "QUERY_AJAX" => [
            "NAME" => "Разрешить Ajax запросы",
            "TYPE" => "CHECKBOX",
            "PARENT" => "BASE",
            "DEFAULT" => '',
        ],
        "SOCIAL_AUTH" => [
            "NAME" => "Авторизация через соц.сети",
            "TYPE" => "CHECKBOX",
            "PARENT" => "BASE",
            "DEFAULT" => ""
        ],
        "SOCIAL_BACK_URL" => [
            "NAME" => "Редирект после авторизации",
            "TYPE" => "TEXT",
            "PARENT" => "BASE",
            "DEFAULT" => "/"
        ],
        "PAGE_FORGOT" => [
            "NAME" => "Страница забыли пароль",
            "TYPE" => "TEXT",
            "PARENT" => "BASE",
            "DEFAULT" => ""
        ],
        "PAGE_WELCOME" => [
            "NAME" => "Страница приветствия",
            "TYPE" => "TEXT",
            "PARENT" => "BASE",
            "DEFAULT" => ""
        ]
    )
);