<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();


global $USER, $APPLICATION;


$componentPage = 'authForm';
$type = $_REQUEST['type'] ? strip_tags($_REQUEST['type']) : null;

$ajax = false;
if(isset($_REQUEST['ajax']) && ($_REQUEST['ajax']==='y' || $_REQUEST['ajax']==1)) $ajax = true;


if(strlen($arParams["CONFIRM_CODE"]) <= 0)
    $arParams["CONFIRM_CODE"] = "confirm_code";


if(strlen($arParams["USER_ID"]) <= 0)
    $arParams["CONFIRM_USER_ID"] = "confirm_user_id";

if(strlen($arParams["LOGIN"]) <= 0)
    $arParams["CONFIRM_LOGIN"] = "login";


$arResult['SOCIAL_AUTH'] = $arParams['SOCIAL_AUTH']==='Y' ? true : false;
$arResult['URL_AUTH'] = '?type=authForm';
$arResult['URL_ADD'] = '?type=addForm';
$arResult['AJAX'] = $ajax;
$arResult['BACK_URL'] = $_REQUEST['backurl'] ? $_REQUEST['backurl'] : '/';


/**
 * Обрабатывем заполненые значения
 * веб формы
 */
if(isset($_REQUEST['form']) & $_REQUEST['form']!=='')
{

    $auth = false;

    $jsonData = array(
        'status' => false,
        'message' => null
    );


    // Авторизация пользователя
    if(isset($_REQUEST['LOGIN']) and isset($_REQUEST['PASS'])) {
        $login = trim($_REQUEST['LOGIN']);
        $password = $_REQUEST['PASS'];

        $isAuth = $USER->Login($login, $password);
        if($isAuth===true && !is_array($isAuth)) {
            $auth = true;
            $jsonData['status'] = true;
            $jsonData['redirect'] = $arResult['BACK_URL'];
        } else {
            $jsonData['status'] = false;
            $jsonData['message'] = "Логин или Пароль указан неверно.<br/>";
        }
    }
    $APPLICATION->RestartBuffer();

    if($arResult['AJAX']) {
        $APPLICATION->RestartBuffer();
        echo json_encode($jsonData);
        die();
    } else {
        $arResult['RESULT'] = $jsonData;
    }

    if($auth && !$ajax) LocalRedirect($arResult['BACK_URL']);
};



if($arParams['SEF_MODE']==='Y')
{

}


if($ajax)
{
    $APPLICATION->RestartBuffer();

    ob_start();
    include_once __DIR__ . "/templates/.default/addForm.php";
    $addForm = ob_get_contents();
    ob_clean();

    ob_start();
    include_once __DIR__ . "/templates/.default/authForm.php";
    $authForm = ob_get_contents();
    ob_clean();

    if($type==='addForm') $arResult['FORM'] = array('reg' => $addForm, 'entry' => $authForm);
    else $arResult['FORM'] = array('entry' => $authForm, 'reg' => $addForm);
}


if($type === 'forgot') $componentPage = 'forgotPassword';       // Форма восстановления пароля
if($type === 'addForm') $componentPage = 'addForm';     // Форма регистрации
if($type === 'authForm') $componentPage = 'authForm';   // Форма авторизации
if($_REQUEST['change_password']==='yes') $componentPage = 'changePassword';   // Форма авторизации
if($ajax && $type) $componentPage = 'ajaxForm';


$arResult['TYPE'] = $type;

$this->IncludeComponentTemplate($componentPage);