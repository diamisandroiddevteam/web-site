<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();
?>
<div class="office-container">
    <div class="title office-title">Мои сообщения</div>
    <div class="office-ctabs">
        <div class="office-ctabs-item active">Список сообщений</div>
        <div class="office-ctabs-item">Черный список</div>
    </div>
    <div class="office-message">
        <?
        foreach ($arResult['ITEMS'] as $item):
            ?>
            <a href="/profile/message/<?=$item['ID'];?>/" class="message">
                <div class="message-ads">
                    <div class="message-image">
                        <img src="<?=array_shift($item['FILES']);?>" alt="<?=$item['NAME'];?>">
                    </div>
                </div>
                <div class="message-user">
                    <div class="message-user-image">
                        <img src="<?=SITE_TEMPLATE_PATH;?>/images/no_user.jpg" alt="">
                    </div>
                    <div class="message-user-wrap">
                        <div class="message-user-title"></div>
                        <div class="message-user-title"><?=$item['NAME'];?></div>
                    </div>
                    <div class="message-user-content"><?=$item['TEXT'];?></div>
                </div>
                <div class="message-tools">

                </div>
            </a>
            <?
        endforeach;
        ?>
    </div>
</div>


