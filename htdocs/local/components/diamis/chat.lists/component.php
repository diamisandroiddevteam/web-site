<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


use \Bitrix\Main\Loader;
use \Diamis\Chat\Base as ChatBase;
use \Diamis\Ads\AdsTable;


if (!Loader::includeModule('diamis.chat')) return;


global $USER;

$UserID = $USER->GetID();
$arElements = array();
$arChat = ChatBase::getChat($UserID);

$arFilter = array();
foreach($arChat as $item) {
    $arFilter['ID'][] = $item['chat_entity_id'];
}

if(count($arFilter)) {

    $arElements = AdsTable::getElements($arFilter);
}

$arResult['ITEMS'] = $arElements;
$this->IncludeComponentTemplate();