<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Diamis\Ads\DomElement as AdsDomElement;
?>
<div class="section">
    <div class="content">
        <div class="crumbs">
            <?
            foreach($arResult['CRUMBS'] as $key=>$item):
                ?>
                <div class="crumbs-item">
                    <a href="<?=$item['link'];?>" class="crumbs-link"><?=$item['name'];?></a>
                </div>
                <?
                if($key<count($arResult['CRUMBS'])-1):
                    ?>
                    <div class="crumbs-item"><span>/</span></div>
                    <?
                endif;
            endforeach;
            ?>
        </div>
        <div class="title catalog-title"><?=$arResult['CATEGORY']['NAME'];?></div>
        <div class="catalog-count"></div>
    </div>
</div>
<div class="catalog">
    <div class="catalog-options">
        <div class="btn-tabs">
            <a href="#" class="btn-tabs-item" onclick="$('button[value=map]').click();">На карте</a>
            <a href="#" class="btn-tabs-item select">Список</a>
        </div>
        <? 
        $sort =  array(
            'id' => 'filter[sort]',
            'title' => 'Сортировать',
            'type'  => 'select',
            'formId' => 'filter',
            'currentValue' => $arResult['SORT'],
            'values' => array(
                array(
                    'name'  => 'Цена по убыванию',
                    'value' => 'price_down'
                ),
                array(
                    'name'  => 'Цена по возрастанию',
                    'value' => 'price_up'
                ),
                array(
                    'name'  => 'Недавно обновлены',
                    'value' => 'update'
                )
            )
         );
        echo AdsDomElement::get($sort);
        ?>
        <div class="btn-view btn-view-table">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="btn-view btn-view-list active">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <div class="catalog-main">
        <div class="catalog-list">
            <div class="catalog-content catalog-line">
                <?
                foreach($arResult['ITEMS'] as $item):

                    $APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH.'/include/element.php',
                        array(
                            'ELEMENT' => $item,
                            'FAVORITES' => $arResult['FAVORITES'],
                            'PREFIX_URL'=> '/'.$arResult['CITY']['CITY_CODE'],
                        )
                    );

                endforeach;
                ?>
            </div>
        </div>
    </div>

    <?
    $APPLICATION->IncludeComponent(
        "bitrix:main.pagenavigation",
        "",
        array(
            "NAV_OBJECT" => $arResult['NAV'],
            "SEF_MODE" => "N",
            "SHOW_COUNT" => "N",
        ),
        false
    );
    ?>

</div>