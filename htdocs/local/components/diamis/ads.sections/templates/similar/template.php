<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(count($arResult['ITEMS'])):
?>
<div class="product-promo promo-like">
    <div class="promo-like-title">Похожие объявления</div>
    <div class="promo-like-list">
        <div class="catalog-content catalog-table">
            <?
            foreach($arResult['ITEMS'] as $item):

                $APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH.'/include/element.php',
                    array(
                        'ELEMENT' => $item,
                        'FAVORITES' => $arResult['FAVORITES'],
                        'PREFIX_URL'=> '/'.$arResult['CITY']['CITY_CODE'],
                    )
                );

            endforeach;
            ?>
        </div>
    </div>
</div>
<? endif; ?>