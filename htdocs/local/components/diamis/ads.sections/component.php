<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


use \Bitrix\Main\Loader;

use \Diamis\Ads\Base as AdsBase;
use \Diamis\Ads\CategoryTable;
use \Diamis\Ads\TypeTable;
use \Diamis\Ads\AdsTable;
use \Diamis\Ads\FavoritesTable;


if (!Loader::includeModule('diamis.ads')) return;

global $DB, $USER, $APPLICATION;


$error404 = false;
$errorMessage = null;

$region_code    = strip_tags($_REQUEST['REGION_CODE']);
$section_code   = strip_tags($_REQUEST['SECTION_CODE']);
$type_code      = strip_tags($_REQUEST['TYPE_CODE']);
$element_code   = strip_tags($_REQUEST['ELEMENT_CODE']);


$filterName = $arParams['FILTER_NAME'] ? $arParams['FILTER_NAME'] : 'arCatalogFilter';

global $$filterName;



// Если не заданы парамерты фильтра
if(!count($$filterName)):
    // Проверяем на существование данного региона
    if(!isset($GLOBALS['CITY'][$region_code])){
        $error404 = true;
        $errorMessage[] = "Не верно указан город.";
    };


    $category = null;
    if($section_code && !$category = CategoryTable::getCategoryCode($section_code)) {
        $error404 = true;
        $errorMessage[] = "Категория указа не верно";
    }


    $type = null;
    if($type_code && !$type = TypeTable::getTypeCode($type_code)) {
        $error404 = true;
        $errorMessage[] = "Тип объявления указан не верно";
    }


    if($element_code) {
        $componentPage = 'element';
        $arElements = AdsTable::getElements(array(
                'CATEGORY_ID' => $category['ID'],
                'CODE'=>$element_code
            ),
            1
        );

        if(!count($arElements)) {
            $error404 = true;
            $errorMessage[] = "Страница с данным объявление не существует";
        }
    }

endif;

$arResult['CITY'] = $GLOBALS['CITY'][$region_code];
$arResult['CATEGORY'] = $$filterName['filter']['CATEGORY_ID'] ? $$filterName['filter']['CATEGORY_ID'] : $category;
$arResult['TYPE'] = $$filterName['filter']['TYPE_ID'] ? $$filterName['filter']['TYPE_ID'] : $type;
$arResult['PAGE_SIZE'] = ($pageSize = intval($arParams['PAGE_SIZE']) ? $pageSize : 10);


if($arResult['CATEGORY']) $arResult['CATEGORY'] = CategoryTable::getList(array('filter'=>array('ID'=> $arResult['CATEGORY']), 'limit'=>1))->fetch();
if($arResult['TYPE']) $arResult['TYPE'] = TypeTable::getList(array('filter'=>array('ID'=> $arResult['TYPE']), 'limit'=>1))->fetch();


// Получаем список избранного
if($USER->IsAuthorized()):

    $sql = "SELECT `ELEMENT_ID` FROM `".FavoritesTable::getTableName()."` WHERE `USER_ID`='".$USER->GetID()."'";
    $results = $DB->Query($sql);
    while($item = $results->Fetch()) {
        $arResult['FAVORITES'][] = $item['ELEMENT_ID'];
    }

endif;


// хлебные крошки
$link = array();
if(isset($GLOBALS['CITY'][$region_code])) {
    $link[] = $GLOBALS['CITY'][$region_code]['CITY_CODE'];
    $arResult['CRUMBS'][] = array(
        'name' => $GLOBALS['CITY'][$region_code]['CITY'],
        'code' => $GLOBALS['CITY'][$region_code]['CITY_CODE'],
        'link' => '/'.implode('/', $link).'/'
    );
}
if($arResult['CATEGORY']) {
    $link[] = $arResult['CATEGORY']['CODE'];
    $arResult['CRUMBS'][] = array(
        'name' => $arResult['CATEGORY']['NAME'],
        'code' => $arResult['CATEGORY']['CODE'],
        'link' => '/'.implode('/', $link).'/'
    );
}
if($arResult['TYPE']) {
    $link[] = $arResult['TYPE']['CODE'];
    $arResult['CRUMBS'][] = array(
        'name' => $arResult['TYPE']['NAME'],
        'code' => $arResult['TYPE']['CODE'],
        'link' => '/'.implode('/', $link).'/'
    );
}


$arOrder = array('DATE_UPDATE' => 'DESC');
$arSelect = array('ID');
$arFilter = array(
    'ACTIVE' => true
);


if($category['ID']) $arFilter['CATEGORY_ID'] = $category['ID'];
if($type['ID']) $arFilter['TYPE_ID'] = $type['ID'];



$arSelect = array_merge($arSelect, $$filterName['select']);
$arFilter = array_merge($arFilter, $$filterName['filter']);


$arResult['SORT'] = $_SESSION['ADS_CATALOG_SORT'] ? $_SESSION['ADS_CATALOG_SORT'] : 'update';
if($arFilter['sort']):
    $sort = $arFilter['sort'];
    $arResult['SORT'] = $sort[key($sort)];
    unset($arFilter['sort']);
endif;

switch ($arResult['SORT'])
{
    case 'price_down':
        $arSelect['PRICE_PRICE'] = 'PRICE.PRICE';
        $arOrder = array('PRICE_PRICE' => 'DESC');
        break;
    case 'price_up':
        $arSelect['PRICE_PRICE'] = 'PRICE.PRICE';
        $arOrder = array('PRICE_PRICE' => 'ASC');
        break;
}

$_SESSION['ADS_CATALOG_SORT'] = $arResult['SORT'];





// Постраничная навигация без COUNT
// Возможна только прямая навигация.
// Принцип работы: разработчик выбирает чуть больше записей, чтобы определить, возможно ли показать "далее",
// а в своем цикле по записям разработчик определяет границу цикла.
$nav = new \Bitrix\Main\UI\PageNavigation("page");
$nav->allowAllRecords(true)
    ->setPageSize($arResult['PAGE_SIZE'])
    ->initFromUri();

$newFilter = array_filter($arFilter, function($item){ return !empty($item); });
$arFilter = $newFilter;


// Проверяем был ли задан радиус
// и требуемые параметры для фильтрации
if($arFilter['<=DISTANCE']
    || $arFilter['>=DISTANCE']
    || $arFilter['<DISTANCE']
    || $arFilter['>DISTANCE']
    || $arFilter['<=DISTANCE']
) {

    if(empty($arSelect['LOCATION_LAT'])) $arSelect['LOCATION_LAT'] = 'LOCATION.LAT';
    if(empty($arSelect['LOCATION_LNG'])) $arSelect['LOCATION_LNG'] = 'LOCATION.LNG';

    $isDistance = false;
    foreach($arSelect as $item)
    {
        if(is_object($item) && $item->getName()!='DISTANCE')
        {
            $isDistance = true;
        };
    }

    if(!$isDistance) {
        if(empty($arFilter['LOCATION_LAT'])) $LOCATION_LAT = $GLOBALS['FILTER_LAT']; else $LOCATION_LAT = $arFilter['LOCATION_LAT'];
        if(empty($arFilter['LOCATION_LNG'])) $LOCATION_LNG = $GLOBALS['FILTER_LNG']; else $LOCATION_LNG = $arFilter['LOCATION_LNG'];

        $table = 'diamis_ads_ads_location';
        $arSelect[] = new \Bitrix\Main\Entity\ExpressionField(
            'DISTANCE',
            "( 6372.797 * acos( cos( radians('".$LOCATION_LAT."') ) * cos( radians( `".$table."`.`LAT` ) ) * cos( radians( `".$table."`.`LNG` ) - radians('".$LOCATION_LNG."') ) + sin( radians('".$LOCATION_LAT."') ) * sin( radians( `".$table."`.`LAT` ) ) ) )"
        );
    }
}




$dbAds = AdsTable::getList(array(
    'select' => $arSelect,
    'filter' => $arFilter,
    'order'  => $arOrder,
    'offset' => ($offset = $nav->getOffset()),
    'limit'  => (($limit = $nav->getLimit()) > 0 ? $limit + 1 : 0)
));

// Если Пусто то выбираем подкатегории или родителя
$newCategoryID = $arResult['CATEGORY']['PARENT'];
if($dbAds->getSelectedRowsCount()<=0) {

    $dbAds = null;
    $arCategoryID = array();

    // Если текущая категория родитель
    if($arResult['CATEGORY']['PARENT']==0)
    {
        $dbResCategory = CategoryTable::getList(array(
            'select'=> array('ID'),
            'filter'=> array(
                'PARENT' => $arResult['CATEGORY']['ID'],
                'ACTIVE' => true
            )
        ));

        while($item = $dbResCategory->fetch())
            $arCategoryID[] = $item['ID'];
    }
    else
    {
        $arCategoryID[] = $arResult['CATEGORY']['PARENT'];
    }

    $arFilter['CATEGORY_ID'] = $arCategoryID;
    $dbAds = AdsTable::getList(array(
        'select' => $arSelect,
        'filter' => $arFilter,
        'order'  => $arOrder,
        'offset' => ($offset = $nav->getOffset()),
        'limit'  => (($limit = $nav->getLimit()) > 0 ? $limit + 1 : 0)
    ));
}


$n = 0;
$arAdsId = array();
while($item = $dbAds->fetch())
{
    $n++;
    if($limit > 0 && $n > $limit) break;
    $arAdsId[$item['ID']] = $item['ID'];
}

sort($arAdsId);
$nav->setRecordCount($offset + $n);



$filter = array('ID' => $arAdsId);

// TODO Временно отключили
$arResult['ITEMS'] = AdsTable::getElements($filter, array(), true, $arResult['PAGE_SIZE'],0, $arOrder);

$arResult['NAV'] = $nav;



$this->IncludeComponentTemplate();