<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


use \Bitrix\Main\Loader;

use \Diamis\Ads\Base as AdsBase;
use \Diamis\Ads\CategoryTable;
use \Diamis\Ads\TypeTable;
use \Diamis\Ads\AdsTable;
use \Diamis\Ads\FavoritesTable;


if (!Loader::includeModule('diamis.ads')) return;

$componentPage = '';
$filterName = $arParams['FILTER_NAME'] ? $arParams['FILTER_NAME'] : 'arCatalogFilter';
global $$filterName;



$region_code    = strip_tags($_REQUEST['REGION_CODE']);
$section_code   = strip_tags($_REQUEST['SECTION_CODE']);
$type_code      = strip_tags($_REQUEST['TYPE_CODE']);
$element_code   = strip_tags($_REQUEST['ELEMENT_CODE']);


if(!empty($_REQUEST['TEMPLATE']) && $_REQUEST['TEMPLATE']==='MAP')
{
    $componentPage = 'map';
}
else if($element_code)
{

    $componentPage = 'element';
    $exElementCode = explode('_', $element_code);
    $elementID = intval($exElementCode[count($exElementCode)-1]);
    $arResult['ELEMENT_ID'] = $elementID;

}
$arResult['FILTER_NAME'] = $filterName;

$this->IncludeComponentTemplate($componentPage);