<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();


use \Bitrix\Main\Loader;
use \Diamis\Ads\Base as AdsBase;
use \Diamis\Ads\BillingScoreTable as AdsBillingScore;
use \Diamis\Ads\BillingTransactionTable as AdsBillingTransaction;
use \Diamis\Ads\BillingSettingsTable as AdsBillingSettings;
use \Diamis\Ads\PackagesTable as AdsPackages;
use \Diamis\Ads\PackagesServicesTable as AdsPackagesServices;
use \Diamis\Ads\DataServicesTable;

use YandexCheckout\Client;

if (!Loader::includeModule('diamis.ads')) return;
$componentPage = '';


global $USER, $APPLICATION;


$UserID = $USER->GetID();
$arPay = is_array($_REQUEST['pay']) ? $_REQUEST['pay'] : array();
$ScoreID = intval($_REQUEST['SCORE_ID']);
$ID = intval($_REQUEST['ID']) ? intval($_REQUEST['ID']) : null;

$arResult['ID'] = $ID;
$arResult["PACKAGE_ID"] = intval($arParams['PACKAGE_ID']);


$arResult['SCORE_ID'] = $ScoreID;
$arResult['ENTITY'] = $arParams['ENTITY'] ? strip_tags($arParams['ENTITY']) : array_keys(AdsBillingSettings::$entity)[0];

// Настройки платежной ситсемы
$arSettings = array();
$Settings = AdsBillingSettings::getList();
while($stg = $Settings->fetch()) {
    $arSettings[$stg['CODE']] = $stg['VALUE'];
}
$arResult['PAY_SETTINGS'] = $arSettings;


$arResult["USER_ID"] = $UserID;
$arResult["PAY"] = $arPay;
$arResult["ERRORS"]  = array();
$arResult["MESSAGE"] = array();


// Получем пакет и список выбранных услуг
$arPackageSettingsID = array();
$PackageID = $arResult["PACKAGE_ID"];
if($PackageID) {
    $arPackage = AdsPackages::getPackagesID($PackageID);
    foreach($arPackage['SETTINGS'] as $setting) $arPackageSettingsID[] = $setting['ID'];
}


// Выбранные услуги
$action = array();
$arServices = array();
$dbServices = AdsPackagesServices::getList(array('filter' => array( 'TYPE' => 'ads', 'ACTIVE' => true )));
while($service = $dbServices->fetch())
{
    if(!$action[$service['ACTION']])
    {
        if(array_search($service['ID'], $arPackageSettingsID)!==false) {
            $action[$service['ACTION']] = $service['ID'];
            $arResult['SERVICES'][] = $service;
        }
    }
    $arServices[] = $service;
}


// Дополнительные услуги
foreach($arServices as $service)
{
    if(!$action[$service['ACTION']])
    {
        $arResult['SERVICES_MORE'][] = $service;
    }
}



$arResult['PRICE'] = 1200;

//
if(AdsBillingScore::isScore($arResult['SCORE_ID'])):
    $componentPage = 'success';





elseif($_REQUEST['pay_send']):


    $arPay = $_REQUEST['PAY'];
    $arScoreData = array(
        'USER_ID'   => $arResult["USER_ID"],
        'TEXT'      => AdsBase::getUserIP(),
        'NAME'      => 'Разместить Объявление',
        'PRICE'     => $arResult['PRICE'],
        'TYPE'      => $arPay['TYPE'],
        'ENTITY'    => $arResult['ENTITY'],
        'ENTITY_ID' => $arResult['ID']
    );


    // Создаем счет
    $db = AdsBillingScore::add($arScoreData);
    if($db->isSuccess()) {

        $scoreID = $db->getId();

        foreach($arPay['SERVICES'] as $item) {
            $dataService = DataServicesTable::add(array(
                'ADS_ID' => $arResult['ID'],
                'SCORE_ID' => $scoreID,
                'SERVICE_ID' => $item,
                'USER_ID' => $USER->GetID()
            ));
        }

        foreach($arResult['SERVICES'] as $item) {
            $dataService = DataServicesTable::add(array(
                'ADS_ID' => $arResult['ID'],
                'SCORE_ID' => $scoreID,
                'SERVICE_ID' => $item['ID'],
                'USER_ID' => $USER->GetID()
            ));
        }


        $transaction = AdsBillingScore::createTransaction($scoreID); // создаем транзакцию

        // TODO Подключить яндекс кассу когда сайт будет на боевом хосте
/*
        $arrData = array_merge(
            AdsBillingScore::getYandexPrice($arScoreData['PRICE']),
            AdsBillingScore::getYandexMethod($arScoreData['TYPE']),
            AdsBillingScore::getYandexConfirmation($arScoreData['TYPE']));

        try{
            $client = new Client();
            $client->setAuth($arSettings['YandexID'], $arSettings['YandexMoney']);
            $client->capturePayment($arrData, $transaction['TRANSACTION_ID']);

        } catch (Exception $e) {
        }
*/
        if(count($transaction)) {
            $page = $APPLICATION->GetCurPage().$scoreID.'/';
            LocalRedirect($page);
        }
    }


endif;


$this->IncludeComponentTemplate($componentPage);