<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();


?>
<div class="ads-nav-container">
    <a href="/packages/<?=$arResult['ID'];?>/" class="ads-nav-container--back"></a>
    <span class="ads-nav-container--title">Ваше объявление успешно создано</span>
</div>
<div class="order">
    <div class="order-package-title">Выбранные Услуги:</div>
    <div class="order-items">
        <? foreach($arResult['SERVICES'] as $item): ?>
        <div class="order-item">
            <div class="order-item-info">
                <div class="order-item-title"><?=$item['NAME'];?></div>
                <div class="order-item-desc"><?=$item["TEXT"];?></div>
            </div>
        </div>
        <? endforeach;?>
    </div>
    <div class="order-result">
        <div class="order-result-price">
            Итого: <?=$arResult['PRICE'];?>
            <span>a</span>
        </div>
    </div>
</div>
<? if(count($arResult['SERVICES_MORE'])): ?>
<div class="order-more">
    <div class="order-package-title">Рекомендуем подключить:</div>
    <? foreach($arResult['SERVICES_MORE'] as $item): ?>
        <div class="order-item">
            <label class="input-checked">
                <input form="pay" class="input-checked--tag" name="PAY[SERVICES][]" type="checkbox" value="<?=$item['ID'];?>">
                <div class="input-checked--check"></div>
                <div class="input-checked--name order-item-info">
                    <div class="order-item-title"><?=$item['NAME'];?></div>
                    <div class="order-item-desc"><?=$item["TEXT"];?></div>
                </div>
            </label>
        </div>
    <? endforeach;?>
</div>
<? endif;?>
<div class="pay" <?if($arResult['PACKAGE_ID']===1):?>style="display: none;"<?else:?>style="display: block;"<?endif;?>>
    <div class="pay-title">Выберите способ оплаты:</div>
    <form id="pay" name="pay" method="post">
        <?=bitrix_sessid_post()?>
        <input name="ID" type="hidden" value="<?=$arResult['ID'];?>">
        <input name="PACKAGE_ID" type="hidden" value="<?=$arResult['PACKAGE_ID'];?>">
        <input name="pay_send" type="hidden" value="1">
        <div id="yes-pay" class="pay-items">
            <button name="PAY[TYPE]" value="bank_card" class="pay-item">
                <div class="pay-icon"><img src="<?=SITE_TEMPLATE_PATH;?>/images/master.svg"></div>
                <div class="pay-box">
                    <div class="pay-item-title">Банковские карты</div>
                </div>
            </button>
            <button name="PAY[TYPE]" value="sberbank" class="pay-item">
                <div class="pay-icon"><img src="<?=SITE_TEMPLATE_PATH;?>/images/sberbank.svg"></div>
                <div class="pay-box">
                    <div class="pay-item-title">Сбербанк Онлайн</div>
                </div>
            </button>
            <button name="PAY[TYPE]" value="yandex_money" class="pay-item">
                <div class="pay-icon"><img src="<?=SITE_TEMPLATE_PATH;?>/images/yandex-money.svg"></div>
                <div class="pay-box">
                    <div class="pay-item-title">Яндекс.Деньги</div>
                </div>
            </button>
            <button name="PAY[TYPE]" value="qiwi" class="pay-item">
                <div class="pay-icon"><img src="<?=SITE_TEMPLATE_PATH;?>/images/qiwi.svg"></div>
                <div class="pay-box">
                    <div class="pay-item-title">QIWI Кошелёл</div>
                </div>
            </button>
            <button name="PAY[TYPE]" value="webmoney" class="pay-item">
                <div class="pay-icon"><img src="<?=SITE_TEMPLATE_PATH;?>/images/webmoney.svg"></div>
                <div class="pay-box">
                    <div class="pay-item-title">WebMoney</div>
                </div>
            </button>
            <button name="PAY[TYPE]" value="mobile_balance" class="pay-item">
                <div class="pay-icon"><img src="<?=SITE_TEMPLATE_PATH;?>/images/sms.svg"></div>
                <div class="pay-box">
                    <div class="pay-item-title">SMS</div>
                    <div class="pay-item-desc">МТС, мегафон, Билайн, Tele2, Ростелеком, Yota</div>
                </div>
            </button>
            <button name="PAY[TYPE]" value="cash" class="pay-item">
                <div class="pay-icon"></div>
                <div class="pay-box">
                    <div class="pay-item-title">Оплата наличными</div>
                </div>
            </button>
        </div>
    </form>
</div>

<div class="free" <?if($arResult['PACKAGE_ID']===1):?>style="display: block;"<?else:?>style="display: none;"<?endif;?>>
    <div class="pay-title"></div>
    <form id="pay" name="pay" method="post">
        <?=bitrix_sessid_post()?>
        <input name="ID" type="hidden" value="<?=$arResult['ID'];?>">
        <input name="PACKAGE_ID" type="hidden" value="<?=$arResult['PACKAGE_ID'];?>">
        <input name="pay_send" type="hidden" value="1">
        <div id="no-pay" class="pay-items">
            <button name="PAY[TYPE]" value="free" class="pay-item">
                <div class="pay-icon"></div>
                <div class="pay-box">
                    <div class="pay-item-title">Отправить на модерацию</div>
                </div>
            </button>
        </div>
    </form>
</div>