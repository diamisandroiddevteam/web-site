<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();

?>
<div class="ads-nav-container">
    <a href="/" class="ads-nav-container--back"></a>
    <span class="ads-nav-container--title">Объявление отправлено на модерацию</span>
</div>