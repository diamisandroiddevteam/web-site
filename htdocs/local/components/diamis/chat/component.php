<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


use \Bitrix\Main\Loader;


if (!Loader::includeModule('diamis.chat')) return;

$ID = intval($_REQUEST['ID']) ? intval($_REQUEST['ID']) : null;


$componentPage = 'lists';
if($ID) {
    $componentPage = 'message';
}


$arResult['ID'] = $ID;

$this->IncludeComponentTemplate($componentPage);