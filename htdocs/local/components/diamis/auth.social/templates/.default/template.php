<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();

$arClass = array(
    'VKontakte' => 'CSocServVKontakte',
    'Odnoklassniki' => 'CSocServOdnoklassniki',
);

?>
<div class="social_items">
    <? foreach($arResult["DIAMIS_AUTH_SERVICES"] as $key=>$item):?>
        <div class="social_item">
            <? if($item['isAuth']):?>
                <a href="#" class="social_link <?=($arClass[$key] ? $arClass[$key] : '');?>"></a>
            <? else: ?>
                <a href="#" <?=$item['bx_onclick'];?> class="social_link <?=($arClass[$key] ? $arClass[$key] : '');?> <?if($arResult['COLOR']): echo 'social_link_no-color'; endif; ?>"></a>
            <? endif; ?>
        </div>
    <? endforeach;?>
    <?
/*
    foreach($arResult['AUTH_SERVICES'] as $arSocServices):
        if($arSocServices['ID']!=='VKontakte'
           && $arSocServices['ID']!=='Odnoklassniki'
        ):
        ?>
        <div class="social_item">
            <a href="#" onclick="<?=$arSocServices['ONCLICK'];?>" class="social_link <?=$arSocServices['CLASS'];?>"></a>
        </div>
        <?
        endif;
    endforeach;
*/
    ?>
</div>
