<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)
    die();

global $USER;

use \Bitrix\Main\Loader;
use Diamis\Auth\SocialManager;


if (!Loader::IncludeModule('socialservices')) return;
if (!Loader::includeModule('diamis.ads')) return;


if($_REQUEST["auth_service_id"])
    $arResult["CURRENT_SERVICE"] = $_REQUEST["auth_service_id"];

if(isset($arParams['BACKURL']))
    $arResult['BACKURL'] = trim($arParams['BACKURL']);


// Перехватываем обработчик соц сети
// Если имеется от Diamis
$soc = new SocialManager();
$arResult["DIAMIS_AUTH_SERVICES"] = $soc->GetActiveAuthServices();
if($auth_service_id = $_REQUEST['auth_service_id']) {
    $soc->Authorize($auth_service_id);
}

$oAuthManager = new CSocServAuthManager;
$arServices = $oAuthManager->GetActiveAuthServices($arResult);
$arResult["AUTH_SERVICES"] = $arServices;


if(isset($_REQUEST["auth_service_error"]) && $_REQUEST["auth_service_error"] <> '')
{
    $arResult['ERROR_MESSAGE'] = $oAuthManager->GetError($arResult["CURRENT_SERVICE"], $_REQUEST["auth_service_error"]);
}
elseif(!$oAuthManager->Authorize($_REQUEST["auth_service_id"]))
{
    $ex = $GLOBALS["APPLICATION"]->GetException();
    if ($ex)
        $arResult['ERROR_MESSAGE'] = $ex->GetString();
}


$arResult['COLOR'] = $arParams['COLOR']=='Y' ? true : false;


$this->IncludeComponentTemplate();