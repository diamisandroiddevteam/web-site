<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

?>
<div class="develop-box" style="width: 400px; margin:auto;">
    <?
    if($USER->IsAuthorized()):
        echo GetMessage("MAIN_REGISTER_AUTH");
    else:

            // Если вызвана ошибка
            if (count($arResult["ERRORS"]) > 0) {
                foreach ($arResult["ERRORS"] as $key => $error)
                {
                    if (intval($key) == 0 && $key !== 0 && $key !== 'LOGIN'){
                        $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
                    }
                    elseif($key==='LOGIN') {
                        $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_EMAIL")."&quot;", $error);
                    }
                }

                ShowError(implode("<br />", $arResult["ERRORS"]));
            } elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y") {
                // Информация об отправке письма
                echo str_replace('#FIELD', '', GetMessage("REGISTER_EMAIL_WILL_BE_SENT"));
            }
        ?>

        <form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
            <?
            if($arResult["BACKURL"] <> ''):
                ?><input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" /><?
            endif;
            ?>

            <div class="title"><?=GetMessage("AUTH_REGISTER")?></div>
            <?
            foreach ($arResult["SHOW_FIELDS"] as $FIELD):
                if( $FIELD!=='LOGIN' and
                    $FIELD!=='PASSWORD' and
                    $FIELD!=='CONFIRM_PASSWORD') :
                    switch($FIELD)
                    {
                        default:
                            ?>
                            <div>
                                <label for="FIELDS_<?=$FIELD;?>">
                                    <?=GetMessage("REGISTER_FIELD_".$FIELD)?>:<?if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"):?><span class="starrequired">*</span><?endif?>
                                </label>
                                <input id="FIELDS_<?=$FIELD;?>"
                                       size="30"
                                       type="text"
                                       name="REGISTER[<?=$FIELD?>]"
                                       value="<?=$arResult["VALUES"][$FIELD]?>" />
                            </div>
                            <?
                        break;
                    }
                endif;
            endforeach;

            /* CAPTCHA */
            if ($arResult["USE_CAPTCHA"] == "Y"):
                ?>
                <div><?=GetMessage("REGISTER_CAPTCHA_TITLE")?></div>
                <div>
                    <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                </div>
                <div><?=GetMessage("REGISTER_CAPTCHA_PROMT")?>:<span class="starrequired">*</span></div>
                <div><input type="text" name="captcha_word" maxlength="50" value="" /></div>
                <?
            endif;
            /* !CAPTCHA */

            ?>
            <p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
            <div>
                <label><?=GetMessage("REGISTER_FIELD_PASSWORD")?>:<span class="starrequired">*</span></label>
                <input size="30" type="password" name="REGISTER[PASSWORD]" value="<?=$arResult["VALUES"]['PASSWORD']?>" autocomplete="off" class="bx-auth-input" />
            </div>
            <div>
                <label><?=GetMessage("REGISTER_FIELD_CONFIRM_PASSWORD")?>:<span class="starrequired">*</span></label>
                <input size="30" type="password" name="REGISTER[CONFIRM_PASSWORD]" value="<?=$arResult["VALUES"]['CONFIRM_PASSWORD']?>" autocomplete="off" />
            </div>
            <div>
                <input type="submit" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" />
            </div>
        </form>
        <?
    endif;
    ?>
</div>