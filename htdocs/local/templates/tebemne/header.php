<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

IncludeTemplateLangFile(__FILE__);

use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;
use Diamis\Auth\SocialManager;
use Diamis\Ads\FavoritesTable;

Loader::includeModule('diamis.ads');

header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title><?$APPLICATION->ShowTitle()?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?=$APPLICATION->ShowTitle("title",true)?>" />
    <meta property="og:url" content="http://<?=$_SERVER['SERVER_NAME']?><?=$APPLICATION->GetCurPageParam("", Array("clear_cache","bitrix_include_areas"))?>" />
    <meta property="og:site_name" content="<?$arSite=$APPLICATION->GetSiteByDir();echo $arSite["NAME"]?>" />
    <?
    // css
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/styles.css");
    
    ?>
    <!--[if lt IE 9]>
    <script src="<?=SITE_TEMPLATE_PATH;?>/js/html5shiv.js" type='text/javascript'></script>
    <script src="<?=SITE_TEMPLATE_PATH;?>/js/css3-mediaqueries.js" type='text/javascript'></script>
    <script src="<?=SITE_TEMPLATE_PATH;?>/js/respond.js" type='text/javascript'></script>
    <![endif]-->
    <?

    // js
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-3.3.1.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-ui.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/nouislider.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.inputmask.bundle.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/slick.min.js");

    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/libs/d-html-form.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/libs/d-head-animate.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/libs/d-select.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/libs/d-scroll.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/libs/d-form.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/libs/d-dialog.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/libs/d-input.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/libs/d-view.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/libs/d-favorite.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/maps-add-yandex.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/entity.js");

    // show tags
    $APPLICATION->ShowHead();
    ?>
</head>
<body class="page fixedtop">
    <?$APPLICATION->ShowPanel()?>
    <nav class="globalnav">
        <div class="gn-content">
            <div class="flexbox">
                <div class="gn-left">
                    <a href="/" class="gn-link gn-link-logo"></a>
                </div>
                <div class="gn-right">
                    <ul class="gn-list">
                        <li class="gn-item gn-item-ads">
                            <a href="/create/" class="gn-link gn-link-ads">Подать объявление</a>
                            <span class="gn-icon"></span>
                        </li>
                        <li class="gn-item gn-item-location">
                            <a href="#" class="gn-link gn-link-location">
                                <?php
                                if($GLOBALS['CITY_IP']['CITY']):
                                    echo $GLOBALS['CITY_IP']['CITY'];
                                else:
                                    ?>Москва<?
                                endif;
                                ?>
                            </a>
                            <span class="gn-icon"></span>
                        </li>
                        <li class="gn-item gn-item-userbar">
                            <!-- noindex -->
                            <span class="gn-link gn-link-userbar">
                                <? if(!$USER->IsAuthorized()): ?>
                                    <span class="gn-icon"></span>
                                    <a href="/auth/?type=authForm&backurl=<?=$APPLICATION->GetCurPage();?>" data-dialog="reg" onclick="return false;" class="gn-link">Вход</a>
                                    /
                                    <a href="/auth/?type=addForm&backurl=<?=$APPLICATION->GetCurPage();?>" data-dialog="reg"  onclick="return false;" class="gn-link">Регистрация</a>
                                <? else: ?>
                                    <? $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/userbar.php", Array(), Array("MODE"=>"php"));?>
                                <? endif; ?>
                            </span>
                            <!-- /noindex -->
                        </li>
                        <li class="gn-item gn-item-message">
                            <a href="#" class="gn-link gn-link-message">
                                <span class="gn-number">2</span>
                                <span class="gn-icon"></span>
                            </a>
                        </li>
                        <li class="gn-item gn-item-favorite">
                            <!-- noindex -->
                            <a href="/favorites/" class="gn-link gn-link-favorite">
                                <? if($count = FavoritesTable::count()):?>
                                <span class="gn-number"><?=$count;?></span>
                                <? endif;?>
                                <span class="gn-icon"></span>
                            </a>
                            <!-- /noindex -->
                        </li>
                        <li class="gn-item gn-item-menu">
                            <a href="#" class="gn-link gn-link-menu">
                                <span class="gn-icon"></span>
                            </a>
                            <ul class="gn-userbar">
                                <li class="gn-userbar-item">
                                    <a href="#" class="gn-userbar-link">Контакты</a>
                                </li>
                                <li class="gn-userbar-item">
                                    <a href="#" class="gn-userbar-link">О компании</a>
                                </li>
                                <li class="gn-userbar-item">
                                    <a href="#" class="gn-userbar-link">Помощь</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="main">
