$(function(){

    if(!!window.DHeadAnimate)
    {
        var dhanimate = new window.DHeadAnimate();
        dhanimate.init();
    }

    // Требуется файл jquery-ui.min.js
    if(!!window.DDialog)
    {
        var ddialog = new window.DDialog();
        ddialog.init();
    }

    if(!!window.DSelect)
    {
        var dselect = new window.DSelect();
        dselect.init();
    }

    if(!!window.DView)
    {
        var dview = new window.DView();
        dview.init();
    }

    if(!!window.DFavorite)
    {
        var dfavorite = new window.DFavorite();
        dfavorite.init();
    }

    if(!!window.DInput)
    {
        var dinput = new window.DInput();
        dinput.init();
    }
});