(function(window){

    window.DForm = function(FormID) {
        this.form = document.getElementById(FormID);
        this.ajax = false;
        this.error = null;

        this.init();
    };

    window.DForm.prototype.init = function() {
        var inputsMask = $(this.form).find('[data-mask]'),
            _this = this;


        // Подключием маску ввода
        inputsMask.each(function(i,item){

        });

        // Перехватываем отправку формы
        this.form.addEventListener('submit', function(event) {

            _this.testRequired();

            // Останавливаем отправку если это ajax форма
            var type = $(this).data('form');
            if(type==='ajax') event.preventDefault();


            if(!_this.error) _this.send();
        });
    };


    window.DForm.prototype.nodeLoad = function() {
        var loading = $('<div/>');
        loading.addClass('cssload-loader');
        loading.html('<div class="cssload-inner cssload-one"></div><div class="cssload-inner cssload-two"></div><div class="cssload-inner cssload-three"></div>');
        return loading;
    };


    window.DForm.prototype.send = function() {
        var box = $(this.form).parent(),
            action = $(this.form).attr('action') ? $(this.form).attr('action') : window.location.href,
            method = $(this.form).attr('method') ? $(this.form).attr('method') : 'post',
            enctype = $(this.form).attr('enctype'),
            settings,
            _this = this;

        $(box).addClass('form-send');
        this.nodeLoad().appendTo(box);

        settings = {
            url: action,
            type: method,
            dataType: 'json',
            success: function(result) {
                if(!result.status) {
                    $(_this.form).find('.form-error-message').html(result.message);
                    $(_this.form).find('.form-true-message').html('');
                }
                else
                {
                    if(typeof result.redirect!=="undefined") window.location.href = result.redirect;

                    $(_this.form).find('.form-error-message').html('');
                    $(_this.form).find('.form-true-message').html(result.message);
                }
                box.removeClass('form-send');
                box.find('.cssload-loader').remove();
            }
        };


        if(enctype==='multipart/form-data')
        {
            settings.data = this.getFormData();
        }
        else
        {
            settings.data = $(this.form).serialize();
        }

        $.ajax(settings);
    };


    window.DForm.prototype.testRequired = function () {
        var inputsRequired = $(this.form).find('[data-required]'),
            _this = this;

        this.error = false;
        inputsRequired.each(function(i, input) {
            var action = $(input).data('required').toLowerCase();

            if(action==='notnull') _this.testNotNull(input);
        });

    };


    window.DForm.prototype.testNotNull = function(tag) {

        if(tag.tagName.toLowerCase()==='input')
        {
            // Проверяем на заполненость
            if(!$(tag).val().length) {
                $(tag).parent().addClass('field-error');
                this.error = true;
            } else if($(tag).parent().hasClass('field-error')) {
                $(tag).parent().removeClass('field-error');
            }
        }
    };



    window.DForm.prototype.getFormData = function () {
        var fd = new FormData,
            data = $(this.form).serializeArray(),
            inputFiles = $(this.form).find('[type=file]');

        fd.append('ajax', 'y');
        if(inputFiles.length) {
            for(let i=0; i<inputFiles.length; i++) {

                let files = inputFiles[i].files;
                for(let j=0; j<files.length; j++) {

                    fd.append(inputFiles[i].name, files[j]);
                }
            }
        }

        for(let i=0; i<data.length; i++) {
            if(data[i].value) {
                fd.append(data[i].name, data[i].value);
            }
        }

        return fd;
    };


    window.DForm.prototype.mask = function () {

    };


})(window);