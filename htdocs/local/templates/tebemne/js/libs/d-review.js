(function(window) {

    window.DReview = function(url, el) {
        this.el = el;
        this.url = url;
    };

    window.DReview.prototype.init = function(){
        if(this.el[0].childElementCount<=0){
            this.getHtml();
        }
    };

    window.DReview.prototype.getHtml = function(){
        var _this = this;
        _this.el.html('<div class="cssload-loader"><div class="cssload-inner cssload-one"></div><div class="cssload-inner cssload-two"></div><div class="cssload-inner cssload-three"></div></div>');
        $.ajax({
            url: this.url,
            type: 'post',
            data: {
                ajax: 'y',
                type: 'getReview'
            },
            success: function(result) {
                $(_this.el).html(result);
                _this.btn();
                _this.send($(_this.el).find('form'));
            }
        });
    };


    window.DReview.prototype.send = function(form){

        _this = this;

        form.submit(function(){
            var box = form.parent(),
                data = form.serialize(),
                backg = box.find('.cssload-bg'),
                loader = box.find('.cssload-loader');


            var $error = form.find('.form__error'),
                $true = form.find('.form__true');


            _this.showLoad(box, backg, loader);
            $.ajax({
                url: '/api/1.0/review/create/',
                data: data + '&ajax=y&session_id=' + BX.bitrix_sessid(),
                dataType: 'json',
                success: function(result){
                    _this.disLoad();
                    if(!result.status) {
                        $true.removeClass('action');
                        $true.html('');

                        $error.addClass('action');
                        $error.html(result.error);
                    }

                    if(result.status){
                        $error.removeClass('action');
                        $error.html('');

                        $true.addClass('action');
                        $true.html(result.result);

                        form.find('[name=text]').val('');
                        form.find('[name=save]').attr('disabled');
                        setTimeout(function(){ form.find('[name=save]').removeAttr('disabled'); }, 3000);
                    }
                }
            });
            return false;
        })
    };


    window.DReview.prototype.btn = function() {
        var btn = this.el.find('.js-form_btn'),
            form = this.el.find('form');

        btn.on('click', function(){
            var id = parseInt($(this).data('review')),
                box = $(this).parents('.js-form'),
                formBox = box.find('.js-form_wrapper');

            form.find('[name=review]').val(id);
            if(formBox[0].childElementCount<=0
               || formBox[0].childElementCount===2){
                form.appendTo(formBox);
            }
            return false;
        });
    };


    window.DReview.prototype.disLoad = function() {
        var backg = this.el.find('.cssload-bg'),
            loader = this.el.find('.cssload-loader');

        if(backg.length && loader.length){
            backg.css('display', 'none');
            loader.css('display', 'none');
        }
    };


    window.DReview.prototype.showLoad = function(box, backg, loader) {
        console.log(box, backg, loader);
        if(loader.length)
        {
            backg.css('display', 'block');
            loader.css('display', 'block');
        }
        else
        {
            $('<div class="cssload-bg"/>').css({
                background: 'white',
                opacity: '.5'

            }).appendTo(box);
            $('<div class="cssload-loader"/>')
                .html('<div class="cssload-inner cssload-one"></div><div class="cssload-inner cssload-two"></div><div class="cssload-inner cssload-three"></div>')
                .appendTo(box);
        }
    }


})(window);