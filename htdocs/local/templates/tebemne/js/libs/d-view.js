(function(window){

    window.DView = function() {
        this.el = null;
        this.btnClass = 'btn-view';
        this.contenerClass = 'catalog-content';
    };

    window.DView.prototype.init = function() {

        var _this = this;

        document.addEventListener("click", function(event) {
            let target = event.target;

            this.el = null;
            while(target!==null) {
                if(!("tagName" in target)) break;
                if(target.tagName.toLowerCase()==="body") break;
                if($(target).hasClass(_this.btnClass)) {
                    _this.el = target;
                    break;
                }
                target = target.parentNode;
            }

            if(_this.el!==null) _this.view();
            _this.el = null;
        });
    };


    window.DView.prototype.view = function(){
        let content = document.querySelector('.' + this.contenerClass);

        this.clearActive();
        $(this.el).addClass('active');

        if($(this.el).hasClass('btn-view-list'))
        {
            $(content).removeClass('catalog-table');
            $(content).addClass('catalog-line');
        }
        else if($(this.el).hasClass('btn-view-table'))
        {
            $(content).removeClass('catalog-line');
            $(content).addClass('catalog-table');
        }
    };

    window.DView.prototype.clearActive = function(){
        let btn = document.querySelectorAll('.' + this.btnClass);
        for(let i=0; i<btn.length; i++) {
            $(btn[i]).removeClass('active');
        }
    }


})(window);