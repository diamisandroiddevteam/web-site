(function(window){

    window.DScroll = function() {
        this.el = null;
        this.active = 0;
        this.itemHeight = [];
    };

    window.DScroll.prototype.init = function() {
        this.el = $('[data-scroll]');
        if(this.el.length) {
            this.el.css('overflow','hidden');
            this.generation();
        }
    };

    // Формируем стили
    window.DScroll.prototype.generation = function()
    {
        let items = this.el.find('.'+this.el.data('scroll')),
            width = this.el.parents('#dialog') ? this.el.parents('#dialog').width() : items.parent().width()-10,
            wrapRow = $('<div/>'),
            _this = this;

        wrapRow.css({
            width: (width + 20) * items.length,
            position: 'relative',
            overflow: 'hidden'
        });

        for(let i=0; i<items.length; i++)
        {
            if($(items[i]).hasClass('show'))
            {
                this.itemHeight[i] = $(items[i]).height() + 50 +'px';

                this.active = i;
                $(wrapRow).animate({'height': $(items[i]).height()+'px'});

                this.el.parents('#dialog')
                    ? this.el.parents('#dialog').css({
                        'min-height': this.itemHeight[i],
                        'transition': 'all .3s',
                    })
                    : null;
            }

            $(items[i]).attr('data-scroll-key', i);
            $(items[i]).css({
                display: 'block',
                float: 'left',
                width: width - 10 + 'px',
                marginRight: '20px',
                position: 'relative'
            });

            $(items[i]).find('[data-scroll-target]').on('click', function(e){
                let idActive = _this.el
                    .find('[data-scroll-id=' + $(items[i]).find('[data-scroll-target]').data('scroll-target') + ']')
                    .data('scroll-key');

                _this.drive(_this.el, idActive, e);
            });

            wrapRow.append(items[i]);
        }

        let clear = $('<div/>').css('clear','both');
        wrapRow.append(clear);

        this.el.innerHTML = '';
        this.el.append(wrapRow);
        this.drive(this.el, this.active);

        if(this.el.parents('#dialog').length)
        {
            $('#dialog').css({height: this.itemHeight[this.active], 'min-height': this.itemHeight[this.active]});
            $('#dialog-body').css({height: this.itemHeight[this.active]});
        }
    };



    window.DScroll.prototype.drive = function(Element, idActive, evt=null)
    {
        if(evt) evt.preventDefault();

        let items = Element.find('.' + Element.data('scroll')),
            width = $(items[idActive]).width(),
            height = $(items[idActive]).height() + 50;


        $(Element).children().animate({
            'left': '-' + (width + 20) * idActive +'px',
            'height': height + 'px'
        },400);

        if(this.el.parents('#dialog').length)
        {
            $('#dialog').css({height: height, 'min-height': height});
            $('#dialog-body').css({height: height});
        }

    }
})(window);