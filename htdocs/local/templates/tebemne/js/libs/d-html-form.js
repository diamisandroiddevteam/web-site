(function(window){

    window.DHtmlForm = function(){

    };

    window.DHtmlForm.prototype.getField = function(data){
        var type = data.type,
            result = null;

        if(typeof type==="undefined") return false;

        switch(type)
        {
            case 'select':      result = this.select(data); break;
            case 'multiselect': result = this.select(data, true); break;
            case 'string':      result = this.string(data); break;
            case 'number':      result = this.number(data); break;
            case 'slider':      result = this.slider(data); break;
        }

        return result;
    };

    window.DHtmlForm.prototype.number = function(data){
        let input = $('<div/>'),
            id = data.id.replace('[','_');

        id = id.replace(']','');

        input.addClass('input input-up input-max form-field-number');

        $('<div/>').addClass('input-placeholder').html(data['placeholder']).appendTo(input);
        $('<input/>').addClass('input-tag')
            .attr('name', data.id)
            .attr('id', id)
            .attr('data-mask', 'int')
            .val(data['currentValue'])
            .appendTo(input);

        if(data['currentValue']) input.addClass('active');

        return {
            id: 'input_' + id,
            html: input
        };
    };

    window.DHtmlForm.prototype.string = function(data){
        var input = $('<div/>'),
            id = data.id.replace('[','_');

        id = id.replace(']','');

        input.addClass('input input-up input-max form-field-string');

        $('<div/>').addClass('input-placeholder').html(data['placeholder']).appendTo(input);
        $('<input/>').addClass('input-tag')
            .attr('name', data.id)
            .attr('id', id)
            .appendTo(input);

        return {
            id: 'input_' + id,
            html: input
        };
    };

    window.DHtmlForm.prototype.select = function(data, multiselect = false){
        var values = data.values,
            id = data.id.replace('[','_');

        id = id.replace(']','');


        let select = $('<div/>');
        select.addClass('select');
        select.addClass('select_' + id + '');

        if(multiselect) select.addClass('select-multi');

        var htmlplaceholder = '';
        if(data['placeholder']) htmlplaceholder = '<div class="select-placeholder">' + data['placeholder'] + '</div>';

        $('<div class="select-content"><i class="select-icon"></i>'+htmlplaceholder+'<div class="select-result"></div></div>').appendTo(select);

        var liList = $('<ul/>'),
            checkList = $('<div/>');

        liList.addClass('select-lst-items');
        checkList.addClass('select-check');

        for(let i=0; i<=values.length; i++)
        {
            if(typeof values[i]==="undefined") break;


            $('<li/>').addClass('select-item')
                .attr('data-for', id + '_' + values[i].value)
                .html('<span>' + values[i].name+'</span>')
                .appendTo(liList);


            let checked = false,
                input = $('<input/>').attr('id', id + '_' + values[i].value)
                    .attr('name', data.id + '[' + values[i].value + ']')
                    .attr('type', 'checkbox')
                    .val(values[i].value);


            if(typeof data.currentValue!=="undefined" && values[i].value==data.currentValue) checked = true;
            else if(typeof data.currentValues!=="undefined" && data.currentValues.indexOf(values[i].value)!== -1) checked = true;

            if(checked) {
                input.checked = true;
                input.attr('checked','checked');
                select.addClass('select-active');
                select.find('.select-result').html(values[i].name);
            }

            input.appendTo(checkList);
        }

        var liContainer = $('<div/>'),
            liLists = $('<div/>');

        liContainer.addClass('select-lists');
        liLists.addClass('select-lst-container');

        liList.appendTo(liLists);
        liLists.appendTo(liContainer);


        liContainer.appendTo(select);
        checkList.appendTo(select);

        if(data.inDisable) select.addClass('disabled');

        return {
            id: 'select_' + id,
            html: select
        };
    };

    window.DHtmlForm.prototype.slider = function(data){
        var slider = $('<div/>'),
            container = $('<div/>'),
            lists = $('<div/>'),
            listsSlider = $('<div/>'),
            listsSliderInput = $('<div/>'),
            listsSliderContent = $('<div/>'),
            id = data.id.replace('[','_');

        id = id.replace(']','');
        slider.addClass('select select-slider');


        var htmlplaceholder = '';
        if(data['placeholder']) htmlplaceholder = '<div class="select-placeholder">' + data['placeholder'] + '</div>';


        container.addClass('select-content');
        container.html('<i class="select-icon"></i>'+htmlplaceholder+'<div class="select-result"></div>');
        container.appendTo(slider);


        lists.addClass('select-lists');
        listsSlider.addClass('slc-slider-wrap');
        listsSliderInput.addClass('slc-input');
        listsSliderContent.addClass('slc-slider');
        $('<div/>').addClass('slc-slider-container')
            .attr('id', id)
            .appendTo(listsSliderContent);

        for(let i=0; i<data.values.length; i++) {
            if (i===2) break;

            var input = $('<div/>');
            input.addClass('slc-input-wrap');
            input.html('<input type="text" id="'+id+'_'+i+'" name="'+data.id+'[]" value="" data-start="'+data.values[i].value+'" />');
            input.appendTo(listsSliderInput);
        }

        listsSliderInput.appendTo(listsSlider);
        listsSliderContent.appendTo(listsSlider);
        listsSlider.appendTo(lists);
        lists.appendTo(slider);

        return {
            id: 'slider_' + id,
            html: slider
        };
    };

    window.DHtmlForm.prototype.checkbox = function(data){

    };

})(window);