(function(window){

    window.DHeadAnimate = function (height) {
        this.doc = document.documentElement;
        this.body = document.querySelector('body');
        this.header = document.querySelector('.globalnav');
        this.scroll = false;
        this.changeHeader = height ? height : 160;
    };

    window.DHeadAnimate.prototype.init = function() {
        this.scrollPage();
        window.addEventListener('scroll', ()=>{
            if(!this.scroll) {
                this.scroll = true;
                this.scrollPage();
            }
        }, false);
    };

    window.DHeadAnimate.prototype.scrollPage = function() {
        const sy = this.scrollY();
        if( sy >= this.changeHeader )
        {
            $(this.body).addClass('scroll');
            $(this.header).addClass('header-scroll');
        }
        else
        {
            $(this.body).removeClass('scroll');
            $(this.header).removeClass('header-scroll');
        }
        this.scroll = false;
    };

    window.DHeadAnimate.prototype.scrollY = function() {
        return window.pageYOffset || this.doc.scrollTop;
    };

})(window);