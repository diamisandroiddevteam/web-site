(function(window){


    window.DFavorite = function(){
        this.el = null;
        this.url =  '/api/1.0/Element/updateFavorite/';
        this.session_id = BX.bitrix_sessid();
    };


    window.DFavorite.prototype.init = function () {
        var _this = this;

        document.addEventListener("click", function(event) {
            var target = event.target;

            _this.el = null;
            while(target!==null) {
                if(!("tagName" in target)) break;
                if(target.tagName.toLowerCase()==="body") break;
                if($(target).hasClass('js-favorite')) {
                    _this.el = target;
                    break;
                }
                target = target.parentNode;
            }

            if(!_this.el) return;

            var id = $(_this.el).data('favorite');
            _this.updateFavorite(id);
        });
    };


    window.DFavorite.prototype.updateFavorite = function (id){
        var _this = this;
        $.ajax({
            url: this.url,
            data: {
                id: id,
                ajax: 'y',
                session_id: _this.session_id,
            },
            method: 'post',
            dataType: 'json',
            success: function(data) {
                if (data.status){
                    if(data.result.type==='add') { $(_this.el).addClass('favorite-active'); }
                    if(data.result.type==='delete') { $(_this.el).removeClass('favorite-active'); }
                }

                $('.gn-link-favorite .gn-number').remove();
                if(data.result.count>0){
                    $('<span class="gn-number"/>').html(data.result.count).appendTo('.gn-link-favorite');
                }
            }
        })
    };


})(window);