(function($, window) {


    window.DInput = function() {
        this.el = null;
        this.name = null;
        this.currentInput = {};
    };


    window.DInput.prototype.init = function() {
        var _this = this;

        document.addEventListener("click", function (event) {
            var target = event.target,
                tagName = target.tagName.toLowerCase();

            if(_this.el) _this.clear();


            while (target !== null) {
                if (!("tagName" in target)) break;
                if (tagName === "body") break;
                if (tagName === "input") {
                    _this.el = target;
                    break;
                }
                target = target.parentNode;
            }

            if(_this.el===null) return false;
            _this.start();
        });

    };

    window.DInput.prototype.clear = function() {
        var el = this.el,
            parent  = $(el.parentNode);

        if(parent.hasClass('active') && !el.value.length) {
            parent.removeClass('active');
        }

        this.el = null;
    };


    window.DInput.prototype.start = function() {
        var el = $(this.el),
            mask = el.data('mask'),
            name = el.attr('name');

        this.name = name;

        if(el.hasClass('input-tag')) this.placecholder();
        if(el.hasClass('js-checked-tools')) this.checked();

        if(typeof this.currentInput[name] !== "undefined") return false;

        if(mask==='phone') this.maskPhone();
        if(mask==='email') this.maskEmail();
        if(mask==='int') this.maskInt();
    };


    window.DInput.prototype.checked = function() {
        var parent = $(this.el).parents('.js-checked-tools-box'),
            status = $(this.el).prop('checked'),
            id = $(this.el).attr('id');

        if(status) {
            parent.addClass('active');
            $('#' + id + '_on').prop('checked', true);
            $('#' + id + '_off').prop('checked', false);
        }
        else {
            parent.removeClass('active');
            $('#' + id + '_on').prop('checked', false);
            $('#' + id + '_off').prop('checked', true);
        }
    };


    window.DInput.prototype.placecholder = function() {
        var parent = $(this.el).parent();

        if(parent.hasClass('field-error'))
           parent.removeClass('field-error');

        if(parent.hasClass('active') && !this.el.value.length) {
           parent.removeClass('active');
        }
        else {
           parent.addClass('active');
        }
    };


    window.DInput.prototype.maskPhone = function() {
        var im = new Inputmask('+7 (999) 999-99-99');
        im.mask(this.el);

        this.currentInput[this.name] = true;
    };

    window.DInput.prototype.maskEmail = function() {
        Inputmask({
            mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
            greedy: false
        }).mask(this.el);

        this.currentInput[this.name] = true;
    };

    window.DInput.prototype.maskInt = function() {
        Inputmask({
            regex: "[0-9\ ]*",
        }).mask(this.el);

        this.currentInput[this.name] = true;
    };



    $(document).on('keydown', function(event) {
        if (event.keyCode == 9 && !event.shiftKey) {
            var target =  event.target,
                parent  = $(target.parentNode);


            if(parent.hasClass('active') && !target.value.length) {
                parent.removeClass('active');
            }
        } else if(event.keyCode !== 9) {
            if($(event.target).hasClass('input-tag')) {
                var target =  event.target,
                    parent  = $(target.parentNode);

                if(!parent.hasClass('active') && target.value.length!==false) {
                    parent.addClass('active');
                }
            }
        }
    });


})(jQuery, window);



