(function(window) {

    window.DSelect = function(){
        this.el = null;
        this.opt = null;
        this.sClassName = 'select';
        this.sValueClassName = 'select-result';
        this.sItemsClassName = 'select-item';
        this.sCloseClassName = 'select-icon';
        this.sSearchClassName = 'select-find-input';

        this.multi = false;
        this.scroll = false;

        this.uislider = noUiSlider;
    };


    window.DSelect.prototype.init = function() {

        var _this = this;

        document.addEventListener("click", function(event) {
            var target = event.target,
                clear = false;

            _this.el = null;
            _this.opt = null;

            while(target!==null)
            {
                if(!("tagName" in target)) break;
                if(target.tagName.toLowerCase()==="body") break;
                if($(target).hasClass(_this.sCloseClassName)) clear = true;
                if($(target).hasClass(_this.sItemsClassName)) _this.opt = target;
                if($(target).hasClass(_this.sSearchClassName)){
                    $(target).on('keyup', function() { _this.search($(this)); });
                }
                if($(target).hasClass(_this.sClassName)) {
                    _this.el = target;
                    break;
                }
                target = target.parentNode;
            }

            if(clear) { _this.clear(); return false; }
            if(_this.el!==null && _this.opt===null) _this.open();
            if(_this.el!==null && _this.opt!==null) _this.update();
            if(_this.el===null) _this.closeAll();
        });

        if(_this.el===null) return false;

        var selects = document.querySelectorAll("." + this.sClassName );
        for(var i=0; i<selects.length; i++) {
            var value = selects[i].querySelector("." + this.sValueClassName),
                active = selects[i].querySelector('input:checked');

            if(active !== null && active.value.length>0) {
                var id = active.id,
                    text = selects[i].querySelector('[data-for='+id+']').textContent;

                $(selects[i]).addClass('select-active');
                value.innerHTML = text;
            }
        }
    };


    // Выполняем поиск элемента
    window.DSelect.prototype.search = function(element) {
        var box = element.parents('.select-lists'),
            value = element.val(),
            lists = box.find('li');

        if(value.length<=2) {
            lists.css('display', 'block');
            return;
        }

        value.toLowerCase();
        lists.each(function(){
            var text = $(this).text().toLowerCase();
            if(text && ~text.indexOf(value)){
                $(this).css('display', 'block');
            } else {
                $(this).css('display', 'none');
            }
        });
    };


    window.DSelect.prototype.update = function() {

        if(this.el===null || this.opt===null) return false;

        var value = this.el.querySelector('.'+this.sValueClassName),
            label = this.opt.textContent.trim(),
            multiVal = 0;

        var placeholder = this.el.querySelector('.select-placeholder'),
            name = placeholder ? placeholder.innerHTML : '';


        this.multi = $(this.el).hasClass('select-multi');

        if(typeof value==="undefined") {
            this.el = null;
            this.close();
            return false;
        }


        var dataFor = this.opt.dataset.for,
            inputs = this.el.querySelectorAll('input');

        for(var i=0; i<inputs.length; i++)
        {
            if(inputs[i].id === dataFor)
            {
                if(!inputs[i].checked)
                {
                    inputs[i].checked = true;
                    inputs[i].setAttribute('checked','checked');
                    $(inputs[i]).change();

                    value.innerHTML = label;

                    if($(this.el).hasClass('field-error')) $(this.el).removeClass('field-error');

                    $(this.el).addClass('select-active');
                    $(this.opt).addClass('select-active-item');
                }
                else
                {
                    inputs[i].checked = false;
                    inputs[i].removeAttribute('checked');
                    $(inputs[i]).change();

                    value.innerHTML = '';

                    $(this.opt).removeClass('select-active-item');
                }

                this.trueSelect(inputs[i]);
            }
            else if(this.multi===false)
            {
                inputs[i].checked = false;
                inputs[i].removeAttribute('checked');
                var item = this.el.querySelector('.' + this.sItemsClassName + '[data-for=' + inputs[i].getAttribute('id') + ']');
                if(item!==null) $(item).removeClass('select-active-item');
            }

            if(this.multi && inputs[i].checked)
            {
                multiVal += 1;
            }
        }

        if(multiVal > 0) {
            value.innerHTML = '<span>('+ multiVal + ')</span> ' + name;
        }

        if(value.innerHTML.length===0) $(this.el).removeClass('select-active');

        if(this.multi===false) this.close();
    };



    window.DSelect.prototype.slider = function() {

        var value = $(this.el).find('.'+this.sValueClassName);

        var placeholder = this.el.querySelector('.select-placeholder'),
            name = (placeholder ? placeholder.innerHTML : '');

        var container = this.el.querySelector('.slc-slider-container'),
            inputs = this.el.querySelectorAll('input'),
            id = container.getAttribute('id'),
            def = {
                start: [0],
                connect: true,
                range: {
                    min: 0,
                    max: 100
                }
            };


        if($(this.el).hasClass('field-error')) $(this.el).removeClass('field-error');

        // if(typeof value==="undefined") {
        //     this.el = null;
        //     this.close();
        //     return false;
        // }

        if(typeof noUiSlider==="undefined") return false;
        if(typeof this.uislider==="undefined") this.uislider = noUiSlider;


        if(!$(container).hasClass('noUi-target'))
        {
            var snapValues = [];

            for(var i=0; i<inputs.length; i++) {
                snapValues.push(inputs[i]);
                def.start[i] =  (inputs[i].value
                        ? parseInt(inputs[i].value)
                        : parseInt(inputs[i].dataset.start)
                );
                if(i===0 && inputs.length>1) def.range.min = parseInt(inputs[i].dataset.start);
                if(i!==0 && i===inputs.length-1) def.range.max = parseInt(inputs[i].dataset.start);
                if(parseInt(inputs[i].dataset.max)>0) def.range.max = parseInt(inputs[i].dataset.max);
            }

            if(snapValues.length===1) {
                def.connect = [true, false];
            }


            this.uislider.create(container, def);


            // обновление инпут при скролинге
            container.noUiSlider.on('update', function(values, handle) {

                if(!$(this.el).hasClass('select-active')) $(this.el).addClass('select-active');

                var val = name.length<=0 ? '' : name + ': ';

                snapValues[handle].value = parseInt(values[handle]);
                $(snapValues[handle]).change();

                if (snapValues.length === 2)
                {
                    if(parseInt(values[0])!==def.range.min) val += 'от ' + parseInt(values[0]) + ' ';
                    if(parseInt(values[1])!==def.range.max) val += 'до ' + parseInt(values[1]) + ' ';
                }
                else
                {
                    if(snapValues[handle].getAttribute('placeholder'))
                        val += snapValues[handle].getAttribute('placeholder') + ' ';
                    else
                        val += 'до ';

                    val +=  parseInt(values[0]);
                }

                $(value).html(val);
            });


            // ставим обработчки на отслеживание изменений
            snapValues.forEach(function(input, handle){

                input.addEventListener('change', function(event){
                    var values = container.noUiSlider.get();


                    if(typeof values === "object") values[handle] = parseInt(event.target.value);
                    else values = parseInt(event.target.value);

                    container.noUiSlider.set(values);
                });

            });

        }
    };


    window.DSelect.prototype.clear = function() {
        $(this.el).removeClass('select-active');

        var value = this.el.querySelector('.'+this.sValueClassName),
            inputs = this.el.querySelectorAll('input');

        for(var i=0; i<inputs.length; i++)
        {
            if(inputs[i].getAttribute('type') === "checkbox"){

                if(inputs[i].checked) $(inputs[i]).change();

                inputs[i].checked = false;
                inputs[i].removeAttribute('checked');

                var item = this.el.querySelector('.' + this.sItemsClassName + '[data-for=' + inputs[i].getAttribute('id') + ']');
                if(item!==null) $(item).removeClass('select-active-item');
            } else {
                inputs[i].value = '';
            };
        }
        value.innerHTML = '';
        this.close();
    };


    window.DSelect.prototype.open = function() {
        if(this.el===null) return false;
        if($(this.el).hasClass('disabled')) return false;

        this.closeAll();
        this.scroll = $(this.el).hasClass('select-slider');
        if(this.scroll) this.slider();

        $(this.el).addClass(!$(this.el).hasClass('dropdown') ? 'dropdown' : '');
    };

    window.DSelect.prototype.close = function() {
        if(this.el===null) return false;
        $(this.el).removeClass('dropdown');
        this.el = null;
    };

    window.DSelect.prototype.closeAll = function() {
        var selects = document.querySelectorAll("." + this.sClassName + ".dropdown");
        for(var i=0; i<selects.length; i++) {
            if(this.el===selects[i]) continue;
            $(selects[i]).removeClass('dropdown');
        }
    };

    window.DSelect.prototype.trueSelect = function(input) {
        var name = input.name,
            value = input.value;

        if(name==='field[type]')
        {
            var category = document.querySelector("[name*=category]:checked"),
                promise = new Promise(function(res, fail) {

                    var request = new XMLHttpRequest(),
                        formData = new FormData();

                    formData.append('ajax', true);
                    formData.append('type', 'field');
                    formData.append('action', 'get');
                    formData.append('field[category]', category.value);
                    formData.append('field[type]' , value);

                    request.open('POST', window.location.href, true);
                    request.addEventListener('load', function(){
                        request.status >= 200 && request.status < 400
                            ? res(request.responseText)
                            : fail(new Error(`Request Failed: ${request.statusText}`));
                    });
                    request.send(formData);
                });

            promise.then(function(data){
                var prop = document.getElementById('section-property');


                if(prop)
                {
                    var $arData = JSON.parse(data),
                        secMain = document.getElementById('section-main'),
                        secProp = document.getElementById('section-property'),
                        secCont = document.getElementById('section-contact');

                    secMain.className = '';
                    secProp.className = '';
                    secCont.className = '';

                    prop.innerHTML = '';
                    $arData.forEach(function(item) {
                        var group = document.createElement('div');
                        group.className = 'form-group';
                        group.innerHTML = item;
                        prop.appendChild(group);
                    });
                }
            });
        }
    }

})(window);