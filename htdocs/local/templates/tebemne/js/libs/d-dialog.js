(function(window){

    const actionDialog = [];

    window.DDialog = function (){
        this.window = null;
    };


    window.DDialog.prototype.init = function () 
    {
        var _this = this;

        $('[data-dialog]').on('click', function(event) {
            event.preventDefault();

            var el   = $(this),
                link = el.attr('href'),
                action  = _this.toggleAction(el);

            if(action===false) return false;
            
            $('body').addClass('ui-window');
            _this.toggleBg();

            $.ajax({
                url: link,
                data: {
                    ajax: 'y'
                },
                success: function(data) {
                    _this.open(data, action);
                }
            });
        });
    };

    window.DDialog.prototype.toggleAction = function(el){
        var key = $(el).data('dialog-key'),
            result = false;
        
        if(typeof key==="undefined") {
            $(el).attr('data-dialog-key', actionDialog.length);
            actionDialog.push(true);
            result = actionDialog.length;
        }
        else
        if(!actionDialog[key]) {
            
            result = key;
        }

        if(result!==false) 
        {
            for(var i=0; i<actionDialog.length; i++) {
                actionDialog[i] = false;
            }
            actionDialog[key] = true;
        }

        return result;
    }


    window.DDialog.prototype.open = function(html, actionID) {
        var container = this.toggleDialog(),
            _this = this;

        if(!this.window) this.window = container;

        $('<div/>').attr('id','dialog-close').appendTo(container);              // close
        $('<div/>').attr('id','dialog-body').html(html).appendTo(container);    // body
        $('<div/>').attr('id','dialog-footer').appendTo(container);             // footer

        // this.resize(container);

        $('#dialog-container').click(function(evt){
            // дополнительная проверка что клик пришелся на фон
            if($(evt.target).attr('id')==='dialog-container') {
                _this.close(container, actionID);
            }
        });
        container.find('#dialog-close').click(function(){ _this.close(container, actionID); });

        this.autoPosition();
        // $(window).resize(function(){ _this.autoPosition(); });
    };


    window.DDialog.prototype.resize = function(container) {
        var boxH = $(container).find('.window').height();

        if(boxH>50)
        {
            $(container).find('#dialog-body').css({
                minHeight: boxH + 50 + 'px'
            });
            $(container).css({
                minHeight: boxH + 50 + 'px'
            });
        }
    };


    window.DDialog.prototype.autoPosition = function(){
        var box = $(this.window).parent(),
            posX = $(document).outerWidth()/2 - $(box).outerWidth()/2,
            posY = $(document).scrollTop() + 80;

        $(box).css('top', posY + 'px');
        $(box).css('left', posX + 'px');
    };


    window.DDialog.prototype.toggleBg = function(){
        var container = $('#dialog-container');

        if(!container.length)
        {
            var loading = $('<div/>');
            loading.addClass('cssload-loader');
            loading.html('<div class="cssload-inner cssload-one"></div><div class="cssload-inner cssload-two"></div><div class="cssload-inner cssload-three"></div>');

            container = $('<div/>').attr('id', 'dialog-container');
            loading.appendTo(container);
            container.appendTo('body');
        }

        return container;
    }


    window.DDialog.prototype.toggleDialog = function() {
        var box = $('#dialog'),
            container = this.toggleBg(),
            append = false,
            _this = this;

        if(!box.length) { 
            box = $('<div/>').attr('id','dialog'); 
            box.appendTo(container);
        }

        box.html('');
        if(!box.hasClass('ui-dialog-content'))
        {
            box.dialog({
                autoOpen: false,
                modal: true,
                close: function(){ _this.close(); }
            });
            box.appendTo(container);
        }
        else
        {
            box.css({'opacity':'1'});
        }

        return box;
    };


    window.DDialog.prototype.close = function(dialog, actionID) {
        var box = dialog ? dialog : this.window;
        box.dialog("close");
        box.html("");
        box.css({'opacity': '0'});
        $('body').removeClass('ui-window');
        actionDialog[actionID-1] = false;
    }

})(window);