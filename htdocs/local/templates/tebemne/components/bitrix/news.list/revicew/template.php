<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(count($arResult["ITEMS"])):
    ?>
    <div class="section home-reviews-section">
        <div class="line-style"></div>
        <div class="content">
            <div class="title">Отзывы компаний</div>
            <div class="scroll review-scroll">
                <div class="scroll-arrow scroll-arrow-left"></div>
                <div class="scroll-list review-list">
                    <?foreach($arResult["ITEMS"] as $arItem):?>
                    <div class="scroll-item">
                        <div class="review-item">
                            <? if($arItem["PREVIEW_PICTURE"]["SRC"]): ?>
                            <div class="review-top">
                                <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>" class="review-image">
                            </div>
                            <? endif;?>
                            <div class="review-title"><?=$arItem["NAME"]?></div>
                            <div class="review-content"><?=$arItem["DETAIL_TEXT"]?></div>
                        </div>
                    </div>
                    <?endforeach;?>
                </div>
                <div class="scroll-arrow scroll-arrow-right"></div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $('.review-list').slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        prevArrow: $('.review-scroll .scroll-arrow-left'),
        nextArrow: $('.review-scroll .scroll-arrow-right'),
        responsive: [
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    </script>
    <?
endif;