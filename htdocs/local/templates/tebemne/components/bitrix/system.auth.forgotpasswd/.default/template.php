<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

ShowMessage($arParams["~AUTH_RESULT"]);

?><div class="title office-title">Восстановление пароля</div><?

if($_REQUEST['forgot_password']==='yes'):
    ?>
    <div class="desc">
        <p>На указанный E-Mail было отправлено письмо для восстановления парял.</p>
    </div>
    <?
else:
    ?>
    <div class="desc">
        <p><?=GetMessage("AUTH_FORGOT_PASSWORD_1")?></p>
    </div>
    <div class="webform">
        <form name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
            <? if (strlen($arResult["BACKURL"]) > 0): ?>
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <? endif; ?>
            <input type="hidden" name="AUTH_FORM" value="Y">
            <input type="hidden" name="TYPE" value="SEND_PWD">
            <? /*
            <div class="form-group">
                <div class="form-field form-field--min">
                    <div class="form-field--label"><?=GetMessage("AUTH_LOGIN")?></div>
                    <div class="form-field--param"><?=htmlInputMin(array('name'=>'AUTH_LOGIN'));?></div>
                </div>
            </div>
            <div class="form-desc form-desc--min">или</div>
            */ ?>
            <div class="form-group">
                <div class="form-field form-field--min">
                    <div class="form-field--label"><?=GetMessage("AUTH_EMAIL")?></div>
                    <div class="form-field--param"><?=htmlInputMin(array('name'=>'USER_EMAIL'));?></div>
                </div>
            </div>
            <? if($arResult["USE_CAPTCHA"]):?>
                <div class="form-group">
                    <div class="form-field--label"><?=GetMessage("system_auth_captcha")?></div>
                    <div class="form-field--param">
                        <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                    </div>
                    <div class="form-field--param">
                        <input type="text" name="captcha_word" maxlength="50" value="" />
                    </div>
                </div>
            <? endif?>
            <br>
            <div class="form-btns form-btns--min">
                <input class="btn btn-rempty form-btn" type="submit" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>" />
                <a class="link form-btn" href="/auth/?type=authForm&backurl=/" data-dialog="reg" onclick="return false;"><b><?=GetMessage("AUTH_AUTH")?></b></a>
            </div>
        </form>
    </div>
    <?
endif;