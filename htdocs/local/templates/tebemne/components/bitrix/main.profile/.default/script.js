(function(window){

    window.userFormSettings = function(){
        this.form = null;
        this.timerId = null;
    };

    window.userFormSettings.prototype.init = function(formId) {
        this.form = $('#'+formId);
        this.change();
    };

    window.userFormSettings.prototype.change = function() {
        var _this = this;

        this.form.on('change', function(event){
            if(_this.timerId) clearTimeout(_this.timerId);
            _this.timerId = setTimeout(function(){ _this.send(event); }, 1000);
        });
    };

    window.userFormSettings.prototype.send = function(event) {
        var data = this.form.serialize(),
            url = '/api/1.0/User/update/',
            _this = this;

        console.log(event);
        $.ajax({
            url: url,
            data: data,
            type: 'post',
            dataType: 'json',
            success: function(result) {
                console.log(result);
                _this.timerId = null;
            }
        })
    };

})(window);