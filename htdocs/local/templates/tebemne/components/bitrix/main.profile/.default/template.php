<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Diamis\Ads\Base;
?>
<div class="office">
    <?
    $APPLICATION->IncludeComponent(
        'bitrix:menu',
        'profile',
        array(
            "ROOT_MENU_TYPE" => "profile",
            "MAX_LEVEL" => "1",
            "USE_EXT" => "Y"
        )
    );

    ?>
    <div class="office-container">
        <div class="office__title">Личные данные</div>
        <div class="user-error">
            <?ShowError($arResult["strProfileError"]);?>
        </div>
        <form id="user-settings" method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
            <?=$arResult["BX_SESSION_CHECK"]?>
            <input type="hidden" name="lang" value="<?=LANG?>" />
            <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
            <div class="field-list">
                <?=htmlInput(array('title'=>GetMessage('NAME'),'name'=>'NAME','value'=>$arResult["arUser"]["NAME"]));?>
                <?=htmlInput(array('title'=>GetMessage('LAST_NAME'),'name'=>'LAST_NAME','value'=>$arResult["arUser"]["LAST_NAME"]));?>
                <?=htmlInput(array('title'=>GetMessage('EMAIL'),'name'=>'EMAIL','value'=>$arResult["arUser"]["EMAIL"]));?>
                <?=htmlInput(array('title'=>GetMessage('PERSONAL_PHONE'),'name'=>'PERSONAL_PHONE','value'=>Base::formatPhone($arResult["arUser"]["PERSONAL_PHONE"])));?>
            </div>
            <div class="office__title">Подписка на уведомления</div>
            <div class="field-list">
                <?=htmlCheck(array('id'=>'push_sale', 'title'=>'Акции','name'=>'UF_PUSH_SALE', 'checked'=>$arResult['arUser']['UF_PUSH_SALE']));?>
                <?=htmlCheck(array('id'=>'push_news', 'title'=>'Новости','name'=>'UF_PUSH_NEWS', 'checked'=>$arResult['arUser']['UF_PUSH_NEWS']));?>
                <?=htmlCheck(array('id'=>'push_search', 'title'=>'Подборка объявлений', 'name'=>'UF_PUSH_SEARCH', 'checked'=>$arResult['arUser']['UF_PUSH_SEARCH']));?>
            </div>
        </form>
        <form id="user-password" method="post" name="password" action="<?=$arResult["FORM_TARGET"]?>">
            <div class="office__title">Смена пароля</div>
            <div class="field-list">
                <?=htmlPass(array('title'=>GetMessage('NEW_PASSWORD_REQ'),'name'=>'NEW_PASSWORD'));?>
                <?=htmlPass(array('title'=>GetMessage('NEW_PASSWORD_CONFIRM'),'name'=>'NEW_PASSWORD_CONFIRM'));?>
            </div>
            <input class="office__btn"
                   type="submit"
                   name="save"
                   value="Изменить пароль">
        </form>
        <script>
        $(function(){
            var userForm = new window.userFormSettings();
            userForm.init('user-settings');
        });
        </script>
    </div>
</div>

<?php
/*

      <div class="user-title"><?=GetMessage("REG_SHOW_HIDE")?></div>
        <div class="user-profile-block-<?=strpos($arResult["opened"], "reg") === false ? "hidden" : "shown"?>" id="user-reg">
            <div class="user-group">
                <?=GetMessage('USER_PHONE')?>
                <input type="text" name="PERSONAL_PHONE" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" />
            </div>
            <div class="user-group">
                <?=GetMessage("USER_PHOTO")?>
                <?=$arResult["arUser"]["PERSONAL_PHOTO_INPUT"]?>
                <? if (strlen($arResult["arUser"]["PERSONAL_PHOTO"])>0) : ?>
                    <?=$arResult["arUser"]["PERSONAL_PHOTO_HTML"]?>
                <? endif; ?>
            </div>



if($arResult["SOCSERV_ENABLED"])
{
    $APPLICATION->IncludeComponent(
        "bitrix:socserv.auth.split",
        ".default", array(
            "SHOW_PROFILES" => "Y",
            "ALLOW_DELETE" => "Y"
        ),
        false
    );
}
?>
</div>
*/?>