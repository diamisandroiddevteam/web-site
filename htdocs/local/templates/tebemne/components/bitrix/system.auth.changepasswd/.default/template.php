<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();


?>
<div class="title office-title"><?=GetMessage("AUTH_CHANGE_PASSWORD")?></div>
<div class="desc">
    <p><?=$arParams["~AUTH_RESULT"]["MESSAGE"];?></p>
    <p><?=$arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]?></p>
</div>
<div class="webform">
    <form method="post" action="<?=$arResult["AUTH_FORM"]?>" name="bform">
        <?if (strlen($arResult["BACKURL"]) > 0): ?>
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
        <? endif ?>
        <input type="hidden" name="AUTH_FORM" value="Y">
        <input type="hidden" name="TYPE" value="CHANGE_PWD">
        <input type="hidden" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" class="bx-auth-input" />
        <input type="hidden" name="USER_CHECKWORD" maxlength="50" value="<?=$arResult["USER_CHECKWORD"]?>" class="bx-auth-input" />
        <div class="form-group">
            <div class="form-field">
                <div class="form-field--label"><?=GetMessage("AUTH_NEW_PASSWORD_REQ")?></div>
                <div class="form-field--param"><?=htmlInputMin(array('name'=>'USER_PASSWORD', 'type'=>'password'));?></div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-field">
                <div class="form-field--label"><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?></div>
                <div class="form-field--param"><?=htmlInputMin(array('name'=>'USER_CONFIRM_PASSWORD', 'type'=>'password'));?></div>
            </div>
        </div>
        <?if($arResult["USE_CAPTCHA"]):?>
            <div class="form-group">
                <div class="form-field">
                    <div class="form-field--label"><?=GetMessage("system_auth_captcha")?></div>
                    <div class="form-field--param">
                        <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                    </div>
                    <div class="form-field--param">
                        <?=htmlInputMin(array('name'=>'captcha_word'));?>
                    </div>
                </div>
            </div>
        <?endif?>
        <div class="form-btns">
            <input class="btn btn-rempty form-btn" type="submit" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>" />
            <a class="link form-btn" href="/auth/?type=authForm&backurl=/" data-dialog="reg" onclick="return false;"><b><?=GetMessage("AUTH_AUTH")?></b></a>
        </div>
    </form>
</div>
