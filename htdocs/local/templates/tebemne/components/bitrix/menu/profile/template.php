<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Diamis\Ads\Base;

global $USER;

$arUser = CUser::GetList($USER->GetID())->Fetch();

?>

<div class="office-bar">
    <div class="office-bar-avatar">
        <div class="office-bar-image">
            <img src="<?=SITE_TEMPLATE_PATH;?>/images/no_user.jpg" style="width:100%;">
        </div>

        <div class="office-bar-edit">
            <form action="/profile/" method="post" enctype="multipart/form-data">
                <?=bitrix_sessid_post()?>
                <label>Изменить фото
                    <input name="PERSONAL_PHOTO" style="display: none;" size="20" type="file">
                </label>
            </form>
        </div>
        <div class="office-bar-info">
            <span class="office-bar-title"><?=$USER->GetFullName() ? $USER->GetFullName() : $USER->GetLogin();?></span>
            <span class="office-bar-title"><?=Base::formatPhone($arUser['PERSONAL_PHONE']);?></span>
            <span class="office-bar-title"><?=$arUser['EMAIL'];?></span>
        </div>
    </div>
    <div class="office-bar-social office-bar-social--title">Социальные сети</div>
    <div class="office-bar-social office-bar-social--desc">Заходите на сайт через аккаунты в социальных сетях.</div>
    <? $APPLICATION->IncludeComponent(
            'diamis:auth.social',
            '',
            array(
                'COLOR' => 'Y'
            )
    );?>
    <br>
    <div class="office-bar-list">
    <?
    foreach($arResult as $arItem):
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;
        ?>
        <?if($arItem["SELECTED"]):?>
            <a href="<?=$arItem["LINK"]?>" class="office-bar-item active"><?=$arItem["TEXT"]?></a>
        <?else:?>
            <a href="<?=$arItem["LINK"]?>" class="office-bar-item"><?=$arItem["TEXT"]?></a>
        <?endif?>
    <?endforeach?>
    </div>
</div>