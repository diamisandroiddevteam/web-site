<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if (!empty($arResult)):
    ?>
    <div class="site-nav-bar">
        <div class="site-nav-bar-icon"></div>
        <div class="site-nav-bar-wrap">
            <ul class="site-nav-items">
                <?
                foreach($arResult as $arItem):
                    if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                        continue;

                    if($arItem["SELECTED"]):
                        ?><li class="site-nav-item"><a href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a></li><?
                    else:
                        ?><li class="site-nav-item"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li><?
                    endif;

                endforeach;
                ?>
            </ul>
        </div>
    </div>
    <?
endif;
?>