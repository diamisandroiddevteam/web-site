<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$this->setFrameMode(false);

if (!empty($arResult["ERRORS"])):
    ShowError(implode("<br />", $arResult["ERRORS"]));
endif;

if (strlen($arResult["MESSAGE"]) > 0):
    ShowNote($arResult["MESSAGE"]);
endif;


$dataSelectCategory = array(
    'id' => 'category',
    'name'  => 'IBLOCK_SECTION',
    'title' => 'Выберите категорию'
);
$dataSelectCategoryLever = array();
foreach($GLOBALS['CATEGORY'] as $ctg) {
    if($ctg['DEPTH_LEVEL']==1) {
        $dataSelectCategory['items'][] = array(
            'id' => $ctg['ID'],
            'name' => $ctg['NAME'],
            'value' => $ctg['ID']
        );
    }
}

?>
<form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
    <?=bitrix_sessid_post()?>
    <?if ($arParams["MAX_FILE_SIZE"] > 0):?>
        <input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" />
    <?endif?>

    <div class="group">
        <?=htmlSelect($dataSelectCategory);?>
    </div>

    <div class="group">
        <div class="title">Название объявления</div>
        <input type="text" name="PROPERTY[NAME][0]" size="30" value="">
    </div>

    <div class="group">
        <div class="title">Цена</div>
        <input type="text" name="PROPERTY[PRICE][0]" size="30" value="">
    </div>

    <?
    $params = array();
    $arProps = array();
    if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])):
        foreach ($arResult["PROPERTY_LIST"] as $propertyID):
            // PROPERTY
            if(is_numeric($propertyID))
            {
                $exCode = explode('_', $arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE']);

                if(count($exCode)>1)
                $arProps[$exCode[0]][$propertyID] = $arResult["PROPERTY_LIST_FULL"][$propertyID];
            }
            else
            {
                $params[$propertyID] = $arResult["PROPERTY_LIST_FULL"][$propertyID];
            }
        endforeach;

        foreach($arProps as $type=>$props) {
            ?>
            <br>
            <div class="props-title"><b>Свойства <?=$type;?></b></div>
            <br>
            <div class="props-<?=$type;?>">
                <?
                foreach ($props as $propertyID=>$prop):
                    ?>
                    <div class="group">
                        <div class="title"><?=$arResult["PROPERTY_LIST_FULL"][$propertyID]['NAME'];?></div>
                        <input type="text" name="PROPERTY[<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]['CODE'];?>][0]" size="30" value="">
                    </div>
                    <?
                endforeach;
                ?>
            </div>
            <?
        }
    endif;

    ?>
    <br>
    <div class="group">
        <div id="map" style="widows:600px; height: 280px;"></div>
    </div>
    <br>
    <div class="group">
        <div>Добавить фотографии</div>
        <div>
            <input type="hidden" name="PROPERTY[38][0]" value="">
            <input type="file" size="30" name="PROPERTY_FILE_38_0"><br>
            <input type="hidden" name="PROPERTY[38][1]" value="">
            <input type="file" size="30" name="PROPERTY_FILE_38_1"><br>
            <input type="hidden" name="PROPERTY[38][2]" value="">
            <input type="file" size="30" name="PROPERTY_FILE_38_2"><br>
            <input type="hidden" name="PROPERTY[38][3]" value="">
            <input type="file" size="30" name="PROPERTY_FILE_38_3"><br>
            <input type="hidden" name="PROPERTY[38][4]" value="">
            <input type="file" size="30" name="PROPERTY_FILE_38_4"><br>
        </div>
    </div>
    <br>
    <div class="group">
        <div>Детальное описание</div>
        <textarea cols="30" rows="5" name="PROPERTY[DETAIL_TEXT][0]"></textarea>
    </div>

    <?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
        <div><?=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?></div>
        <div>
            <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
        </div>
        <div>
            <div><?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span>:</div>
            <div><input type="text" name="captcha_word" maxlength="50" value=""></div>
        </div>
    <?endif?>

    <div class="group">
        <input type="submit" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
    </div>

</form>