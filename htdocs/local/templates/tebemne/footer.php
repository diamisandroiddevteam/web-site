<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
    </div>
    <footer class="footer">
        <div class="fr-content fr-category--header">
            <a href="/<?=$GLOBALS['CITY_IP']['CITY_CODE'];?>/" class="fr-category-title link-active">Все объявления</a>
            <div class="flexbox">

            </div>
        </div>
        <div class="fr-line"></div>
        <div class="fr-content">
            <div class="flexbox">
                <ul class="fr-items">
                    <li class="fr-item"><a href="/" class="fr-link-logo"></a></li>
                    <li class="fr-item fr-copy">&copy; 2018 Ты мне Я тебе</li>
                </ul>
                <ul class="fr-items fr-nav">
                    <li class="fr-item"><a href="/company/" class="fr-link">О компании</a></li>
                    <li class="fr-item"><a href="/company/contacts/" class="fr-link">Контакты</a></li>
                    <li class="fr-item"><a href="/company/callback/" class="fr-link">Обратная связь</a></li>
                </ul>
                <ul class="fr-items fr-nav">
                    <li class="fr-item"><a href="/<?=$GLOBALS['CITY_IP']['CITY_CODE'];?>/" class="fr-link">Все объявления</a></li>
                    <li class="fr-item"><a href="/news/" class="fr-link">Новости</a></li>
                    <li class="fr-item"><a href="/site-map/" class="fr-link">Карта сайта</a></li>
                </ul>
                <ul class="fr-items fr-store">
                    <li class="fr-item fr-store-title"><a class="fr-link" href="#">Скачать приложение</a></li>
                    <li class="fr-item fr-app"><a class="home-s-app" href="#"></a></li>
                    <li class="fr-item fr-play"><a class="home-s-play" href="#"></a></li>
                </ul>
            </div>
        </div>
    </footer>
    <div class="develop"></div>
</body>
</html>