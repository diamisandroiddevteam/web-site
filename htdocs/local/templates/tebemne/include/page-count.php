<div class="section home-count-section">
    <div class="line-style"></div>
    <div class="content">
        <div class="title">С нами уже более</div>
        <div class="home-count">1 425 000</div>
        <div class="home-count-icon"></div>
        <div class="home-count-title">Подписчиков</div>
        <div class="home-count-btn">
            <a href="/registration.php?id=reg" class="btn" data-dialog="reg">Зарегистрироваться</a>
            <div class="link">Задать вопрос</div>
        </div>
    </div>
    <div class="section-style-down"></div>
</div>