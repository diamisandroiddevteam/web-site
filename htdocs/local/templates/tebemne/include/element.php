<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$item = $arParams['ELEMENT'];
if($item)
{
    $date = $item['DATE_UPDATE']->format("d.m.Y");
    if($date==date('d.m.Y')){
        $date = 'Сегодня в ' . $item['DATE_UPDATE']->format("h:i");
    }elseif($date==date('d.m.Y', strtotime('last day'))){
        $date = 'Вчера в ' . $item['DATE_UPDATE']->format("h:i");
    }



    $loc = $item['LOCATION'][key($item['LOCATION'])];
    $item['DISTANCE'] = Diamis\Ads\Base::getDistance($loc['LAT'], $loc['LNG'], $GLOBALS['FILTER_LAT'], $GLOBALS['FILTER_LNG']);


    $distance = 0;
    $distPrefix = 'м';
    $exDistance = explode(',',$item['DISTANCE']);
    if(!$exDistance[0]) {
        $distPrefix = 'м';
        $distance = $exDistance[1];
    }else {
        $distPrefix = 'км';
        $distance = $exDistance[0];
    }

    $favorite = '';
    if(array_search($item['ID'], $arParams['FAVORITES'])!==false
       && array_search($item['ID'], $arParams['FAVORITES'])!==null)
        $favorite = ' favorite-active';


    ?>
    <div class="ads <?if($item['SELECT']):?>ads-select<?endif;?>">
        <a href="<?=$arParams['PREFIX_URL'].$item['DETAIL_PAGE'];?>">
            <div class="ads-wrap ads-wrap-images">
                <?
                $active = ' active';
                foreach($item['FILES'] as $file):
                    $image[] = '<div class="ads-img-item"><img src="'.$file.'" class="ads-img b-lazy"></div>';
                    $nav[] = '<div class="ads-nav-item'.$active.'" style="width:'.(100/count($item['FILES'])).'%;"></div>';
                    $active = '';
                endforeach;
                ?>
                <div class="ads-images">
                    <div class="ads-images-items" style="width:<?=count($item['FILES'])*100;?>%;"><?=implode("\n", $image);?><div class="clear"></div></div>
                </div>
                <div class="ads-nav"><?=implode("\n", $nav);?></div>
            </div>
            <div class="ads-wrap ads-wrap-info">
                <div class="ads-info">
                    <a href="<?=$arParams['PREFIX_URL'].$item['DETAIL_PAGE'];?>" title="<?=$item['NAME'];?>" class="ads-title"><?=$item['NAME'];?></a>
                    <div class="ads-info-stat">
                        <div class="ads-distance"><?=$distance.' '.$distPrefix;?></div>
                        <div class="ads-date"><?=$date;?></div>
                    </div>
                    <div class="ads-desc"><?=$item['TEXT'];?></div>
                    <?if($item['PRICE']):?>
                        <div class="ads-price">
                            <div class="ads-price-num"><?=$item['PRICE'][key($item['PRICE'])]['PRICE'];?></div>
                            <div class="ads-price-type">a</div>
                            <div class="ads-rating"></div>
                        </div>
                    <?endif;?>
                    <div class="favorite js-favorite<?=$favorite;?>" data-favorite="<?=$item['ID'];?>">
                        <div class="favorite-icon"></div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <?
}