<div class="section plus-home-section">
    <div class="line-style"></div>
    <div class="plus-home-head">
        <div class="big-title">Преимущества</div>
        <div class="min-title">для вашего бизнеса</div>
    </div>
    <div class="plus-h-bg">
        <div class="plus-h-bg-radius"></div>
        <div class="plus-h-bg-line"></div>
    </div>
    <div class="plus-home">
        <div class="plus-mobile-bg plus-mobile-bg-1">
            <div class="plus-tablet-bg"></div>
            <div class="plus-mobile-bg-line-top left"></div>
            <div class="plus-mobile-bg-radius-right"></div>
            <div class="plus-h-item">
                <div class="plus-h-icon"><span class="plus-hi-brand"></span></div>
                <div class="plus-h-text">
                    <div class="plus-ht-title"><span>Брендинг</span></div>
                    <div class="plus-ht-desc">
                        <span>Расскажите о своей</span>
                        <span>компании своим</span>
                        <span>потенциальным</span>
                        <span>клентам</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="plus-mobile-bg plus-mobile-bg-2">
            <div class="plus-tablet-bg"></div>
            <div class="plus-mobile-bg-radius-left"></div>
            <div class="plus-h-item">
                <div class="plus-h-icon"><span class="plus-hi-docs"></span></div>
                <div class="plus-h-text">
                    <div class="plus-ht-title">
                        <span>Электронный</span>
                        <span>документооборот</span>
                    </div>
                    <div class="plus-ht-desc">
                        <span>Заключайте договора</span>
                        <span>не выходя из офиса.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="plus-mobile-bg plus-mobile-bg-3">
            <div class="plus-tablet-bg"></div>
            <div class="plus-mobile-bg-radius-right"></div>
            <div class="plus-h-item">
                <div class="plus-h-icon"><span class="plus-hi-sale"></span></div>
                <div class="plus-h-text">
                    <div class="plus-ht-title"><span>Спецпредложения</span></div>
                    <div class="plus-ht-desc">
                        <span>Оповещайте своих</span>
                        <span>клиентов о новых</span>
                        <span>предложениях и акциях</span>
                        <span>в один кли.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="plus-mobile-bg plus-mobile-bg-4">
            <div class="plus-tablet-bg"></div>
            <div class="plus-mobile-bg-line-bottom right"></div>
            <div class="plus-mobile-bg-radius-left min"></div>
            <div class="plus-h-item">
                <div class="plus-h-icon"><span class="plus-hi-delev"></span></div>
                <div class="plus-h-text">
                    <div class="plus-ht-title"><span>Логистика</span></div>
                    <div class="plus-ht-desc">
                        <span>Доставляйте товары</span>
                        <span>до порога клиента</span>
                        <span>с нашим сервисом</span>
                        <span>или самостоятельно.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="plus-h-title">И это еще не все!</div>
        <div class="plus-mobile-bg plus-mobile-bg-5">
            <div class="plus-tablet-bg"></div>
            <div class="plus-mobile-bg-line-top right"></div>
            <div class="plus-mobile-bg-radius-left"></div>
            <div class="plus-h-item">
                <div class="plus-h-icon"><span class="plus-hi-map"></span></div>
                <div class="plus-h-text">
                    <div class="plus-ht-title"><span>Поиск на карте</span></div>
                    <div class="plus-ht-desc">
                        <span>Поиск на карте</span>
                        <span>Поиск объявлений</span>
                        <span>в казаном радиусе.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="plus-mobile-bg plus-mobile-bg-6">
            <div class="plus-tablet-bg"></div>
            <div class="plus-mobile-bg-radius-right"></div>
            <div class="plus-h-item">
                <div class="plus-h-icon"><span class="plus-hi-chat"></span></div>
                <div class="plus-h-text">
                    <div class="plus-ht-title"><span>Онлайн-чат</span></div>
                    <div class="plus-ht-desc">
                        <span>Общайтесь с продавцом</span>
                        <span>или покупателем</span>
                        <span>в реальном времени.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="plus-mobile-bg plus-mobile-bg-7">
            <div class="plus-tablet-bg"></div>
            <div class="plus-mobile-bg-radius-left"></div>
            <div class="plus-h-item">
                <div class="plus-h-icon"><span class="plus-hi-barter"></span></div>
                <div class="plus-h-text">
                    <div class="plus-ht-title"><span>Бартер</span></div>
                    <div class="plus-ht-desc">
                        <span>Объменяй ненужные</span>
                        <span>вещи на нужные</span>
                        <span>не выходя из дома.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="plus-mobile-bg plus-mobile-bg-8">
            <div class="plus-tablet-bg"></div>
            <div class="plus-mobile-bg-line-bottom left"></div>
            <div class="plus-mobile-bg-radius-right min"></div>
            <div class="plus-h-item">
                <div class="plus-h-icon plus_badge_container"><span class="badge__icon badge__icon-security"></span></div>
                <div class="plus-h-text">
                    <div class="plus-ht-title">
                        <span>Безопасная</span>
                        <span>сделка</span>
                    </div>
                    <div class="plus-ht-desc">
                        <span>Обезопась покупку</span>
                        <span>и продажу товаров</span>
                        <span>или услуг</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>