<div class="section home-store">
    <div class="home-icon"></div>
    <div class="home-info">
        <div class="home-i-title">Бесплатное</div>
        <div class="home-i-title">Мобильное приложение</div>
        <div class="home-i-desc">
            <p>Все самые интересные объявления и услуги в вашем телефону.</p>
            <p>Быстрый и удобный доступ к вашим объявления и услугам.</p>
            <p>Моментальные оповещения о новых собщениях и предложениях.</p>
            <p>Будьте в курсе находясь в любой точке страны!</p>
            <p>Размещайте новые объявления и услуги со своего телефона!</p>
        </div>
        <div class="home-store-link">
            <a href="#" class="home-s-app"></a>
            <a href="#" class="home-s-play"></a>
        </div>
    </div>
</div>