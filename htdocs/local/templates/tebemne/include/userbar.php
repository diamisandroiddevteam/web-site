<? 
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

global $USER;
if($USER->IsAuthorized()):
	?>
	<div class="userbar dropdown">
		<div class="dropdown-btn" data-toggle="dropdown">
			<span class="gn-icon"></span>
			<span class="dropdown-title">Мой кабинет</span>
		</div>
		<ul class="dropdown-menu">
            <? if($USER->IsAdmin()):?>
                <li class="dropdown-item">
                    <a href="/bitrix/admin/diamis_ads_category.php" target="_blank" class="dropdown-link">
                        <span class="dropdown-item-icon"></span>
                        <span class="dropdown-item-title">Панель Управления</span>
                    </a>
                </li>
            <? endif;?>
			<li class="dropdown-item">
				<a href="/profile/ads/" class="dropdown-link">
					<span class="dropdown-item-icon"></span>
					<span class="dropdown-item-title">Мои объявления</span>
				</a>
			</li>
			<li class="dropdown-item">
				<a href="/profile/order/" class="dropdown-link">
					<span class="dropdown-item-icon"></span>
					<span class="dropdown-item-title">Мои заказы</span>
				</a>
			</li>
			<li class="dropdown-item">
				<a href="/profile/message/" class="dropdown-link">
					<span class="dropdown-item-icon"></span>
					<span class="dropdown-item-title">Мои сообщения</span>
				</a>
			</li>
			<li class="dropdown-item">
				<a href="/profile/company/" class="dropdown-link">
					<span class="dropdown-item-icon"></span>
					<span class="dropdown-item-title">Моя компания</span>
				</a>
			</li>
			<li class="dropdown-item">
				<a href="/profile/barter/" class="dropdown-link">
					<span class="dropdown-item-icon"></span>
					<span class="dropdown-item-title">Мои настройки</span>
				</a>
			</li>
            <li class="dropdown-item">
                <a href="?logout=yes" class="dropdown-link">
                    <span class="dropdown-item-icon"></span>
                    <span class="dropdown-item-title">Выход</span>
                </a>
            </li>
		</ul>
	</div>	
	<? 
endif;