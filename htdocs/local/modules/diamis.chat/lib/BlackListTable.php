<?php
namespace Diamis\Chat;

use \Bitrix\Main\Entity;


class BlackListTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'diamis_chat_black_list';
    }


    public static function getMap()
    {
        return array(
            new Entity\IntegerField('id', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\IntegerField('author', [
                'title' => 'Владелец'
            ]),
            new Entity\IntegerField('user_id', [
                'title' => 'ID пользователя'
            ]),
            new Entity\DatetimeField('date_created', [
                'title' => 'ID сущности'
            ])
        );
    }
}