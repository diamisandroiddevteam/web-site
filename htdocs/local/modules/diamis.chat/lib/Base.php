<?php
namespace Diamis\Chat;

use Bitrix\Main\ArgumentException;
use \Bitrix\Main\Entity;
use \Diamis\Ads\AdsTable;
use \Diamis\Chat\ChatTable;
use \Diamis\Chat\ContentTable;
use \Diamis\Chat\MessageTable;
use \Diamis\Chat\BlackListTable;
use \Exception;

class Base
{
    const TYPE_ADS = 1;


    public function getNameChate($type = 1){
        $arType = array(
            1 => 'Обявление',
        );

        return $arType[$type];
    }


    public function getElement($id) {
        return AdsTable::getElementID($id);
    }


    public function getMessages($entity_id, $to, $from, $type=null){
        $entity_id = intval($entity_id);
        $to = intval($to);
        $from = intval($from);
        if(!$type)
            $type = self::TYPE_ADS;

        $arMessage = array();
        try {
            $db = MessageTable::getList(array(
                'select' => array(
                    'id',
                    'date_created',
                    'date_update',
                    'to_id',
                    'from_id',

                    'chat_name' => 'chat.name',
                    'chat_type' => 'chat.type',
                    'chat_entity' => 'chat.entity_id',

                    'content_value' => 'content.content',
                ),
                'filter' => array(
                    'to_id' => $to,
                    'from_id' => $from,
                    'chat_entity' => $entity_id
                ),
                'runtime' => array(
                    new Entity\ReferenceField(
                        'content',
                        'Diamis\Chat\ContentTable',
                        array('this.content_id' => 'ref.id')
                    ),
                    new Entity\ReferenceField(
                        'chat',
                        'Diamis\Chat\ChatTable',
                        array('this.chat_id' => 'ref.id')
                    )
                ),
                'order' => array('date_created' => 'DESC'),
                'limit' => 50
            ));


            while($item = $db->fetch()){
                $arMessage[] = $item;
            }

            sort($arMessage);

        } catch (ArgumentException $e) { }

        return $arMessage;
    }


    public function setMessage($id, $to, $from, $content, $type=null) {
        $result = null;

        $id = intval($id);
        if(!$type)
            $type = self::TYPE_ADS;


        if(!$chat_id = self::isChat($id, $type)) {
            $db = ChatTable::add(array(
                'name' => self::getNameChate($type),
                'type' => $type,
                'entity_id' => $id
            ));
            $chat_id = $db->getId();
        }

        $to = intval($to);
        $from = intval($from);
        $content = trim(strip_tags($content));

        if(self::isBlackList($to, $from)){
            throw new Exception('Вы не можете отправлять сообщения данному пользователю');
        }

        $res = ContentTable::add(array('content'=>$content));
        if($res->isSuccess()){
            $content_id = $res->getId();
            $res = MessageTable::add(array(
                'to_id' => $to,
                'from_id' => $from,
                'content_id' => $content_id,
                'chat_id' => $chat_id
            ));
            if($res->isSuccess()) $result = $res->getId();
        }

        return $result;
    }


    public function isBlackList($user_id, $author) {

        $user_id = intval($user_id);
        $author = intval($author);

        $res = BlackListTable::getList(array(
            'select' => array('id','date_created'),
            'filter' => array('author' => $author, 'user_id'=>$user_id),
            'limit' => 1
        ))->fetch();

        if($res['id']) return $res['date_created'];

        return false;
    }


    public function getChat($author) {
        global $DB;
        $arChat = array();
        $author = intval($author);

        $table = MessageTable::getTableName();
        $tableChat = ChatTable::getTableName();
        $sql = "
        SELECT
          `message`.`id` as `id`,
          `message`.`content_id` as `content_id`,
          `message`.`chat_id` as `chat_id`,
          `chat`.`name` as `chat_name`,
          `chat`.`type` as `chat_type`,
          `chat`.`entity_id` as `chat_entity_id`              
        FROM
          `{$table}` as `message`
        LEFT JOIN `{$tableChat}` as chat ON `chat`.`id` = `message`.`chat_id` 
        WHERE
          `message`.`from_id` = '{$author}'
        GROUP BY `message`.`chat_id`          
        ";

        $res = $DB->Query($sql);
        while($item = $res->Fetch())
            $arChat[] = $item;

        return $arChat;
    }


    public function getBlackList($author) {

        $author = intval($author);
        if(!$author)
            throw new Exception("ID владельца не передан");


        $res = BlackListTable::getList(array(
            'select' => array('*'),
            'filter' => array('author' => $author),
        ));

        return $res->fetchAll();
    }


    // Проверяем на существование чата
    public function isChat($id, $type) {
        $result = false;
        $db = ChatTable::getList(array(
            'select' => array('id'),
            'filter' => array(
                'type' => intval($type),
                'entity_id' => intval($id)
            ),
            'limit' => 1
        ))->fetch();


        if(count($db['id']))
            $result = $db['id'];


        return $result;
    }
}