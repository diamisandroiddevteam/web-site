<?php
namespace Diamis\Chat;

use \Bitrix\Main\Entity;


class ChatTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'diamis_chat';
    }


    public static function getMap()
    {
        return array(
            new Entity\IntegerField('id', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\StringField('name', [
                'title' => 'Название чата'
            ]),
            new Entity\IntegerField('type', [
                'title' => 'Тип чата',
                'default_value' => 1
            ]),
            new Entity\IntegerField('entity_id', [
                'title' => 'ID сущности'
            ])
        );
    }
}