<?php
namespace Diamis\Chat;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

use \Exception;


class MessageTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'diamis_chat_message';
    }


    public static function getMap()
    {
        return array(
            new Entity\IntegerField('id', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\IntegerField('chat_id', [
                'title' => 'ID чата'
            ]),
            new Entity\IntegerField('to_id', [
                'title' => 'Отправитель'
            ]),
            new Entity\IntegerField('from_id', [
                'title' => 'Получатель'
            ]),
            new Entity\IntegerField('content_id', [
                'title' => 'ID Контента'
            ]),
            new Entity\DatetimeField('date_created', [
                'title' => 'Дата создания',
                'default_value' => function () {
                    $day = date('Y-m-d h:i:s');
                    return new Type\DateTime($day, 'Y-m-d h:i:s');
                }
            ]),
            new Entity\DatetimeField('date_update', [
                'title' => 'Дата редактирования'
            ])
        );
    }
}