<?php
namespace Diamis\Chat;

use \Bitrix\Main\Entity;

use \Exception;


class ContentTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'diamis_chat_content';
    }


    public static function getMap()
    {
        return array(
            new Entity\IntegerField('id', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\TextField('content', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_CREATE')
            ])
        );
    }
}