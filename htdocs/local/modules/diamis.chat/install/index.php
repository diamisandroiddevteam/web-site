<?php
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

Loc::loadMessages(__FILE__);


Class diamis_chat extends CModule
{
    var $MODULE_ID = "diamis.chat";
    var $MODULE_NAME;
    var $MODULE_DIR;
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_DESCRIPTION;

    var $PARTNER_NAME;
    var $PARTNER_URI;

    function __construct()
    {
        $this->MODULE_PATH = str_replace('\\', '/', realpath(dirname(__FILE__) . "/.."));

        $arModuleVersion = array();
        include($this->MODULE_DIR . '/install/version.php');

        $this->MODULE_NAME = Loc::getMessage('DIAMIS_CHAT_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('DIAMIS_CHAT_MODULE_DESCRIPTION');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

        $this->PARTNER_NAME = Loc::getMessage('DIAMIS_CHAT_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('DIAMIS_CHAT_PARTNER_URI');
    }


    // Проверяем поддрежку D7
    function isVersionD7()
    {
        global $APPLICATION;

        if($status = CheckVersion(ModuleManager::getVersion('main'), '14.00.00')) {
            $APPLICATION->ThrowException(Loc::getMessage('DIAMIS_CHAT_ERROR_VERCION_D7'));
        }
        return $status;
    }


    function DoInstall()
    {
        global $USER;

        if ($this->isVersionD7() && $USER->IsAdmin()) {
            ModuleManager::registerModule($this->MODULE_ID);
            $this->InstallFiles();
        }
    }

    function DoUninstall()
    {
        global $USER;

        if ($this->isVersionD7() && $USER->isAdmin()) {
            $this->UnInstallFiles();
            ModuleManager::unRegisterModule($this->MODULE_ID);
        }
    }

    function InstallFiles()
    {
        CopyDirFiles($this->MODULE_DIR."/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFiles($this->MODULE_DIR."/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
        return true;
    }
}