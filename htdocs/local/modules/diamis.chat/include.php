<?php

try {

    \Bitrix\Main\Loader::registerAutoLoadClasses(
    'diamis.chat',
    [
        '\Diamis\Chat\Base' => 'lib/Base.php',
        '\Diamis\Chat\ChatTable' => 'lib/ChatTable.php',
        '\Diamis\Chat\MessageTable' => 'lib/MessageTable.php',
        '\Diamis\Chat\ContentTable' => 'lib/ContentTable.php',
        '\Diamis\Chat\BlackListTable' => 'lib/BlackListTable.php',
    ]);

} catch (\Bitrix\Main\LoaderException $e) {

}