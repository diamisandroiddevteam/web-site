<?php
IncludeModuleLangFile(__FILE__);

if($APPLICATION->GetGroupRight("diamis.chat")>"D")
{
    if(is_file($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php")):
        require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php");
    else:
        require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/prolog.php");
    endif;


    $aMenu = array(
        "parent_menu" => "global_menu_services",
        "section" => ADMIN_MODULE_NAME,
        "sort" => 12,
        "module_id" => ADMIN_MODULE_NAME,
        "text" => 'Онлайн Чат',
        "title"=> 'Онлайн Чат',
        "icon" => "sys_menu_icon",
        "page_icon" => "sys_menu_icon",
        "items_id" => "diamis_menu_chat",
        "items" => array(
            array(
                "text" => 'Сообщения',
                "title" => 'Сообщения',
                "url" => "diamis_chat.php?lang=".LANGUAGE_ID,
                "more_url" => array('diamis_chat.php', 'diamis_chat_edit.php'),
            ),
        )
    );

    return $aMenu;
}

return false;