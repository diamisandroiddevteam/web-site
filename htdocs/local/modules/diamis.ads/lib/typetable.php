<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;


IncludeModuleLangFile(__FILE__);

class TypeTable extends Entity\DataManager
{


	public static function getTableName() 
	{
		return 'diamis_ads_type';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\IntegerField('CATEGORY_ID', [
                'required' => true,
                'default_value' => 0,
                'title' => GetMessage('DIAMIS_ADS_TYPE_TABLE_CATEGORY')
            ]),
            new Entity\StringField('NAME', [
                'required' => true,
                'title' => GetMessage('DIAMIS_ADS_TYPE_TABLE_NAME'),
                'default_value' => 'text'
            ]),
            new Entity\StringField('CODE', [
                'required' => true,
                'title' => GetMessage('DIAMIS_ADS_TYPE_TABLE_CODE'),
                'default_value' => 'text'
            ]),
            new Entity\BooleanField('ACTIVE', [
                'default_value' => false,
                'title' => GetMessage('DIAMIS_ADS_TYPE_TABLE_ACTIVE')
            ]),
            new Entity\IntegerField('SORT', [
                'title' => GetMessage('DIAMIS_ADS_TYPE_TABLE_SORT')
        	]),

            new Entity\ReferenceField(
                'CATEGORY',
                '\Diamis\Ads\CategoryTable',
                array('=this.CATEGORY_ID' => 'ref.ID')
            ),
		);
	}

    public static function getTypeCode($code)
    {
        $arType = TypeTable::getList(array(
            'filter' => array('CODE' => $code),
            'limit' => 1
        ))->fetch();
        return $arType;
    }


    public static function onBeforeAdd(Entity\Event $event)
    {
        $result = new Entity\EventResult;
        $data = $event->getParameter("fields");

        $code = trim($data['CODE']);
        if(strlen($code)<=0 && isset($data['NAME'])) {
            $code = Base::translite($data['NAME']);
            $result->modifyFields(array('CODE' => $code));
        }

        return $result;
    }
}