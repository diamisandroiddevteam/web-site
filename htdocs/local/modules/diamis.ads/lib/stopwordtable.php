<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

IncludeModuleLangFile(__FILE__);

class StopWordTable extends Entity\DataManager
{


	public static function getTableName() 
	{
		return 'diamis_ads_stop_word';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\StringField('WORD', [
            	'required' => true,
                'title' => GetMessage('DIAMIS_ADS_STOPWORD_TABLE_WORD')
    		]),
        );
	}
}