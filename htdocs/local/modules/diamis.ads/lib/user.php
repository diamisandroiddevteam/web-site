<?
namespace Diamis\Ads;

use Bitrix\Main\ArgumentException;
use \Diamis\Ads\AdsTable;

Class User {

    public static function getProp($arUserID)
    {
        $arUserProp = array();
        if(count($arUserID))
        {
            $filter = array("ID"=>implode("|", $arUserID));
            $arParameters = array("FIELDS"=>array(), "SELECT"=>array("UF_*"));


        }

        return $arUserProp;
    }

    /** Проверяет являеться авторизованный пользователь автором или нет
     * @param $ADS_ID - ID объявления
     * @return bool
     */
    public static function inAuthor($ADS_ID) {
        global $USER;

        $inAuthor = false;

        try {
            $USER_ID = $USER->GetID();

            $ads = AdsTable::getList(array(
                'select' => array('ID'),
                'filter' => array(
                    'ID' => intval($ADS_ID),
                    'USER_ID' => $USER_ID
                ),
                'limit' => 1
            ));

            $inAuthor = $ads->getSelectedRowsCount()
                ? true
                : false;

        } catch (ArgumentException $e) {

        }
        return $inAuthor;
    }
}