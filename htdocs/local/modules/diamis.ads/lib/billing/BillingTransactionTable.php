<?php
namespace Diamis\Ads;

use Bitrix\Main\ArgumentException;
use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;


IncludeModuleLangFile(__FILE__);

class BillingTransactionTable extends Entity\DataManager
{
    public static $kodStatus = array(
        "F"=>"[F] Выполнена",
        "C"=>"[C] Отменена",
        "N"=>"[N] Новая",
        "R"=>"[R] Возврат"
    );


    public static function getTableName() {
        return 'diamis_ads_billing_transaction';
    }


    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\StringField('STATUS', [
                'title' => 'Статус'
            ]),
            new Entity\IntegerField('USER_ID', [
                'title' => 'Пользователь'
            ]),
            new Entity\StringField('SUM', [
                'title' => 'Сумма'
            ]),
            new Entity\StringField('TRANSACTION_ID', [
                'title' => 'Внешний ID'
            ]),
            new Entity\DatetimeField('CREATE_DATE', [
                'title' => 'Дата и Время',
                'default_value' => function () {
                    $lastFriday = date('Y-m-d h:i:s', strtotime('last friday'));
                    return new Type\Date($lastFriday, 'Y-m-d h:i:s');
                }
            ]),
            new Entity\IntegerField('CREATED_USER', [
                'title' => 'Пользователь'
            ]),
            new Entity\StringField('CREATED_REMOTE_ADDR', [
                'title' => 'Запрос'
            ]),
            new Entity\DatetimeField('RESPONSE_DATE', [
                'title' => 'Дата и Время Ответа',
                'default_value' => function () {
                    $lastFriday = date('Y-m-d h:i:s', strtotime('last friday'));
                    return new Type\Date($lastFriday, 'Y-m-d h:i:s');
                }
            ]),
            new Entity\StringField('RESPONSE_REMOTE_ADDR', [
                'title' => 'Ответ'
            ]),
            new Entity\StringField('DESCRIPTION', [
                'title' => 'Описание'
            ])
        );
    }
}