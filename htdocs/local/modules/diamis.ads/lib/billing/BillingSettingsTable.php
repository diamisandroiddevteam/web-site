<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;


IncludeModuleLangFile(__FILE__);

class BillingSettingsTable extends Entity\DataManager
{

    public static $kodStatus = array(
        "N"=>"[N] Новая",
        "F"=>"[F] Выполнена",
        "C"=>"[C] Отменена",
        "R"=>"[R] Возврат"
    );


    public static $entity = array(
        'ads' => 'Объявление',
        'user' =>'Аккаунт'
    );


    // Способы оплаты
    public static $methodYandex = array(
        'bank_card'      => 'Банковская карта',
        'yandex_money'   => 'Яндекс.Деньги',
        'sberbank'       => 'Сбербанк Онлайн',
        'qiwi'           => 'QIWI Wallet',
        'webmoney'       => 'Webmoney',
        'alfabank'       => 'Альфа-Клик',
        'mobile_balance' => 'Баланс мобильного телефона',
        'apple_pay'      => 'Криптограмма Apple Pay',
        'cash'           => 'Оплата наличными в терминале',
        'installments'   => 'Оплата через сервис «Заплатить по частям» (в кредит или рассрочку)',
    );


    public $errorYandex = array(
        'invalid_request'       => 'Неправильный запрос. Чаще всего этот статус выдается из-за нарушения правил взаимодействия с API.',
        'not_supported'         => 'Неправильный запрос. Чаще всего этот статус выдается из-за нарушения правил взаимодействия с API.',
        'invalid_credentials'   => 'Неверный идентификатор вашего аккаунта в Яндекс.Кассе или секретный ключ (имя пользователя и пароль при аутентификации).',
        'forbidden'             => 'Секретный ключ верный, но не хватает прав для совершения операции.',
        'not_found'             => 'Ресурс не найден.',
        'too_many_requests'     => 'Превышен лимит запросов в единицу времени. Попробуйте снизить интенсивность запросов (можно ориентироваться на поле retry_after — в нем указано количество миллисекунд, через которое стоит повторить запрос).',
        'internal_server_error' => 'На стороне Яндекс.Кассы что-то пошло не так.',
    );


    public static function getTableName() {
        return 'diamis_ads_billing_settings';
    }


    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\StringField('NAME', [
                'title' => 'Заголовок'
            ]),
            new Entity\StringField('CODE', [
                'title' => 'Символьный код'
            ]),
            new Entity\StringField('VALUE', [
                'title' => 'Значение'
            ])
        );
    }
}