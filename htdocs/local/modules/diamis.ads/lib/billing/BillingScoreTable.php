<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Diamis\Ads\Base as AdsBase;
use \Diamis\Ads\BillingTransactionTable;

IncludeModuleLangFile(__FILE__);

class BillingScoreTable extends Entity\DataManager
{
    public static function getTableName() {
        return 'diamis_ads_billing_score';
    }


    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\DatetimeField('DATE_CREATE', [
                'title' => 'Дата и Время',
                'default_value' => function () {
                    $lastFriday = date('Y-m-d d:i:s', strtotime('last friday'));
                    return new Type\Date($lastFriday, 'Y-m-d d:i:s');
                }
            ]),
            new Entity\IntegerField('USER_ID', [
                'title' => 'Пользователь'
            ]),
            new Entity\TextField('TEXT', [
                'title' => 'Описание'
            ]),
            new Entity\StringField('NAME', [
                'title' => 'Заголовок'
            ]),
            new Entity\StringField('PRICE', [
                'title' => 'Сумма'
            ]),
            new Entity\StringField('TYPE', [
                'title' => 'Метод Оплаты'
            ]),
            new Entity\StringField('STATUS', [
                'title' => 'Статус',
                'default_value' => 'N'
            ]),
            new Entity\StringField('ENTITY', [
                'title' => 'Сущьность'
            ]),
            new Entity\IntegerField('ENTITY_ID', [
                'title' => 'ID Сущьность'
            ]),
        );
    }

    public static function isScore($ID) {
        $db = BillingScoreTable::getList(array('filter'=>array('ID'=>$ID)));
        return $db->getSelectedRowsCount() ? true : false;
    }


    public static function createTransaction($idScore) {
        global $USER;

        $dataTransaction = null;

        $score = BillingScoreTable::getList(array(
            'filter' => array('ID'=>$idScore),
            'limit' => 1
        ))->fetch();

        if(count($score)) {
            $dataTransaction = array(
                'STATUS'        => 'N',
                'SUM'           => $score['PRICE'],
                'USER_ID'       => $score['USER_ID'] ? $score['USER_ID'] : $USER->GetID(),
                'TRANSACTION_ID'=> uniqid('', true),
                'DESCRIPTION'   => AdsBase::getUserIP(),
            );
            $db = BillingTransactionTable::add($dataTransaction);
            if($db->isSuccess()) {
                $scoreID = $score['ID'];
                $transactionID = $db->getId();
                $db = BillingTable::add(array('SCORE_ID'=> $scoreID, 'TRANSACTION_ID'=> $transactionID));
                if($db->isSuccess()) {
                    $result = $transactionID;
                }
            }
        }

        return $dataTransaction;
    }



    public static function onBeforeAdd(Entity\Event $event)
    {
        $result = new Entity\EventResult;
        $data = $event->getParameter("fields");

        $STATUS = trim(strip_tags($data['STATUS']));

        if (!$STATUS) {
            $result->modifyFields(array('STATUS' => "N"));
        }

        return $result;
    }



    public static function getYandexConfirmation($method) {
        $result = array();

        switch($method)
        {
            case 'back_card':
            case 'apple_pay':
                $result['confirmation'] = array();
                break;
            case 'bank_card':
            case 'yandex_money':
            case 'sberbank':
            case 'qiwi':
            case 'webmoney':
            case 'cash':
            case 'installments':
                $result['confirmation'] = array(
                    'type' => 'redirect',
                    'return_url' => 'https://diamis.ru/'
                );
                break;
            case 'mobile_balance ':
            case 'sberbank ':
            case 'alfabank ':
                $result['confirmation'] = array(
                    'type' => 'redirect',
                    'return_url' => 'https://diamis.ru/'
                );
                break;
        };

        return $result;
    }



    /**
     * Генерируем массив для отправки запроса в яндекс кассы
     * https://kassa.yandex.ru/docs/checkout-api/#sozdanie-platezha
     */
    public static function getYandexPrice($price) {
        return array(
            'amount' => array(
                'value' => $price,
                'currency' => "RUB"
            )
        );
    }

    /** Данные, необходимые для создания способа оплаты (payment_method),
     * которым будет платить пользователь. Подробнее про
     * https://kassa.yandex.ru/docs/guides/#sposoby-oplaty
     * @param $method
     * @return array
     */
    public static function getYandexMethod($method) {
        $result = array();
        if(isset(BillingSettingsTable::$methodYandex[$method])) {
            $result = array(
                'payment_method_data' => array(
                    'type' => $method
                )
            );
        }
        return $result;
    }
}