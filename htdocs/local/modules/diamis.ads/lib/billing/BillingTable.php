<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;

class BillingTable extends Entity\DataManager
{
    public static function getTableName() {
        return 'diamis_ads_billing';
    }


    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\IntegerField('SCORE_ID', [
                'title' => 'Пользователь'
            ]),
            new Entity\IntegerField('TRANSACTION_ID', [
                'title' => 'Пользователь'
            ]),
        );
    }
}
