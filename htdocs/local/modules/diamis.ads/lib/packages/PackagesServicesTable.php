<?php
namespace Diamis\Ads;

use Bitrix\Main\ArgumentException;
use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;


IncludeModuleLangFile(__FILE__);

class PackagesServicesTable extends Entity\DataManager
{
    public static $type = array(
        'ads' => 'Объявление'
    );

    public static $actions = array(
        'ads_update' => 'Обновление объявления',
        'ads_select' => 'Выделение объявления',
        'ads_right'  => 'Размещение в блоке с права',
        'ads_top'    => 'Спецразмещение',
    );


	public static function getTableName() 
	{
		return 'diamis_ads_packages_services';
	}

    //TODO Поддержка многоязычности
	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\BooleanField('ACTIVE', [
                'default_value' => false,
                'title' => 'Включен'
            ]),
            new Entity\StringField('TYPE', [
                'title' => 'Тип'
            ]),
            new Entity\StringField('ACTION', [
                'title' => 'Действие'
            ]),


            new Entity\StringField('NAME', [
                'title' => 'Название'
            ]),
            new Entity\TextField('TEXT', [
                'title' => 'Описание действия'
            ]),
            new Entity\StringField('VALUE', [
                'title' => 'Значение'
            ]),
            new Entity\IntegerField('SORT', [
                'title' => 'Сортировать'
            ])
		);
	}
}