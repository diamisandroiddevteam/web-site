<?php
namespace Diamis\Ads;

use Bitrix\Main\ArgumentException;
use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Diamis\Ads\PackagesCategoryTable;
use \Diamis\Ads\PackagesActionTable;


IncludeModuleLangFile(__FILE__);

class PackagesTable extends Entity\DataManager
{

    public static $type = array(
        'ads_package' => 'Пакет для объявления'
    );

	public static function getTableName() 
	{
		return 'diamis_ads_packages';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\BooleanField('ACTIVE', [
                'default_value' => false,
                'title' => "Активно"
            ]),
            new Entity\StringField('TYPE', [
                'title' => 'Тип Услуги'
            ]),
            new Entity\StringField('NAME', [
                'title' => 'Название услуги'
            ]),
            new Entity\TextField('TEXT', [
                'title' => 'Описание услуги'
            ]),
            new Entity\IntegerField('USER_ID', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_MODER_USER_ID')
            ]),
            new Entity\IntegerField('SORT', [
                'title' => "Сортировать"
            ]),
            new Entity\DatetimeField('DATE_CREATE', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_CREATE'),
                'default_value' => function () {
                    $lastFriday = date('Y-m-d', strtotime('last friday'));
                    return new Type\Date($lastFriday, 'Y-m-d');
                }
            ]),
            new Entity\DatetimeField('DATE_UPDATE', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_UPDATE'),
                'default_value' => function () {
                    $lastFriday = date('Y-m-d', strtotime('last friday'));
                    return new Type\Date($lastFriday, 'Y-m-d');
                }
            ]),
            new Entity\DatetimeField('DATE_EXPIRE', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_EXPIRE')
            ])
		);
	}


	public static function getPackagesID($ID) {
	    if(!$ID) return false;

	    $result = array();
        try {
            $dbAction = PackagesActionTable::getList(array(
                'select' => array(
                    'ID',
                    'ACTIVE',
                    'PACKAGE_ID',
                    'ACTION_ID',

                    'NAME' => 'PACKAGE.NAME',
                    'TEXT' => 'PACKAGE.TEXT',
                    'TYPE' => 'PACKAGE.TYPE',

                    'SETTING_ID'   => 'SETTING.ID',
                    'SETTING_NAME' => 'SETTING.NAME',
                    'SETTING_TEXT' => 'SETTING.TEXT',
                    'SETTING_TYPE' => 'SETTING.TYPE',
                    'SETTING_VALUE' => 'SETTING.VALUE',
                ),
                'filter' => array(
                    'PACKAGE_ID' => $ID
                )
            ));

            while($action = $dbAction->fetch()) {

                if(!$result) {
                    $result['ID']   = $action['PACKAGE_ID'];
                    $result['NAME'] = $action['NAME'];
                    $result['TEXT'] = $action['TEXT'];
                    $result['TYPE'] = $action['TYPE'];
                }

                $result['SETTINGS'][$action['ACTION_ID']] = array(
                    'ID' => $action['ACTION_ID'],
                    'NAME'  => $action['SETTING_NAME'],
                    'TEXT'  => $action['SETTING_TEXT'],
                    'TYPE'  => $action['SETTING_TYPE'],
                    'VALUE' => $action['SETTING_VALUE'],
                );
            }

        } catch (ArgumentException $e) {

        }
        return $result;

    }


    /** Возвращает массив Услуг с Действиям для выбранной категории
     * @param $ID integer ID категории
     * @return array
     */
	public static function getPackagesCategory($ID)
    {
        if(!$ID) return false;

        $result = array();
        try {
            $arPackage = array();
            $dbPackage = PackagesCategoryTable::getList(array(
                'select' => array(
                    'ID',
                    'ACTIVE',
                    'BONUS',
                    'PRICE',
                    'PACKAGE_ID' => 'PACKAGES_ID',
                    'NAME' => 'PACKAGE.NAME',
                    'TEXT' => 'PACKAGE.TEXT',
                    'TYPE' => 'PACKAGE.TYPE',
                ),
                'filter' => array(
                    'CATEGORY_ID' => $ID
                )
            ));

            $packageID = array();
            while($package = $dbPackage->fetch()) {
                $packageID[] = $package['PACKAGE_ID'];
                $arPackage[$package['PACKAGE_ID']] = $package;
            }


            $arAction = array();
            $dbAction = PackagesActionTable::getList(array(
                'select' => array(
                    'ID', 'ACTIVE', 'PACKAGE_ID',

                    'SETTING_ID'   => 'SETTING.ID',
                    'SETTING_NAME' => 'SETTING.NAME',
                    'SETTING_TEXT' => 'SETTING.TEXT',
                    'SETTING_TYPE' => 'SETTING.TYPE',
                    'SETTING_VALUE' => 'SETTING.VALUE',
                ),
                'filter' => array(
                    'PACKAGE_ID' => $packageID
                )
            ));

            while($action = $dbAction->fetch()) {

                $arAction[$action['PACKAGE_ID']]['SETTINGS'][$action['SETTING_ID']] = array(
                    'ID' => $action['ID'],
                    'NAME'  => $action['SETTING_NAME'],
                    'TEXT'  => $action['SETTING_TEXT'],
                    'TYPE'  => $action['SETTING_TYPE'],
                    'VALUE' => $action['SETTING_VALUE'],
                );
            }

            foreach($arPackage as $item):

                $result[] = isset($arAction[$item['PACKAGE_ID']]) ? array_merge($item,$arAction[$item['PACKAGE_ID']]) : $item;
            endforeach;

        } catch (ArgumentException $e) {

        }
        return $result;
    }
}