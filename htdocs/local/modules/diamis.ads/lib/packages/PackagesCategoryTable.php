<?php
namespace Diamis\Ads;

use Bitrix\Main\ArgumentException;
use \Bitrix\Main\Entity;
use Bitrix\Main\Entity\Event;
use \Bitrix\Main\Type;


IncludeModuleLangFile(__FILE__);

class PackagesCategoryTable extends Entity\DataManager
{

    public static $defProcent = 0.1;


	public static function getTableName()
	{
		return 'diamis_ads_packages_category';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\BooleanField('ACTIVE', [
                'default_value' => false,
                'title' => "Активно"
            ]),
            new Entity\IntegerField('CATEGORY_ID', [
                'title' => 'Категория'
            ]),
            new Entity\IntegerField('PACKAGES_ID', [
                'title' => 'Услгуга'
            ]),
            new Entity\StringField('BONUS', [
                'title' => 'Кол-во бонусов'
            ]),
            new Entity\StringField('PRICE', [
                'title' => 'Цена'
            ]),

            new Entity\ReferenceField(
                'PACKAGE',
                'Diamis\Ads\PackagesTable',
                array('=this.PACKAGES_ID' => 'ref.ID')
            ),

            new Entity\ReferenceField(
                'ACTIONS',
                'Diamis\Ads\PackagesActionTable',
                array('=this.PACKAGES_ID' => 'ref.PACKAGE_ID')
            )

		);
	}



	public static function onBeforeUpdate(Event $event)
    {
        $result = new Entity\EventResult;
        $data = $event->getParameter("fields");

        $bonus = trim($data['BONUS']);
        $price = trim($data['PRICE']);

        if ($price && !$bonus)
        {
            $bonus = $price / 100 * PackagesCategoryTable::$defProcent;
            $pos = stripos($bonus, '.');
            $bonus = substr($bonus, 0, $pos+4);
            $result->modifyFields(array('BONUS' => $bonus));
        }
        else if(!$price && !$bonus)
        {
            $result->modifyFields(array('BONUS' => 0));
            $result->modifyFields(array('PRICE' => 0));
        }

        return $result;
    }




    public static function onBeforeAdd(Entity\Event $event)
    {
        $result = new Entity\EventResult;
        $data = $event->getParameter("fields");

        $bonus = trim($data['BONUS']);
        $price = trim($data['PRICE']);

        if ($price && !$bonus)
        {
            $bonus = $price / 100 * PackagesCategoryTable::$defProcent;
            $pos = stripos($bonus, '.');
            $bonus = substr($bonus, 0, $pos+4);
            $result->modifyFields(array('BONUS' => $bonus));
        }
        else if(!$price && !$bonus)
        {
            $result->modifyFields(array('BONUS' => 0));
            $result->modifyFields(array('PRICE' => 0));
        }

        return $result;
    }
}