<?php
namespace Diamis\Ads;

use Bitrix\Main\ArgumentException;
use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Diamis\Ads\PackagesCategoryTable;


IncludeModuleLangFile(__FILE__);

class PackagesActionTable extends Entity\DataManager
{


	public static function getTableName() 
	{
		return 'diamis_ads_packages_action';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\BooleanField('ACTIVE', [
                'default_value' => false,
                'title' => "Активно"
            ]),
            new Entity\IntegerField('PACKAGE_ID', [
                'title' => 'Пакет'
            ]),
            new Entity\IntegerField('ACTION_ID', [
                'title' => 'Действие'
            ]),

            new Entity\ReferenceField(
                'PACKAGE',
                'Diamis\Ads\PackagesTable',
                array('=this.PACKAGE_ID' => 'ref.ID')
            ),

            new Entity\ReferenceField(
                'SETTING',
                'Diamis\Ads\PackagesServicesTable',
                array('=this.ACTION_ID' => 'ref.ID')
            )
		);
	}


}