<?php
namespace Diamis\Auth;


class SocialAuth {

    protected static $pathTmp = '/upload/tmp/';


    public static function getPathTmp(){
        return $_SERVER['DOCUMENT_ROOT'] . self::$pathTmp;
    }



    public static function post($url, $param)
    {
        $result = false;
        $curl = curl_init();
        if($curl)
        {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($param));
            $result = curl_exec($curl);
            curl_close($curl);
        }

        return $result;
    }

    public static function isHttps()
    {
        if(!empty($_SERVER['HTTPS']) && 'off' !== strtolower($_SERVER['HTTPS'])) {
            return true;
        }
        return false;
    }

    public function getPage()
    {
        $host = $_SERVER['HTTP_HOST'];
        $uri = $_SERVER['REQUEST_URI'];
        if(!empty($_SERVER['HTTPS']) && 'off' !== strtolower($_SERVER['HTTPS'])) {
            $proto = 'https://';
        } else {
            $proto = 'http://';
        }
        return $proto . $host . $uri;
    }


    /** Загрузка файла по url
     * @param $path
     * @return mixed
     */
    public function getFileUrl($path)
    {
        $result = array();
        if(preg_match("#^(http[s]?)://#", $path))
        {
            $urlComponents = parse_url($path);
            preg_match("#[^\\\\/]+$#", $urlComponents['path'], $match);
            $tmp_path = self::getPathTmp() . $match[0];

            $ch = curl_init($path);
            $fp = fopen($tmp_path, "wb");
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);

            $result['name'] = $match[0];
            $result['size'] = filesize($tmp_path);
            $result['tmp_name'] = $tmp_path;
            $result['type'] = image_type_to_mime_type(exif_imagetype($tmp_path));
        }

        return $result;
    }
}