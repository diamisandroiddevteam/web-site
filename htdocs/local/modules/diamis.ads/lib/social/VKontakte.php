<?php
namespace Diamis\Auth;

use Bitrix\Main\Context;
use Bitrix\Main\Config\Option;

use Cake\Database\Exception;
use Diamis\Auth\SocialAuth;



class VKontakte extends SocialAuth{

    const ID = "VKontakte";
    const AUTH_URL      = "https://oauth.vk.com/authorize";
    const TOKEN_URL     = "https://oauth.vk.com/access_token";
    const CONTACTS_URL  = "https://api.vk.com/method/users.get";
    const FRIENDS_URL   = "https://api.vk.com/method/friends.get";
    const MESSAGE_URL   = "https://api.vk.com/method/messages.send";
    const APP_URL       = "https://api.vk.com/method/apps.get";

    protected $v = '5.78'; // Версия API ВКонтакте

    protected $appID;
    protected $appSecret;

    protected $code = false;
    protected $access_token = false;
    protected $expires_in = false;
    protected $user_id = false;
    protected $email = false;

    protected $redirect = false;

    public $error = false;
    public $error_description = false;

    public function __construct()
    {
        $this->getRedirect();
        $settings = $this->getSettings();
        $this->appID = $settings[0];
        $this->appSecret = $settings[1];
    }


    public function getID()
    {
        return self::ID;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setAccessToken($access_token)
    {
        $this->access_token = $access_token;
    }

    public function setExpiresIn($expires_in)
    {
        $this->expires_in = $expires_in;
    }

    public function setUserID($id)
    {
        $this->user_id = intval($id);
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }


    /** Получение access_token
     * https://vk.com/dev/implicit_flow_user?f=3.%20%D0%9F%D0%BE%D0%BB%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D0%B5%20access_token
     * @param $redirect_uri
     * @return bool
     */
    public function getToken($redirect_uri)
    {
        if($this->code === false) return false;
        if($this->appID === false) return false;
        if($this->appSecret === false) return false;

        $query = array(
            'client_id' => $this->appID,
            'client_secret' => $this->appSecret,
            'code' => $this->code,
            'redirect_uri' => $redirect_uri,
        );

        $result = json_decode(parent::post(self::TOKEN_URL, $query), true);

        if ((isset($result["access_token"]) && $result["access_token"] !== '')
            && isset($result["user_id"])
            && $result["user_id"] <> ''
        ) {
            $this->access_token = $result["access_token"];
            $this->user_id = $result["user_id"];
            $this->email = $result["email"];

            $_SESSION["OAUTH_DATA"] = array("OATOKEN" => $this->access_token);

            return true;
        } else {
            $this->error = $result['error'];
            $this->error_description = $result['error_description'];

            throw new Exception('Возникла ошибка в работе с социальной сетью. Попробуйте воспользоваться сервисом позже или выбирите другой способ Аутентификации.');
        }

        return false;
    }


    public function setSettings($appid, $appsecret)
    {
        $this->appID = $appid;
        $this->appSecret = $appsecret;
    }

    public function getSettings()
    {
        return array(
            Option::get("socialservices", "vkontakte_appid_bx_site_".SITE_ID),
            Option::get("socialservices", "vkontakte_appsecret_bx_site_".SITE_ID)
        );
    }

    public function getRedirect()
    {
        $uri = parent::getPage();
        if(stripos($uri, '?')!==false){
            $uri = explode('?', $uri)[0];
        }

        $this->redirect = $uri . '?auth_service_id=' . self::ID;
    }


    /** Документация
     * https://vk.com/dev/implicit_flow_user?f=1.%20%D0%9E%D1%82%D0%BA%D1%80%D1%8B%D1%82%D0%B8%D0%B5%20%D0%B4%D0%B8%D0%B0%D0%BB%D0%BE%D0%B3%D0%B0%20%D0%B0%D0%B2%D1%82%D0%BE%D1%80%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8
     * @return string
     */
    public function getAuthUrl() {
        $query = array(
            'client_id' => $this->appID,
            'redirect_uri' => $this->redirect,
            'display' => 'popup',
            'scope' => 'friends,offline,email',
            'v' => $this->v,
            'response_type' => 'code'
        );

        return self::AUTH_URL . '?' . urldecode(http_build_query($query));
    }

    public function getFormHtml() {
        $link = $this->getAuthUrl();
        return array(
            'link' => $link,
            'bx_html' => '<a href="javascript:void(0)" onclick="BX.util.popup(\'' . $link . '\', 660, 425)" class="vkontakte">'.$this->getID().'</a>',
            'bx_onclick' => 'onclick="BX.util.popup(\'' . $link . '\', 660, 425)"'
        );
    }


    /**
     * Выполняем обработку url страници от соц.сети ВКонтакте
     */
    public function authorize()
    {
        $result = array();
        if ((isset($_REQUEST["code"]) && $_REQUEST["code"] !== ''))
        {
            $GLOBALS["APPLICATION"]->RestartBuffer();

            $this->setCode($_REQUEST['code']);
            if($this->getToken($this->redirect))
            {
                $arUser = $this->getCurrentUser();
                if(is_array($arUser))
                {
                    $result = $this->prepareUser($arUser);
                }
            };
        }
        return $result;
    }


    /**
     * Подготовливаем полученные данные из соц. сети
     * для дальнейшей регистрации / авторизации пользователя
     * возвращает массив полей для CMS Bitrix
     * @param $arUser
     * @return array
     */
    public function prepareUser($arUser)
    {
        $fields = $arUser['response']['0'];

        // Указываем пол
        if(isset($fields['sex']) && $fields['sex'] != '')
        {
            if ($fields['sex'] == '2') $fields['sex'] = 'M';
            elseif ($fields['sex'] == '1') $fields['sex'] = 'F';
        }

        $result = array(
            'SITE_ID' => SITE_ID,
            'XML_ID' => self::getID(),
            'EXTERNAL_AUTH_ID' => $this->user_id,
            'LOGIN' => "VKuser" . $this->user_id,
            'EMAIL' => $this->email,
            'NAME' => $fields['first_name'],
            'LAST_NAME' => $fields['last_name'],
            'PERSONAL_GENDER' => $fields['sex'],
            'OATOKEN' => $this->access_token,
            'OATOKEN_EXPIRES' => $this->expires_in,
            'PERSONAL_WWW' => 'https://vk.com/id'.$this->user_id
        );

        // Получаем аватарку
        if(isset($fields['photo_max_orig']))
        {
            $arPic = parent::getFileUrl($fields['photo_max_orig']);
            $result["PERSONAL_PHOTO"] = $arPic;
        }

        // Дата рождения
        if(isset($fields['bdate'])) {
            $result['PERSONAL_BIRTHDAY'] = $fields['bdate'];
        }

        return $result;
    }


    /**
     * Получаем данные пользователя
     *
     */

    public function getCurrentUser()
    {
        $query = array(
            'v' => $this->v,
            'user_id' => $this->user_id,
            'fields' => 'uid,first_name,last_name,nickname,screen_name,sex,bdate,city,country,timezone,photo,photo_medium,photo_max_orig,photo_rec,email',
            'access_token' => $this->access_token
        );

        $result = json_decode(parent::post(self::CONTACTS_URL, $query), true);
        if($result['error'] && $result['error_description'])
        {
            $this->error = $result['error'];
            $this->error_description = $result['error_description'];

            throw new Exception('Возникла ошибка в работе с социальной сетью. Попробуйте воспользоваться сервисом позже или выбирите другой способ Аутентификации.');
        } else {
            return $result;
        }
    }

}