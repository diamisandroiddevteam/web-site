<?php
namespace Diamis\Auth;


use Bitrix\Main\Config\Option;
use Diamis\Auth\SocialAuth;

use Cake\Database\Exception;


class Odnoklassniki extends SocialAuth{

    const ID = "Odnoklassniki";
    const AUTH_URL = "https://www.odnoklassniki.ru/oauth/authorize";
    const TOKEN_URL = "https://api.odnoklassniki.ru/oauth/token.do";
    const CONTACTS_URL = "https://api.odnoklassniki.ru/fb.do";

    protected $appID;
    protected $appSecret;
    protected $appKey;

    protected $code = false;
    protected $access_token = false;
    protected $refresh_token = false; // маркер обновления информации
    protected $sign = false;
    protected $user_id = false;

    protected $redirect = false;

    public $error = false;
    public $error_description = false;

    public function __construct()
    {
        $this->getRedirect();
        $settings = $this->getSettings();
        $this->appID = $settings[0];
        $this->appKey = $settings[1];
        $this->appSecret = $settings[2];
    }


    public function getID()
    {
        return self::ID;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setAccessToken($access_token)
    {
        $this->access_token = $access_token;
    }

    public function setUserID($id)
    {
        $this->user_id = intval($id);
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }


    public function getRedirect()
    {
        $uri = parent::getPage();
        if(stripos($uri, '?')!==false){
            $uri = explode('?', $uri)[0];
        }

        $this->redirect = $uri . '?auth_service_id=' . self::ID;
    }


    public function GetSettings()
    {
        return array(
            Option::get("socialservices", "odnoklassniki_appid_bx_site_".SITE_ID),
            Option::get("socialservices", "odnoklassniki_appkey_bx_site_".SITE_ID),
            Option::get("socialservices", "odnoklassniki_appsecret_bx_site_".SITE_ID)
        );
    }



    public function getAuthUrl() {
        $query = array(
            'client_id' => $this->appID,
            'redirect_uri' => $this->redirect,
            'response_type' => 'code'
        );

        return self::AUTH_URL . '?' . urldecode(http_build_query($query));
    }


    /**
     * Выполняем обработку url страници от соц.сети ВКонтакте
     */
    public function authorize()
    {
        $result = array();
        if ((isset($_REQUEST["code"]) && $_REQUEST["code"] !== ''))
        {
            $GLOBALS["APPLICATION"]->RestartBuffer();

            $this->setCode($_REQUEST['code']);
            if($this->getToken($this->redirect))
            {
                $arUser = $this->getCurrentUser();
                $result = $this->prepareUser($arUser);
            }
        }
        return $result;
    }


    public function getFormHtml() {
        $link = $this->getAuthUrl();
        return array(
            'link' => $link,
            'bx_html' => '<a href="javascript:void(0)" onclick="BX.util.popup(\'' . $link . '\', 660, 425)" class="odnoklassniki">'.$this->getID().'</a>',
            'bx_onclick' => 'onclick="BX.util.popup(\'' . $link . '\', 660, 425)"'
        );
    }


    /**
     * @param $redirect_uri
     * @return bool
     * @throws Exception
     */
    public function getToken($redirect_uri)
    {
        if($this->code === false) return false;
        if($this->appID === false) return false;
        if($this->appSecret === false) return false;

        $query = array(
            "code" => $this->code,
            "client_id" => $this->appID,
            "client_secret" => $this->appSecret,
            "redirect_uri" => $redirect_uri,
            "grant_type" => "authorization_code",
        );

        $result = json_decode(parent::post(self::TOKEN_URL, $query), true);

        if(isset($result["access_token"]) && $result["access_token"] !== '')
        {
            $this->access_token = $result["access_token"];
            $_SESSION["OAUTH_DATA"] = array("OATOKEN" => $this->access_token);
            if(isset($result["refresh_token"]) && $result["refresh_token"] !== '')
            {
                $this->refresh_token = $result["refresh_token"];
                $_SESSION["OAUTH_DATA"]["REFRESH_TOKEN"] = $this->refresh_token;
            }

            return true;
        } else {
            $this->error = $result['error'];
            $this->error_description = $result['error_description'];

            throw new Exception('Возникла ошибка в работе с социальной сетью. Попробуйте воспользоваться сервисом позже или выбирите другой способ Аутентификации.');
        }

        return false;
    }


    /**
     * Получаем данные пользователя
     *
     */

    public function getCurrentUser()
    {
        $query = array(
            'method' => 'users.getCurrentUser',
            'access_token' => $this->access_token,
            'application_key' => $this->appKey,
        );

        $sig = strtolower(md5('application_key='.$query['application_key'].'method='.$query['method'].md5($this->access_token.$this->appSecret)));
        $query['sig'] = $sig;

        $result = json_decode(parent::post(self::CONTACTS_URL, $query), true);

        if(!is_array($result) || $result['uid']=='')
        {
            $this->error = $result['error'];
            $this->error_description = $result['error_description'];

            throw new Exception('Возникла ошибка в работе с социальной сетью. Попробуйте воспользоваться сервисом позже или выбирите другой способ Аутентификации.');
        } else {
            return $result;
        }
    }


    /**
     * Подготовливаем полученные данные из соц. сети
     * для дальнейшей регистрации / авторизации пользователя
     * возвращает массив полей для CMS Bitrix
     * @param $arUser
     * @return array
     */
    public function prepareUser($arUser)
    {
        $fields = $arUser;

        $this->setUserID($fields['uid']);

        // Указываем пол
        if(isset($fields['gender']) && $fields['gender'] != '')
        {
            if ($fields['gender'] == 'male') $fields['gender'] = 'M';
            elseif ($fields['gender'] == 'female') $fields['gender'] = 'F';
        }

        $result = array(
            'SITE_ID'   => SITE_ID,
            'XML_ID'    => self::getID(),
            'EXTERNAL_AUTH_ID' => $this->user_id,
            'LOGIN'     => "OKuser" . $this->user_id,
            'NAME'      => $fields['first_name'],
            'LAST_NAME' => $fields['last_name'],
            'PERSONAL_GENDER' => $fields['gender'],
            'OATOKEN' => $this->access_token,
            'REFRESH_TOKEN' => $this->refresh_token,
            'PERSONAL_CITY' => $fields['location']['city'],
            'PERSONAL_WWW' => 'https://ok.ru/profile/' . $this->user_id,
        );

        // Получаем аватарку
        if(isset($fields['pic_2']))
        {
            $arPic = parent::getFileUrl($fields['pic_2']);
            $result["PERSONAL_PHOTO"] = $arPic;
        }

        // Дата рождения
        if(isset($fields['birthday'])) {
            $date = \DateTime::createFromFormat('Y-m-d', $fields['birthday']);
            $result['PERSONAL_BIRTHDAY'] = $date->format('d.m.Y');
        }

        return $result;
    }
}