<?php
namespace Diamis\Auth;

use Bitrix\Main\Config\Option;
use Bitrix\Socialservices\UserTable;
use Cake\Database\Exception;

// BITRIX для сохранения CFIle
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

/**
 * Данный класс реализует прослоку между классами соц.сети и CRM 1C-Bitirx
 * Class SocialManager
 * @package Diamis\Auth
 */

class SocialManager {

    protected static $arAuthServices = array();

    protected $userID = null;
    protected $checkRestrictions = true;
    protected $allowChangeOwner = true;

    public function __construct()
    {
        if(!count(self::$arAuthServices)) {
            $suffix = self::OptionsSuffix();
            self::$arAuthServices = self::AppyUserSettings($suffix);
        }
    }

    /**
     * Получаем активные сервисы из Bitrix
     * на социальные сети
     */
    public function GetActiveAuthServices()
    {
        global $DB, $USER;

        if($USER->IsAuthorized())
        {
            $socialSelectUser = array();
            $user_id = $USER->GetID();
            $dbRes = $DB->Query("SELECT `XML_ID`, `USER_ID`, `ID` FROM `b_socialservices_user` WHERE `USER_ID`='".$user_id."'");
            while($item = $dbRes->Fetch()){
                $socialSelectUser[$item['XML_ID']] = $item;
            }
        }

        $result = array();
        foreach(self::$arAuthServices as $service=>$params) {
            if(file_exists(__DIR__ . '/' . $service . '.php')){
                $classes = 'Diamis\Auth\\'.$service;
                $controller = new $classes();
                $result[$service] = $controller->getFormHtml();
                $result[$service]['isAuth'] = ($socialSelectUser[$service] ? true : false);
            }
        }
        return $result;
    }


    public function Authorize($service_id)
    {

        if(isset(self::$arAuthServices[$service_id]))
        {
            $service = self::$arAuthServices[$service_id];
            if(file_exists(__DIR__ . '/' . $service["CLASS"] . '.php'))
            {
                try{
                    $classes = 'Diamis\Auth\\' . $service["CLASS"];
                    $controller = new $classes();

                    if(method_exists($controller, 'authorize') === false) {
                        throw new Exception('Method "authorize" is invalid');
                    }

                    $data = $controller->authorize(); // Получаем данные из соц.сети

                    $url = parse_url($controller->getPage());
                    $url['path'] = str_replace('registration.php','',$url['path']);
                    $url['path'] = str_replace('auth/','',$url['path']);

                    if(count($data))
                    {
                        $action = 'SocialAuthorize';
                        if(method_exists(self::class, $action) === false) {
                            throw new Exception('Method "'.$action.'" not found');
                        }

                        self::$action($data);
                    }
                    else
                    {
                        throw new Exception('DATA is invalid');
                    }


                    ?>
                    <script type="text/javascript">
                    if(window.opener) {
                        window.opener.location = '<?=($url['scheme'] . '://'. $url['host'] . $url['path']);?>';
                    }
                    window.close();
                    </script>
                    <?
                } catch (Exception $e) {
                    // Записываем Лог ошибки в файл
                    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/social.log', '['.date('d.m.Y h:i:s').'] Error '.$service_id.': '. $controller->error.' Description: '.$controller->error_description ."\n", FILE_APPEND);
                    ?>
                    <div class="error-message">
                        <i class="error-icon"></i>
                        <div class="error-content">
                            <div class="error-text">
                                <?=$e->getMessage();?>
                            </div>
                        </div>
                    </div>
                    <style>.error-message{
                            background-color: #fa3e3e;
                            border-radius: 3px;
                            border: 1px solid #fa3e3e;
                            overflow: hidden;
                            padding: 0 0 0 40px;
                            position: relative;
                        }
                        .error-icon{
                            width: 20px;
                            height: 20px;
                            position: relative;
                        }
                        .error-content{
                            background: #fff;
                            margin: 0;
                            padding: 9px 10px;
                            font-size: 14px;
                            line-height: 18px;
                        }
                        .error-text{
                            font-size: 12px;
                            line-height: 16px;
                        }</style>
                    <?
                }
                exit;
            }
        }

    }

    /**
     * Метод вызывается автоматически из $this->Authorize($service_id)
     * Позволяет авторизовывать и регистрировать пользователя через соц.сеть
     * в CRM 1С-Bitrix
     * @param $data
     * @return bool|int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    public static function SocialAuthorize($data)
    {
        global $USER, $APPLICATION;

        if(!isset($data['EXTERNAL_AUTH_ID']) || $data['EXTERNAL_AUTH_ID'] == '')
        {
            return false;
        }

        $oauthKeys = array();
        if(isset($data["OATOKEN"])) $oauthKeys["OATOKEN"] = $data["OATOKEN"];
        if(isset($data["OATOKEN_EXPIRES"])) $oauthKeys["OATOKEN_EXPIRES"] = $data["OATOKEN_EXPIRES"];

        $errorCode = SOCSERV_AUTHORISATION_ERROR;

        $dbSocUser = UserTable::getList(array(
            'select' => array("ID", "USER_ID", "ACTIVE" => "USER.ACTIVE"),
            'filter' => array(
                '=XML_ID' => $data['XML_ID'],
                '=EXTERNAL_AUTH_ID' => $data['EXTERNAL_AUTH_ID']
            ),
        ));
        $socservUser = $dbSocUser->fetch();


        if($USER->IsAuthorized())
        {
            // Если пользователь Авторизован и Данная соц сеть
            // ни закем не закреплена
            if(!$socservUser)
            {
                $data["USER_ID"] = $USER->GetID();
                $result = UserTable::add(UserTable::filterFields($data));
                $id = $result->getId();
            }
            else
            {
                $id = $socservUser['ID'];
                if($socservUser['USER_ID'] != $USER->GetID())
                {
                    // Меняем Владельца
                    $dbSocUser = UserTable::getList(array(
                        'filter' => array(
                            '=USER_ID' => $USER->GetID(),
                            '=EXTERNAL_AUTH_ID' => $data['EXTERNAL_AUTH_ID']
                        ),
                        'select' => array("ID")
                    ));
                    if($dbSocUser->fetch()){
                        return 1;
                    } else {
                        $oauthKeys['USER_ID'] = $USER->GetID();
                        $oauthKeys['CAN_DELETE'] = 'Y';
                    }

                }
            }

            if($_SESSION["OAUTH_DATA"] && is_array($_SESSION["OAUTH_DATA"]))
            {
                $oauthKeys = array_merge($oauthKeys, $_SESSION['OAUTH_DATA']);
                unset($_SESSION["OAUTH_DATA"]);
            }

            UserTable::update($id, $oauthKeys);
        }
        else
        {
            $entryId = 0;
            $USER_ID = 0;
            if($socservUser)
            {
                $entryId = $socservUser['ID'];
                if($socservUser["ACTIVE"] === 'Y')
                {
                    $USER_ID = $socservUser["USER_ID"];
                }
            }
            else
            {
                foreach(GetModuleEvents('socialservices', 'OnFindSocialservicesUser', true) as $event)
                {
                    $eventResult = ExecuteModuleEventEx($event, array(&$data));
                    if($eventResult > 0)
                    {
                        $USER_ID = $eventResult;
                        break;
                    }
                }


                if(!$USER_ID)
                {
                    // ХЗ что тут происходит
                    if (
                        Option::get("main", "new_user_registration", "N") == "Y"
                        && Option::get("socialservices", "allow_registration", "Y") == "Y"
                    )
                    {
                        $data['PASSWORD'] = randString(30); //not necessary but...
                        $data['LID'] = SITE_ID;

                        $def_group = Option::get('main', 'new_user_registration_def_group', '');
                        if($def_group <> '')
                        {
                            $data['GROUP_ID'] = explode(',', $def_group);
                        }

                        $userFields = $data;
                        $userFields["EXTERNAL_AUTH_ID"] = "socservices";
                        if(isset($userFields['PERSONAL_PHOTO']) && is_array($userFields['PERSONAL_PHOTO']))
                        {
                            $res = \CFile::CheckImageFile($userFields["PERSONAL_PHOTO"]);
                            if($res <> '')
                            {
                                unset($userFields['PERSONAL_PHOTO']);
                            }
                        }
                        $USER_ID = $USER->Add($userFields);
                    }
                    elseif(Option::get("main", "new_user_registration", "N") == "N")
                    {
                        $errorCode = SOCSERV_REGISTRATION_DENY;
                    }
                    $data['CAN_DELETE'] = 'N';
                }
            }

            if(isset($_SESSION["OAUTH_DATA"]) && is_array($_SESSION["OAUTH_DATA"]))
            {
                foreach ($_SESSION['OAUTH_DATA'] as $key => $value)
                {
                    $data[$key] = $value;
                }
                unset($_SESSION["OAUTH_DATA"]);
            }


            if($USER_ID > 0)
            {
                if($entryId > 0)
                {
                    UserTable::update($entryId, UserTable::filterFields($data));
                }
                else
                {
                    $data['USER_ID'] = $USER_ID;
                    UserTable::add(UserTable::filterFields($data));
                }

                if(isset($data["TIME_ZONE_OFFSET"]) && $data["TIME_ZONE_OFFSET"] !== null)
                {
                    CTimeZone::SetCookieValue($data["TIME_ZONE_OFFSET"]);
                }

                $USER->AuthorizeWithOtp($USER_ID);

                if($USER->IsJustAuthorized())
                {
                    foreach(GetModuleEvents("socialservices", "OnUserLoginSocserv", true) as $arEvent)
                    {
                        ExecuteModuleEventEx($arEvent, array($data));
                    }
                }
            }
            else
            {
                return $errorCode;
            }

            $APPLICATION->StoreCookies();
        }

        return true;
    }




    public static function OptionsSuffix()
    {
        $arUseOnSites = unserialize(Option::get("socialservices", "use_on_sites", ""));
        return ($arUseOnSites[SITE_ID] == "Y"? '_bx_site_'.SITE_ID : '');
    }



    public static function AppyUserSettings($suffix)
    {
        $arAuthServices = self::$arAuthServices;
        $arServices = unserialize(Option::get("socialservices", "auth_services".$suffix, ""));
        if(is_array($arServices))
        {
            $i = 0;
            foreach($arServices as $serv=>$active)
            {
                if($active==='Y')
                {
                    $arAuthServices[$serv]["CLASS"] = $serv;
                    $arAuthServices[$serv]["__sort"] = $i++;
                    $arAuthServices[$serv]["__active"] = ($active == "Y");
                }
            }
        }
        return $arAuthServices;
    }
}