<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;


IncludeModuleLangFile(__FILE__);

class FieldsGroupTable extends Entity\DataManager
{


	public static function getTableName() 
	{
		return 'diamis_ads_fields_group';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\StringField('NAME', [
                'required' => true,
                'title' => GetMessage('DIAMIS_ADS_FIELDS_GROUP_TABLE_NAME'),
                'default_value' => 'text'
            ]),
            new Entity\BooleanField('ACTIVE', [
                'default_value' => false,
                'title' => GetMessage('DIAMIS_ADS_FIELDS_GROUP_TABLE_ACTIVE')
            ]),
            new Entity\IntegerField('SORT', [
                'title' => GetMessage('DIAMIS_ADS_FIELDS_GROUP_TABLE_SORT')
        	]),
		);
	}
}