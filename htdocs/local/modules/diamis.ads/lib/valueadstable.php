<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

IncludeModuleLangFile(__FILE__);

class ValueAdsTable extends Entity\DataManager
{


	public static function getTableName() 
	{
		return 'diamis_ads_value_ads';
	}

	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\IntegerField('FIELD_ID', [
            	'required' => true,
            ]),
            new Entity\TextField('VALUE', [
            	'required' => true,
    		]),
        );
	}
}