<?php

namespace Diamis\Ads;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

use \Diamis\Ads\Base as AdsBase;


class CategoryTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'diamis_ads_category';
    }


    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\StringField('CODE', [
                'required' => true,
                'title' => 'Символьный код'
            ]),
            new Entity\StringField('NAME', [
                'required' => true,
                'title' => 'Название раздела'
            ]),
            new Entity\StringField('FILE_ID', [
                'title' => 'Изображение'
            ]),
            new Entity\BooleanField('ACTIVE', [
                'default_value' => false,
                'title' => 'Активность'
            ]),
            new Entity\IntegerField('SORT', [
                'required' => true,
                'default_value' => 100,
                'title' => 'Сортировка'
            ]),
            new Entity\IntegerField('PARENT', [
                'title' => 'Родительский раздел'
            ]),
            new Entity\TextField('META_TITLE',[
                'title' => 'META TITLE'
            ]),
            new Entity\TextField('META_DESCRIPTION',[
                'title' => 'META DESCRIPTION'
            ]),
            new Entity\TextField('META_KEYWORDS',[
                'title' => 'META KEYWORDS'
            ]),
            new Entity\TextField('TEXT',[
                'title' => 'Описание'
            ]),
        );
    }


    public static function getCategoryCode($code)
    {
        $arCategory = CategoryTable::getList(array(
            'filter' => array('CODE' => $code),
            'limit' => 1
        ))->fetch();
        return $arCategory;
    }


    public static function getTree ()
    {
        $arCategory = CategoryTable::getList()->fetchAll();
        return AdsBase::tree($arCategory);
    }


    public static function onBeforeAdd(Entity\Event $event)
    {
        $result = new Entity\EventResult;
        $data = $event->getParameter("fields");

        $code = trim($data['CODE']);
        if(strlen($code)<=0 && isset($data['NAME'])) {
            $code = Base::translite($data['NAME']);
            $result->modifyFields(array('CODE' => $code));
        }

        return $result;
    }
}