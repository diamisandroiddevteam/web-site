<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Entity\Event;
use \Diamis\Ads\FieldsGroupTable;
use \Diamis\Ads\FieldsGroupRelationsTable;
use \Diamis\Ads\FieldsEnumsTable;

IncludeModuleLangFile(__FILE__);

class FieldsTable extends Entity\DataManager
{

    public static $displaying = array(
        'default' => true,
        'color' => true,
    );

	public static $type = array(
		'number'    => true,
        'string'    => true,
        'text'      => true,
        'select'    => true,
		'file'      => true,
		'radio'   => true,
		'slider'    => true,
	);

	public static function getTableName() 
	{
		return 'diamis_ads_fields';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\StringField('TYPE', [
                'required' => true,
                'title' => GetMessage('DIAMIS_ADS_FIELDS_TABLE_TYPE'),
                'default_value' => 'text'
        	]),
            new Entity\StringField('NAME', [
                'required' => true,
                'title' => GetMessage('DIAMIS_ADS_FIELDS_TABLE_NAME'),
                'default_value' => 'text'
            ]),
        	new Entity\IntegerField('SORT', [
                'title' => GetMessage('DIAMIS_ADS_FIELDS_TABLE_SORT')
        	]),
        	new Entity\BooleanField('ACTIVE', [
                'default_value' => false,
                'title' => GetMessage('DIAMIS_ADS_FIELDS_TABLE_ACTIVE')
            ]),
            new Entity\BooleanField('MULTY',[
            	'default_value' => false,
            	'title' => GetMessage('DIAMIS_ADS_FIELDS_TABLE_MULTY')
        	]),
            new Entity\StringField('DISPLAY',[
                'title' => GetMessage('DIAMIS_ADS_FIELDS_TABLE_DISPLAY')
            ]),
            new Entity\StringField('HINT', [
                'title' => GetMessage('DIAMIS_ADS_FIELDS_TABLE_HINT')
            ]),

            new Entity\ReferenceField(
                'VALUE',
                'Diamis\Ads\FieldsEnumsTable',
                array('=this.ID' => 'ref.FIELD_ID')
            ),
		);
	}


	public static function getPropertyID(){

    }



	public static function updateGroup($FieldID, $Group = array())
    {
        if(!$FieldID) return false;

        $error = array();
        foreach($Group as $group)
        {
            $field = array(
                'FIELD_ID' => $FieldID,
                'GROUP_ID' => $group['GROUP_ID']
            );

            if($group['RELATIONS_ID'] && !$group['GROUP_ID'])
            {
                $dellRela = FieldsGroupRelationsTable::delete($group['RELATIONS_ID']);
                if(!$dellRela->isSuccess()) {
                    $error[] = $dellRela->getErrorMessages();
                }
            }
            else
            {
                $addRela = FieldsGroupRelationsTable::add($field);
                if(!$addRela->isSuccess()) {
                    $error[] = $addRela->getErrorMessages();
                }
            }
        }
        return $error;
    }



	public static function addGroup($FieldID, $Group)
    {
        if(!$FieldID || !count($Group)) return false;

        global $DB;

        $table = FieldsGroupRelationsTable::getTableName();
        $fields = array();
        foreach($Group as $group) {
            $fields[] = "'" . intval($FieldID) . "','" . intval($group) . "'";
        }
        $sql = "INSERT INTO `".$table."` (`FIELD_ID`,`GROUP_ID`) VALUES (".implode('),(',$fields).");";
        $DB->Query($sql);
    }


    public static function updateEnum($FieldID, $Enums = array())
    {
        $error = array();
        foreach($Enums as $enum)
        {
            $enum['ACTIVE'] = ($enum['ACTIVE'] ? true : false);
            $enum['FIELD_ID'] = $FieldID;

            $id = intval($enum['ID']);
            $val = trim($enum['VALUE']);
            if($id && strlen($val))
            {
                $updateEnum = FieldsEnumsTable::update($id, $enum);
                if(!$updateEnum->isSuccess()) {
                    $error[] = $updateEnum->getErrorMessages();
                }
            }
            else if(strlen($val))
            {
                $addEnum = FieldsEnumsTable::add($enum);
                if(!$addEnum->isSuccess()) {
                    $error[] = $addEnum->getErrorMessages();
                }
            }
        }
        return $error;
    }


    public static function addEnum($FieldID, $Enums = array())
    {
        if(!$FieldID || !count($Enums)) return false;

        $error = array();
        foreach($Enums as $enum):
            $val = trim($enum['VALUE']);
            if(strlen($val))
            {
                $enum['ACTIVE'] = ($enum['ACTIVE'] ? true : false);
                $enum['FIELD_ID'] = $FieldID;

                $addEnum = FieldsEnumsTable::add($enum);

                if(!$addEnum->isSuccess()){
                    $error[] = $addEnum->getErrorMessages();
                }
            }
        endforeach;

        return $error;
    }


    public static function addRelations($FieldID, $Relations=array())
    {
        $items = $Relations['CATEGORY'];

        $error = array();
        if(count($items))
        {
            foreach($items as $item)
            {
                $arFields = array(
                    'FIELD_ID' => $FieldID,
                    'CATEGORY_ID' => $item['ID']
                );

                if($item['TYPE'])
                {
                    foreach($item['TYPE'] as $typeId)
                    {
                        $arFields['TYPE_ID'] = $typeId;
                        $result = RelationsTable::add($arFields);


                        if(!$result->isSuccess()) {
                            $error[] = $result->getErrorMessages();
                        }
                    }
                }
                else
                {
                    $result = RelationsTable::add($arFields);


                    if(!$result->isSuccess()) {
                        $error[] = $result->getErrorMessages();
                    }
                }
            }
        }

        return $error;
    }



	public static function onBeforeUpdate(Entity\Event $event)
    {
        $result = new Entity\EventResult;

        return $result;
    }


    public static function onBeforeAdd(Entity\Event $event)
    {
        $result = new Entity\EventResult;

        return $result;
    }
}