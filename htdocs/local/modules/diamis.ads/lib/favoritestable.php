<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;
use \Diamis\Ads\AdsTable;
use \Exception;

class FavoritesTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'diamis_ads_favorites';
    }


    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\IntegerField('USER_ID', [
                'required' => true,
                'title' => 'ID пользователя'
            ]),
            new Entity\IntegerField('ELEMENT_ID', [
                'required' => true,
                'title' => 'ID элемента'
            ]),
            new Entity\StringField('TYPE', [
                'default_value' => 'favorites',
                'title' => 'Тип'
            ]),
        );
    }


    public function getFavorites($user_id = null) {
        global $USER, $DB;

        $favoriteID = array();
        $user_id = $USER->GetID();
        $sql = "SELECT `ID`,`ELEMENT_ID` FROM `" . self::getTableName() . "` WHERE `USER_ID`='" . intval($user_id) . "'";
        $results = $DB->Query($sql);
        while($item = $results->Fetch()){
            $favoriteID[] = $item['ELEMENT_ID'];
        };

        return $favoriteID;
    }

    public function count()
    {
        global $DB, $USER;
        if($USER->IsAuthorized()) {
            $user_id = $USER->GetID();
            $sql = "SELECT COUNT(*) as COUNT FROM `" . self::getTableName() . "` WHERE `USER_ID`='" . intval($user_id) . "'";
            $count = $DB->Query($sql)->Fetch();

            return $count['COUNT'];
        }
        return false;
    }


    public function updateFavorite($id, $user_id = null) {
        global $DB, $USER;

        if(!intval($id))
            throw new Exception("ID не передан");

        $isElement = AdsTable::getList(array(
            'select'=>array('ID'),
            'filter'=>array('ID'=>intval($id))
        ))->fetch();


        if(!count($isElement))
            throw new Exception("Элемент не существует");


        if(!$USER->IsAuthorized()) {
            throw new Exception("Для добавления объявлений в избранное, пожалуйсто авторизируйтесь или зарегистрируйтесь");
        }


        if(!$user_id)
            $user_id = $USER->GetID();

        $data = array(
            'USER_ID' => intval($user_id),
            'ELEMENT_ID' => intval($id),
        );


        $sql = "SELECT * FROM `".self::getTableName()."` WHERE `USER_ID`='".intval($user_id)."' AND `ELEMENT_ID`='".intval($id)."'";
        $isFavorites = $DB->Query($sql)->Fetch();

        if($isFavorites['ID']) {
            $type = 'delete';
            $db = self::delete($isFavorites['ID']);
        } else {
            $type = 'add';
            $db = self::add($data);
        }

        return array(
            'type' => $type,
            'count' =>  self::count()
        );
    }
}