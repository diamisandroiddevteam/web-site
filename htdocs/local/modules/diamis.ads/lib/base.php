<?php
namespace Diamis\Ads;


use \Bitrix\Main\Loader;

use \Diamis\Ads\CategoryTable;
use \Diamis\Ads\FilesTable;
use \Diamis\Ads\ValueAdsTable;
use \Diamis\Ads\ValueCategoryTable;
use \Diamis\Ads\RelationsTable;
use \Diamis\Ads\FieldsTable;
use \Diamis\Ads\FieldsEnumsTable;
use \Diamis\Ads\TypeTable;
use \Diamis\GeoIp\GeoBase;
use PhpOffice\PhpSpreadsheet\Calculation\Category;

class Base
{
    // Данный параметр задет процент от цены преобразуемый в бонус
    private static $bonus = 0.1;

    public static $MODULE_ID = "diamis.ads";


    public static function getBonusDefault(){
        return self::$bonus;
    }


    public static function moduleDir()
    {
        return str_replace('\\', '/', realpath(dirname(__FILE__) . "/.."));
    }


    /**
     * Формируем номер телефона для записи в базу
     */
    public static function formatPhoneNumber($phone)
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);
        if(strlen($phone)>6 && substr($phone, 0, 2)=='89') $phone = '7'.substr($phone,0,1);
        return intval($phone);
    }

    /** Возвращает текущий IP клиента
     * @return mixed IP клиента
     */
    public static function getUserIP() {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }

        return $ip;
    }

    /**  функция для обрезки текста */
    public static function crop($text, $length, $clearTags = true)
    {
        $text = trim(preg_replace("/ {2,}/"," ", $text));
        if($clearTags) $text = strip_tags($text);
        if($length<=0 || strlen($text)<$length)
            return $text;

        $out = mb_substr($text, 0, $length);
        $pos = mb_strrpos($out, ' ');

        if($pos)
            $out = mb_substr($out, 0, $pos).' ...';

        return $out;
    }


    /** Возвращает с обрамленным тегом каждого обзаца
     * @param $text
     * @return string
     */
    public static function concludeTag($text, $tagName='p' ){
        $result = '';
        $text = explode(PHP_EOL, strip_tags($text));
        foreach($text as $row) {
            if(trim($row)) $result .= "<".$tagName.">".$row."</".$tagName.">";
        }

        return $result;
    }


    /**
     * Формирует номер телефона
     * @param  string $phone телефонный номер
     * @return string
     */
    public static function formatPhone($phone = '')
    {
        if (empty($phone)) return '';

        $phone = preg_replace("/[^0-9A-Za-z]/", "", trim($phone));

        // конвертируем буквенный номер в цифровой
        if (!is_numeric($phone)) {
            $replace = array(
                '2' => array('a', 'b', 'c'),
                '3' => array('d', 'e', 'f'),
                '4' => array('g', 'h', 'i'),
                '5' => array('j', 'k', 'l'),
                '6' => array('m', 'n', 'o'),
                '7' => array('p', 'q', 'r', 's'),
                '8' => array('t', 'u', 'v'),
                '9' => array('w', 'x', 'y', 'z')
            );
            foreach ($replace as $digit => $letters) {
                $phone = str_ireplace($letters, $digit, $phone);
            }
        }

        if (strlen($phone) == 7) {
            return preg_replace("/([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "$1-$2", $phone);
        } elseif (strlen($phone) == 10) {
            return preg_replace("/([0-9a-zA-Z]{3})([0-9a-zA-Z]{3})([0-9a-zA-Z]{2})([0-9a-zA-Z]{2})/", "+7 ($1) $2-$3-$4", $phone);
        } elseif (strlen($phone) == 11) {
            return preg_replace("/([0-9a-zA-Z]{1})([0-9a-zA-Z]{3})([0-9a-zA-Z]{3})([0-9a-zA-Z]{2})([0-9a-zA-Z]{2})/", "+7 ($2) $3-$4-$5", $phone);
        }

        return $phone;
    }


    /**
     * Транслитерация строки
     * @param  string $str исходная строка
     * @return string      траслитизированная строка
     */
    public static function translite($str = '')
    {
        if (empty($str)) return '';

        $translite = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch', 'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z', 'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch', 'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        );

        $str = strip_tags(trim($str));
        $str = str_replace(array("\n", "\r"), " ", $str);
        $str = preg_replace("/\s+/", ' ', $str);
        $str = function_exists('mb_strtolower') ? mb_strtolower($str) : strtolower($str);
        $str = strtr($str, $translite);
        $str = preg_replace("~[^a-z0-9_-]+~u", '-', $str);

        return $str;
    }


    /**
     * Генератор случийных символов
     * @param  int $len длина случайных символов
     * @return string
     */
    public static function keygen($len = 8)
    {
        $keygen = '';
        $arr = array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'v', 'x', 'y', 'z',
            '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'
        );
        while (mb_strlen($keygen) < $len) {
            $key = rand(0, count($arr) - 1);
            $keygen .= $arr[$key];
        }
        return $keygen;
    }


    /**
     * Формируем дерево по ключу PARENT
     * @param  array $tree Массив содержащий ключь PARENT
     * @param  integer $parent_id Корневой раздел, по умолчанию 0
     * @param  integer $level Уровень вложенности
     * @return array              Возвращает массив в виде дерева
     */
    public static function tree($tree, $parent_id = 0, $level = 0)
    {
        $result = array();
        foreach ($tree as $row) {
            if ($row['PARENT'] == $parent_id) {
                if ($row['FILE_ID'] > 0 && !isset($row['FILE_PATH'])) {
                    $row['FILE_PATH'] = \CFile::GetPath($row['FILE_ID']);
                }

                $child = Base::tree($tree, $row['ID'], ($level + 1));
                $result[] = array_merge($row, array('LEVEL' => ($level + 1), 'CHILD' => $child));
            }
        }
        return $result;
    }


    /**
     * Осуществляем поиск по дереву
     * @param $tree
     * @param $field_id
     * @param $find
     * @return array
     */
    public static function findTree($tree, $field_id, $find, $level = 0)
    {
        $result = array();
        foreach ($tree as $row) {
            if ($row[$field_id] === $find) {
                $result = $row;
            }

            if (!count($result) && count($row['CHILD']) > 0) {
                $child = Base::findTree($row['CHILD'], $field_id, $find, $level);
                if (count($child))
                    return array_merge($row, array('LEVEL' => ($level + 1), 'CHILD' => $child));
            }

        }
        return $result;
    }

    /**
     * Формируем список с присвоенным уровнем вложенности по ключу PARENT
     * @param  array $tree Массив содержащий ключь PARENT
     * @param  integer $parent_id Корневой раздел, по умолчанию 0
     * @param  integer $level Уровень вложенности
     * @return array              Возвращает массив в виде списка
     */
    public static function lists($tree, $parent_id = 0, $level = 0)
    {
        $result = array();
        foreach ($tree as $row) {
            if ($row['PARENT'] == $parent_id) {

                $row['~NAME'] = str_pad('', $level * 3, '.') . $row['NAME'];

                $child = Base::lists($tree, $row['ID'], $level + 1);
                $result[] = array_merge($row, array('LEVLE' => $level + 1));

                if (count($child)) {
                    $result = array_merge($result, $child);
                }
            }
        }
        return $result;
    }







    /**
     * Сохраняет файл средствами CMS
     * @param  array $files массив $_FILES
     * @return array
     */
    public static function saveFiles($files = array(), $entity_id, $entity = 'ads')
    {
        if (!count($files) || !$entity_id) return false;

        $data = array();
        foreach ($files as $file) {
            // множественное сохранение
            // думаю есть лучшее решения для множественного сохранения (нет вермини, дедлайн!!!)
            foreach ($file['tmp_name'] as $field_id => $tmp_name) {
                if ($tmp_name) {
                    $f = array(
                        'name' => $file['name'][$field_id],
                        'size' => $file['size'][$field_id],
                        'tmp_name' => $file['tmp_name'][$field_id],
                        "type" => $file['type'][$field_id],
                        "old_file" => '',
                        "del" => '',
                        "MODULE_ID" => self::$MODULE_ID
                    );

                    $selects = FilesTable::getList(array(
                        'select' => array(
                            '*',
                            'MULTY' => 'FIELD.MULTY'
                        ),
                        'filter' => array(
                            'FIELD_ID' => $field_id,
                            'ENTITY' => $entity,
                            'ENTITY_ID' => $entity_id
                        )
                    ));
                    // удаляем если не мультифайл
                    while ($select = $selects->fetch()) {
                        if (!$select['MULTY']) {
                            \CFile::Delete($select['FILE_ID']);
                            FilesTable::delete($select['ID']);
                        }
                    }

                    $file_id = \CFile::SaveFile($f, self::$MODULE_ID);

                    if ($file_id > 0) {
                        $add = FilesTable::add(array(
                            'FIELD_ID' => $field_id,
                            'FILE_ID' => $file_id,
                            'ENTITY' => $entity,
                            'ENTITY_ID' => $entity_id
                        ));
                        $data[] = $file_id;
                    }
                }

            }
        }
        return $data;
    }

    /** Возвращает родительский элемент категории
     * @param $CategoryID
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function parentCategoryID($CategoryID)
    {
        $parent = CategoryTable::getList(array(
            'select' => array('ID'),
            'filter' => array('ID' => $CategoryID),
            'limit' => 1
        ))->fetch();
        return $parent['ID'];
    }


    /** Выводит список св-тв категории и ее наследников
     * @param $CategoryID integer ID категории
     * @param $all boolean Не учитывать значение "активно"
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    function getCategoryPropID($CategoryID, $all = false)
    {
        if (!$CategoryID) return false;

        $arFilter = array(
            "LOGIC" => "AND",
            array(
                "LOGIC" => "OR",
                array("CATEGORY_ID" => $CategoryID),
                array("CATEGORY.PARENT" => $CategoryID)
            )
        );

        // Выводим только активные свойства
        if (!$all)
            $arFilter['ACTIVE'] = true;


        $dbData = RelationsTable::getList(array(
            'select' => array(
                '*',
                'CATEGORY_NAME' => 'CATEGORY.NAME',
                'CATEGORY_CODE' => 'CATEGORY.CODE',
                'CATEGORY_PARENT' => 'CATEGORY.PARENT',
                'CATEGORY_ACTIVE' => 'CATEGORY.ACTIVE',
                'CATEGORY_SORT' => 'CATEGORY.SORT',

                'TYPE_NAME' => 'TYPE.NAME',
                'TYPE_CODE' => 'TYPE.CODE',
                'TYPE_ACTIVE' => 'TYPE.ACTIVE',
                'TYPE_SORT' => 'TYPE.SORT',

                'FIELD_NAME' => 'FIELD.NAME',
                'FIELD_TYPE' => 'FIELD.TYPE',
                'FIELD_SORT' => 'FIELD.SORT',
                'FIELD_MULTY' => 'FIELD.MULTY',
                'FIELD_HINT' => 'FIELD.HINT',
            ),
            "filter" => $arFilter
        ));

        $arData = array();
        foreach ($dbData as $data) {
            $data['FIELD_HTML_ID'] = 'field[' . $data['FIELD_ID'] . ']';

            if (!$data['REQUIRED'] || $data['REQUIRED'] === 'N') $data['REQUIRED'] = false;
            else $data['REQUIRED'] = true;

            $arData['ITEMS'][] = $data;

            if (!is_array($arData['TREE'][$data['CATEGORY_ID']])):
                $arData['TREE'][$data['CATEGORY_ID']] = array(
                    'ID' => $data['CATEGORY_ID'],
                    'NAME' => $data['CATEGORY_NAME'],
                    'CODE' => $data['CATEGORY_CODE'],
                    'PARENT' => $data['CATEGORY_PARENT'],
                    'ACTIVE' => $data['CATEGORY_ACTIVE'],
                    'SORT' => $data['CATEGORY_SORT'],
                    'TYPES' => array()
                );
            endif;

            if (!is_array($arData['TREE'][$data['CATEGORY_ID']]['TYPES'][$data['TYPE_ID']])):
                $arData['TREE'][$data['CATEGORY_ID']]['TYPES'][$data['TYPE_ID']] = array(
                    'ID' => $data['TYPE_ID'],
                    'NAME' => $data['TYPE_NAME'],
                    'CODE' => $data['TYPE_CODE'],
                    'ACTIVE' => $data['TYPE_ACTIVE'],
                    'SORT' => $data['TYPE_SORT'],
                    'FIELDS' => array()
                );
            endif;

            $arData['TREE'][$data['CATEGORY_ID']]['TYPES'][$data['TYPE_ID']]['FIELDS'][$data['FIELD_ID']] = array(
                'ID' => $data['FIELD_ID'],
                'NAME' => $data['FIELD_NAME'],
                'TYPE' => $data['FIELD_TYPE'],
                'MULTY' => $data['FIELD_MULTY'],
                'HINT' => $data['FIELD_HINT'],
                'SORT' => $data['FIELD_SORT'],
            );
        }


        return $arData;
    }


    /** Возвращает список типов для данной категории и ее родителя
     * @param $CategoryID
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function getTypes($CategoryID)
    {
        $result = array();
        if($CategoryID>0):

            $arOrder  = array('SORT'=>'ASC');
            $arSelect = array(
                'ID',
                'CATEGORY_ID',
                'NAME',
                'VALUE' => 'ID',
                'CODE',
                'ACTIVE',
                'SORT'
            );

            $dbType = TypeTable::getList(array(
                'select' => $arSelect,
                'order'  => $arOrder,
                'filter' => array(
                    "CATEGORY_ID" => $CategoryID
                )
            ));

            while($type = $dbType->fetch()) {
                $result[$type['CATEGORY_ID']][$type['ID']] = $type;
            }
        endif;

        return $result;
    }


    public static function isType($CategoryID = array())
    {
        $arOrder  = array('SORT'=>'ASC');
        $arSelect = array('ID');
        $dbType = TypeTable::getList(array(
            'select' => $arSelect,
            'order'  => $arOrder,
            'filter' => array("CATEGORY_ID" => $CategoryID),
            'limit'  => 1
        ));

        return $dbType->getSelectedRowsCount() ? true : false;
    }




    /** Возвращаем массив свойств выбранной категории
     * @param $CategoryID
     * @param null $TypeID
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function getFields($CategoryID, $TypeID=0)
    {
        $result = array();
        $filter = array(
            "CATEGORY_ID" => intval($CategoryID),
            'TYPE_ID' => intval($TypeID)
        );

        if($CategoryID>0):

            $dbFields = RelationsTable::getList(array(
                'select' => array(
                    'ID',
                    'ACTIVE',
                    'REQUIRED',
                    'FILTER',
                    'FIELD_ID',
                    'TYPE_ID',
                    'CATEGORY_ID',
                    'FIELD_NAME'  => 'FIELD.NAME',
                    'FIELD_TYPE'  => 'FIELD.TYPE',
                    'FIELD_HINT'  => 'FIELD.HINT',
                    'FIELD_SORT'  => 'FIELD.SORT',
                    'FIELD_MULTY' => 'FIELD.MULTY',
                ),
                'order' => array(
                    'SORT' => 'ASC'
                ),
                'filter' => $filter
            ));

            while($field = $dbFields->fetch())
            {
                if($field['FIELD_TYPE']=='int') $field['FIELD_TYPE']='number';


                if($field['FIELD_TYPE']=='enum'
                || $field['FIELD_TYPE']=='select'
                || $field['FIELD_TYPE']=='radio'
                || $field['FIELD_TYPE']=='slider'
                ):

                    $arEnum = FieldsEnumsTable::getList(array(
                        'select'=> array(
                            'ID',
                            'NAME'=>'CODE',
                            'VALUE'
                        ),
                        'filter'=> array(
                            'FIELD_ID' => $field['FIELD_ID'],
                            'ACTIVE' => true
                        )
                    ))->fetchAll();

                    $field['FIELD_VALUES'] = $arEnum;
                endif;

                $result[$field['FIELD_ID']] = $field;
            }
        endif;

        return $result;
    }



    /**
     * Формируем массив для фильтра
     * Пример входных данных:
     *
     * Array(
     *      category => Array(2)
     *      region => Array(824)
     *      radius => 60
     *      type => Array(1)
     *      1 => Array(2017)
     *      2 => Array(35, 100)
     *      3 => Array(12)
     *);
     */
    public static function genFilter($data)
    {
        $arSelect = array(
            'DATA_FIELD_ID' => 'DATA.FIELD_ID',
            'DATA_VALUE'    => 'DATA.VALUE',
            'LOCATION_LAT'  => 'LOCATION.LAT',
            'LOCATION_LNG'  => 'LOCATION.LNG',
        );
        $arFilter = array();
        $arAlias = array(
            'category' => 'CATEGORY_ID',
            'type' => 'TYPE_ID',
        );


        $region = self::getFilterRegion($data['region']);
        foreach($data as $key=>$item)
        {
            // переопределяем ключ
            if($arAlias[$key]) $key = $arAlias[$key];


            switch ($key)
            {
                case is_numeric($key):
                    // Удаляем пустые элементы массивы
                    $result = null;
                    if(is_array($item)) $result = array_filter($item, function($el){ return !empty($el); });
                    elseif($item) $result = $item;

                    if($result):
                        if(empty($arFilter[0]['LOGIC'])) $arFilter[0]['LOGIC'] = 'AND';
                        $arFilter[0][] = array('DATA_FIELD_ID' => intval($key), 'DATA_VALUE' => $result);
                    endif;

                    break;
                case 'radius':
                    // Описание запроса доступно тут
                    // https://developers.google.com/maps/solutions/store-locator/clothing-store-locator#finding-locations-with-mysql
                    // To get distance in kilometers, just replace 3959 with 6372.797, an approximation of the earth's radius in km.
                    $table = 'diamis_ads_ads_location';
                    $arSelect[] = new \Bitrix\Main\Entity\ExpressionField(
                        'DISTANCE',
                        "( 6372.797 * acos( cos( radians('".$region['LAT']."') ) * cos( radians( `".$table."`.`LAT` ) ) * cos( radians( `".$table."`.`LNG` ) - radians('".$region['LNG']."') ) + sin( radians('".$region['LAT']."') ) * sin( radians( `".$table."`.`LAT` ) ) ) )"
                    );
                    $arFilter['<=DISTANCE'] = intval($item);
                    break;
                case 'region':
                    break;

                default:
                   $arFilter[$key] = $item;
                   break;
            }
        }

        return array(
            'filter' => $arFilter,
            'select' => $arSelect
        );
    }

    /**
     * Вычисляем растояние между двух точек в км
     * @param $lat1
     * @param $lng1
     * @param $lat2
     * @param $lng2
     * @return string
     */
    public static function getDistance($lat1, $lng1, $lat2, $lng2)
    {
        // радиус сферы (Земли) в км
        $rad = 6372.797;

        // в радианах
        $lat1  = $lat1 * M_PI/180;
        $lng1  = $lng1 * M_PI/180;
        $lat2  = $lat2 * M_PI/180;
        $lng2  = $lng2 * M_PI/180;

        return  number_format($rad * acos( cos( $lat1 ) * cos( $lat2 ) * cos( $lng1 - $lng2 ) + sin( $lat1 ) * sin( $lat2 ) ), 3, ',', ' ');
    }


    protected static function getFilterRegion($region) {
        $region = GeoBase::getCityID($region);



        $GLOBALS['FILTER_LAT'] = $region['LAT'];
        $GLOBALS['FILTER_LNG'] = $region['LNG'];

        return array(
            'LAT' => $region['LAT'],
            'LNG' => $region['LNG']
        );
    }

}
