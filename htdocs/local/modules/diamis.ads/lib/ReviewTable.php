<?php
namespace Diamis\Ads;

use Bitrix\Main\ArgumentException;
use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

use \Diamis\Ads\Base as AdsBase;
use \Diamis\Ads\FieldsTable as AdsFields;
use \Diamis\Ads\CategoryTable as AdsCategory;
use \Diamis\Ads\TypeTable as AdsType;
use \Diamis\Ads\DataContactsTable;
use \Exception;

IncludeModuleLangFile(__FILE__);

class ReviewTable extends Entity\DataManager
{



	public static function getTableName() 
	{
		return 'diamis_ads_review';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\DatetimeField('DATE_CREATE', [
                'title' => 'Дата добавления',
                'default_value' => function () {
                    $day = date('Y-m-d h:i:s');
                    return new Type\DateTime($day, 'Y-m-d h:is');
                }
            ]),
            new Entity\BooleanField('ACTIVE', [
                'default_value' => false,
                'title' => 'Активен'
            ]),
            new Entity\IntegerField('MODER_USER_ID', [
                'title' => 'Модератор'
        	]),
            new Entity\IntegerField('SORT', [
                'title' => 'Сортировать'
        	]),
        	new Entity\IntegerField('LIKE', [
                'title' => 'Лайк'
        	]),
            new Entity\IntegerField('DISLIKE', [
                'title' => 'ДисЛайк'
            ]),
            new Entity\IntegerField('PARENT', [
                'title' => 'Родитель'
            ]),
        	new Entity\IntegerField('VIEW', [
                'title' => 'Коли-во Просмотров'
        	]),
            new Entity\IntegerField('USER_ID', [
                'title' => 'Автор'
        	]),
        	new Entity\IntegerField('ADS_ID', [
                'title' => 'ID Объявления'
        	]),
        	new Entity\StringField('NAME', [
                'title' => 'Заголовок'
        	]),
        	new Entity\TextField('TEXT', [
                'title' => 'Комментарий'
        	])
		);
	}

    public function create($data)
    {
        global $USER;

        $id = intval($data['element']) ? intval($data['element']) : intval($data['id']);
        $review = intval($data['review']);
        $text = strip_tags($data['text']);


        if(!$id)
            throw new Exception("ID объявления не передано");


        if(strlen($text)<3)
            throw new Exception("Количество символов меньше 3");

        if(!$USER->IsAuthorized())
            throw new Exception("Только авторизованный пользователь может оставлять комментарий");

        $db = self::add(array(
            'ADS_ID' => $id,
            'PARENT' => $review,
            'TEXT' => $text,
            'USER_ID' => $USER->GetID()
        ));

        if($db->isSuccess()) {
            return "Комментарий добавлен. И скоро появиться в разделе сайта";
        }
    }


    public function getReview($filter, $order=array('ID'=>'DESC'), $page = 12, $offset = 0)
    {
        if(!$filter)
            throw new Exception("Параметры фильтра не заданы");

        $db = ReviewTable::getList(array(
            'select' => array('*'),
            'filter' => $filter,
            'order'  => $order,
            'offset' => $offset,
            'limit'  => $page
        ));
        while($item = $db->fetch()) {
            $arUser = \CUser::GetByID($item['USER_ID'])->Fetch();

            $item['DATE'] = $item['DATE_CREATE']->format('d.m.Y h:i');
            $item['USER_NAME'] = strlen($arUser['NAME'] .' '. $arUser['LAST_NAME']) ? $arUser['NAME'] .' '. $arUser['LAST_NAME'] : 'Пользователь';

            $arItems[] = $item;
        }

        return $arItems;
    }
}