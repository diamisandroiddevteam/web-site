<?php
namespace Diamis\Ads;


class DomElement {

    /** Получаем dom элемент
     * @param $data array массив вида:
     * array(
     *   id => атрибут поля name
     *   title => placeholder
     *   type => тип dom элемента (select, multiSelect, slider)
     *   currentValue => выбранное значение
     *   values => array(
     *      array(
     *         name => название
     *         value => значение поля input
     *         max => максимальное значение для слайдера (slider)
     *      )
     *      ...
     *   )
     * )
     * @return string
     */
    public static function get($data)
    {
        $result = '';
        switch ($data['type'])
        {
            case 'slider':
                $result = DomElement::slider($data);
                break;
            case 'file':
                $result = DomElement::file($data);
                break;
            case 'boolean':
                $result = DomElement::boolean($data);
                break;
            case 'int':
                $result = DomElement::int($data);
                break;
            case 'string':
                $result = DomElement::string($data);
                break;
            case 'select':
                $result = DomElement::select($data);
                break;
            case 'multiSelect':
                $result = DomElement::select($data,true);
                break;
        }
        return $result;
    }




    /**
     * @param $data
     * @return string
     */
    public static function file($data) {
        $files = null;
        $count = $data['count'] ? $data['count'] : 1;

        $i = 0;
        while($i<$count) {
            $i++;
            $img = '';
            if(is_array($data['values'][$i]['value'])):
                $img = '<img src="'.$data['values'][$i]['value'].'" alt="'.$data['values'][$i]['name'].'" />';
            endif;

            $files .= '<label class="field-file">'.
                      '    <input name="' . $data['id'] . '" type="file" accept="image/jpeg,image/png,image/gif" />'.
                      '    <div class="field-file--preview">' . $img . '</div>'.
                      '    <div class="field-file--action">Добавить фото</div>'.
                      '</label>';
        }

        return '<div class="field-files">'.$files.'</div>';
    }




    /**
     * @param $data
     * @return string
     */
    public static function boolean($data, $max=false) {
        $class = $max ? ' input-checked--max' : '';
        $checked = $data['currentValue']
                   ? 'checked'
                   : '';

        return '<label class="input-checked'.$class.'">'.
               '    <input class="input-checked--tag" name="' . $data['id'] . '" type="checkbox" '.$checked.' />'.
               '    <div class="input-checked--check"></div>'.
               '    <span class="input-checked--name">' . $data['title'] . '</span>'.
               '</label>';
    }


    public static function int($data)
    {
        return '<div class="form-field-input form-field-input--min-line">'.
               '    <input name="name" type="text" data-mask="int" class="form-field-input-tag" value="'.$data['currentValue'].'"/>'.
               '</div>';
    }

    public static function string($data)
    {

    }




    public static function slider($data)
    {
        $input_id = str_replace('[','_', $data['id']);
        $input_id = str_replace(']','', $input_id);

        $result = $data['placeholder'] ? $data['placeholder'] . ': ' : '';


        $items = '';
        foreach($data['values'] as $key=>$value):

            if(count($data['currentValue']) && $data['currentValue'][$key])
                $value['value'] = $data['currentValue'][$key];

            if($value['value'])
                $result.= trim($value['name'] . ' ' . intval($value['value']));


            $items .= '<div class="slc-input-wrap">';
            $items .= '    <input id="'.$input_id.'" name="'.$data['id'].'" class="slc-slider-to" type="text" placeholder="'.$value['name'].'" value="'.$value['value'].'" data-max="'.$value['max'].'">';
            $items .= '</div>';

        endforeach;

        return  '<div class="select select-slider">'.
                '    <div class="select-content">' .
                '        <i class="select-icon"></i>' .
                '        <div class="select-placeholder">' . $data['placeholder'] . '</div>' .
                '        <div class="select-result">' . $result . '</div>' .
                '    </div>' .
                '    <div class="select-lists">' .
                '        <i class="select-lst-icon"></i>' .
                '        <div class="slc-slider-wrap">' .
                '            <div class="slc-input">' . $items . '</div>' .
                '        </div>' .
                '        <div class="slc-slider">' .
                '            <div class="slc-slider-container" ></div>' .
                '        </div>' .
                '   </div>' .
                '</div>';
    }

    /** Формирует выподающий список
     * @param $data
     * @param bool $multiselect
     * @return string
     */
    public static function select($data, $multiselect=false)
    {
        $classAdd = $multiselect ? ' select-multi' : '';

        $disable  = count($data['values'])<=0 ? ' disabled' : false;
        $input_id = str_replace('[','_', $data['id']);
        $input_id = str_replace(']','', $input_id);

        $search = count($data['values'])>10 ? ' select-find-active' : '';

        if(!is_array($data['currentValue']))
            $data['currentValue'] = array($data['currentValue']);

        $formID = '';
        if($data['formId']) $formID="form=\"".$data['formId']."\"";

        $items = null;
        $inputs = null;
        $result = '';
        foreach($data['values'] as $value):
            $active = '';
            $checked = '';
            if(array_search($value['value'], $data['currentValue'])!==false) {
                $checked = 'checked';
                $active = ' select-active-item';
                $result = $value['name'];
            }

            $items  .= "<li data-for='".$input_id.'_'.$value['value']."' class='select-item".$active."'><span>".$value['name']."</span></li>\n";
            $inputs .= "<input ".$formID." name='".$data['id']."[".$value['value']."]' type='checkbox' id='".$input_id.'_'.$value['value']."' value='".$value['value']."' ".$checked."/>\n";
        endforeach;

        $selectActive = '';
        if($result) $selectActive = ' select-active';

        return '<div class="select'.$classAdd.' select_'.$input_id.$disable.$selectActive.'">'.
               '    <div class="select-content">'.
               '       <i class="select-icon"></i>'.
               '       <div class="select-placeholder">'.$data['placeholder'].'</div>'.
               '       <div class="select-result">'.$result.'</div>'.
               '    </div>'.
               '    <div class="select-lists">'.
               '       <div class="select-find'.$search.'">' .
               '            <input type="text" placeholder="Поиск..." class="select-find-input" value=""/>' .
               '       </div>' .
               '       <div class="select-lst-container">'.
               '            <ul class="select-lst-items">'.$items.'</ul>'.
               '       </div>'.
               '    </div>'.
               '    <div class="select-check">'.$inputs.'</div>'.
               '</div>';
    }
}