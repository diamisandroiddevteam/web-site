<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

IncludeModuleLangFile(__FILE__);

class FilesTable extends Entity\DataManager
{


    public static $entity = array(
        'ads',
        'category'
    );


	public static function getTableName() 
	{
		return 'diamis_ads_files';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\IntegerField('FIELD_ID',[
                'required' => true,
                'title' => GetMessage('DIAMIS_ADS_FILES_TABLE_FIELD_ID')
            ]),
            new Entity\IntegerField('FILE_ID',[
                'required' => true,
                'title' => GetMessage('DIAMIS_ADS_FILES_TABLE_FILE_ID')
            ]),
            new Entity\IntegerField('SORT', [
                'title' => GetMessage('DIAMIS_ADS_FILES_TABLE_SORT')
            ]),
            new Entity\StringField('ENTITY', [
                'required' => true,
                'title' => GetMessage('DIAMIS_ADS_FILES_TABLE_ENTITY'),
                'default_value' => 'ADS'
            ]),
        	new Entity\IntegerField('ENTITY_ID', [
                'required' => true,
                'title' => GetMessage('DIAMIS_ADS_FILES_TABLE_ENTITY_ID')
            ]),

            new Entity\ReferenceField(
                'FIELD',
                'Diamis\Ads\FieldsTable',
                array('=this.FIELD_ID' => 'ref.ID')
            )
		);
	}
}