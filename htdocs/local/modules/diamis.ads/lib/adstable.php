<?php
namespace Diamis\Ads;

use Bitrix\Main\ArgumentException;
use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

use \Diamis\Ads\Base as AdsBase;
use \Diamis\Ads\FieldsTable as AdsFields;
use \Diamis\Ads\CategoryTable as AdsCategory;
use \Diamis\Ads\TypeTable as AdsType;
use \Diamis\Ads\DataContactsTable;
use \Exception;

IncludeModuleLangFile(__FILE__);

class AdsTable extends Entity\DataManager
{

    protected $tableFavorite = 'b_favorites';


	public static function getTableName() 
	{
		return 'diamis_ads';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\DatetimeField('DATE_CREATE', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_CREATE'),
                'default_value' => function () {
                    $day = date('Y-m-d h:i:s');
                    return new Type\DateTime($day, 'Y-m-d h:is');
                }
            ]),
            new Entity\DatetimeField('DATE_UPDATE', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_UPDATE'),
                'default_value' => function () {
                    $day = date('Y-m-d h:i:s');
                    return new Type\DateTime($day, 'Y-m-d h:i:s');
                }
            ]),
            new Entity\DatetimeField('DATE_EXPIRE', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_EXPIRE')
            ]),
            new Entity\DatetimeField('DATE_DELETE', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_DELETE')
            ]),
            new Entity\BooleanField('ACTIVE', [
                'default_value' => false,
                'title' => GetMessage('DIAMIS_ADS_TABLE_ACTIVE')
            ]),
            new Entity\IntegerField('MODER_USER_ID', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_MODER_USER_ID')
        	]),
            new Entity\IntegerField('SORT', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_SORT')
        	]),
        	new Entity\DatetimeField('SORT_DATE', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_SORT_DATE')
        	]),
        	new Entity\DatetimeField('SORT_DATE_SELECT', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_SORT_DATE_SELECT')
        	]),
        	new Entity\IntegerField('VIEW', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_VIEW')
        	]),
            new Entity\IntegerField('USER_ID', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_USER_ID')
        	]),
        	new Entity\IntegerField('CATEGORY_ID', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_CATEGORY_ID')
        	]),
            new Entity\IntegerField('TYPE_ID', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_CATEGORY_ID')
            ]),
        	new Entity\StringField('CODE', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_CODE')
    		]),
            new Entity\StringField('URL', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_URL')
            ]),
        	new Entity\TextField('NAME', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_NAME')
        	]),
        	new Entity\TextField('TEXT', [
                'title' => GetMessage('DIAMIS_ADS_TABLE_TEXT')
        	]),

            new Entity\ReferenceField(
                'DATA',
                'Diamis\Ads\DataFieldsTable',
                array(
                    'this.ID'=>'ref.ADS_ID'
                )
            ),

            new Entity\ReferenceField(
                'FIELD',
                'Diamis\Ads\FieldsTable',
                array(
                    'this.DATA.FIELD_ID'=>'ref.ID'
                )
            ),

            new Entity\ReferenceField(
                'CONTACT',
                'Diamis\Ads\DataContactsTable',
                array(
                    'this.ID'=>'ref.ADS_ID'
                )
            ),
            new Entity\ReferenceField(
                'FILE',
                'Diamis\Ads\DataFilesTable',
                array(
                    'this.ID' => 'ref.ADS_ID'
                )
            ),
            new Entity\ReferenceField(
                'LOCATION',
                'Diamis\Ads\DataLocationsTable',
                array(
                    'this.ID' => 'ref.ADS_ID'
                )
            ),
            new Entity\ReferenceField(
                'PRICE',
                'Diamis\Ads\DataPriceTable',
                array(
                    'this.ID' => 'ref.ADS_ID'
                )
            )
		);
	}


	public static function getElementFields()
    {
        $field = array();
        $fields = AdsTable::getMap();
        foreach($fields as $item) {
            $name = $item->getName();
            if( $name!=='DATA'
                && $name!=='FIELD'
                && $name!=='CONTACT'
                && $name!=='FILE'
                && $name!=='PRICE'
                && $name!=='LOCATION'
            ) {
                $field[] = $name;
            }
        }
        $result = array_merge($field, array(
            'DATA_FIELD_ID' => 'DATA.FIELD_ID',
            'DATA_VALUE' => 'DATA.VALUE',
            'FIELD_NAME' => 'FIELD.NAME',
            'FIELD_TYPE' => 'FIELD.TYPE',

            'CONTACT_ID' => 'CONTACT.ID',
            'CONTACT_TYPE' => 'CONTACT.TYPE',
            'CONTACT_VALUE' => 'CONTACT.VALUE',

            'FILE_ID' => 'FILE.FILE_ID',

            'PRICE_ID' => 'PRICE.ID',
            'PRICE_DATE_CREATE' => 'PRICE.DATE_CREATE',
            'PRICE_PRICE' => 'PRICE.PRICE',
            'PRICE_PRICE_SALE' => 'PRICE.PRICE_SALE',
            'PRICE_CURRENCY' => 'PRICE.CURRENCY',

            'LOCATION_ID' => 'LOCATION.ID',
            'LOCATION_LAT' => 'LOCATION.LAT',
            'LOCATION_LNG' => 'LOCATION.LNG',
            'LOCATION_VALUE' => 'LOCATION.VALUE',
        ));

        return array(
            'all' => $result,
            'table' => $field
        );
    }




	public static function getElements($filter=array(), $select=array(), $expire=true, $pageSize = 10, $offset = 0, $order=array('DATE_UPDATE' => 'DESC'))
    {
        $elementFields = self::getElementFields();
        $elementFields['table'][] = 'CATEGORY_NAME';
        $elementFields['table'][] = 'CATEGORY_CODE';
        $elementFields['table'][] = 'CATEGORY_PARENT';

        $elementFields['table'][] = 'TYPE_NAME';
        $elementFields['table'][] = 'TYPE_CODE';

        $elementFields['table'][] = 'SERVICE_ID';
        $elementFields['table'][] = 'PACKAGES_SERVICES_NAME';

        $query = array();
        $query['select'] = array_merge(
            $elementFields['all'],
            array(
                'SERVICE_ID' => 'SERVICES.SERVICE_ID',
                'PACKAGES_SERVICES_ID' => 'PACKAGES_SERVICES.ID',
                'PACKAGES_SERVICES_NAME' => 'PACKAGES_SERVICES.NAME',
                'PACKAGES_SERVICES_TEXT' => 'PACKAGES_SERVICES.TEXT',
                'PACKAGES_SERVICES_ACTION' => 'PACKAGES_SERVICES.ACTION',

                'CATEGORY_NAME' => 'CATEGORY.NAME',
                'CATEGORY_CODE' => 'CATEGORY.CODE',
                'CATEGORY_PARENT' => 'CATEGORY.PARENT',

                'TYPE_NAME' => 'TYPE.NAME',
                'TYPE_CODE' => 'TYPE.CODE'
            )
        );
        $query['filter'] = $filter;



        // Сортировка
        $query['order'] = $order;

        $query['runtime'] = array(
            new Entity\ReferenceField(
                'CATEGORY',
                'Diamis\Ads\CategoryTable',
                array(
                    'this.CATEGORY_ID' => 'ref.ID'
                )
            ),
            new Entity\ReferenceField(
                'TYPE',
                'Diamis\Ads\TypeTable',
                array(
                    'this.TYPE_ID' => 'ref.ID'
                )
            ),
            new Entity\ReferenceField(
                'SERVICES',
                'Diamis\Ads\DataServicesTable',
                array(
                    'this.ID' => 'ref.ADS_ID'
                    // TODO добавить проверку на актуальность сервиса
                )
            ),
            new Entity\ReferenceField(
                'PACKAGES_SERVICES',
                'Diamis\Ads\PackagesServicesTable',
                array(
                    'this.SERVICES.SERVICE_ID' => 'ref.ID'
                    // TODO добавить проверку на актуальность сервиса
                )
            )
        );

        // Ограничение по дате окончания по-умолчанию
        if($expire) {
            $query['filter']['>=DATE_EXPIRE'] = date('Y-m-d H:i:s');
        }


        // Постраничная навигация
        if($pageSize)
        {
            $pageData = array();
            $pageData['select'] = array('ID');
            $pageData['select'] = array_merge($pageData['select'], $select);
            $pageData['filter'] = $query['filter'];
            $pageData['offset'] = intval($offset);
            $pageData['limit'] = intval($pageSize);


            $con = \Bitrix\Main\Application::getConnection();
            $con->startTracker();

            $dbAds = AdsTable::getList($pageData);

            $arAdsId = array();
            while($item = $dbAds->fetch())
                $arAdsId[] = $item['ID'];



            $query['filter'] = array('ID' => $arAdsId);
        }



        $dbElements = AdsTable::getList($query);

        $id = null;
        $key = null;
        $arElements = array();


        while($element = $dbElements->fetch()) {

            // Новый элемент
            if($id!==$element['ID']) {
                $id = $element['ID'];
                $arElements[] = array();
                $key = count($arElements)-1;
                foreach($elementFields['table'] as $keyField) {
                    $arElements[$key][$keyField] = $element[$keyField];
                }

                if($arElements[$key]['TEXT']) {
                    $text=explode(PHP_EOL, strip_tags($arElements[$key]['TEXT']));
                    foreach($text as $row) {
                        if(trim($row)) $arElements[$key]['~TEXT'] .= "<p>".$row."</p>";
                    }
                }

                $arDetailPage = array();

                if(trim($element['CATEGORY_CODE'])) $arDetailPage[] = $element['CATEGORY_CODE'];
                if(trim($element['TYPE_CODE'])) $arDetailPage[] = $element['TYPE_CODE'];
                $arDetailPage[] = $element['CODE'].'_'.$element['ID'];

                $arElements[$key]['DETAIL_PAGE'] = '/'.implode('/', $arDetailPage).'/';

                $arElements[$key]['CONTACTS'] = array();
                $arElements[$key]['FILES'] = array();
                $arElements[$key]['LOCATION'] = array();
                $arElements[$key]['PROPERTY'] = array();

                if(!$element['FILE_ID']) $arElements[$key]['FILES'][] = '/local/templates/tebemne/images/no_photo.jpg';
            }

            // формируем контактные данные
            switch($element['CONTACT_TYPE'])
            {
                case DataContactsTable::TYPE_PHONE:
                    if(!isset($arElements[$key]['CONTACTS']['PHONE'][$element['CONTACT_ID']]))
                        $arElements[$key]['CONTACTS']['PHONE'][$element['CONTACT_ID']] = AdsBase::formatPhoneNumber($element['CONTACT_VALUE']);
                    break;
                case DataContactsTable::TYPE_EMAIL:
                    if(!isset($arElements[$key]['CONTACTS']['EMAIL'][$element['CONTACT_ID']]))
                        $arElements[$key]['CONTACTS']['EMAIL'][$element['CONTACT_ID']] = $element['CONTACT_VALUE'];
                    break;
            }

            // формируем геолокацию
            if($element['LOCATION_ID'] && !isset($arElements[$key]['LOCATION'][$element['LOCATION_ID']]))
            {
                $arElements[$key]['LOCATION'][$element['LOCATION_ID']] = array(
                    'LAT' => $element['LOCATION_LAT'],
                    'LNG' => $element['LOCATION_LNG'],
                    'VALUE' => $element['LOCATION_VALUE'],
                );
            }

            // получайем файл
            if($element['FILE_ID'] && !isset($arElements[$key]['FILES'][$element['FILE_ID']]))
            {
                $arElements[$key]['FILES'][$element['FILE_ID']] = \CFile::GetPath($element['FILE_ID']);
            }

            // Список подключенных сервисов
            if($element['SERVICE_ID'] && !isset($arElements[$key]['SERVICES'][$element['SERVICE_ID']]))
            {
                if($element['PACKAGES_SERVICES_ACTION']==='ads_select') $arElements[$key]['SELECT'] = true;
                if($element['PACKAGES_SERVICES_ACTION']==='ads_right') $arElements[$key]['VIP'] = true;

                $arElements[$key]['SERVICES'][$element['PACKAGES_SERVICES_ID']] = array(
                    'ID' => $element['PACKAGES_SERVICES_ID'],
                    'NAME' => $element['PACKAGES_SERVICES_NAME'],
                    'TEXT' => $element['PACKAGES_SERVICES_TEXT'],
                    'ACTION' => $element['PACKAGES_SERVICES_ACTION'],
                );
            }

            // Цена
            if($element['PRICE_ID'] && !isset($arElements[$key]['PRICE'][$element['PRICE_ID']]))
            {
                $arElements[$key]['PRICE'][$element['PRICE_ID']] = array(
                    'ID' => $element['PRICE_ID'],
                    'DATE_CREATE' => $element['PRICE_DATE_CREATE'],
                    'PRICE' => $element['PRICE_PRICE'],
                    'PRICE_SALE' => $element['PRICE_PRICE_SALE'],
                    'CURRENCY' => $element['PRICE_CURRENCY'],
                );
            }

            // формируем свойство
            if($element['DATA_FIELD_ID'] && !isset($arElements[$key]['PROPERTY'][$element['DATA_FIELD_ID']])) {
                $arElements[$key]['PROPERTY'][$element['DATA_FIELD_ID']] = array(
                    'ID'    => $element['DATA_FIELD_ID'],
                    'NAME'  => $element['FIELD_NAME'],
                    'TYPE'  => $element['FIELD_TYPE'],
                    'VALUE' => $element['DATA_VALUE']
                );
            }
            else if(isset($arElements[$key]['PROPERTY'][$element['DATA_FIELD_ID']]) && $element['FIELD_TYPE']==='slider') {
                $val = $arElements[$key]['PROPERTY'][$element['DATA_FIELD_ID']]['VALUE'];
                $val = !is_array($val) ? array($val) : $val;

                $arElements[$key]['PROPERTY'][$element['DATA_FIELD_ID']]['VALUE'] = array_merge($val, array($element['DATA_VALUE']));
            }

        }


        return $arElements;
    }


    /**
     * Метод возвращаеть информацию объявления в виде массива
     * @param $id integer ID объявления
     * @return array|string
     */
    public static function getElementID($id) {
        $result = array();
        if($id) {
            $arElements = AdsTable::getElements(array('ID'=>$id), array(), false, 1);
            $arElements[0]['PRICE'] = array_shift($arElements[0]['PRICE']);
            $result = $arElements[0];
        }

        return $result;
    }


    public function updateElement($id, $data=array()){

        $id = intval($id);
        $result = array(
            'status' => false
        );

        try {
            global $USER, $DB;

            if(!count($data))
                throw new Exception("Параметры не переданы");

            $USER_ID = intval($USER->GetID());
            $alias = array(
                'category' => array('CATEGORY_ID', 'int'),
                'type' => array('TYPE_ID', 'int'),
                'name' => array('NAME', 'string'),
                'text' => array('TEXT', 'string'),
//                'user' => array('USER_ID', 'int'),
//                'code' => array('CODE', 'translite')
            );


            $Element = self::getElementID($id);
            $elementData = array();
            if(!$Element)
                throw new Exception("Элемент с ID равным ".$id." не найден.");


            foreach ($alias as $als => $field) {

                if ($data[$als] && $field[1] === 'int') {
                    $elementData[$field[0]] = is_array($data[$als]) ? intval(array_shift($data[$als])) : intval($data[$als]);
                }

                if ($data[$als] && $field[1] === 'string') {
                    $elementData[$field[0]] = htmlspecialchars(strip_tags($data[$als]));
                }
            }

            $dateUpdate = date('Y-m-d h:i:s');
            $elementData['DATE_UPDATE'] = new Type\DateTime($dateUpdate, 'Y-m-d h:i:s');


            // Отправляем на модерацию
            $elementData['ACTIVE'] = false;

            $db = AdsTable::update($id, $elementData);
            if(!$db->isSuccess())
                throw new Exception("Error: ". $db->getErrorMessages());

            // Обновляем контактные данные
            $arSqlContact = array();
            foreach($data['phone'] as $item_id=>$item_val)
            {
                $item = AdsBase::formatPhoneNumber($item_val);
                if(!$Element['CONTACTS']['PHONE'][$item_id])
                {
                    $sqlItem = array('ADS_ID'=>$id, 'TYPE'=>DataLocationsTable::TYPE_PHONE, 'VALUE'=> AdsBase::formatPhoneNumber($item),'EMAIL_NO_SEND'=>'');
                    $arSqlContact[] = "('".implode("', '",$sqlItem)."')";
                }
                elseif($Element['CONTACTS']['PHONE'][$item_id]!=$item && $item!='')
                {
                    $sql = "UPDATE `diamis_ads_data_contacts` SET `VALUE`='".$item."' WHERE `ID`='".$item_id."'";
                    $DB->Query($sql);
                }
            }

            foreach($data['email'] as $item_id=>$item_val)
            {
                $item = strip_tags($item_val);
                if(!$Element['CONTACTS']['EMAIL'][$item_id])
                {
                    $sqlItem = array('ADS_ID'=>$id, 'TYPE'=>DataLocationsTable::TYPE_EMAIL, 'VALUE'=> $item, 'EMAIL_NO_SEND'=> ($data['email']['notice'] ? true : false) );
                    $arSqlContact[] = "('".implode("', '",$sqlItem)."')";
                }
                elseif($Element['CONTACTS']['EMAIL'][$item_id]!=$item && $item!='')
                {
                    $sql = "UPDATE `diamis_ads_data_contacts` SET `VALUE`='".$item."', `EMAIL_NO_SEND`='".($data['email']['notice'] ? true : false)."'  WHERE `ID`='".$item_id."'";
                    $DB->Query($sql);
                }
            }

            if(count($arSqlContact)) {
                $sql = "INSERT INTO `diamis_ads_data_contacts` (`ADS_ID`,`TYPE`,`VALUE`,`EMAIL_NO_SEND`) VALUES ".implode(', ', $arSqlContact);
                $DB->Query($sql);
            }
            // TheEnd Обновляем контактные данные


            // Обновляем цену
            if($data['price'])
            {
                $price = (float) preg_replace("/[^0-9\.]/", '', $data['price']);
                if($price)
                {
                    if(!$price_id = $Element['PRICE'][key($Element['PRICE'])]['ID'])
                    {
                        $sql = "INSERT INTO `diamis_ads_data_price` (`ADS_ID`,`PRICE`) VALUES ('".$id."','".$price."')";
                        $DB->Query($sql);
                    }
                    elseif($price!=$Element['PRICE'][key($Element['PRICE'])]['PRICE'])
                    {
                        $sql = "UPDATE `diamis_ads_data_price` SET `PRICE`='".$price."' WHERE `ID`='".$price_id."' ";
                        $DB->Query($sql);
                    }
                }
            }

            // LOCATION UPDATE
            $data['location']['lat'] = (float) $data['location']['lat'];
            $data['location']['lng'] = (float) $data['location']['lng'];
            if(count($Element['LOCATION'][key($Element['LOCATION'])]))
            {
                $location = $Element['LOCATION'][key($Element['LOCATION'])];
                if($data['location']['lat']!=$location['LAT'] || $data['location']['lng']!=$location['LNG'])
                {
                    $sql = "UPDATE 
                              `diamis_ads_data_locations` 
                            SET 
                              `LAT`='".$data['location']['lat']."', 
                              `LNG`='".$data['location']['lng']."', 
                              `VALUE`='".strip_tags($data['location']['name'])."' 
                            WHERE `ID`='".key($Element['LOCATION'])."'";

                    $DB->Query($sql);
                }

            }
            // THE END -> LOCATION UPDATE


            $result['status'] = true;
            $result['result'] = $id;
            $result['redirect'] = '/packages/'.$id.'/';

        } catch (\Exception $e) {
            $result['error'] = $e->getMessage();
        }

        return $result;
    }





	public function create($data) {

        global $USER, $DB;

        $result = array(
            'status' => false
        );

        try {

            if(!is_array($data))
                throw new Exception("Параметры не переданы");

            $USER_ID = intval($USER->GetID());

            if (!$data['user']) $data['user'] = $USER_ID;

            $elementData = array();
            $elementDataProp = array();
            $alias = array(
                'category' => array('CATEGORY_ID', 'int'),
                'type' => array('TYPE_ID', 'int'),
                'name' => array('NAME', 'string'),
                'text' => array('TEXT', 'string'),
                'user' => array('USER_ID', 'int'),
                'code' => array('CODE', 'translite')
            );

            foreach ($alias as $als => $field) {

                if ($data[$als] && $field[1] === 'int') {
                    $elementData[$field[0]] = is_array($data[$als]) ? intval(array_shift($data[$als])) : intval($data[$als]);
                }

                if ($data[$als] && $field[1] === 'string') {
                    $elementData[$field[0]] = htmlspecialchars(strip_tags($data[$als]));
                }

                if ($field[1] === 'translite') {
                    $str = $data[$als] ? $data[$als] : $data['name'];
                    $elementData[$field[0]] = AdsBase::translite($str);
                }
            }

            if (!$arCategory = AdsTable::isCategoryID($elementData['CATEGORY_ID'])) {
                throw new Exception("Категория не указана или указана неверно");
            }

            if ($elementData['TYPE_ID'] && !($arType = AdsTable::isTypeID($elementData['TYPE_ID']))) {
                throw new Exception("Тип не указана или указана неверно");
            }


            $fields = AdsBase::getFields($elementData['CATEGORY_ID'], $elementData['TYPE_ID']);
            foreach ($fields as $field)
            {
                if (AdsTable::isRequired($field) && !$data[$field['FIELD_ID']])
                {
                    $result['result'][] = array('field' => '[id^=field_'.$field['FIELD_ID'].']');
                    throw new Exception("Обязательное поле \"" . $field['FIELD_NAME'] . "\" не заполнено");
                }

                if ($dataField = AdsTable::genPropData($field, $data[$field['FIELD_ID']])) {
                    if (isset($dataField[0])) {
                        $elementDataProp = array_merge($elementDataProp, $dataField);
                    } else {
                        $elementDataProp[] = $dataField;
                    }
                }
            }

            if (!is_array($data['phone'])) {
                $result['result'][] = array('field' => '[id^=field_phone]');
                throw new Exception("Обязательное поле \"Телефон\" не заполнено");
            }

            if (!is_array($data['email'])) {
                $result['result'][] = array('field' => '[id^=field_email]');
                throw new Exception("Обязательное поле \"E-Mail\" не заполнено");
            }

            if (!$elementData['USER_ID']) {
                throw new Exception("Владец объявления не указан или не определен");
            }


            $data['location']['lat'] = $data['location']['lat'] ? (float) $data['location']['lat'] : ($GLOBALS['CITY_IP']['LAT'] ? $GLOBALS['CITY_IP']['LAT'] : null);
            $data['location']['lng'] = $data['location']['lng'] ? (float) $data['location']['lng'] : ($GLOBALS['CITY_IP']['LNG'] ? $GLOBALS['CITY_IP']['LNG'] : null);


            $elementData['ACTIVE'] = false;

            // по умолчанию объявление активно 30 дней
            $date = date('Y-m-d h:i:s', strtotime("+30 days"));
            if(empty($data['DATE_EXPIRE']))
                $elementData['DATE_EXPIRE'] = new Type\DateTime($date, 'Y-m-d h:i:s');


            $dateSort = date('Y-m-d h:i:s');
            $elementData['SORT_DATE'] = new Type\DateTime($dateSort, 'Y-m-d h:i:s');


            // генерируем URL страницу
            $arUrl = array();
            if($data['location']['lat']===$GLOBALS['CITY_IP']['LAT'] && $data['location']['lng']===$GLOBALS['CITY_IP']['LNG']) {
                $arUrl[] = $GLOBALS['CITY_IP']['CITY_CODE'];
            }
            elseif($data['location']['lat'] && $data['location']['lng']) {
                $local = \Diamis\GeoIp\GeoBase::getCityLatLng($data['location']['lat'], $data['location']['lng']);
                if($local['CITY_CODE']) $arUrl[] = $local['CITY_CODE'];
            }
            else{
                $arUrl[] = $GLOBALS['CITY_IP']['CITY_CODE'];
            }

            if($arCategory['CODE']) $arUrl[] = $arCategory['CODE'];
            if($arType['CODE']) $arUrl[] = $arType['CODE'];

            $elementData['URL'] = '/'.implode('/', $arUrl).'/';
            // TheEnd


            $db = AdsTable::add($elementData);
            $elementID = $db->getId();
            if($elementID)
            {

                // CONTACT ADD
                $arSql = array();
                foreach($data['phone'] as $item) {
                    $sqlItem = array('ADS_ID'=>$elementID, 'TYPE'=>DataLocationsTable::TYPE_PHONE, 'VALUE'=> AdsBase::formatPhoneNumber($item),'EMAIL_NO_SEND'=>'');
                    $arSql[] = "('".implode("', '",$sqlItem)."')";
                }

                foreach($data['email'] as $item) {
                    $sqlItem = array('ADS_ID'=>$elementID, 'TYPE'=>DataLocationsTable::TYPE_EMAIL, 'VALUE'=> $item, 'EMAIL_NO_SEND'=> ($data['email']['notice'] ? true : false) );
                    $arSql[] = "('".implode("', '",$sqlItem)."')";
                }

                $sql = "INSERT INTO `diamis_ads_data_contacts` (`ADS_ID`,`TYPE`,`VALUE`,`EMAIL_NO_SEND`) VALUES ".implode(', ', $arSql);
                $DB->Query($sql);
                // THE END -> CONTACT ADD


                // PROPS ADD
                if($elementDataProp):
                    $arSql = array();
                    foreach($elementDataProp as $key=>$item) {
                        $elementDataProp[$key]['ADS_ID'] = $elementID;
                        $arSql[] = "('".implode("', '",$elementDataProp[$key])."')";
                    }
                    $sql = "INSERT INTO `diamis_ads_data_fields` (`".implode("`, `", array_keys($elementDataProp[0])) ."`) VALUES ".implode(', ', $arSql);
                    $DB->Query($sql);
                endif;
                // THE END -> PROPS ADD



                // LOCATION ADD
                if(is_float($data['location']['lat']) && is_float($data['location']['lng'])) {
                    $sql = "INSERT INTO `diamis_ads_data_locations` (`ADS_ID`, `LAT`, `LNG`, `VALUE`) VALUE ('".$elementID."','".$data['location']['lat']."','".$data['location']['lng']."','".strip_tags($data['location']['name'])."')";
                    $DB->Query($sql);
                }
                // THE END -> LOCATION ADD


                // IMAGE ADD
                $arSql = array();
                foreach($data['files'] as $item) {
                    if(intval($item)) {
                        $sqlItem = array('ADS_ID'=>$elementID, 'FILE_ID'=> intval($item));
                        $arSql[] = "('".implode("', '",$sqlItem)."')";
                    }
                }
                if(count($arSql)) {
                    $sql = "INSERT INTO `diamis_ads_data_files` (`ADS_ID`,`FILE_ID`) VALUES ".implode(', ', $arSql);
                    $DB->Query($sql);
                }
                // THE END -> IMAGE ADD


                $price = (float) preg_replace("/[^0-9\.]/", '', $data['price']);
                if($price) {
                    $sql = "INSERT INTO `diamis_ads_data_price` (`ADS_ID`,`PRICE`) VALUES ('".$elementID."','".$price."')";
                    $DB->Query($sql);
                }
            }


            $result['status'] = true;
            $result['result'] = $elementID;
            $result['redirect'] = '/packages/'.$elementID.'/';

        } catch (\Exception $e) {
            $result['error'] = $e->getMessage();
        }




        return $result;
    }


    //TODO запись в базу данныы контактов необходимо переделать (дедлайны горят)
    public function genContactData($data, $ads_id=null) {
	    $result = array();
	    $keyMax = count($data['phone'])>count($data['email']) ? count($data['phone']) : count($data['email']);
	    $i=0;
	    while($i<$keyMax) {
            $result[] = array(
                'ADS_ID' => $ads_id,
                'NAME'   => null,
                'PHONE'  => $data['phone'][$i] ? AdsBase::formatPhoneNumber($data['phone'][$i]) : null,
                'EMAIL'  => $data['email'][$i] ? $data['email'][$i] : null
            );
            $i++;
        }
        return $result;
    }

    /*
     * Проверяем на корректность пререданных данных
     */
    public function genPropData($field, $data=array(), $ads_id=null) {

        if(!count($data)) return false;

        $result = array();
        switch($field['FIELD_TYPE'])
        {
            case 'select':
            case 'radio':
                $props = array();
                foreach($field['FIELD_VALUES'] as $val) {
                    $props[$val['VALUE']] = $val;
                }

                foreach($data as $val) {
                    if(isset($props[$val]))
                    {
                        $result[] = array(
                            'ADS_ID'   => $ads_id,
                            'FIELD_ID' => $field['FIELD_ID'],
                            'VALUE_ID' => $props[$val]['ID'],
                            'VALUE'    => $props[$val]['VALUE'],
                        );
                    }
                }
                break;
            case 'slider':
                foreach($data as $val):
                    $result[] = array(
                        'ADS_ID' => $ads_id,
                        'FIELD_ID' => $field['FIELD_ID'],
                        'VALUE_ID' => '',
                        'VALUE'     => strip_tags(trim($val))
                    );
                endforeach;
                break;
            case 'number':
                $result = array(
                    'ADS_ID' => $ads_id,
                    'FIELD_ID' => $field['FIELD_ID'],
                    'VALUE_ID' => '',
                    'VALUE'     => intval(preg_replace('/[^\d]+/','', $data))
                );
                break;
            default:
                $result = array(
                    'ADS_ID' => $ads_id,
                    'FIELD_ID' => $field['FIELD_ID'],
                    'VALUE_ID' => '',
                    'VALUE'     => htmlspecialchars(strip_tags(trim($data)))
                );
                break;
        }

        return $result;
    }



    /*
     * Проверяет существует тип данная категории или нет
     */
    public function isCategoryTypeID($categoryID, $typeID) {
        $dbCtg = AdsType::getList(array(
            'filter'=>array(
                'ID' => $typeID,
                'CATEGORY_ID' => $categoryID
            ),'limit'=>1));
        return $dbCtg->getSelectedRowsCount() ? true : false;
    }


    public function isTypeID($typeID)
    {
        $dbType = AdsType::getList(array(
            'select' => array('ID','NAME', 'CODE'),
            'filter' => array('ID' => $typeID),
            'limit' => 1
        ));

        return $dbType->getSelectedRowsCount() ? $dbType->fetch() : false;
    }

    /*
     * Проверяет существует данная категория или нет
     */
    public function isCategoryID($categoryID) {
        $dbCtg = AdsCategory::getList(array(
            'select'=>array(
                'ID',
                'NAME',
                'CODE',
            ),
            'filter'=>array(
                'ID'=>$categoryID
            ),'limit'=>1)
        );
        return $dbCtg->getSelectedRowsCount() ? $dbCtg->fetch() : false;
    }



    /*
     * Проверяем является поле обзятальным или нет
     */
    public function isRequired($field) {
	    return $field['REQUIRED'] ? true : false;
    }



    /*
     * Получаем параметры свойства по его ID
     */
    public function getPropertyID($propertyID) {
        $result = array();
        $values = AdsFields::getList(array(
            'select' => array(
                '*',
                'VALUE_ID'=>'VALUE.ID',
                'VALUE_CODE'=>'VALUE.CODE',
                'VALUE_VALUE'=>'VALUE.VALUE',
                'VALUE_ACTIVE'=>'VALUE.ACTIVE',
                'VALUE_SORT'=>'VALUE.SORT',
            ),
            'filter' => array('ID' => $propertyID)
        ));

        while($val = $values->fetch()) {
            if(!count($result)) {
                $result = array(
                    'ID'=> $val['ID'],
                    'TYPE' => $val['TYPE'],
                    'NAME' => $val['NAME'],
                    'SORT' => $val['SORT'],
                    'ACTIVE' => $val['ACTIVE'],
                    'MULTY'  => $val['MULTY'],
                    'DISPLAY' => $val['DISPLAY'],
                    'HINT' => $val['HINT']
                );
            }
            if($val['VALUE_ID']){
                $result['VALUES'][] = array(
                    'ID' => $val['VALUE_ID'],
                    'SORT' => $val['VALUE_SORT'],
                    'ACTION' => $val['VALUE_ACTIVE'],
                    'CODE' => $val['VALUE_CODE'],
                    'VALUE' => $val['VALUE_VALUE'],
                );
            }
        }

        return $result;
    }

}