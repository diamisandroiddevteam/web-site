<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

IncludeModuleLangFile(__FILE__);

class DataPriceTable extends Entity\DataManager
{

    const TYPE_NAME  = 0;
    const TYPE_PHONE = 1;
    const TYPE_EMAIL = 2;
    const TYPE_SKYPE = 3;
    const TYPE_WEBSITE   = 4;
    const TYPE_TELEGRAM  = 5;


	public static function getTableName() 
	{
		return 'diamis_ads_data_price';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\DatetimeField('DATE_CREATE', [
                'title' => 'Дата создания',
                'default_value' => function () {
                    $lastFriday = date('Y-m-d h:i:s', strtotime('last friday'));
                    return new Type\Date($lastFriday, 'Y-m-d h:i:s');
                }
            ]),
            new Entity\IntegerField('ADS_ID', [
                'required' => true,
                'title' => 'ID Объявления'
        	]),
            new Entity\FloatField('PRICE', [
                'title' => 'Цена',
            ]),
            new Entity\FloatField('PRICE_SALE', [
                'title' => 'Скидка'
            ]),
            new Entity\StringField('CURRENCY', [
                'default_value' => 'RU',
                'title' => 'Валюта'
            ])
		);
	}
}