<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;

IncludeModuleLangFile(__FILE__);

class DataFieldsTable extends Entity\DataManager
{


	public static function getTableName() 
	{
		return 'diamis_ads_data_fields';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\IntegerField('ADS_ID', [
                'required' => true,
                'title' => 'ID Объявления'
        	]),
            new Entity\IntegerField('FIELD_ID', [
                'required' => true,
                'title' => 'ID свойства',
            ]),
        	new Entity\IntegerField('VALUE_ID', [
                'title' => 'ID Enum'
        	]),
        	new Entity\StringField('VALUE', [
                'default_value' => false,
                'title' => 'Значение'
            ])
		);
	}
}