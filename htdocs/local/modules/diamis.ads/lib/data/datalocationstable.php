<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;

IncludeModuleLangFile(__FILE__);

class DataLocationsTable extends Entity\DataManager
{

    const TYPE_NAME  = 0;
    const TYPE_PHONE = 1;
    const TYPE_EMAIL = 2;
    const TYPE_SKYPE = 3;
    const TYPE_WEBSITE   = 4;
    const TYPE_TELEGRAM  = 5;


	public static function getTableName() 
	{
		return 'diamis_ads_data_locations';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\IntegerField('ADS_ID', [
                'required' => true,
                'title' => 'ID Объявления'
        	]),
            new Entity\FloatField('LAT', [
                'title' => 'Lat',
            ]),
        	new Entity\FloatField('LNG', [
                'title' => 'Lng'
        	]),
            new Entity\StringField('VALUE', [
                'default_value' => false,
                'title' => 'Краткое описание'
            ])
		);
	}
}