<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

IncludeModuleLangFile(__FILE__);

class DataServicesTable extends Entity\DataManager
{

    const TYPE_NAME  = 0;
    const TYPE_PHONE = 1;
    const TYPE_EMAIL = 2;
    const TYPE_SKYPE = 3;
    const TYPE_WEBSITE   = 4;
    const TYPE_TELEGRAM  = 5;


	public static function getTableName() 
	{
		return 'diamis_ads_data_services';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\DatetimeField('DATE_CREATE', [
                'title' => 'Дата создания',
                'default_value' => function () {
                    $lastFriday = date('Y-m-d h:i:s', strtotime('last friday'));
                    return new Type\Date($lastFriday, 'Y-m-d h:i:s');
                }
            ]),
            new Entity\DatetimeField('ACTIVE_FROM', [
                'title' => 'Начало активности'
            ]),
            new Entity\DatetimeField('ACTIVE_TO', [
                'title' => 'Окончание активности'
            ]),
            new Entity\IntegerField('SCORE_ID', [
                'required' => true,
                'title' => 'ID Счета'
            ]),
            new Entity\IntegerField('ADS_ID', [
                'required' => true,
                'title' => 'ID Объявления'
        	]),
            new Entity\IntegerField('SERVICE_ID', [
                'required' => true,
                'title' => 'ID Услуги',
            ]),
            new Entity\IntegerField('USER_ID', [
                'title' => 'ID Пользователя',
            ]),
		);
	}
}