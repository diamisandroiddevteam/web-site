<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;

IncludeModuleLangFile(__FILE__);

class RelationsTable extends Entity\DataManager
{



	public static function getTableName() 
	{
		return 'diamis_ads_relations';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\BooleanField('ACTIVE', [
                'default_value' => true,
                'title' => GetMessage('DIAMIS_ADS_RELATIONS_TABLE_ACTIVE')
            ]),
            new Entity\BooleanField('REQUIRED', [
                'title' => GetMessage('DIAMIS_ADS_RELATIONS_TABLE_REQUIRED')
            ]),
            new Entity\BooleanField('FILTER', [
                'default_value' => true,
                'title' => GetMessage('DIAMIS_ADS_RELATIONS_TABLE_FILTER')
            ]),
            new Entity\IntegerField('CATEGORY_ID', [
                'required' => true,
                'title' => GetMessage('DIAMIS_ADS_RELATIONS_TABLE_CATEGORY'),
                'default_value' => 'text'
            ]),
            new Entity\IntegerField('TYPE_ID', [
                'required' => true,
                'title' => GetMessage('DIAMIS_ADS_RELATIONS_TABLE_TYPE'),
                'default_value' => 'text'
            ]),
            new Entity\IntegerField('FIELD_ID', [
                'required' => true,
                'title' => GetMessage('DIAMIS_ADS_RELATIONS_TABLE_FIELD'),
                'default_value' => 'text'
        	]),

            new Entity\IntegerField('SORT', [
                'title' => GetMessage('DIAMIS_ADS_RELATIONS_TABLE_SORT')
            ]),


            new Entity\ReferenceField(
                'CATEGORY',
                'Diamis\Ads\CategoryTable',
                array('=this.CATEGORY_ID' => 'ref.ID')
            ),
            new Entity\ReferenceField(
                'TYPE',
                'Diamis\Ads\TypeTable',
                array('=this.TYPE_ID' => 'ref.ID')
            ),
            new Entity\ReferenceField(
                'FIELD',
                'Diamis\Ads\FieldsTable',
                array('=this.FIELD_ID' => 'ref.ID')
            )
		);
	}
}