<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;


Class FieldsGroupRelationsTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'diamis_ads_fields_group_relations';
    }

    static public function getMap(){
        return array(
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\IntegerField('FIELD_ID', []),
            new Entity\IntegerField('GROUP_ID', []),
            new Entity\ReferenceField(
                'FIELD',
                'Diamis\Ads\FieldsTable',
                array('=this.FIELD_ID' => 'ref.ID')
            ),
            new Entity\ReferenceField(
                'GROUP',
                'Diamis\Ads\FieldsGroupTable',
                array('=this.GROUP_ID' => 'ref.ID')
            ),
        );
    }
}