<?php
namespace Diamis\Ads;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

IncludeModuleLangFile(__FILE__);

class FieldsEnumsTable extends Entity\DataManager
{


	public static function getTableName() 
	{
		return 'diamis_ads_fields_enums';
	}


	public static function getMap()
	{
		return array(
			new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\IntegerField('FIELD_ID', [
                'required' => true,
                'title' => GetMessage('DIAMIS_ADS_ENUMS_TABLE_FIELD')
        	]),
        	new Entity\IntegerField('SORT', [
        		'default_value' => 100,
                'title' => GetMessage('DIAMIS_ADS_ENUMS_TABLE_SORT')
        	]),
        	new Entity\BooleanField('ACTIVE', [
                'default_value' => true,
                'title' => GetMessage('DIAMIS_ADS_ENUMS_TABLE_ACTIVE')
            ]),
            new Entity\StringField('CODE', [
                'title' => GetMessage('DIAMIS_ADS_ENUMS_TABLE_CODE')
    		]),
    		new Entity\StringField('VALUE', [
        		'required' => true,
                'title' => GetMessage('DIAMIS_ADS_ENUMS_TABLE_VALUE')
    		]),
		);
	}



    public static function onBeforeAdd(Entity\Event $event)
    {
        $result = new Entity\EventResult;
        $data = $event->getParameter("fields");

        $code = trim($data['CODE']);
        if(strlen($code)<=0 && isset($data['VALUE'])) {
            $code = Base::translite($data['VALUE']);
            $result->modifyFields(array('CODE' => $code));
        }

        return $result;
    }
}