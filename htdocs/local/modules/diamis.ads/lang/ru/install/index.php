<?php
$MESS['DIAMIS_ADS_MODULE_NAME'] = 'Diamis - Объявления';
$MESS['DIAMIS_ADS_MODULE_DESCRIPTION'] = 'Модуль позволяет размещать на сайте объявления';
$MESS['DIAMIS_ADS_PARTNER_NAME'] = 'Diamis';
$MESS['DIAMIS_ADS_PARTNER_URI'] = 'https://diamis.ru';
$MESS['DIAMIS_ADS_ERROR_VERCION_D7'] = 'Текущая верся ядра не поддерживает технологию D7';