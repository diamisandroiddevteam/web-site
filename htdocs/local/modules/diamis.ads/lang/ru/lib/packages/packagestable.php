<?php
$MESS['DIAMIS_ADS_TABLE_DELETE'] = 'Дата Удаления';
$MESS['DIAMIS_ADS_TABLE_UPDATE'] = 'Дата Обновления';
$MESS['DIAMIS_ADS_TABLE_EXPIRE'] = 'Дата Отключения';
$MESS['DIAMIS_ADS_TABLE_CREATE'] = 'Дата Создания';
$MESS['DIAMIS_ADS_TABLE_ACTIVE'] = 'Активность';
$MESS['DIAMIS_ADS_TABLE_USER_ID'] = 'Автор';
$MESS['DIAMIS_ADS_TABLE_CATEGORY_ID'] = 'Категория';
$MESS['DIAMIS_ADS_TABLE_NAME'] = 'Название';
$MESS['DIAMIS_ADS_TABLE_TEXT'] = 'Описание';
$MESS['DIAMIS_ADS_TABLE_SORT'] = 'Сортировать';
$MESS['DIAMIS_ADS_TABLE_SORT_DATE'] = 'Сортировка по дате';
$MESS['DIAMIS_ADS_TABLE_SORT_DATE_SELECT'] = 'Сортировка по дате Платная';
$MESS['DIAMIS_ADS_TABLE_CODE'] = 'Символьный код';
$MESS['DIAMIS_ADS_TABLE_VIEW'] = 'Кол-во просмотров';
$MESS['DIAMIS_ADS_TABLE_MODER_USER_ID'] = 'Модератор';