<?php

try {

    \Bitrix\Main\Loader::registerAutoLoadClasses(
    'diamis.ads',
    [
        '\Diamis\Ads\ReviewTable' => 'lib/ReviewTable.php',
        '\Diamis\Ads\FavoritesTable' => 'lib/favoritestable.php',

        //    Social
        '\Diamis\Auth\SocialManager' => 'lib/social/SocialManager.php',
        '\Diamis\Auth\SocialAuth' => 'lib/social/SocialAuth.php',
        '\Diamis\Auth\VKontakte' => 'lib/social/VKontakte.php',
        '\Diamis\Auth\Odnoklassniki' => 'lib/social/Odnoklassniki.php',
        //....................................................................................//


        //    Billing
        '\Diamis\Ads\BillingTable' => 'lib/billing/BillingTable.php',
        '\Diamis\Ads\BillingScoreTable' => 'lib/billing/BillingScoreTable.php',
        '\Diamis\Ads\BillingSettingsTable' => 'lib/billing/BillingSettingsTable.php',
        '\Diamis\Ads\BillingTransactionTable' => 'lib/billing/BillingTransactionTable.php',
        //....................................................................................//


        //    Services


        //........................................................................//


        // свойства и группы свойств
        '\Diamis\Ads\FieldsTable' => 'lib/fieldstable.php',
        '\Diamis\Ads\FieldsEnumsTable' => 'lib/fieldsenumstable.php',
        '\Diamis\Ads\FieldsGroupTable' => 'lib/fieldsgrouptable.php',
        '\Diamis\Ads\FieldsGroupRelationsTable' => 'lib/fieldsgrouprelationstable.php',


        // Packages
        '\Diamis\Ads\PackagesTable'         => 'lib/packages/PackagesTable.php',
        '\Diamis\Ads\PackagesActionTable'   => 'lib/packages/PackagesActionTable.php',
        '\Diamis\Ads\PackagesServicesTable' => 'lib/packages/PackagesServicesTable.php',
        '\Diamis\Ads\PackagesCategoryTable' => 'lib/packages/PackagesCategoryTable.php',


        '\Diamis\Ads\DataLocationsTable' => 'lib/data/datalocationstable.php',
        '\Diamis\Ads\DataContactsTable' => 'lib/data/datacontactstable.php',
        '\Diamis\Ads\DataFieldsTable' => 'lib/data/datafieldstable.php',
        '\Diamis\Ads\DataFilesTable' => 'lib/data/datafilestable.php',
        '\Diamis\Ads\DataPriceTable' => 'lib/data/datapricetable.php',
        '\Diamis\Ads\DataServicesTable' => 'lib/data/DataServicesTable.php',


        '\Diamis\Ads\Base' => 'lib/base.php',
        '\Diamis\Ads\Ads' => 'lib/ads.php',
        '\Diamis\Ads\User' => 'lib/user.php',
        '\Diamis\Ads\DomElement' => 'lib/domelement.php',
        '\Diamis\Ads\AdsTable' => 'lib/adstable.php',
        '\Diamis\Ads\FilesTable' => 'lib/filestable.php',
        '\Diamis\Ads\TypeTable' => 'lib/typetable.php',

        '\Diamis\Ads\CategoryTable' => 'lib/categorytable.php',
        '\Diamis\Ads\StopWordTable' => 'lib/stopwordtable.php',
        '\Diamis\Ads\ValueAdsTable' => 'lib/valueadstable.php',
        '\Diamis\Ads\RelationsTable' => 'lib/relationstable.php',
        '\Diamis\Ads\ValueCategoryTable' => 'lib/valuecategorytable.php',
    ]);
} catch (\Bitrix\Main\LoaderException $e) {

}