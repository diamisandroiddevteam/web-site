<?php
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Base;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Application;


Class diamis_ads extends CModule
{
    var $MODULE_ID = "diamis.ads";
    var $MODULE_NAME;
    var $MODULE_DIR;
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_DESCRIPTION;

    var $PARTNER_NAME;
    var $PARTNER_URI;


    function __construct()
    {
        $this->MODULE_PATH = str_replace('\\', '/', realpath(dirname(__FILE__)."/.."));

        $arModuleVersion = array();
        include($this->MODULE_DIR . '/install/version.php');

        $this->MODULE_NAME = Loc::getMessage('DIAMIS_ADS_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('DIAMIS_ADS_MODULE_DESCRIPTION');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

        $this->PARTNER_NAME = Loc::getMessage('DIAMIS_ADS_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('DIAMIS_ADS_PARTNER_URI');
    }




    // Проверяем поддрежку D7
    function isVersionD7()
    {
        global $APPLICATION;

        if($status = CheckVersion(ModuleManager::getVersion('main'), '14.00.00'))
        {
            $APPLICATION->ThrowException(Loc::getMessage('DIAMIS_ADS_ERROR_VERCION_D7'));
        }
        return $status;
    }




    function DoInstall()
    {
        global $USER, $step;
        $step = IntVal($step);

        if($this->isVersionD7() && $USER->IsAdmin())
        {
            ModuleManager::registerModule($this->MODULE_ID);

            $this->InstallFiles();
//            $this->InstallEvents();
            $this->InstallDB();
        }
    }





    function DoUninstall()
    {
        global $APPLICATION, $USER, $step;

        $step = IntVal($step);
        // if($this->isVersionD7() && $USER->isAdmin())
        // {
        //     if($step<=0)
        //     {
        //         $APPLICATION->IncludeAdminFile('Удаление', $this->MODULE_PATH."/install/unstep1.php");
        //     }
        //     elseif($step>0)
        //     {
        //         if($_REQUEST["savedata"]!='Y') 
                    $this->UnInstallDB();


                $this->UnInstallFiles();
                ModuleManager::unRegisterModule($this->MODULE_ID);
        //     }
        // }
        
    }





    function InstallFiles()
    {
        $dir = str_replace('\\', '/', realpath(dirname(__FILE__)."/.."))."/install/admin/";
        CopyDirFiles($dir, $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
        return true;
    }



    function UnInstallFiles()
    {
        $dir = str_replace('\\', '/', realpath(dirname(__FILE__)."/.."))."/install/admin/";
        DeleteDirFiles($dir, $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
        return true;
    }




    function InstallDB()
    {
        global $DB;

        Loader::includeModule($this->MODULE_ID);


        $AdsTable = Base::getInstance('\Diamis\Ads\AdsTable')->getDBTableName();
        $CategoryTable = Base::getInstance('\Diamis\Ads\CategoryTable')->getDBTableName();
        $FieldsTable = Base::getInstance('\Diamis\Ads\FieldsTable')->getDBTableName();
        $FilesTable = Base::getInstance('\Diamis\Ads\FilesTable')->getDBTableName();
        $EnumsTable = Base::getInstance('\Diamis\Ads\FieldsEnumsTable')->getDBTableName();
        $TypeTable = Base::getInstance('\Diamis\Ads\TypeTable')->getDBTableName();
        $StopWordTable = Base::getInstance('\Diamis\Ads\StopWordTable')->getDBTableName();
        $RelationsTable = Base::getInstance('\Diamis\Ads\RelationsTable')->getDBTableName();

        $ValueAdsTable = Base::getInstance('\Diamis\Ads\ValueAdsTable')->getDBTableName();
        $ValueCategoryTable = Base::getInstance('\Diamis\Ads\ValueCategoryTable')->getDBTableName();

        $DataContacts = Base::getInstance('\Diamis\Ads\DataContactsTable')->getDBTableName();
        $DataFields = Base::getInstance('\Diamis\Ads\DataFieldsTable')->getDBTableName();

        $PackageTable = Base::getInstance('\Diamis\Ads\PackagesTable')->getDBTableName();
        $PackageActionTable = Base::getInstance('\Diamis\Ads\PackagesActionTable')->getDBTableName();
        $PackageCategoryTable = Base::getInstance('\Diamis\Ads\PackagesCategoryTable')->getDBTableName();
        $PackageSettingsTable = Base::getInstance('\Diamis\Ads\PackagesServicesTable')->getDBTableName();




        if(!Application::getConnection(\Diamis\Ads\AdsTable::getConnectionName())->isTableExists($AdsTable)) {
            Base::getInstance('\Diamis\Ads\AdsTable')->createDbTable();
        }

        if(!Application::getConnection(\Diamis\Ads\CategoryTable::getConnectionName())->isTableExists($CategoryTable)) {
            Base::getInstance('\Diamis\Ads\CategoryTable')->createDbTable();
        }

        if(!Application::getConnection(\Diamis\Ads\FieldsTable::getConnectionName())->isTableExists($FieldsTable)) {
            Base::getInstance('\Diamis\Ads\FieldsTable')->createDbTable();
        }

        if(!Application::getConnection(\Diamis\Ads\FilesTable::getConnectionName())->isTableExists($FilesTable)) {
            Base::getInstance('\Diamis\Ads\FilesTable')->createDbTable();
        }

        if(!Application::getConnection(\Diamis\Ads\FieldsEnumsTable::getConnectionName())->isTableExists($EnumsTable)) {
            Base::getInstance('\Diamis\Ads\FieldsEnumsTable')->createDbTable();
        }

        if(!Application::getConnection(\Diamis\Ads\TypeTable::getConnectionName())->isTableExists($TypeTable)) {
            Base::getInstance('\Diamis\Ads\TypeTable')->createDbTable();
        }

        if(!Application::getConnection(\Diamis\Ads\RelationsTable::getConnectionName())->isTableExists($RelationsTable)) {
            Base::getInstance('\Diamis\Ads\RelationsTable')->createDbTable();
        }

        if(!Application::getConnection(\Diamis\Ads\StopWordTable::getConnectionName())->isTableExists($StopWordTable)) {
            Base::getInstance('\Diamis\Ads\StopWordTable')->createDbTable();
        }

        if(!Application::getConnection(\Diamis\Ads\ValueAdsTable::getConnectionName())->isTableExists($ValueAdsTable)) {
            Base::getInstance('\Diamis\Ads\ValueAdsTable')->createDbTable();
        }
        if(!Application::getConnection(\Diamis\Ads\ValueCategoryTable::getConnectionName())->isTableExists($ValueCategoryTable)) {
            Base::getInstance('\Diamis\Ads\ValueCategoryTable')->createDbTable();
        }
        if(!Application::getConnection(\Diamis\Ads\FieldsGroupTable::getConnectionName())->isTableExists($ValueCategoryTable)) {
            Base::getInstance('\Diamis\Ads\FieldsGroupTable')->createDbTable();
        }
        if(!Application::getConnection(\Diamis\Ads\DataContactsTable::getConnectionName())->isTableExists($DataContacts)) {
            Base::getInstance('\Diamis\Ads\DataContactsTable')->createDbTable();
        }
        if(!Application::getConnection(\Diamis\Ads\DataFieldsTable::getConnectionName())->isTableExists($DataFields)) {
            Base::getInstance('\Diamis\Ads\DataFieldsTable')->createDbTable();
        }


        if(!Application::getConnection(\Diamis\Ads\PackagesTable::getConnectionName())->isTableExists($PackageTable)) {
            Base::getInstance('\Diamis\Ads\PackagesTable')->createDbTable();
        }
        if(!Application::getConnection(\Diamis\Ads\PackagesActionTable::getConnectionName())->isTableExists($PackageActionTable)) {
            Base::getInstance('\Diamis\Ads\PackagesActionTable')->createDbTable();
        }
        if(!Application::getConnection(\Diamis\Ads\PackagesCategoryTable::getConnectionName())->isTableExists($PackageCategoryTable)) {
            Base::getInstance('\Diamis\Ads\PackagesCategoryTable')->createDbTable();
        }
        if(!Application::getConnection(\Diamis\Ads\PackagesServicesTable::getConnectionName())->isTableExists($PackageSettingsTable)) {
            Base::getInstance('\Diamis\Ads\PackagesServicesTable')->createDbTable();
        }


        // Billing
        $BillingScoreTable = Base::getInstance('\Diamis\Ads\BillingScoreTable')->getDBTableName();
        if(!Application::getConnection(\Diamis\Ads\BillingScoreTable::getConnectionName())->isTableExists($BillingScoreTable))
            Base::getInstance('\Diamis\Ads\BillingScoreTable')->createDbTable();

        $BillingSettingsTable = Base::getInstance('\Diamis\Ads\BillingSettingsTable')->getDBTableName();
        if(!Application::getConnection(\Diamis\Ads\BillingSettingsTable::getConnectionName())->isTableExists($BillingSettingsTable))
            Base::getInstance('\Diamis\Ads\BillingSettingsTable')->createDbTable();

        $BillingTransactionTable = Base::getInstance('\Diamis\Ads\BillingTransactionTable')->getDBTableName();
        if(!Application::getConnection(\Diamis\Ads\BillingTransactionTable::getConnectionName())->isTableExists($BillingTransactionTable))
            Base::getInstance('\Diamis\Ads\BillingTransactionTable')->createDbTable();



        $DataLocationsTable = Base::getInstance('\Diamis\Ads\DataLocationsTable')->getDBTableName();
        if(!Application::getConnection(\Diamis\Ads\DataLocationsTable::getConnectionName())->isTableExists($DataLocationsTable))
            Base::getInstance('\Diamis\Ads\DataLocationsTable')->createDbTable();

        $DataFilesTable = Base::getInstance('\Diamis\Ads\DataFilesTable')->getDBTableName();
        if(!Application::getConnection(\Diamis\Ads\DataFilesTable::getConnectionName())->isTableExists($DataFilesTable))
            Base::getInstance('\Diamis\Ads\DataFilesTable')->createDbTable();

        $DataPriceTable = Base::getInstance('\Diamis\Ads\DataPriceTable')->getDBTableName();
        if(!Application::getConnection(\Diamis\Ads\DataPriceTable::getConnectionName())->isTableExists($DataPriceTable))
            Base::getInstance('\Diamis\Ads\DataPriceTable')->createDbTable();



        $FavoritesTable = Base::getInstance('\Diamis\Ads\FavoritesTable')->getDBTableName();
        if(!Application::getConnection(\Diamis\Ads\FavoritesTable::getConnectionName())->isTableExists($FavoritesTable))
        {
            Base::getInstance('\Diamis\Ads\FavoritesTable')->createDbTable();
            $DB->Query('ALTER TABLE `diamis_ads_favorites` ADD INDEX(`USER_ID`, `ELEMENT_ID`);');
        }



        $DB->Query('ALTER TABLE `diamis_ads_fields_enums` ADD INDEX(`FIELD_ID`);');
        $DB->Query('ALTER TABLE `diamis_ads_fields_group` ADD INDEX(`FIELD_ID`);');
        $DB->Query('ALTER TABLE `diamis_ads_type` ADD INDEX(`CATEGORY_ID`);');
        $DB->Query('ALTER TABLE `diamis_ads_relations` ADD INDEX(`CATEGORY_ID`,`TYPE_ID`,`FIELD_ID`);');
        $DB->Query('ALTER TABLE `diamis_ads` ADD INDEX(`CATEGORY_ID`, `TYPE_ID`, `SORT_DATE`, `SORT_DATE_SELECT`);');
        $DB->Query('ALTER TABLE `diamis_ads_data_contacts` ADD INDEX(`ADS_ID`, `TYPE`);');
        $DB->Query('ALTER TABLE `diamis_ads_data_locations` ADD INDEX(`ADS_ID`, `LAT`, `LNG`);');
        $DB->Query('ALTER TABLE `diamis_ads_data_files` ADD INDEX(`ADS_ID`, `FILE_ID`);');
        $DB->Query('ALTER TABLE `diamis_ads_data_fields` ADD INDEX(`ADS_ID`,`FIELD_ID`,`VALUE_ID`);');
        $DB->Query('ALTER TABLE `diamis_ads_data_price` ADD INDEX(`ADS_ID`);');
        $DB->Query('ALTER TABLE `'.$PackageActionTable.'` ADD INDEX(`PACKAGE_ID`,`ACTION_ID`);');
    }




    function UnInstallDB()
    {
        Loader::includeModule($this->MODULE_ID);


        $FavoritesTable = Base::getInstance('\Diamis\Ads\FavoritesTable')->getDBTableName();


        $AdsTable = Base::getInstance('\Diamis\Ads\AdsTable')->getDBTableName();
        $FilesTable = Base::getInstance('\Diamis\Ads\FilesTable')->getDBTableName();
        $EnumsTable = Base::getInstance('\Diamis\Ads\FieldsEnumsTable')->getDBTableName();
        $TypeTable = Base::getInstance('\Diamis\Ads\TypeTable')->getDBTableName();
        $FieldsTable = Base::getInstance('\Diamis\Ads\FieldsTable')->getDBTableName();
        $CategoryTable = Base::getInstance('\Diamis\Ads\CategoryTable')->getDBTableName();
        $StopWordTable = Base::getInstance('\Diamis\Ads\StopWordTable')->getDBTableName();
        $RelationsTable = Base::getInstance('\Diamis\Ads\RelationsTable')->getDBTableName();

        $ValueAdsTable = Base::getInstance('\Diamis\Ads\ValueAdsTable')->getDBTableName();
        $ValueCategoryTable = Base::getInstance('\Diamis\Ads\ValueCategoryTable')->getDBTableName();

        $DataContacts = Base::getInstance('\Diamis\Ads\DataContactsTable')->getDBTableName();
        $DataLocationsTable = Base::getInstance('\Diamis\Ads\DataLocationsTable')->getDBTableName();
        $DataFields = Base::getInstance('\Diamis\Ads\DataFieldsTable')->getDBTableName();
        $DataFiles = Base::getInstance('\Diamis\Ads\DataFilesTable')->getDBTableName();
        $DataPrice = Base::getInstance('\Diamis\Ads\DataPriceTable')->getDBTableName();

        $PackageTable = Base::getInstance('\Diamis\Ads\PackagesTable')->getDBTableName();
        $PackageActionTable = Base::getInstance('\Diamis\Ads\PackagesTable')->getDBTableName();
        $PackageCategoryTable = Base::getInstance('\Diamis\Ads\PackagesCategoryTable')->getDBTableName();
        $PackageSettingsTable = Base::getInstance('\Diamis\Ads\PackagesServicesTable')->getDBTableName();


        $BillingScore = Base::getInstance('\Diamis\Ads\BillingScoreTable')->getDBTableName();
        $BillingSettings = Base::getInstance('\Diamis\Ads\BillingSettingsTable')->getDBTableName();
        $BillingTransaction = Base::getInstance('\Diamis\Ads\BillingTransactionTable')->getDBTableName();




        // uninstall file
        $categoryFiles = Diamis\Ads\CategoryTable::getList(array(
            'select' => array('FILE_ID'),
            'filter' => array(
                '!=FILE_ID' => null
            )
        ));
        foreach($categoryFiles as $file)
        {
            if($file['FILE_ID'])
            {
                \CFile::Delete($file['FILE_ID']);
            }
        }
        // the end


        Application::getConnection(\Diamis\Ads\FavoritesTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $FavoritesTable);

        Application::getConnection(\Diamis\Ads\AdsTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $AdsTable);
        Application::getConnection(\Diamis\Ads\CategoryTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $CategoryTable);
        Application::getConnection(\Diamis\Ads\FieldsTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $FieldsTable);
        Application::getConnection(\Diamis\Ads\FilesTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $FilesTable);
        Application::getConnection(\Diamis\Ads\FieldsEnumsTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $EnumsTable);
        Application::getConnection(\Diamis\Ads\TypeTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $TypeTable);
        Application::getConnection(\Diamis\Ads\StopWordTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $StopWordTable);
        Application::getConnection(\Diamis\Ads\RelationsTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $RelationsTable);
        Application::getConnection(\Diamis\Ads\ValueAdsTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $ValueAdsTable);
        Application::getConnection(\Diamis\Ads\ValueCategoryTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $ValueCategoryTable);

        Application::getConnection(\Diamis\Ads\DataFilesTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $DataFiles);
        Application::getConnection(\Diamis\Ads\DataFieldsTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $DataFields);
        Application::getConnection(\Diamis\Ads\DataContactsTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $DataContacts);
        Application::getConnection(\Diamis\Ads\DataLocationsTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $DataLocationsTable);
        Application::getConnection(\Diamis\Ads\DataPriceTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $DataPrice);

        Application::getConnection(\Diamis\Ads\PackagesTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $PackageTable);
        Application::getConnection(\Diamis\Ads\PackagesActionTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $PackageActionTable);
        Application::getConnection(\Diamis\Ads\PackagesCategoryTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $PackageCategoryTable);
        Application::getConnection(\Diamis\Ads\PackagesServicesTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $PackageSettingsTable);


        Application::getConnection(\Diamis\Ads\BillingScoreTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $BillingScore);
        Application::getConnection(\Diamis\Ads\BillingSettingsTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $BillingSettings);
        Application::getConnection(\Diamis\Ads\BillingTransactionTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $BillingTransaction);



        \Bitrix\Main\Config\Option::delete($this->MODULE_ID);
    }
}