<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

if(is_file($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php")):
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php";
else:
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/prolog.php";
endif;


$blogModulePermissions = $APPLICATION->GetGroupRight("diamis.ads");
if ($blogModulePermissions < "R"):
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
endif;

IncludeModuleLangFile(__FILE__);
ClearVars();


use Bitrix\Main\Loader;
use Diamis\Ads\CategoryTable;
use Diamis\Ads\FieldsTable;
use Diamis\Ads\FilesTable;
use Diamis\Ads\Base;
use Diamis\Ads;

Loader::includeModule("diamis.ads");



$ID = intval($_REQUEST['ID']);
if($ID>0)
{
    $data = CategoryTable::getList(array(
        'select' => array('*'),
        'filter' => array('ID'=>$ID),
        'limit'  => 1
    ))->fetch();
}


// ===================
//        save
if ($REQUEST_METHOD=="POST"
    && check_bitrix_sessid() 
) {
    $result = false;
    $arFields = array();
    $dbFields = CategoryTable::getMap();
    foreach($dbFields as $resField)
    {
        $type = $resField->getDataType();
        $key = $resField->getName();
        if($key!=='ID' && !empty($_REQUEST[$key])) 
        {
            switch ($type) 
            {
                case 'boolean':
                    $arFields[$key] = ($_REQUEST[$key]=='Y' ? true : false );
                    break;
                case 'integer':
                    $arFields[$key] = intval($_REQUEST[$key]);
                    break;
                default:
                    $arFields[$key] = $_REQUEST[$key];
                    break;
            }
        }
    }



    if(strlen($_FILES['FILE_ID']['tmp_name']))
    {
        // Удаляем файл старый файл перед добавлением
        if($ID > 0 && $data['FILE_ID'])
        {
            \CFile::Delete($data['FILE_ID']);
        }

        $file = array_merge($_FILES['FILE_ID'], array('MODULE_ID'=>Base::$MODULE_ID));
        $file_id = \CFile::SaveFile($file, Base::$MODULE_ID);
        $arFields['FILE_ID'] = $file_id;
    }



    if($ID > 0) // -- Update
    {
        $update = CategoryTable::update($ID, $arFields);
        if($update->isSuccess())
        {
            $result = $ID;
        }
    }
    else        // -- Add
    {
        $add = CategoryTable::add($arFields);
        if($add->isSuccess())
        {
            $result = $add->getId();
        }
    }


    if(!$result)
    {
        // $ex = $APPLICATION->GetException();
        $errorMessage .= "Что то пошло не так<br />";
    }
    else
    {
        if (strlen($apply) <= 0)
        {
            LocalRedirect("/bitrix/admin/diamis_ads_category.php?lang=".LANG);
        }
        else
        {
            LocalRedirect("/bitrix/admin/diamis_ads_category_edit.php?lang=".LANG."&ID=".$result);
        }
    }
}




// Получае список полей
$fields = array();
$fieldsLang = array();
$dbFields = CategoryTable::getMap();
foreach($dbFields as $field):

    $fieldsLang[$field->getName()] = $field->getTitle();

    if($field->getName()!=='ID')
        $fields[] = $field;

endforeach;

// вкладки
$aTabs = array(
    array(
        'DIV' => 'general',
        'TAB' => GetMessage("DIAMIS_ADS_CATEGORY_TABS_GENERAL"),
        'ICON' => 'diamis_ads_category_edit',
        'TITLE' => GetMessage("DIAMIS_ADS_CATEGORY_TABS_GENERAL_TITLE"),
    ),
    array(
        'DIV' => 'props',
        'TAB' => GetMessage("DIAMIS_ADS_CATEGORY_TABS_PROPS"),
        'ICON'=> 'diamis_ads_category_edit',
        'TITLE' => GetMessage("DIAMIS_ADS_CATEGORY_TABS_PROPS_TITLE"),
    ),
    array(
        'DIV' => 'seo',
        'TAB' => GetMessage("DIAMIS_ADS_CATEGORY_TABS_SEO"),
        'ICON'=> 'diamis_ads_category_edit',
        'TITLE' => GetMessage("DIAMIS_ADS_CATEGORY_TABS_SEO_TITLE"),
    )
);


$tabControl = new CAdminTabControl(
    "tabControl",
    $aTabs,
    true,
    true
);


$copy_id = intval($_REQUEST['copy_id']);
$reset_id = intval($_REQUEST['reset_id']);
$strError = '';



$APPLICATION->SetTitle(GetMessage("DIAMIS_ADS_CATEGORY_PAGE_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// Дерево Категорий
$categoryes = CategoryTable::getList(array())->fetchAll();
$resultCategoryes = Base::lists($categoryes);

?>
<form name="ads" method="POST" action="<?=$APPLICATION->GetCurPage()?>?" enctype="multipart/form-data">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="ID" value=<?=$ID?> />
    <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
    <input type="hidden" name="FORM_STRUCTURE" value="" />
    <?
    $tabControl->Begin();
    //********************
    //General Tab
    //********************
    $tabControl->BeginNextTab();
    ?>
    <tr>
        <td width="40%" ><?=$fieldsLang['ACTIVE'];?></td>
        <td width="60%" >
            <input id="ACTIVE" name="ACTIVE" type="checkbox" value="Y" <?if($data['ACTIVE']):?>checked<?endif;?>/>
        </td>
    </tr>
    <tr>
        <td width="40%" ><?=$fieldsLang['NAME'];?></td>
        <td width="60%" >
            <input id="NAME" name="NAME" style="width:100%;" type="text" value="<?=$data['NAME'];?>"/>
        </td>
    </tr>
    <tr>
        <td width="40%" ><?=$fieldsLang['CODE'];?></td>
        <td width="60%" >
            <input id="CODE" name="CODE" style="width:100%;" type="text" value="<?=$data['CODE'];?>"/>
        </td>
    </tr>
    <tr>
        <td width="40%" ><?=$fieldsLang['SORT'];?></td>
        <td width="60%" >
            <input id="SORT" name="SORT" style="width:50px;" type="text" value="<?=$data['SORT'];?>"/>
        </td>
    </tr>
    <tr>
        <td width="40%" ><?=$fieldsLang['FILE_ID'];?></td>
        <td width="60%" >
            <?
            if($data['FILE_ID'] && is_file($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($data['FILE_ID'])))
            {
                echo CFile::GetPath($data['FILE_ID']);
                ?>
                <div style="display:inline-block; overflow:hidden; width: 100%; height:80px; border-radius:6px;padding:5px;  background:#dcdcdc;">
                    <img style="width:100%;height:100%; max-width: 170px; min-width: 80px; min-height:80px;" src="<?=CFile::GetPath($data['FILE_ID']);?>">
                </div>
                <?
            }
            ?>
            <input id="FILE_ID" name="FILE_ID" style="width:100%;" type="file" value="<?=$data['FILE_ID'];?>"/>
        </td>
    </tr>
    <tr>
        <td width="40%" style="vertical-align: top;" ><?=$fieldsLang['PARENT'];?></td>
        <td width="60%">
            <select  id="PARENT" name="PARENT" size="8" style="width:100%; max-width:250px; min-height:80px;">
                <option value='0'>..</option>
                <?
                foreach($resultCategoryes as $category)
                {
                    ?><option <?if($category['ID']==$data['PARENT']):?>selected<?endif;?>
                    value='<?=$category['ID'];?>'><?=$category['~NAME'];?></option><?
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td width="100%" colspan="2">
            <label style='width:100%; padding: 20px; display:block; text-align: center;'><?=$fieldsLang['TEXT'];?></label>
            <textarea style="width:100%; min-height:180px;" id="TEXT" name="TEXT"><?=$data['TEXT'];?></textarea>
        </td>
    </tr>
    <?
    //********************
    //Props Tab
    //********************
    $tabControl->BeginNextTab();
    $fields = FieldsTable::getList(array(
        'order' => array('SORT'=>'ASC'),
        'filter' => array('ACTIVE' => true)
    ));
    ?>
    <tr>
        <td width="100%" colspan='2'>
            <a href="/bitrix/admin/diamis_ads_fields_edit.php?ID=<?=$ID;?>&ENTITY=CATEGORY">Добавить св-во</a>
        </td>
    </tr>
    <?
    //********************
    //SEO Tab
    //********************
    $tabControl->BeginNextTab();
    ?>
    <tr>
        <td width="40%" ><?=$fieldsLang['META_TITLE'];?></td>
        <td width="60%" >
            <input id="META_TITLE" style="width:100%;" name="META_TITLE" type="text" value="<?=$data['META_TITLE'];?>"/>
        </td>
    </tr>
    <tr>
        <td width="40%" ><?=$fieldsLang['META_DESCRIPTION'];?></td>
        <td width="60%" >
            <textarea style="width:100%; min-height:40px;" id="META_DESCRIPTION" name="META_DESCRIPTION"><?=$data['META_DESCRIPTION'];?></textarea>
        </td>
    </tr>
    <tr>
        <td width="40%" ><?=$fieldsLang['META_KEYWORDS'];?></td>
        <td width="60%" >
            <textarea style="width:100%; min-height:40px;" id="META_KEYWORDS" name="META_KEYWORDS"><?=$data['META_KEYWORDS'];?></textarea>
        </td>
    </tr>
    <?
    $tabControl->EndTab();
    $tabControl->Buttons(array(
        "disabled" => null,
        "back_url" => (strlen($back_url) > 0 ? $back_url : "diamis_ads_category.php?lang=".LANGUAGE_ID))
    );
    $tabControl->End();
    ?>
</form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>