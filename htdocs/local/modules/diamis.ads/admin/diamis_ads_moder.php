<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

$urlModulesStyle = '';
if(is_file($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php")):
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php";
    $urlModulesStyle = "/local/modules/diamis.ads/admin/css/style.css";
else:
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/prolog.php";
    $urlModulesStyle = "/bitrix/modules/diamis.ads/admin/css/style.css";
endif;


$blogModulePermissions = $APPLICATION->GetGroupRight("diamis.ads");
if ($blogModulePermissions < "R"):
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
endif;

IncludeModuleLangFile(__FILE__);

use Bitrix\Main\Loader;
use Bitrix\Main\Type;
use Diamis\Ads\AdsTable;


global $USER;

$entity = empty($_REQUERI['entity']) ? 'ads' : $_REQUERI['entity'];

Loader::includeModule("diamis.ads");



if(intval($_REQUEST['ID'])) {
    $id = intval($_REQUEST['ID']);
    switch ($type){
        case 'active_yes': AdsTable::update($id, array('ACTIVE'=>true,'MODER_USER_ID'=>$USER->GetID(), 'DATE_UPDATE'=>new Type\DateTime() )); break;
        case 'active_not': AdsTable::update($id, array('ACTIVE'=>false,'MODER_USER_ID'=>$USER->GetID(), 'DATE_UPDATE'=>new Type\DateTime() )); break;
    };
}



$sTableID = "tbl_diamis_ads";
$fileID = 'diamis_ads';


$headers = array(
    array('id'=>'ID','content'=>'ID','default'=>true),
    array('id'=>'IMAGE','content'=>'Изображения','default'=>true),
    array('id'=>'DATE_CREATE','content'=>'Дата Создания','default'=>true),
    array('id'=>'DATE_UPDATE','content'=>'Дата Обновления','default'=>true),
    array('id'=>'ACTIVE','content'=>'Активен','default'=>true),
    array('id'=>'USER_ID','content'=>'Пользователь','default'=>true),
    array('id'=>'CATEGORY_ID','content'=>'Категория','default'=>true),
    array('id'=>'TYPE_ID','content'=>'Тип','default'=>true),
    array('id'=>'NAME','content'=>'Название','default'=>true),
    array('id'=>'TEXT','content'=>'Описание','default'=>true),
    array('id'=>'PROPERTY','content'=>'Свойства','default'=>true),
);


$oSort = new CAdminSorting($sTableID, "ID", "asc");
$lAdmin = new CAdminList($sTableID, $oSort);


$elements = AdsTable::getElements(array('ACTIVE'=>false), array(), false);
foreach($elements as $element):

    // Получаем данные пользователя
    $user = CUser::GetByID($element['USER_ID'])->Fetch();
    $userTitle = array();
    if($user['LAST_NAME']) $userTitle[] = $user['LAST_NAME'];
    if($user['NAME']) $userTitle[] = $user['NAME'];
    if(!count($userTitle)) $userTitle[] = $user['LOGIN'];


    // Получаем данные изображений
    $images = array();
    foreach($element['FILES'] as $image):
        $images[] = "<div class='admin-image'><img src='".$image."'></div>";
    endforeach;

    // Получем свойства
    $property = array('<ul class="admin-items">');
    foreach($element['PROPERTY'] as $prop):
        $property[] = '<li>'.$prop["NAME"].': '.$prop["VALUE"].'</li>';
    endforeach;
    $property[] = '</ul>';

    $date = '';
    if($element['DATE_CREATE']) $date = $element['DATE_CREATE']->format("Y-m-d H:i:s");

    $dateUpeate = '';
    if($element['DATE_UPDATE']) $dateUpeate = $element['DATE_UPDATE']->format("Y-m-d H:i:s");



    $row =& $lAdmin->AddRow(
            $element["ID"],
        $element
    );
    $row->AddField('ID', '<a href="/bitrix/admin/diamis_ads_edit.php?ID='.$element['ID'].'">'.$element['ID'].'</a>');
    $row->AddField('ACTIVE', ($element['ACTIVE'] ? 'Y' : 'N'));
    $row->AddField('DATE_CREATE', $date);
    $row->AddField('DATE_UPDATE', $dateUpeate);
    $row->AddField('USER_ID', '<a href="/bitrix/admin/user_edit.php?lang=ru&ID='.$element['USER_ID'].'" target="_blank">' . implode(' ',$userTitle) . '</a>');
    $row->AddField('CATEGORY_ID', '('.$element['CATEGORY_ID'].') ' . $element['CATEGORY_NAME']);
    $row->AddField('TYPE_ID', '('.$element['TYPE_ID'].') ' . $element['TYPE_NAME']);
    $row->AddField('NAME', $element['NAME']);
    $row->AddField('TEXT', $element['TEXT']);
    $row->AddField('IMAGE', "<div class='admin-images'>" . implode('', $images) . "</div>");
    $row->AddField('PROPERTY', implode('', $property));


    $arActions = array();

    if(!$element['ACTIVE']){
        $arActions[] = array(
            "ICON" => "update",
            "TEXT" => 'Включить',
            "ACTION"=> $lAdmin->ActionRedirect("diamis_ads_moder.php?ID=".$element["ID"].'&type=active_yes')
        );
    }else{
        $arActions[] = array(
            "ICON" => "update",
            "TEXT" => 'Отключить',
            "ACTION"=> $lAdmin->ActionRedirect("diamis_ads_moder.php?ID=".$element["ID"].'&type=active_not')
        );
    }

    $arActions[] = array(
        "ICON" => "update",
        "TEXT" => 'Изменить',
        "ACTION"=> $lAdmin->ActionRedirect("diamis_ads_edit.php?ID=".$element["ID"])
    );
    $arActions[] = array(
        "ICON" => "delete",
        "TEXT" => 'Удалить',
        "ACTION"=>"if(confirm('Вы действительно хотите удалить?')) ".$lAdmin->ActionDoGroup($element["ID"], "delete", 'bucket='.urlencode($obBucket->ID).'&path='.urlencode($path))
    );

    if(!empty($arActions))
        $row->AddActions($arActions);

endforeach;


$lAdmin->InitFilter(array());
$lAdmin->AddHeaders($headers);


$aMenu[] = array(
    "TEXT"  => GetMessage("DIAMIS_ADS_CREATE"),
    "TITLE" => GetMessage("DIAMIS_ADS_CREATE_TITLE"),
    "LINK" => $fileID . "_edit.php?lang=".LANG,
    "ICON" => "btn_new"
);

$lAdmin->AddAdminContextMenu($aMenu);
$lAdmin->CheckListMode();


$APPLICATION->SetTitle('На модерации');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<link rel="stylesheet" href="<?=$urlModulesStyle;?>">
<form name="ads" method="POST" action="<?=$APPLICATION->GetCurPage()?>?">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="ID" value=<?=$ID?> />
    <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
    <input type="hidden" name="FORM_STRUCTURE" value="" />
    <?
    $oFilter = new CAdminFilter(
        $sTableID."_filter",
        array()
    );

    $oFilter->Begin();

    $oFilter->Buttons(array("table_id"=>$sTableID, "url"=>$APPLICATION->GetCurPage()));
    $oFilter->End();
 	?>
</form>
<?

$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>