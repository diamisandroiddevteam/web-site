<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

if(is_file($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php")):
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php";
else:
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/prolog.php";
endif;


$blogModulePermissions = $APPLICATION->GetGroupRight("diamis.ads");
if ($blogModulePermissions < "R"):
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
endif;

IncludeModuleLangFile(__FILE__);

use Bitrix\Main\Loader;
use Diamis\Ads\RelationsTable;

$entity = empty($_REQUERI['entity']) ? 'ads' : $_REQUERI['entity'];

Loader::includeModule("diamis.ads");



$sTableID = "tbl_diamis_ads_relations";
$fileID = 'diamis_ads_relations';


// Получае список полей
$fields = RelationsTable::getMap();
foreach($fields as $field):
    $key = $field->getName();

    if($key!=='CATEGORY' AND $key!=='FIELD' AND $key!=='TYPE')
    {
        // Список полей
        $arFilterFields[] = $key;

        // Загововок столцов
        $headers[] = array(
            'id' => $key,
            'content' => $key,
            'sort' => 's_'.$key,
            'default' => true
        );
    }
endforeach;



$oSort = new CAdminSorting($sTableID, "ID", "asc");
$lAdmin = new CAdminList($sTableID, $oSort);


if($active === 'dell' && $ID) {
    RelationsTable::delete($ID);
    LocalRedirect("/bitrix/admin/diamis_ads_relations.php?lang=".LANG);
}


$rsData = RelationsTable::getList(array(
    'select'=> array(
        '*',
        'CATEGORY_NAME' =>'CATEGORY.NAME',
        'TYPE_NAME'     =>'TYPE.NAME',
        'FIELD_NAME'    =>'FIELD.NAME'
    ),
    'filter'=> array()
));
while($arRes = $rsData->fetch()):
    $row =& $lAdmin->AddRow($arRes["ID"], $arRes);
    $row->AddField('ID', '<a href="/bitrix/admin/diamis_ads_relations_edit.php?ID='.$arRes['ID'].'">'.$arRes['ID'].'</a>');
    $row->AddField('ACTIVE', ($arRes['ACTIVE'] ? 'Y' : 'N'));
    $row->AddField('REQUIRED', ($arRes['REQUIRED'] ? 'Y' : 'N'));
    $row->AddField('FILTER', ($arRes['FILTER'] ? 'Y' : 'N'));
    $row->AddField('CATEGORY_ID', '(<a target="_blank" href="/bitrix/admin/diamis_ads_category_edit.php?ID='.$arRes['CATEGORY_ID'].'">'.$arRes['CATEGORY_ID'].'</a>) '.$arRes['CATEGORY_NAME']);
    $row->AddField('TYPE_ID', $arRes['TYPE_ID'] ? '(<a target="_blank" href="/bitrix/admin/diamis_ads_types_edit.php?ID='.$arRes['TYPE_ID'].'">'.$arRes['TYPE_ID'].'</a>) '.$arRes['TYPE_NAME'] : '-');
    $row->AddField('FIELD_ID', '(<a target="_blank" href="/bitrix/admin/diamis_ads_fields_edit.php?ID='.$arRes['FIELD_ID'].'">'.$arRes['FIELD_ID'].'</a>) '.$arRes['FIELD_NAME']);


    $arActions = array();
    $arActions[] = array(
        "ICON" => "update",
        "TEXT" => 'Изменить',
        "ACTION"=> $lAdmin->ActionRedirect("diamis_ads_relations_edit.php?ID=".$arRes["ID"])
    );
    $arActions[] = array(
        "ICON" => "delete",
        "TEXT" => 'Удалить',
        "ACTION"=>"if(confirm('Вы действительно хотите удалить?')) ".$lAdmin->ActionRedirect("diamis_ads_relations.php?ID=".$arRes["ID"]."&active=dell")
    );

    if(!empty($arActions))
        $row->AddActions($arActions);
endwhile;


$lAdmin->InitFilter($arFilterFields);
$lAdmin->AddHeaders($headers);


$aMenu[] = array(
    "TEXT"  => GetMessage("DIAMIS_ADS_RELATIONS_CREATE"),
    "TITLE" => GetMessage("DIAMIS_ADS_RELATIONS_CREATE_TITLE"),
    "LINK" => $fileID . "_edit.php?lang=".LANG,
    "ICON" => "btn_new"
);

$lAdmin->AddAdminContextMenu($aMenu);
$lAdmin->CheckListMode();


$APPLICATION->SetTitle(GetMessage("DIAMIS_ADS_RELATIONS_PAGE_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<form name="ads" method="POST" action="<?=$APPLICATION->GetCurPage()?>?">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="ID" value=<?=$ID?> />
    <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
    <input type="hidden" name="FORM_STRUCTURE" value="" />
    <?
    $oFilter = new CAdminFilter(
        $sTableID."_filter",
        array()
    );

    $oFilter->Begin();

    $oFilter->Buttons(array("table_id"=>$sTableID, "url"=>$APPLICATION->GetCurPage()));
    $oFilter->End();
 	?>
</form>
<?

$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>