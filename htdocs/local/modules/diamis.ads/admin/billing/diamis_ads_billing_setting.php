<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

if(is_file($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php")):
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php";
else:
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/prolog.php";
endif;


$blogModulePermissions = $APPLICATION->GetGroupRight("diamis.ads");
if ($blogModulePermissions < "R"):
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
endif;

IncludeModuleLangFile(__FILE__);

use Bitrix\Main\Loader;
use Diamis\Ads\BillingSettingsTable;

Loader::includeModule("diamis.ads");

/* Яндекс.Кассы
 * тестовая карта: 1111111111111026, срок действия: 12 25, cvv: 000.
 */


$sTableID = BillingSettingsTable::getTableName();
$dbData = BillingSettingsTable::getList();
$Data = array();
while($data = $dbData->fetch())
{
    $Data[$data['CODE']] = array(
        'ID' => $data["ID"],
        'CODE' => $data['CODE'],
        'VALUE' => $data['VALUE']
    );
}



if (isset($_REQUEST['save']))
{
    try {
        foreach ($DATA as $item)
        {
            if (intval($item['ID'])>0) {
                BillingSettingsTable::update($item['ID'], array(
                    'NAME'  => htmlspecialchars($item['NAME']),
                    'CODE'  => htmlspecialchars($item['CODE']),
                    'VALUE' => htmlspecialchars($item['VALUE'])
                ));
            } else {
                BillingSettingsTable::add(array(
                    'NAME'  => htmlspecialchars($item['NAME']),
                    'CODE'  => htmlspecialchars($item['CODE']),
                    'VALUE' => htmlspecialchars($item['VALUE'])
                ));
            }
        }
        LocalRedirect('/bitrix/admin/diamis_ads_billing_setting.php?lang='.LANG);
    } catch (Exception $e) {

    }
}


$APPLICATION->SetTitle("Настройки модуля");
$aTabs = array(
    array(
        "DIV" => "edit1",
        "TAB" => 'Настройки модуля',
        "ICON" => "form_edit",
        "TITLE" => 'Настройки модуля'
    )
);
$tabControl = new CAdminTabControl("tabControl", $aTabs, true, true);


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<form name="ads" method="POST" action="<?=$APPLICATION->GetCurPage()?>?">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="ID" value=<?=$ID?> />
    <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
    <input type="hidden" name="save" value="Y" />
    <?
    $tabControl->Begin();
    $tabControl->BeginNextTab();
    ?>
    <tr class="adm-detail-required-field">
        <td width="20%">Яндекс.ID</td>
        <td width="80%">
            <input name="DATA[0][ID]" type="hidden" value="<?=$Data['YandexID']['ID'];?>">
            <input name="DATA[0][NAME]" type="hidden" value="Яндекс.ID">
            <input name="DATA[0][CODE]" type="hidden" value="YandexID">
            <input name="DATA[0][VALUE]" type="text" size="60" maxlength="255" value="<?=htmlspecialcharsbx($Data['YandexID']['VALUE']);?>">
        </td>
    </tr>
    <!--tr class="adm-detail-required-field">
        <td width="20%">Идентификатор магазина</td>
        <td width="80%">
            <input name="DATA[1][ID]" type="hidden" value="<?=$Data['YandexShopID']['ID'];?>">
            <input name="DATA[1][NAME]" type="hidden" value="Идентификатор магазина">
            <input name="DATA[1][CODE]" type="hidden" value="YandexShopID">
            <input name="DATA[1][VALUE]" type="text" size="60" maxlength="255" value="<?=htmlspecialcharsbx($Data['YandexShopID']['VALUE']);?>">
        </td>
    </tr-->
    <tr class="adm-detail-required-field">
        <td width="20%">Яндекс.Кассы</td>
        <td width="80%">
            <input name="DATA[1][ID]" type="hidden" value="<?=$Data['YandexMoney']['ID'];?>">
            <input name="DATA[1][NAME]" type="hidden" value="Яндекс.Кассы">
            <input name="DATA[1][CODE]" type="hidden" value="YandexMoney">
            <input name="DATA[1][VALUE]" type="text" size="60" maxlength="255" value="<?=htmlspecialcharsbx($Data['YandexMoney']['VALUE']);?>">
        </td>
    </tr>
    <?
    $tabControl->EndTab();
    $tabControl->Buttons(array(
        "table_id"=>$sTableID,
        "url"=>$APPLICATION->GetCurPage()
    ));
    $tabControl->End();
    ?>
</form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>
