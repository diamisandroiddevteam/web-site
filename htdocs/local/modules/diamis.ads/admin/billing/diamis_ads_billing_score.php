<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

if(is_file($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php")):
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php";
else:
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/prolog.php";
endif;


$blogModulePermissions = $APPLICATION->GetGroupRight("diamis.ads");
if ($blogModulePermissions < "R"):
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
endif;

IncludeModuleLangFile(__FILE__);

use Bitrix\Main\Loader;
use Diamis\Ads\BillingScoreTable;
use Diamis\Ads\BillingSettingsTable;
use Diamis\Ads\BillingTransactionTable;

$entity = empty($_REQUERI['entity']) ? 'ads' : $_REQUERI['entity'];

Loader::includeModule("diamis.ads");


try {
    // Удаляем элемент
    if($active === 'dell' && $ID) {
        //BillingTransactionTable::delete($ID);
        //LocalRedirect("/bitrix/admin/diamis_ads_billing_score.php?lang=".LANG);
    }



    $Status = BillingTransactionTable::$kodStatus;
    $Data = BillingScoreTable::getList(array(
        'order' => array('ID'=>'DESC')
    ));
    $sTableID = 'tbl_'.BillingScoreTable::getTableName();
    $fields = BillingScoreTable::getMap();

    foreach($fields as $field):

        $key = $field->getName();
        if(stripos($field->getDataType() , '\\')===false):

            // Список полей
            $arFilterFields[] = $key;
            $headers[] = array(
                'id' => $key,
                'content' => $key,
                'sort' => 's_'.$key,
                'default' => true
            );
        endif;
    endforeach;



    $oSort = new CAdminSorting($sTableID, "ID", "asc");
    $lAdmin = new CAdminList($sTableID, $oSort);

    global $USER;

    while($arRes = $Data->fetch()):

        $row =& $lAdmin->AddRow($arRes["ID"], $arRes);

        $row->AddField('ID',        '<a href="/bitrix/admin/diamis_ads_billing_score_edit.php?ID='.$arRes['ID'].'">'.$arRes['ID'].'</a>');
        $row->AddField('STATUS',     $Status[$arRes['ACTIVE']]);
        $row->AddField('DATE_CREATE',$arRes['DATE_CREATE']);
        $row->AddField('USER_ID',    $arRes['USER_ID']);
        $row->AddField('TEXT',       $arRes['TEXT']);
        $row->AddField('NAME',       $arRes['NAME']);
        $row->AddField('PRICE',      $arRes['PRICE']);
        $row->AddField('TYPE',       BillingSettingsTable::$methodYandex[$arRes['TYPE']]);
        $row->AddField('ENTITY',     $arRes['ENTITY']);
        $row->AddField('ENTITY_ID',  $arRes['ENTITY_ID']);


        $arActions = array();
        $arActions[] = array(
            "ICON" => "update",
            "TEXT" => 'Изменить',
            "ACTION"=> $lAdmin->ActionRedirect("diamis_ads_billing_score_edit.php?ID=".$arRes["ID"])
        );
        $arActions[] = array(
            "ICON" => "delete",
            "TEXT" => 'Удалить',
            "ACTION"=>"if(confirm('Вы действительно хотите удалить?')) ".$lAdmin->ActionRedirect("diamis_ads_billing_score.php?ID=".$arRes["ID"]."&active=dell")
        );

        if(!empty($arActions))
            $row->AddActions($arActions);

    endwhile;

    $lAdmin->InitFilter($arFilterFields);
    $lAdmin->AddHeaders($headers);


    $aMenu[] = array(
        "TEXT"  => 'Добавить пакет',
        "TITLE" => 'Добавить пакет',
        "LINK" => "diamis_ads_billing_score_edit.php?lang=".LANG,
        "ICON" => "btn_new"
    );

    $lAdmin->AddAdminContextMenu($aMenu);
    $lAdmin->CheckListMode();

} catch (\Bitrix\Main\ArgumentException $e) {
    die($e->getMessage());
}



// Формируем вывод

$APPLICATION->SetTitle(GetMessage("diamis_ads_billing_score_PAGE_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<form name="ads" method="POST" action="<?=$APPLICATION->GetCurPage()?>?">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="ID" value=<?=$ID?> />
    <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
    <input type="hidden" name="FORM_STRUCTURE" value="" />
    <?
    $oFilter = new CAdminFilter(
        $sTableID."_filter",
        array()
    );

    $oFilter->Begin();
    $oFilter->Buttons(array(
        "table_id"=>$sTableID,
        "url"=>$APPLICATION->GetCurPage()
    ));
    $oFilter->End();
    ?>
</form>
<?
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>
