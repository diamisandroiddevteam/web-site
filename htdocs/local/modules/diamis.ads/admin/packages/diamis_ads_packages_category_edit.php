<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

$urlModulesStyle = '';
if(is_file($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php")):
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php";
    $urlModulesStyle = "/local/modules/diamis.ads/admin/css/style.css";
else:
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/prolog.php";
    $urlModulesStyle = "/bitrix/modules/diamis.ads/admin/css/style.css";
endif;


$blogModulePermissions = $APPLICATION->GetGroupRight("diamis.ads");
if ($blogModulePermissions < "R"):
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
endif;

IncludeModuleLangFile(__FILE__);

use Bitrix\Main\Loader;
use Diamis\Ads\PackagesCategoryTable;
use Diamis\Ads\PackagesTable;
use Diamis\Ads\CategoryTable;
use Diamis\Ads\Base as AdsBase;

$errorMessage = array();
Loader::includeModule("diamis.ads");
$ID = intval($_REQUEST['ID']);


// =========================================================
// ===================       save        ===================
// =========================================================
if ($REQUEST_METHOD=="POST" && check_bitrix_sessid() )
{
    // Получаем список полей и их тип
    $fields = PackagesCategoryTable::getMap();
    $arFields = array();
    foreach($fields as $field):
        $arFields[$field->getName()] = $field->getDataType();
    endforeach;


    $arCategory = $_REQUEST['CATEGORY_ID'];
    unset($_REQUEST['CATEGORY_ID']);

    // Формируем массив для записи
    $arData = array();
    foreach($arFields as $key=>$type):

        if($key==='ACTIVE' && !isset($_REQUEST[$key])) $_REQUEST[$key] = false;

        if($key!=='ID' && isset($_REQUEST[$key])) {

            switch($type) {
                case 'boolean':
                    $arData[$key] = $_REQUEST[$key] ? true : false;
                    break;
                case 'integer':
                    $arData[$key] = intval($_REQUEST[$key]);
                    break;
                default:
                    $arData[$key] = strip_tags($_REQUEST[$key]);
                break;
            }
        }
    endforeach;

    $result = false;
    try{

        // Записываем значение для каждой категории
        if(count($arCategory)) {

            foreach($arCategory as $ctg) {
                $arData['CATEGORY_ID'] = intval($ctg);

                // -- Update
                if($ID)
                {
                    $db = PackagesCategoryTable::update($ID, $arData);
                    if($db->isSuccess()) $result = $db->getId();
                    else $errorMessage[] = $db->getErrorMessages();
                }
                // -- Add
                else
                {
                    $db = PackagesCategoryTable::add($arData);
                    if($db->isSuccess()) $result = $db->getId();
                    else $errorMessage[] = $db->getErrorMessages();
                }
            }
        }


        if(!count($errorMessage))
        {
            // Выполняем редирект
            if(!$_REQUEST['apply']) {
                LocalRedirect('/bitrix/admin/diamis_ads_packages_category.php?lang='.LANG);
            }
            else {
                LocalRedirect('/bitrix/admin/diamis_ads_packages_category_edit.php?lang='.LANG.'&ID='.$ID);
            }
        }

    } catch (Exception $e) {
        $errorMessage[] = $e->getMessage();
    }

}
// =========================================================



try {

    $Data = array();
    if($ID) {
        $Data = PackagesCategoryTable::getList(array('filter'=>array('ID'=>$ID)))->fetch();
    }


    $fields = PackagesCategoryTable::getMap();
    $arFields = array();
    $arFieldsIgnore = array(
        'DATE_CREATE',
        'DATE_UPDATE',
        'DATE_EXPIRE',
        'USER_ID'
    );
    foreach($fields as $field):

        if(array_search($field->getName(), $arFieldsIgnore)===false):
            // Список полей
            $arFields[] = array(
                'TITLE' => $field->getTitle(),
                'KEY'   => $field->getName(),
                'TYPE'  => $field->getDataType()
            );
        endif;
    endforeach;



} catch (\Bitrix\Main\ArgumentException $e) {
    $errorMessage[] = $e->getMessage();
}


// вкладки
$aTabs = array(
    array(
        'DIV'   => 'general',
        'TAB'   => "Параметры",
        'ICON'  => 'diamis_ads_packages',
        'TITLE' => "Параметры",
    ),
);
$tabControl = new CAdminTabControl(
    "tabControl",
    $aTabs,
    true,
    true
);



// Дерево Категорий
$categoryes = CategoryTable::getList(array())->fetchAll();
$resultCategoryes = AdsBase::lists($categoryes);


$arPackage = array();
$dbPackage = PackagesTable::getList();
while($package = $dbPackage->fetch()) {
    $arPackage[$package['ID']] = $package;
}


// Формируем вывод
$APPLICATION->SetTitle("Пакет ".$Data['NAME']);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
CAdminMessage::ShowMessage($errorMessage);
?>
<link rel="stylesheet" href="<?=$urlModulesStyle;?>">
<form name="ads" method="POST" action="<?=$APPLICATION->GetCurPage()?>?">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="ID" value=<?=$ID?> />
    <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
    <input type="hidden" name="FORM_STRUCTURE" value="" />
    <?
    $tabControl->Begin();
    //********************
    //General Tab
    //********************
    $tabControl->BeginNextTab();
    foreach($arFields as $field) {
        ?>
        <tr>
            <td width="40%" ><?=$field['TITLE'];?>:</td>
            <td width="60%" >
                <?
                if($field['KEY']=='ID') {
                    echo $Data['ID'];
                } else if($field['KEY']=='CATEGORY_ID') {
                    ?>
                    <select  id="CATEGORY_ID" name="CATEGORY_ID[]" multiple size="8" style="width:100%; max-width:250px; min-height:80px;">
                        <option value='0'>..</option>
                        <?
                        foreach($resultCategoryes as $category)
                        {
                            ?><option <?if($category['ID']==$Data['CATEGORY_ID']):?>selected<?endif;?>
                            value='<?=$category['ID'];?>'><?=$category['~NAME'];?></option><?
                        }
                        ?>
                    </select>
                    <?
                } else if($field['KEY']=='PACKAGES_ID') {
                    ?>
                    <select  id="PACKAGES_ID" name="PACKAGES_ID" style="width:100%; max-width:250px;">
                        <?
                        foreach($arPackage as $package)
                        {
                            ?><option <?if($package['ID']==$Data['PACKAGES_ID']):?>selected<?endif;?>
                            value='<?=$package['ID'];?>'><?=$package['NAME'];?></option><?
                        }
                        ?>
                    </select>
                    <?
                } else {
                    switch($field['TYPE']):
                        case 'datetime':
                            break;
                        case 'boolean':
                            ?><input id="<?=$field['KEY'];?>" name="<?=$field['KEY'];?>" type="checkbox" <?if($Data[$field['KEY']]):?>checked<?endif;?> value="1"/><?
                            break;
                        case 'string':
                        case 'integer':
                            ?><input  class="custom-input" id="<?=$field['KEY'];?>" name="<?=$field['KEY'];?>" type="text" value="<?=$Data[$field['KEY']];?>"/><?
                            break;
                        case 'text':
                            ?><textarea class="custom-textarea" id="<?=$field['KEY'];?>" name="<?=$field['KEY'];?>"><?=$Data[$field['KEY']];?></textarea><?
                            break;
                    endswitch;
                }
                ?>

            </td>
        </tr>
        <?
    }

    $tabControl->EndTab();
    $tabControl->Buttons(array(
        "disabled" => null,
            "back_url" => (strlen($back_url) > 0 ? $back_url : "diamis_ads_packages_category_edit.php?lang=".LANGUAGE_ID)
    ));
    $tabControl->End();
    ?>
</form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>
