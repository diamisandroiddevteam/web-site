<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

$urlModulesStyle = '';
if(is_file($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php")):
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php";
    $urlModulesStyle = "/local/modules/diamis.ads/admin/css/style.css";
else:
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/prolog.php";
    $urlModulesStyle = "/bitrix/modules/diamis.ads/admin/css/style.css";
endif;


$blogModulePermissions = $APPLICATION->GetGroupRight("diamis.ads");
if ($blogModulePermissions < "R"):
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
endif;

IncludeModuleLangFile(__FILE__);

use Bitrix\Main\Loader;
use Diamis\Ads\PackagesTable;
use Diamis\Ads\PackagesServicesTable;
use Diamis\Ads\PackagesActionTable;

$errorMessage = array();
Loader::includeModule("diamis.ads");
$ID = intval($_REQUEST['ID']);


// =========================================================
// ===================       save        ===================
// =========================================================
if ($REQUEST_METHOD=="POST" && check_bitrix_sessid() )
{
    // Получаем список полей и их тип
    $fields = PackagesTable::getMap();
    $arFields = array();
    foreach($fields as $field):
        $arFields[$field->getName()] = $field->getDataType();
    endforeach;


    // Формируем массив для записи
    $arData = array();
    foreach($arFields as $key=>$type):

        if($key==='ACTIVE' && !isset($_REQUEST[$key])) $_REQUEST[$key] = false;

        if($key!=='ID' && isset($_REQUEST[$key])) {

            switch($type) {
                case 'boolean':
                    $arData[$key] = $_REQUEST[$key] ? true : false;
                    break;
                case 'integer':
                    $arData[$key] = intval($_REQUEST[$key]);
                    break;
                default:
                    $arData[$key] = strip_tags($_REQUEST[$key]);
                break;
            }
        }
    endforeach;

    $result = false;
    try{
        // -- Update
        if($ID)
        {
            $db = PackagesTable::update($ID, $arData);
            if($db->isSuccess()) $result = $db->getId();
            else $errorMessage[] = $db->getErrorMessages();
        }
        // -- Add
        else
        {
            $db = PackagesTable::add($arData);
            if($db->isSuccess()) $result = $db->getId();
            else $errorMessage[] = $db->getErrorMessages();
        }


        if(!count($errorMessage))
        {
            // Выполняем редирект
            if(!$_REQUEST['apply']) {
                LocalRedirect('/bitrix/admin/diamis_ads_packages.php?lang='.LANG);
            }
            else {
                LocalRedirect('/bitrix/admin/diamis_ads_packages_edit.php?lang='.LANG.'&ID='.$ID);
            }
        }

    } catch (Exception $e) {
        $errorMessage[] = $e->getMessage();
    }

}
// =========================================================



try {

    $Data = array();
    $Services = PackagesServicesTable::getList(array())->fetchAll();
    if($ID) {
        $Data = PackagesTable::getList(array('filter'=>array('ID'=>$ID)))->fetch();
        $DataServices = array();
        $dbServices = PackagesActionTable::getList(array(
            'select'=>array('ACTION_ID'),
            'filter'=>array(
                'PACKAGE_ID'=>$ID
            )
        ));
        while($item = $dbServices->fetch()){
            $DataServices[] = $item['ACTION_ID'];
        }
    }


    $fields = PackagesTable::getMap();
    $arFields = array();
    $arFieldsIgnore = array(
        'DATE_CREATE',
        'DATE_UPDATE',
        'DATE_EXPIRE',
        'USER_ID'
    );
    foreach($fields as $field):

        if(array_search($field->getName(), $arFieldsIgnore)===false):
            // Список полей
            $arFields[] = array(
                'TITLE' => $field->getTitle(),
                'KEY'   => $field->getName(),
                'TYPE'  => $field->getDataType()
            );
        endif;
    endforeach;



} catch (\Bitrix\Main\ArgumentException $e) {
    $errorMessage[] = $e->getMessage();
}


// вкладки
$aTabs = array(
    array(
        'DIV'   => 'general',
        'TAB'   => "Параметры",
        'ICON'  => 'diamis_ads_packages',
        'TITLE' => "Параметры",
    ),
    array(
        'DIV'   => 'services',
        'TAB'   => "Услуги",
        'ICON'  => 'diamis_ads_services',
        'TITLE' => "Услуги",
    )
);
$tabControl = new CAdminTabControl(
    "tabControl",
    $aTabs,
    true,
    true
);



// Формируем вывод
$APPLICATION->SetTitle("Пакет ".$Data['NAME']);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

CAdminMessage::ShowMessage($errorMessage);
?>
<link rel="stylesheet" href="<?=$urlModulesStyle;?>">
<form name="ads" method="POST" action="<?=$APPLICATION->GetCurPage()?>?">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="ID" value=<?=$ID?> />
    <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
    <input type="hidden" name="FORM_STRUCTURE" value="" />
    <?
    $tabControl->Begin();
    //********************
    //General Tab
    //********************
    $tabControl->BeginNextTab();
    foreach($arFields as $field) {
        ?>
        <tr>
            <td width="40%" ><?=$field['TITLE'];?>:</td>
            <td width="60%" >
                <?
                if($field['KEY']=='ID') {
                    echo $Data['ID'];
                }
                else if($field['KEY']==='TYPE') {
                    ?><select name="<?=$field['KEY'];?>" id="<?=$field['KEY'];?>"><?
                    foreach(PackagesTable::$type as $key=>$type)
                    {
                        ?><option value="<?=$key;?>"><?=$type;?></option><?
                    }
                    ?></select><?
                }
                else
                {
                    switch($field['TYPE']):
                        case 'datetime':
                            break;
                        case 'boolean':
                            ?><input id="<?=$field['KEY'];?>" name="<?=$field['KEY'];?>" type="checkbox" <?if($Data[$field['KEY']]):?>checked<?endif;?> value="1"/><?
                            break;
                        case 'string':
                        case 'integer':
                            ?><input  class="custom-input" id="<?=$field['KEY'];?>" name="<?=$field['KEY'];?>" type="text" value="<?=$Data[$field['KEY']];?>"/><?
                            break;
                        case 'text':
                            ?><textarea class="custom-textarea" id="<?=$field['KEY'];?>" name="<?=$field['KEY'];?>"><?=$Data[$field['KEY']];?></textarea><?
                            break;
                    endswitch;
                }
                ?>

            </td>
        </tr>
        <?
    }

    //********************
    //Services Tab
    //********************
    $tabControl->BeginNextTab();

    foreach($Services as $item) {
        $checked = '';
        if(array_search($item['ID'], $DataServices)!==false) $checked = 'checked';
        ?>
    <tr style="border-bottom: 1px solid #cecece;">
        <td width="40%" class="custom-bottom" ><input id="services_input_<?=$item['ID'];?>" name="SERVICES[<?=$item['ID'];?>]" value="<?=$item['ID'];?>" type="checkbox" <?=$checked;?>/></td>
        <td width="60%" class="custom-bottom" >
            <label for="services_input_<?=$item['ID'];?>">
                <div class="custom-td-title"><?=$item['NAME'];?></div>
                <div><?=$item['TEXT'];?></div>
            </label>
        </td>
    </tr>
        <?
    }

    $tabControl->EndTab();
    $tabControl->Buttons(array(
        "disabled" => null,
            "back_url" => (strlen($back_url) > 0 ? $back_url : "diamis_ads_packages.php?lang=".LANGUAGE_ID)
    ));
    $tabControl->End();
    ?>
</form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>
