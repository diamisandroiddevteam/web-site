<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

if(is_file($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php")):
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php";
else:
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/prolog.php";
endif;


$blogModulePermissions = $APPLICATION->GetGroupRight("diamis.ads");
if ($blogModulePermissions < "R"):
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
endif;

IncludeModuleLangFile(__FILE__);

use \Bitrix\Main\Loader;
use \Diamis\Ads\TypeTable;
use \Diamis\Ads\FieldsTable;
use \Diamis\Ads\FieldsEnumsTable;
use \Diamis\Ads\CategoryTable;
use \Diamis\Ads\RelationsTable;



Loader::includeModule("diamis.ads");
CJSCore::Init(array("jquery"));

$errorMessage = array();

$ID = intval($_REQUEST['ID']);

if($_REQUEST['type']=='dellRelations')
{
    $db = RelationsTable::getList(array(
        'select' => array('ID'),
        'filter' => array(
            'FIELD_ID' => intval($_REQUEST['field_id']),
            'CATEGORY_ID' => intval($_REQUEST['category_id']),
        )
    ));
    while($res = $db->fetch())
        RelationsTable::delete($res['ID']);
    die();
}
if($_REQUEST['type']=='dell')
{
    FieldsEnumsTable::delete($_REQUEST['enum']);
    die();
}
else if($_REQUEST['type']=='type')
{
    global $APPLICATION;

    $APPLICATION->RestartBuffer();
    $types = TypeTable::getList(array(
        'filter' => array(
            'CATEGORY_ID'=> $_REQUEST['category']
        )
    ))->fetchAll();

    echo json_encode($types);
    die();
}


// ===================
//        save
if ($REQUEST_METHOD=="POST"
    && check_bitrix_sessid() 
) {
    $arFields = array();
    if(!empty($_REQUEST['TYPE'])) $arFields['TYPE'] = $_REQUEST['TYPE'];
    if(!empty($_REQUEST['NAME'])) $arFields['NAME'] = trim($_REQUEST['NAME']);
    if(!empty($_REQUEST['SORT'])) $arFields['SORT'] = intval($_REQUEST['SORT']);
    if(!empty($_REQUEST['HINT'])) $arFields['HINT'] = trim($_REQUEST['HINT']);
    if(!empty($_REQUEST['MULTY'])) $arFields['MULTY'] = ($_REQUEST['MULTY'] ? true : false);
    if(!empty($_REQUEST['ACTIVE'])) $arFields['ACTIVE'] = ($_REQUEST['ACTIVE'] ? true : false);

    $arEnum = $_REQUEST['ENUM'];
    $arGroup = $_REQUEST['GROUP'];
    $arRelations = $_REQUEST['RELATIONS'];

    if($ID > 0) // -- Update
    {
        $update = FieldsTable::update($ID, $arFields);
        if($update->isSuccess())
        {
            $resGroup = FieldsTable::updateGroup($ID, $arGroup);
            $resEnum = FieldsTable::updateEnum($ID, $arEnum);
            $resRelations = FieldsTable::addRelations($ID, $arRelations);
        }
    }
    else        // -- Add
    {

        $add = FieldsTable::add($arFields);
        if($add->isSuccess()) $ID = $add->getId();

        if($ID>0)
        {
            $resGroup = FieldsTable::addGroup($ID, $arGroup);
            $resEnum = FieldsTable::addEnum($ID, $arEnum);
            $resRelations = FieldsTable::addRelations($ID, $arRelations);
        }
    }


    if(count($resEnum)) $errorMessage = array_merge($errorMessage, $resEnum);
    if(count($resGroup)) $errorMessage = array_merge($errorMessage, $resGroup);
    if(count($resRelations)) $errorMessage = array_merge($errorMessage, $resRelations);


    if(count($errorMessage))
    {
        p($errorMessage);
    }
    else
    {
        if (strlen($apply) <= 0)
        {
            LocalRedirect("/bitrix/admin/diamis_ads_fields.php?lang=".LANG);
        }
        else
        {
            LocalRedirect("/bitrix/admin/diamis_ads_fields_edit.php?lang=".LANG."&ID=".$ID);
        }
    }
    
}
// ===== the end =====
// ===================


if($ID > 0)
{
    $data = FieldsTable::getList(array(
        'filter'=> array('ID' => $ID),
        'limit' => 1
    ))->fetch();
}




// вкладки
$aTabs = array(
    array(
        'DIV' => 'general',
        'TAB' => GetMessage("DIAMIS_ADS_FIELDS_TABS_GENERAL"),
        'ICON' => 'diamis_ads_fields_edit',
        'TITLE' => GetMessage("DIAMIS_ADS_FIELDS_TABS_GENERAL_TITLE"),
    ),
    array(
        'DIV' => 'groups',
        'TAB' => GetMessage("DIAMIS_ADS_FIELDS_GROUP_TABS_GROUP"),
        'ICON' => 'diamis_ads_fields_edit',
        'TITLE' => GetMessage("DIAMIS_ADS_FIELDS_GROUP_TABS_GROUP_TITLE"),
    ),
    array(
        'DIV' => 'relations',
        'TAB' => GetMessage("DIAMIS_ADS_FIELDS_TABS_RELATIONS"),
        'ICON' => 'diamis_ads_fields_edit',
        'TITLE' => GetMessage("DIAMIS_ADS_FIELDS_TABS_RELATIONS_TITLE"),
    )
);
$tabControl = new CAdminTabControl(
    "tabControl",
    $aTabs,
    true,
    true
);



$APPLICATION->SetTitle(GetMessage("DIAMIS_ADS_RELATIONS_PAGE_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$keyEnum = 0;

?>

<? CAdminMessage::ShowMessage($errorMessage);?>
<form name="ads" method="POST" action="<?=$APPLICATION->GetCurPage()?>?">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="ID" value=<?=$ID?> />
    <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
    <input type="hidden" name="FORM_STRUCTURE" value="" />
    <?
    $tabControl->Begin();
	//********************
    //   General Tab
    //********************
    $tabControl->BeginNextTab();
    if($ID > 0)
    {
        ?><tr>
            <td width="40%">ID:</td>
            <td width="60%"><?=$ID;?></td>
        </tr>
        <?
    }
    ?>
    <tr>
        <td width="40%" >Активность:</td>
        <td width="60%" >
            <input id="ACTIVE" name="ACTIVE" type="checkbox" <?if($data['ACTIVE']):?>checked<?endif;?> value="1"/>
        </td> 
    </tr>
    <tr>
        <td width="40%" >Множественное:</td>
        <td width="60%" >
            <input id="MULTY" name="MULTY" type="checkbox" <?if($data['MULTY']):?>checked<?endif;?> value="1"/>
        </td>
    </tr>
	<tr>
		<td width="40%">Тип:</td>
		<td width="60%">
            <?
            if($data['TYPE']=='enum'
                || $data['TYPE']=='select'
                || $data['TYPE']=='radio'
                || $data['TYPE']=='slider'
                || $data['TYPE']=='file'):
                ?>
                <?=$data['TYPE'];?>
            <?else:?>
                <select id="TYPE" name="TYPE" style="width:120px;" onchange="selectList(this);">
                    <?
                    $types = FieldsTable::$type;
                    foreach($types as $type=>$name):
                        ?>
                        <option <?if($data['TYPE']==$type):?>selected<?endif;?>
                                value="<?=$type;?>"><?=$type;?>
                        </option>
                    <?
                    endforeach;
                    ?>
                </select>
            <?endif;?>
		</td>
	</tr>
    <tr>
        <td width="40%">Способ отображения:</td>
        <td width="60%"></td>
    </tr>
    <tr>
        <td width="40%" >Название:</td>
        <td width="60%" >
            <input id="NAME" name="NAME" style="width:100%;" type="text" value="<?=$data['NAME'];?>"/>
        </td> 
    </tr>
    <tr>
        <td width="40%" >Сортировать:</td>
        <td width="60%" >
            <input id="SORT" name="SORT" style="width:50px;" type="text" value="<?=$data['SORT'];?>"/>
        </td>
    </tr>
    <tr class="list"
    <?if($data['TYPE']!=='enum'
      && $data['TYPE']!=='select'
      && $data['TYPE']!=='radio'
      && $data['TYPE']!=='slider'
      && $data['TYPE']!=='file'):
    ?>style="display:block;"<?endif;?>>
        <!--td width="40%" style="vertical-align:top;">Список значеий:</td-->
        <td width="100%" colspan="2">
            <table id="enum-items" style="width:100%;">
                <tr class="list-hread">
                    <th>Значение</th>
                    <th>Символьный код</th>
                    <th>Сортировать</th>
                    <th>Активно</th>
                    <th>Действие</th>
                </tr>
                <?php
                if($ID>0)
                {
                    $enums = \Diamis\Ads\FieldsEnumsTable::getList(array(
                        'filter' => array('FIELD_ID'=>$ID)
                    ));
                    while($enum = $enums->fetch())
                    {
                        ?>
                        <tr>
                            <td width="30%"><input value="<?=$enum['VALUE'];?>" name="ENUM[<?=$keyEnum;?>][VALUE]" style="width:95%;" type="text"/></td>
                            <td width="30%"><input value="<?=$enum['CODE'];?>" name="ENUM[<?=$keyEnum;?>][CODE]"  style="width:95%;" type="text"/></td>
                            <td width="20%"><input value="<?=$enum['SORT'];?>" name="ENUM[<?=$keyEnum;?>][SORT]"  style="width:95%;" type="text"/></td>
                            <td width="10%" style="text-align:center;">
                                <input type="hidden" name="ENUM[<?=$keyEnum;?>][ID]" value="<?=$enum['ID'];?>">
                                <input name="ENUM[<?=$keyEnum;?>][ACTIVE]" value="1" type="checkbox" <?if($enum['ACTIVE']):?> checked<?endif;?> />
                            </td>
                            <td width="10%" style="text-align:center;">
                                <span class="dell-enum" style="margin-left:10px; cursor:pointer;" onclick="dellEnum(this,<?=$enum['ID'];?>);">Удалить</span>
                            </td>
                        </tr>
                        <?
                        $keyEnum++;
                    }
                }
                else
                {
                    $keyEnum = 2;
                    ?>
                    <tr>
                        <td width="30%"><input name="ENUM[0][VALUE]" style="width:95%;" type="text"/></td>
                        <td width="30%"><input name="ENUM[0][CODE]" style="width:95%;" type="text"/></td>
                        <td width="20%"><input name="ENUM[0][SORT]" style="width:95%;" type="text"/></td>
                        <td width="10%" style="text-align:center;">
                            <input name="ENUM[0][ACTIVE]" value="1" type="checkbox" checked/>
                        </td>
                        <td width="10%" style="text-align:center;">
                            <span class="dell-enum" style="margin-left:10px; cursor:pointer;" onclick="dellEnum(this);">Удалить</span>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><input name="ENUM[1][VALUE]" style="width:95%;" type="text"/></td>
                        <td width="30%"><input name="ENUM[1][CODE]" style="width:95%;" type="text"/></td>
                        <td width="20%"><input name="ENUM[1][SORT]" style="width:95%;" type="text"/></td>
                        <td width="10%" style="text-align:center;">
                            <input name="ENUM[1][ACTIVE]" value="1" type="checkbox" checked/>
                        </td>
                        <td width="10%" style="text-align:center;">
                            <span class="dell-enum" style="margin-left:10px; cursor:pointer;" onclick="dellEnum(this);">Удалить</span>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <table>
                <tr>
                    <td colspan="4">
                        <div class="adm-btn" style="display: inline-block; width: 150px; line-height: 30px; text-align: center;" onclick="addEnum();">Добавить</div>
                    </td>
                </tr>
            </table>
        </td> 
    </tr>
	<?

    //********************
    //   Group Tab
    //********************
    $tabControl->BeginNextTab();
    $group = array();
    $dbGSelect = \Diamis\Ads\FieldsGroupRelationsTable::getList(array(
        'filter'=>array(
            'FIELD_ID' => $ID
        )
    ))->fetchAll();
    foreach($dbGSelect as $item)
    {
        $group[$item['ID']] = $item['GROUP_ID'];
    }

    $dbGroup = \Diamis\Ads\FieldsGroupTable::getList(array());
    foreach($dbGroup as $key=>$item):
        $relationsID = array_search($item['ID'], $group);
        ?>
        <tr>
            <td width="100%">
                <label>
                    <?if($relationsID!==false):?>
                        <input name="GROUP[<?=$key;?>][RELATIONS_ID]" type="hidden" value="<?=$relationsID;?>"/>
                        <input name="GROUP[<?=$key;?>][GROUP_ID]" type="checkbox" checked value="<?=$item['ID'];?>" />
                    <?else:?>
                        <input name="GROUP[<?=$key;?>][GROUP_ID]" type="checkbox" value="<?=$item['ID'];?>" />
                    <?endif;?>
                    <?=$item['NAME'];?>
                </label>
            </td>
        </tr>
        <?
    endforeach;

    $tabControl->BeginNextTab();

    $filterType = array();
    $dbRelations = RelationsTable::getList(array(
        'filter'=>array('FIELD_ID' => $ID)
    ));
    while($rel = $dbRelations->fetch())
    {
        $relations[$rel['CATEGORY_ID']]['CATEGORY_ID'] = $rel['CATEGORY_ID'];
        if($rel['TYPE_ID'])
        {
            $relations[$rel['CATEGORY_ID']]['TYPES'][] = $rel['TYPE_ID'];
            $filterType[] = $rel['TYPE_ID'];
        }
    }

    $ctgTypes = array();
    $dbCtgType = TypeTable::getList(array(
        'filter'=> array('ID'=> $filterType)
    ));
    while($rel = $dbCtgType->fetch())
    {
        $ctgTypes[$rel['ID']] = $rel;
    }

    // Дерево Категорий
    $categoryes = CategoryTable::getList(array())->fetchAll();
    $resultCategoryes = \Diamis\Ads\Base::lists($categoryes);
    ?>
    <tr>
        <td width="50%" style="text-align: left;">Категория</td>
        <td width="50%" style="text-align: left;">Тип</td>
    </tr>
    <tr>
        <td width="50%">
            <select onchange="getType(this);"
                    id="RELATIONS_CATEGORY_ID"
                    size="8"
                    style="width:100%; max-width:250px; height:182px;">
                <option value='0'>..</option>
                <?
                foreach($resultCategoryes as $category)
                {
                    if(isset($relations[$category['ID']]))
                    {
                        $relations[$category['ID']]['NAME'] = $category['NAME'];
                    }

                    ?><option <?if($category['ID']==$data['RELATIONS_CATEGORY_ID']):?>selected<?endif;?>
                    value='<?=$category['ID'];?>'><?=$category['~NAME'];?></option><?
                }
                ?>
            </select>
        </td>
        <td width="50%" class="relations_type">
            <select id="RELATIONS_TYPE_ID"
                    size="8"
                    multiple
                    style="width:100%; max-width:250px; height:182px;">
            </select>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="adm-btn" style="display: inline-block; width: 150px; line-height: 30px; text-align: center;" onclick="addRelations();">Добавить связь</div>
        </td>
    </tr>
    <tr>
        <td class="relations_list" colspan="2">
            <?
            if(count($relations)):
                foreach($relations as $relation):
                    ?>
                    <div class="relations_list_item" style="background: #e1eaec;">
                        <div class="relations_list_item_dell" onclick="dellRelations(this, <?=$ID;?>, <?=$relation['CATEGORY_ID'];?>);">Удалить</div>
                        <div><b>Категория:</b> <?=$relation['NAME'];?></div>
                        <div>
                            <?
                            foreach($relation['TYPES'] as $typeID):
                                ?><span><?=$ctgTypes[$typeID]['NAME'];?></span><?
                            endforeach;
                            ?>
                        </div>
                    </div>
                    <?
                endforeach;
            endif;
            ?>
        </td>
    </tr>
    <?
    $tabControl->EndTab();
    $tabControl->Buttons(array(
        "disabled" => null,
        "back_url" => (strlen($back_url) > 0 ? $back_url : "diamis_ads_fields.php?lang=".LANGUAGE_ID))
    );
    $tabControl->End();
    ?>
</form>
<style type="text/css">
.relations_list{
    padding:20px 0;
}
.relations_list_item{
    padding:10px;
    font-size: 15px;
    border: 1px solid #c4ced2;
    margin: 5px 0;
    position: relative;
}
.relations_list_item div{
    margin: 3px 0;
}
.relations_list_item span{
    padding:5px 10px 5px 0;
    font-size: 14px;
}
.relations_list_item_dell{
    position: absolute;
    top: 5px;
    right: 5px;
    display: inline-block;
    text-decoration: underline;
    font-size:12px;
    cursor: pointer;
}
.relations_list_item_dell:hover{
    color: red;
}
</style>
<script type="text/javascript">
var cacheType = [];

function dellRelations(el, fieldID, categoryID)
{
    if(fieldID>0 && categoryID>0)
    {
        $.post('/bitrix/admin/diamis_ads_fields_edit.php',
            {
                type: 'dellRelations',
                field_id: fieldID,
                category_id: categoryID
            });
    }

    var row = $(el).parent();
    row.remove();
}

function addRelations()
{
    var list = $('.relations_list'),
        category = $('#RELATIONS_CATEGORY_ID'),
        categoryName = category.find('[value='+category.val()+']').html(),
        type = $('#RELATIONS_TYPE_ID');

    var $item = $('<div/>').attr({
                           class: 'relations_list_item'
                        }).html('<div class="relations_list_item_dell" onclick="dellRelations(this);">Очистить</div><div><b>Категория:</b> ' + categoryName + '</div>').appendTo(list);

    $('<input/>').attr({
        type: 'hidden',
        name: 'RELATIONS[CATEGORY]['+category.val()+'][ID]',
        value: category.val()
    }).appendTo($item);

    $type = $('<div/>').html('<b>Тип:</b> ');
    $.each(type.val(), function(i, item){
        var name = type.find('[value='+item+']').html();

        $('<span/>').html(name + '; ').appendTo($type);
        $('<input/>').attr({
            type: 'hidden',
            name: 'RELATIONS[CATEGORY]['+category.val()+'][TYPE][]',
            value: item
        }).appendTo($type);
    });

    $type.appendTo($item);
}

function getType(el)
{
    var type = $('.relations_type select'),
        val = $(el).val(),
        key = 'key';


    $.each(val, function(i, item){
        key += '_' + item;
    });

    // не пашет временно
    if(cacheType.length >0 && cacheType[key] !== undefined)
    {
        var Data = cacheType[val];

        type.html('');
        $('<option/>').attr({value: 0}).html('--').appendTo(type);

        $.each(Data, function(i, item){

            $('<option/>').attr({
                value: item.ID
            }).html(item.NAME).appendTo(type);
        })
    }
    else
    {
        $.post(
            '/bitrix/admin/diamis_ads_fields_edit.php',
            {
                'type': 'type',
                'category': val
            },
            function(data) {
                var Data = JSON.parse(data);

                cacheType[key] = Data;
                type.html('');

                $('<option/>').attr({value: 0}).html('--').appendTo(type);

                $.each(Data, function(i, item){

                    $('<option/>').attr({
                        value: item.ID
                    }).html(item.NAME).appendTo(type);
                })
            }
        );
    }
}


function dellEnum(el, id)
{
    var row = $(el).parents('tr');
    row.get(0).remove();

    if(id<0) return false;

    $.post(
        '/bitrix/admin/diamis_ads_fields_edit.php',
        {
            'type': 'dell',
            'enum': id
        }
    );
}

function selectList(el) {
    var list = $('.list'),
        val = $(el).val();

    if(val=='select'
    || val=='radio'
    || val=='enum'
    || val=='slider'
    ) list.css('display', 'table-row');
    else list.css('display', 'none');
}

var enumKey = <?=$keyEnum;?>;
function addEnum()
{
    var table = $('#enum-items'),
        row = '' +
    '<tr>' +
        '<td width="30%"><input name="ENUM['+enumKey+'][VALUE]" style="width:95%;" type="text"/></td>' +
        '<td width="30%"><input name="ENUM['+enumKey+'][CODE]"  style="width:95%;" type="text"/></td>' +
        '<td width="20%"><input name="ENUM['+enumKey+'][SORT]"  style="width:95%;" type="text"/></td>' +
        '<td width="10%" style="text-align:center;">' +
        '   <input name="ENUM['+enumKey+'][ACTIVE]" value="1" type="checkbox" checked />' +
        '</td>' +
        '<td width="10%" style="text-align:center;">' +
        '   <span class="dell-enum" style="margin-left:10px;cursor:pointer;" onclick="dellEnum(this);">Удалить</span>' +
        '</td>' +
    '</tr>';
    $(table).append(row);
    enumKey++;
}
</script>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>