<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

if(is_file($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php")):
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php";
else:
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/prolog.php";
endif;


$blogModulePermissions = $APPLICATION->GetGroupRight("diamis.ads");
if ($blogModulePermissions < "R"):
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
endif;

IncludeModuleLangFile(__FILE__);

use Bitrix\Main\Loader;
use Diamis\Ads\Base;
use Diamis\Ads\TypeTable;
use Diamis\Ads\CategoryTable;


Loader::includeModule("diamis.ads");



$ID = intval($_REQUEST['ID']);
$errorMessage = array();


if($ID > 0)
{
    $data = TypeTable::getList(array(
        'filter'=> array('ID' => $ID),
        'limit' => 1
    ))->fetch();
}



// ===================
//        save
if ($REQUEST_METHOD=="POST"
    && check_bitrix_sessid() 
) {
    $arFields = array();
    if(!empty($_REQUEST['NAME'])) $arFields['NAME'] = trim($_REQUEST['NAME']);
    if(!empty($_REQUEST['CODE'])) $arFields['CODE'] = trim($_REQUEST['CODE']);
    if(!empty($_REQUEST['SORT'])) $arFields['SORT'] = intval($_REQUEST['SORT']);
    if(!empty($_REQUEST['CATEGORY_ID'])) $arFields['CATEGORY_ID'] = intval($_REQUEST['CATEGORY_ID']);
    if(!empty($_REQUEST['ACTIVE'])) $arFields['ACTIVE'] = ($_REQUEST['ACTIVE'] ? true : false);


    if($ID > 0) // -- Update
    {
        $updateCategory = TypeTable::update($ID, $arFields);
        if(!$updateCategory->isSuccess()) {
            $errorMessage = $updateCategory->getErrorMessages();
        }
    }
    else        // -- Add
    {
        $addCategory = TypeTable::add($arFields);
        if(!$addCategory->isSuccess()) {
            $errorMessage = $addCategory->getErrorMessages();
        }
        else {
            $ID = $addCategory->getId();
        }
    }


    if(!count($errorMessage))
    {
        if (strlen($apply) <= 0)
        {
            LocalRedirect("/bitrix/admin/diamis_ads_types.php?lang=".LANG);
        }
        else
        {
            LocalRedirect("/bitrix/admin/diamis_ads_types_edit.php?lang=".LANG."&ID=".$ID);
        }
    }
    
}
// ===== the end =====
// ===================







// вкладки
$aTabs = array(
    array(
        'DIV' => 'general',
        'TAB' => GetMessage("DIAMIS_ADS_TYPE_TABS_GENERAL"),
        'ICON' => 'diamis_ads_category_edit',
        'TITLE' => GetMessage("DIAMIS_ADS_TYPE_TABS_GENERAL_TITLE"),
    )
);
$tabControl = new CAdminTabControl(
    "tabControl",
    $aTabs,
    true,
    true
);


// Дерево Категорий
$categoryes = CategoryTable::getList(array())->fetchAll();
$resultCategoryes = Base::lists($categoryes);


$APPLICATION->SetTitle(GetMessage("DIAMIS_ADS_TYPE_PAGE_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?CAdminMessage::ShowMessage($errorMessage);?>
<form name="ads" method="POST" action="<?=$APPLICATION->GetCurPage()?>?">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="ID" value=<?=$ID?> />
    <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
    <input type="hidden" name="FORM_STRUCTURE" value="" />
    <?
    $tabControl->Begin();
	//********************
    //General Tab
    //********************
    $tabControl->BeginNextTab();
    ?>
    <tr>
        <td width="40%" >Активность:</td>
        <td width="60%" >
            <input id="ACTIVE" name="ACTIVE" type="checkbox" <?if($data['ACTIVE']):?>checked<?endif;?> value="1"/>
        </td> 
    </tr>
    <tr>
        <td width="40%" >Название:</td>
        <td width="60%" >
            <input id="NAME" name="NAME" style="width:100%;" type="text" value="<?=$data['NAME'];?>"/>
        </td>
    </tr>
    <tr>
        <td width="40%" >Символьный код:</td>
        <td width="60%" >
            <input id="CODE" name="CODE" style="width:100%;" type="text" value="<?=$data['CODE'];?>"/>
        </td>
    </tr>
    <tr>
        <td width="40%" >Сортировать:</td>
        <td width="60%" >
            <input id="SORT" name="SORT" style="width:50px;" type="text" value="<?=$data['SORT'];?>"/>
        </td>
    </tr>
    <tr>
        <td width="40%" >Категория:</td>
        <td width="60%" >
            <select  id="CATEGORY_ID" name="CATEGORY_ID" size="8" style="width:100%; max-width:250px; min-height:80px;">
                <option value='0'>..</option>
                <?
                foreach($resultCategoryes as $category)
                {
                    ?><option <?if($category['ID']==$data['CATEGORY_ID']):?>selected<?endif;?>
                    value='<?=$category['ID'];?>'><?=$category['~NAME'];?></option><?
                }
                ?>
            </select>
        </td>
    </tr>
	<?
    $tabControl->EndTab();
    $tabControl->Buttons(array(
        "disabled" => null,
        "back_url" => (strlen($back_url) > 0 ? $back_url : "diamis_ads_types.php?lang=".LANGUAGE_ID))
    );
    $tabControl->End();
    ?>
</form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>