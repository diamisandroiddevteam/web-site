<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

if(is_file($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php")):
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php";
else:
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/prolog.php";
endif;


$blogModulePermissions = $APPLICATION->GetGroupRight("diamis.ads");
if ($blogModulePermissions < "R"):
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
endif;

IncludeModuleLangFile(__FILE__);

use Bitrix\Main\Loader;
use Diamis\Ads\FieldsGroupTable;



Loader::includeModule("diamis.ads");

$ID = intval($_REQUEST['ID']);


// ===================
//        save
if ($REQUEST_METHOD=="POST"
    && check_bitrix_sessid()
) {
    $arFields = array(
        'NAME'   => $NAME,
        'SORT'   => intval($SORT),
        'ACTIVE' => ($_REQUEST['ACTIVE'] ? true : false),
    );

    if($ID > 0) // -- Update
    {
        $update = FieldsGroupTable::update($ID, $arFields);
        if($update->isSuccess())
        {
            $result = $update->getId();
        }
    }
    else        // -- Add
    {
        $add = FieldsGroupTable::add($arFields);
        if($add->isSuccess())
        {
            $result = $add->getId();
        }
    }


    if(!$result)
    {
        $ex = $APPLICATION->GetException();
        $errorMessage .= $ex->GetString()."<br />";
    }
    else
    {
        if (strlen($apply) <= 0)
        {
            LocalRedirect("/bitrix/admin/diamis_ads_fields_group.php?lang=".LANG);
        }
        else
        {
            LocalRedirect("/bitrix/admin/diamis_ads_fields_group_edit.php?lang=".LANG."&ID=".$ID);
        }
    }

}
// ===== the end =====
// ===================


if($ID > 0)
{
    $data = FieldsGroupTable::getList(array(
        'filter'=> array('ID' => $ID),
        'limit' => 1
    ))->fetch();
}




// вкладки
$aTabs = array(
    array(
        'DIV' => 'general',
        'TAB' => GetMessage("DIAMIS_ADS_FIELDS_GROUP_TABS_GENERAL"),
        'ICON' => 'diamis_ads_fields_group_edit',
        'TITLE' => GetMessage("DIAMIS_ADS_FIELDS_GROUP_TABS_GENERAL_TITLE"),
    ),
);
$tabControl = new CAdminTabControl(
    "tabControl",
    $aTabs,
    true,
    true
);



$APPLICATION->SetTitle(GetMessage("DIAMIS_ADS_FIELDS_GROUP_PAGE_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?CAdminMessage::ShowMessage($errorMessage);?>
    <form name="ads" method="POST" action="<?=$APPLICATION->GetCurPage()?>?">
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="ID" value=<?=$ID?> />
        <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
        <input type="hidden" name="FORM_STRUCTURE" value="" />
        <?
        $tabControl->Begin();
        //********************
        //General Tab
        //********************
        $tabControl->BeginNextTab();
        ?>
        <tr>
            <td width="40%" >Активность:</td>
            <td width="60%" >
                <input id="ACTIVE" name="ACTIVE" type="checkbox" <?if($data['ACTIVE']):?>checked<?endif;?> value="1"/>
            </td>
        </tr>
        <tr>
            <td width="40%" >Название:</td>
            <td width="60%" >
                <input id="NAME" name="NAME" style="width:100%;" type="text" value="<?=$data['NAME'];?>"/>
            </td>
        </tr>
        <tr>
            <td width="40%" >Сортировать:</td>
            <td width="60%" >
                <input id="SORT" name="SORT" style="width:50px;" type="text" value="<?=$data['SORT'];?>"/>
            </td>
        </tr>
        <?
        $tabControl->EndTab();
        $tabControl->Buttons(array(
                "disabled" => null,
                "back_url" => (strlen($back_url) > 0 ? $back_url : "diamis_ads_fields_group.php?lang=".LANGUAGE_ID))
        );
        $tabControl->End();
        ?>
    </form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>