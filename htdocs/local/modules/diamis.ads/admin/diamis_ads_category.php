<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

if(is_file($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php")):
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php";
else:
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/prolog.php";
endif;


$blogModulePermissions = $APPLICATION->GetGroupRight("diamis.ads");
if ($blogModulePermissions < "R"):
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
endif;

IncludeModuleLangFile(__FILE__);

use Bitrix\Main\Loader;
use Diamis\Ads\CategoryTable;

Loader::includeModule("diamis.ads");



$fileID = 'diamis_ads_category';
$sTableID = "tbl_diamis_ads_category";


$parent = intval($_REQUEST['parent']);


$aMenu = array();
$arFilter = array(
    'PARENT' => $parent,
);
$arFilterFields = array();
$headers = array();


// Получае список полей
$fields = CategoryTable::getMap();

$headDefault = array(
    'ID',
    'CODE',
    'NAME',
    'ACTIVE',
    'PARENt',
    'SORT'
);

foreach($fields as $field):
    $key = $field->getColumnName();

    // Список полей
    $arFilterFields[] = 'FIND_'.$key;

    // Загововок столцов
    $headers[] = array(
        'id' => $key,
        'content' => $key,
        'sort' => 's_'.$key,
        'default' => (array_search($key, $headDefault)!==false) ? true : false
    );
endforeach;




$oSort = new CAdminSorting($sTableID, "ID", "asc");
$lAdmin = new CAdminList($sTableID, $oSort);



$lAdmin->InitFilter($arFilterFields);
$lAdmin->AddHeaders($headers);



$rsData = CategoryTable::getList(array(
    'filter'=> $arFilter
));
while($arRes = $rsData->fetch()):
    
    $row =& $lAdmin->AddRow($arRes["ID"], $arRes);
    $row->AddField('ID', '<a href="/bitrix/admin/diamis_ads_category.php?parent='.$arRes['ID'].'">'.$arRes['ID'].'</a>');
    $row->AddField('ACTIVE', ($arRes['ACTIVE'] ? 'Y' : 'N'));


    $arActions = array();
    $arActions[] = array(
        "ICON" => "update",
        "TEXT" => 'Изменить',
        "ACTION"=> $lAdmin->ActionRedirect("diamis_ads_category_edit.php?ID=".$arRes["ID"])
    );
    $arActions[] = array(
        "ICON" => "delete",
        "TEXT" => 'Удалить',
        "ACTION"=>"if(confirm('Вы действительно хотите удалить?')) ".$lAdmin->ActionDoGroup($arRes["ID"], "delete", 'bucket='.urlencode($obBucket->ID).'&path='.urlencode($path))
    );

    if(!empty($arActions))
        $row->AddActions($arActions);
    
endwhile;


$aMenu[] = array(
    "TEXT"	=> GetMessage("DIAMIS_ADS_CATEGORY_CREATE"),
    "TITLE" => GetMessage("DIAMIS_ADS_CATEGORY_CREATE_TITLE"),
    "LINK" => $fileID . "_edit.php?lang=".LANG,
    "ICON" => "btn_new"
);


$lAdmin->AddAdminContextMenu($aMenu);
$lAdmin->CheckListMode();


$APPLICATION->SetTitle(GetMessage("DIAMIS_ADS_CATEGORY_PAGE_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

?>
<form name="ads" method="GET" action="<?=$APPLICATION->GetCurPage()?>?">
    <?

    $oFilter = new CAdminFilter(
        $sTableID."_filter",
        array()
    );

    $oFilter->Begin();

    $oFilter->Buttons(array("table_id"=>$sTableID, "url"=>$APPLICATION->GetCurPage()));
    $oFilter->End();

    ?>
</form>
<?

$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>