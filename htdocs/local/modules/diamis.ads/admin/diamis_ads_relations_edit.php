<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

if(is_file($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php")):
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php";
else:
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/include.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/prolog.php";
endif;


$blogModulePermissions = $APPLICATION->GetGroupRight("diamis.ads");
if ($blogModulePermissions < "R"):
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
endif;

IncludeModuleLangFile(__FILE__);

use Bitrix\Main\Loader;
use Diamis\Ads\Base;
use Diamis\Ads\RelationsTable;
use Diamis\Ads\FieldsTable;
use Diamis\Ads\TypeTable;
use Diamis\Ads\CategoryTable;


Loader::includeModule("diamis.ads");



$ID = intval($_REQUEST['ID']);
$data['ENTITY'] = (!isset($ENTITY) ? 'ads' : $ENTITY);
if($ID > 0)
{
    $data = RelationsTable::getList(array(
        'filter'=> array('ID' => $ID),
        'limit' => 1
    ))->fetch();
}

$errorMessage = '';

// ===================
//        save
if ($REQUEST_METHOD=="POST"
    && check_bitrix_sessid() 
) {
    $arFields = array(
        'ACTIVE'   => ($_REQUEST['ACTIVE'] ? true : false),
        'REQUIRED'   => ($_REQUEST['REQUIRED'] ? true : false),
        'FILTER'   => ($_REQUEST['FILTER'] ? true : false),
        'SORT'     => intval($SORT),
        'CATEGORY_ID' => intval($CATEGORY_ID),
        'TYPE_ID'     => intval($TYPE_ID),
        'FIELD_ID'    => is_array($FIELD_ID) ? $FIELD_ID : array(),
    );

    if($ID > 0) // -- Update
    {
        // Проверяем на наличие в базе связи
        // категория тип свойство - для избежания дублей
        $select = RelationsTable::getList(array(
                'select' => array('ID','CATEGORY_NAME'=>'CATEGORY.NAME','TYPE_NAME'=>'TYPE.NAME','FIELD_NAME'=>'FIELD.NAME'),
                'filter' => array(
                    '!ID' => $ID,
                    'CATEGORY_ID'   => $arFields['CATEGORY_ID'],
                    'TYPE_ID'       => $arFields['TYPE_ID'],
                    'FIELD_ID'      => $arFields['FIELD_ID'],
                )
        ));


        if(!$select->getSelectedRowsCount()) {

            if(count($arFields['FIELD_ID'])):
                foreach($arFields['FIELD_ID'] as $key=>$type_id){
                    $filter = $arFields;
                    $filter['FIELD_ID'] = $type_id;

                    if($key===0) $update = RelationsTable::update($ID, $filter);
                    else $add = RelationsTable::add($filter);
                }
            else:
                $update = RelationsTable::update($ID, $arFields);
            endif;

            if ($update->isSuccess()) {
                $result = $ID;
            }
        } else {
            $errorMessage = 'Данная комбинация свойств уже имеется в безе:';
            foreach($select as $item) {
                $errorMessage .= "\n - ".$item['CATEGORY_NAME'].' '.$item['TYPE_NAME'].' '.$item['FIELD_NAME']." ( ID = ".$item['ID']." )";
            }
        }
    }
    else        // -- Add
    {
        $select = RelationsTable::getList(array(
                'select' => array('ID'),
                'filter' => array(
                    'CATEGORY_ID' => $arFields['CATEGORY_ID'],
                    'TYPE_ID'     => $arFields['TYPE_ID'],
                    'FIELD_ID'    => $arFields['FIELD_ID'],
                ),
                'limit' => 1
        ))->fetch();

        if(!$select)
        {
            $add = RelationsTable::add($arFields);
            if(!$add->isSuccess()){
                $errorMessage = $add->getErrorMessages();
            }else{
                $result = $add->getId();
            }
        }
        else
        {
            $ID = $result = $select['ID'];
        }
    }


    if(!$result)
    {
        $ex = $APPLICATION->GetException();
        if($ex) $errorMessage .= $ex->GetString()."<br />";
    }
    else
    {
        if (strlen($apply) <= 0)
        {
            LocalRedirect("/bitrix/admin/diamis_ads_relations.php?lang=".LANG);
        }
        else
        {
            LocalRedirect("/bitrix/admin/diamis_ads_relations_edit.php?lang=".LANG."&ID=".$ID);
        }
    }
    
}
// ===== the end =====
// ===================







// вкладки
$aTabs = array(
    array(
        'DIV' => 'general',
        'TAB' => GetMessage("DIAMIS_ADS_RELATIONS_TABS_GENERAL"),
        'ICON' => 'diamis_ads_category_edit',
        'TITLE' => GetMessage("DIAMIS_ADS_RELATIONS_TABS_GENERAL_TITLE"),
    )
);
$tabControl = new CAdminTabControl(
    "tabControl",
    $aTabs,
    true,
    true
);


// Дерево Категорий
$categoryes = CategoryTable::getList(array())->fetchAll();
$resultCategoryes = Base::lists($categoryes);

$fields = FieldsTable::getList(array())->fetchAll();
$types = TypeTable::getList(array())->fetchAll();

$APPLICATION->SetTitle(GetMessage("DIAMIS_ADS_RELATIONS_PAGE_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?CAdminMessage::ShowMessage($errorMessage);?>
<form name="ads" method="POST" action="<?=$APPLICATION->GetCurPage()?>?">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="ID" value="<?=$ID?>" />
    <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
    <input type="hidden" name="FORM_STRUCTURE" value="" />
    <?
    $tabControl->Begin();
	//********************
    //General Tab
    //********************
    $tabControl->BeginNextTab();
    ?>
    <tr>
        <td width="40%" >Активность:</td>
        <td width="60%" >
            <input id="ACTIVE" name="ACTIVE" type="checkbox" <?if($data['ACTIVE']):?>checked<?endif;?> value="1"/>
        </td>
    </tr>
    <tr>
        <td width="40%" >Обязательно для заполнения:</td>
        <td width="60%" >
            <input id="REQUIRED" name="REQUIRED" type="checkbox" <?if($data['REQUIRED']):?>checked<?endif;?> value="1"/>
        </td>
    </tr>
    <tr>
        <td width="40%" >Показывать в фильтре:</td>
        <td width="60%" >
            <input id="FILTER" name="FILTER" type="checkbox" <?if($data['FILTER']):?>checked<?endif;?> value="1"/>
        </td>
    </tr>
    <tr>
        <td width="40%" >Сортировать:</td>
        <td width="60%" >
            <input id="SORT" name="SORT" style="width:50px;" type="text" value="<?=$data['SORT'];?>"/>
        </td>
    </tr>
    <tr>
        <td width="40%" >Связь:</td>
        <td width="60%" >
            <table>
                <tr>
                    <td width="33%" >
                        <div>Категория:</div>
                        <select  id="CATEGORY_ID" name="CATEGORY_ID" size="8" style="width:100%; max-width:250px; min-height:80px;">
                            <option value='0'>..</option>
                            <?
                            foreach($resultCategoryes as $category)
                            {
                                ?><option <?if($category['ID']==$data['CATEGORY_ID']):?>selected<?endif;?>
                                value='<?=$category['ID'];?>'><?=$category['~NAME'];?></option><?
                            }
                            ?>
                        </select>
                    </td>
                    <td width="33%" >
                        <div>Тип (множественное):</div>
                        <select  id="TYPE_ID" name="TYPE_ID" size="8"  style="width:100%; max-width:250px; min-height:80px;">
                            <option value='0'>..</option>
                            <?
                            foreach($types as $type)
                            {
                                ?><option <?if($type['ID']==$data['TYPE_ID']):?>selected<?endif;?>
                                value='<?=$type['ID'];?>'><?=$type['NAME'];?></option><?
                            }
                            ?>
                        </select>
                    </td>
                    <td width="33%" >
                        <div>Свойство:</div>
                        <select  id="FIELD_ID" name="FIELD_ID[]" size="8" multiple style="width:100%; max-width:250px; min-height:80px;">
                            <option value='0'>..</option>
                            <?
                            foreach($fields as $field)
                            {
                                ?><option <?if($field['ID']==$data['FIELD_ID']):?>selected<?endif;?>
                                value='<?=$field['ID'];?>'><?=$field['NAME'];?></option><?
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
	<?
    $tabControl->EndTab();
    $tabControl->Buttons(array(
        "disabled" => null,
        "back_url" => (strlen($back_url) > 0 ? $back_url : "diamis_ads_relations.php?lang=".LANGUAGE_ID))
    );
    $tabControl->End();
    ?>
</form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>