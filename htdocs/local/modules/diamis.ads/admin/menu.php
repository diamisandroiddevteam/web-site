<?php
IncludeModuleLangFile(__FILE__);

if($APPLICATION->GetGroupRight("diamis.ads")>"D"){

    if(is_file($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php")):
        require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/diamis.ads/prolog.php");
    else:
        require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/diamis.ads/prolog.php");
    endif;

    $aMenu = array(
        "parent_menu" => "global_menu_services",
        "section" => "diamis.ads",
        "sort" => 11, //
        "module_id" => "diamis.ads",
        "text" => GetMessage('DIAMIS_ADS_MENU_NAME'),
        "title"=> GetMessage('DIAMIS_ADS_MENU_NAME'),
        "icon" => "sys_menu_icon",   // sys_menu_icon  bizproc_menu_icon
        "page_icon" => "sys_menu_icon", // sys_menu_icon  bizproc_menu_icon
        "items_id" => "diamis_menu_ads",
        "items" => array(
            array(
                "text" => GetMessage('DIAMIS_ADS_MENU_CATEGORY'),
                "title" => GetMessage('DIAMIS_ADS_MENU_CATEGORY'),
                "url" => "diamis_ads_category.php?lang=".LANGUAGE_ID,
                "more_url" => array('diamis_ads_category.php', 'diamis_ads_category_edit.php'),
            ),
            array(
                "text" => 'Объявления',
                "title" => 'Объявления',
                "items_id" => "diamis_ads.php?lang=".LANGUAGE_ID,
                "items" => array(
                    array(
                        "text" => 'Активные',
                        "title" => 'Активные',
                        "url" => "diamis_ads.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads.php', 'diamis_ads_edit.php'),
                    ),
                    array(
                        "text" => 'На Модерации',
                        "title" => 'На Модерации',
                        "url" => "diamis_ads_moder.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_moder.php', 'diamis_ads_moder_edit.php'),
                    )
                )
            ),
            array(
                "text" => 'Комментарии',
                "title" => 'Комментарии',
                "items_id" => "diamis_ads_review.php?lang=".LANGUAGE_ID,
                "items" => array(
                    array(
                        "text" => 'Все комментарии',
                        "title" => 'Все комментарии',
                        "url" => "diamis_ads_review.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_review.php', 'diamis_ads_review_edit.php'),
                    ),
                    array(
                        "text" => 'На Модерации',
                        "title" => 'На Модерации',
                        "url" => "diamis_ads_review_moder.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_review_moder.php', 'diamis_ads_review_moder_edit.php'),
                    )
                )
            ),
            array(
                "text" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_BILLING'),
                "title" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_BILLING'),
                "items_id" => "diamis_ads_billing",
                "items" => array(
                    array(
                        "text" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_BILLING_TRANSACTION'),
                        "title" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_BILLING_TRANSACTION'),
                        "url" => "diamis_ads_billing_transaction.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_billing_transaction.php', 'diamis_ads_billing_transaction_edit.php'),
                    ),
                    array(
                        "text" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_BILLING_SCORE'),
                        "title" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_BILLING_SCORE'),
                        "url" => "diamis_ads_billing_score.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_billing_score.php', 'diamis_ads_billing_score_edit.php'),
                    ),
                    array(
                        "text" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_BILLING_SETTINGS'),
                        "title" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_BILLING_SETTINGS'),
                        "url" => "diamis_ads_billing_setting.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_billing_setting.php', 'diamis_ads_billing_setting.php'),
                    ),
                ),
            ),
            array(
                "text" => GetMessage('DIAMIS_ADS_MENU_PACKAGES'),
                "title" => GetMessage('DIAMIS_ADS_MENU_PACKAGES'),
                "items_id" => "diamis_ads_packages",
                "items" => array(
                    array(
                        "text" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_LIST'),
                        "title" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_LIST'),
                        "url" => "diamis_ads_packages.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_packages.php', 'diamis_ads_packages_edit.php'),
                    ),
                    array(
                        "text" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_SETTINGS'),
                        "title" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_SETTINGS'),
                        "url" => "diamis_ads_packages_services.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_packages_services.php', 'diamis_ads_packages_services_edit.php'),
                    ),
                    array(
                        "text" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_ACTION'),
                        "title" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_ACTION'),
                        "url" => "diamis_ads_packages_action.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_packages_action.php', 'diamis_ads_packages_action_edit.php'),
                    ),
                    array(
                        "text" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_CATEGORY'),
                        "title" => GetMessage('DIAMIS_ADS_MENU_PACKAGES_CATEGORY'),
                        "url" => "diamis_ads_packages_category.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_packages_category.php', 'diamis_ads_packages_category_edit.php'),
                    ),
                    array(
                        "text" => 'Стоимость Размещенеия',
                        "title" => 'Стоимость Размещенеия',
                        "url" => "diamis_ads_price_category.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_price_category.php', 'diamis_ads_price_category_edit.php'),
                    )
                )
            ),
            array(
                "text" => GetMessage('DIAMIS_ADS_MENU_SETTINGS'),
                "title" => GetMessage('DIAMIS_ADS_MENU_SETTINGS'),
                "items_id" => "diamis_ads_settings",
                "items" => array(
                    array(
                        "text" => GetMessage('DIAMIS_ADS_MENU_FIELDS_GROUP'),
                        "title" => GetMessage('DIAMIS_ADS_MENU_FIELDS_GROUP'),
                        "url" => "diamis_ads_fields_group.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_fields_group.php', 'diamis_ads_fields_group_edit.php'),
                    ),
                    array(
                        "text" => GetMessage('DIAMIS_ADS_MENU_FIELDS'),
                        "title" => GetMessage('DIAMIS_ADS_MENU_FIELDS'),
                        "url" => "diamis_ads_fields.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_fields.php', 'diamis_ads_fields_edit.php'),
                    ),
                    array(
                        "text" => GetMessage('DIAMIS_ADS_MENU_TYPES'),
                        "title" => GetMessage('DIAMIS_ADS_MENU_TYPES'),
                        "url" => "diamis_ads_types.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_types.php', 'diamis_ads_types_edit.php'),
                    ),
                    array(
                        "text" => GetMessage('DIAMIS_ADS_MENU_RELATIONS'),
                        "title" => GetMessage('DIAMIS_ADS_MENU_RELATIONS'),
                        "url" => "diamis_ads_relations.php?lang=".LANGUAGE_ID,
                        "more_url" => array('diamis_ads_relations.php', 'diamis_ads_relations_edit.php'),
                    ),
                )
            )
        )
    );

    return $aMenu;
}
return false;
?>