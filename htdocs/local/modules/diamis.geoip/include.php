<?php
use \Bitrix\Main\Loader;

Loader::registerAutoLoadClasses('diamis.geoip', array(
    '\Diamis\GeoIp\CityTable' => 'lib/citytable.php',
    '\Diamis\GeoIp\IpTable' => 'lib/iptable.php',
    '\Diamis\GeoIp\GeoBase' => 'lib/geobase.php',
));