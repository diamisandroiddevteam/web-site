<?php
$MESS['DIAMIS_GEOIP_MODULE_NAME'] = 'Diamis - ГеоIP';
$MESS['DIAMIS_GEOIP_MODULE_DESCRIPTION'] = 'Модуль определяет местоположение до города по IP из базы ipgeobase.ru';
$MESS['DIAMIS_GEOIP_PARTNER_NAME'] = 'Diamis';
$MESS['DIAMIS_GEOIP_PARTNER_URI'] = 'https://diamis.ru';
$MESS['DIAMIS_GEOIP_ERROR_VERCION_D7'] = 'Текущая верся ядра не поддерживает технологию D7';