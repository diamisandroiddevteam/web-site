<?php

namespace Diamis\GeoIp;

use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Data\Cache;

use Diamis\GeoIp\CityTable;
use Diamis\GeoIp\IpTable;


class GeoBase
{
    public static $CacheTime = 10000;
    public static $CacheID = 'diamis.geoip.city';


    const MAX = 200; // Максимальное кол-во записей строк в одном запросе базы данных

    const URL = 'http://ipgeobase.ru/files/db/Main/geo_files.zip';

    const ALIAS_DISTRICT = [
        'Центральный федеральный округ' => 'Центр',
        'Северо-Западный федеральный округ' => 'Северо-Запад',
        'Южный федеральный округ' => 'Юг',
        'Сибирский федеральный округ' => 'Сибирь',
        'Приволжский федеральный округ' => 'Поволжье',
        'Дальневосточный федеральный округ' => 'Дальний Восток',
        'Северо-Кавказский федеральный округ' => 'Северный Кавказ',
        'Уральский федеральный округ' => 'Урал',
        'Крымский федеральный округ' => 'Республика Крым',
    ];


    public function __construct() {}


    /*
     * Получаем список городов
     */
    public static function getCity()
    {
        $cache = Cache::createInstance();
        if($cache->initCache(self::$CacheTime, self::$CacheID))
        {
            $city = $cache->getVars();
        }
        else
        {
            $city = array();
            $dbCity = CityTable::getList([
                'order' => [
                    'COUNTRY' => 'DESC',
                    'DISTRICT' => 'DESC',
                    'CITY' => 'DESC',
                ]
            ]);
            while($item = $dbCity->fetch())
            {
                $item['CITY_CODE'] = self::translite($item['CITY']);
                $item['REGION_CODE'] = self::translite($item['REGION']);
                $item['COUNTRY_CODE'] = self::translite($item['COUNTRY']);

                $city[$item['CITY_CODE']] = $item;
            }
            $cache->endDataCache($city);
        }

        return $city;
    }

    public static function getCityID($id) {

        $city = CityTable::getList([
            'filter'=> ['ID' => $id],
            'order' => [
                'COUNTRY' => 'DESC',
                'DISTRICT' => 'DESC',
                'CITY' => 'DESC',
            ]
        ]);

        return $city->fetch();
    }

    public static function getCityCode($code) {
        $city = CityTable::getList([
            'filter'=> ['CITY_CODE' => $code],
            'order' => [
                'COUNTRY' => 'DESC',
                'DISTRICT' => 'DESC',
                'CITY' => 'DESC',
            ]
        ]);
        return $city->fetch();
    }


    public static function getCityLatLng($lat, $lng)
    {
        global $DB;
        $sql = "SELECT 
                  `ID`, `CITY`, `CITY_CODE`,
                  ( 6372.797 * acos( cos( radians('".$lat."') ) * cos( radians( `diamis_geoip_city`.`LAT` ) ) * cos( radians( `diamis_geoip_city`.`LNG` ) - radians('".$lng."') ) + sin( radians('".$lat."') ) * sin( radians( `diamis_geoip_city`.`LAT` ) ) ) ) AS DISTANCE  
                FROM `diamis_geoip_city`
                ORDER BY `DISTANCE` ASC
                LIMIT 1";
        $res = $DB->Query($sql);
        return $res->Fetch();
    }

    /*
    * Получение информации о городе по индексу
    */
    public static function getCityIndex($index, $city = null)
    {
        if($city == null) $city = GeoBase::getCity();

        while ($item = current($city))
        {
            if($item['INDEX']==$index) return $item;
            next($city);
        }

        return false;
    }


    /*
    * Получение гео-информации по IP
    */
    public static function getCityIp($ip, $city = null)
    {
        if($city == null)
           $city = GeoBase::getCity();

        $GLOBALS['CITY'] = $city;
        $ip = ip2long($ip);

        $res = IpTable::getList([
            'filter' => [
                '<=FROM' => $ip,
                '>=BEFORE' => $ip
            ],
            'limit' => 1
        ])->fetch();


        if($res['INDEX']>0)
            $res = array_merge($res, GeoBase::getCityIndex($res['INDEX'], $city));

        return $res;
    }

    /**
     * Транслитерация строки
     * @param  string $str исходная строка
     * @return string      траслитизированная строка
     */
    public static function translite($str = '')
    {
        if (empty($str)) return '';

        $translite = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch', 'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z', 'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch', 'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        );

        $str = strip_tags(trim($str));
        $str = str_replace(array("\n", "\r"), " ", $str);
        $str = preg_replace("/\s+/", ' ', $str);
        $str = function_exists('mb_strtolower') ? mb_strtolower($str) : strtolower($str);
        $str = strtr($str, $translite);
        $str = preg_replace("~[^a-z0-9_-]+~u", '-', $str);

        return $str;
    }


    public static function groupCity($arr = null)
    {
        if($arr == null) $arr = GeoBase::getCity();

        $country = null;
        $district = null;

        $data = [];
        foreach($arr as $c)
        {
            if(!$country || $c['COUNTRY'] != $country['title'])
            {
                $country['count']++;
                $country['title'] = $c['COUNTRY'];
                $district['count'] = null;
            }

            if(!$district || $c['DISTRICT'] != $district['title'])
            {
                $district['count']++;
                $district['title'] = $c['DISTRICT'];
            }

            $data[$country['count']]['COUNTRY'] = $c['COUNTRY'];
            $data[$country['count']]['CITY'][$district['count']]['DISTRICT'] = $c['DISTRICT'];
            $data[$country['count']]['CITY'][$district['count']]['CITY'][] = $c;
        }

        return $data;
    }

    public static function updateGeoBase($url = self::URL)
    {
        global $DB;

        @set_time_limit(0);


        Loader::registerAutoLoadClasses('main', array(
            '\CZip' => 'classes/general/zip.php',
        ));

        $zipFile = self::getFile($url);
        $pathFile = pathinfo($zipFile);

        $zip = new \CZip($zipFile);
        // Разархивируем
        if($zip->Unpack($pathFile['dirname'] .'/'. $pathFile['filename']))
        {
            unlink($zipFile);

            $citiesFile = $pathFile['dirname'] .'/'. $pathFile['filename'].'/cities.txt';
            $cidrFile = $pathFile['dirname'] .'/'. $pathFile['filename'].'/cidr_optim.txt';

            if(is_file($cidrFile))
            {
                $DB->Query('TRUNCATE TABLE `'.IpTable::getTableName().'`');
                $limit = 0;
                $data = [];
                $handle = null;
                $handle =fopen($cidrFile, "r");

                while($str = fgets($handle))
                {
                    $str = iconv("windows-1251",LANG_CHARSET, $str);
                    $arRecord = explode("\t", trim($str));

                    list($from, $before, $ip, $code, $index) = $arRecord;

                    $data[] = [
                        'FROM' => (int) $from,
                        'BEFORE' => (int) $before,
                        'IP' => $ip,
                        'CODE' => $code,
                        'INDEX' => (int) $index,
                    ];

                    $limit++;
                    if($limit>=self::MAX and count($data))
                    {
                        $limit = 0;
                        IpTable::insert($data);
                        $data = [];
                    }
                }
                fclose($handle);

                if(count($data)) IpTable::insert($data);
            }


            // Считываем содержимое файла со странами и координатами
            if(is_file($citiesFile))
            {
                $limit = 0;
                $data = [];
                $handle = null;
                $handle =fopen($citiesFile, "r");

                $DB->Query('TRUNCATE TABLE `'.CityTable::getTableName().'`');

                while($str = fgets($handle))
                {
                    $str = iconv("windows-1251",LANG_CHARSET, $str);
                    $arRecord = explode("\t", trim($str));

                    list($id, $city, $region, $district, $lat, $lng) = $arRecord;

                    $country = 'Другие';
                    if(stripos($district, 'федеральный округ')!==false)
                    {
                        $country = 'Россия';
                    }
                    else if(stripos($district, 'украина')!==false)
                    {
                        $country = 'Украина';
                    }

                    $district = !empty(self::ALIAS_DISTRICT[$district]) ?  self::ALIAS_DISTRICT[$district] : $district;

                    $data[] = [
                        'INDEX' => (int) $id,
                        'COUNTRY' => $country,
                        'CITY' => $city,
                        'REGION' => $region,
                        'DISTRICT' => $district,
                        'LAT' => $lat,
                        'LNG' => $lng
                    ];

                    $limit++;
                    if($limit>=self::MAX and count($data))
                    {
                        $limit = 0;
                        CityTable::arrAdd($data);
                        $data = [];
                    }

                }
                fclose($handle);

                if(count($data)) CityTable::arrAdd($data);
            }
        }
    }

    private static function getFile($url)
    {
        $client = new HttpClient();
        $server = Application::getInstance()->getContext()->getServer();

        $fileName = pathinfo($url, PATHINFO_BASENAME);
        $tmpFile = $server->getDocumentRoot().'/bitrix/tmp/'.$fileName;

        if($client->download($url, $tmpFile)) {
            return $tmpFile;
        } else {
            $moduleDir = str_replace('\\', '/', realpath(dirname(__FILE__)."/.."));
            return $moduleDir.'/install/geo_files.zip';
        }
    }
}