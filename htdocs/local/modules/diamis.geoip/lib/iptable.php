<?php

namespace Diamis\GeoIp;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class IpTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'diamis_geoip_ip';
    }


    public static function insert($arData)
    {
        if(!count($arData)) return false;

        global $DB;

        $lastFriday = date('Y-m-d h:i:s', strtotime('last friday'));
        $date = new Type\Date($lastFriday, 'Y-m-d h:i:s');

        $keys = array_keys($arData[0]);
        $keys[] = 'CREATE';

        $values = [];
        foreach($arData as $data)
        {
            list($from, $before, $ip, $code, $index) = array_values($data);
            $values[] = "(".$from.",".$before.",'".$ip."','".$code."',".$index.",'".$date."')";
        }
        $sql = "INSERT INTO `".self::getTableName()."`(`".implode('`,`', $keys)."`) VALUES ".implode(",", $values);
        $DB->Query($sql);
    }


    public static function getMap()
    {
        return [
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\DatetimeField('CREATE', [
                'required' => true,
                'default_value' => function () {
                    $lastFriday = date('Y-m-d', strtotime('last friday'));
                    return new Type\Date($lastFriday, 'Y-m-d');
                }
            ]),
            new Entity\IntegerField('FROM', [
                'required' => true
            ]),
            new Entity\IntegerField('BEFORE', [
                'required' => true
            ]),
            new Entity\StringField('IP', []),
            new Entity\StringField('CODE', []),
            new Entity\IntegerField('INDEX',[])
        ];
    }

}