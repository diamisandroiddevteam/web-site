<?php

namespace Diamis\GeoIp;


use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;


class CityTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'diamis_geoip_city';
    }


    public static function arrAdd($arData)
    {
        if(!count($arData)) return false;

        global $DB;

        $keys = array_keys($arData[0]);
        $values = [];
        foreach($arData as $data)
        {
            $values[] = "'".implode("','", array_values($data))."'";
        }

        $sql = "INSERT INTO `".self::getTableName()."`(`".implode('`,`', $keys)."`) VALUES (".implode('),(', $values).")";
        $DB->Query($sql);
    }


    /** Проверяем существование данного города
     * @param $code
     */
    public static function isCity($code){
        $result = self::getList(array(
            'select' => array('*'),
            'filter' => array(
                'CITY_CODE' => mb_strtolower(strip_tags($code))
            )
        ))->fetch();
        return $result;
    }


    public static function getMap()
    {
        return [
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\DatetimeField('CREATE', [
                'required' => true,
                'default_value' => function () {
                    $lastFriday = date('Y-m-d', strtotime('last friday'));
                    return new Type\Date($lastFriday, 'Y-m-d');
                }
            ]),
            new Entity\IntegerField('INDEX', [
                'primary' => true
            ]),
            new Entity\StringField('COUNTRY', [
                'required' => true
            ]),
            new Entity\StringField('CITY', [
                'required' => true
            ]),
            new Entity\StringField('CITY_CODE', [
                'required' => true
            ]),
            new Entity\StringField('REGION',[
                'required' => true
            ]),
            new Entity\StringField('DISTRICT',[
                'required' => true
            ]),
            new Entity\StringField('LAT',[
                'required' => true
            ]),
            new Entity\StringField('LNG',[
                'required' => true
            ])
        ];
    }


    /**
     * Транслитерация строки
     * @param  string $str исходная строка
     * @return string      траслитизированная строка
     */
    public static function translite($str = '')
    {
        if (empty($str)) return '';

        $translite = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch', 'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z', 'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch', 'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        );

        $str = strip_tags(trim($str));
        $str = str_replace(array("\n", "\r"), " ", $str);
        $str = preg_replace("/\s+/", ' ', $str);
        $str = function_exists('mb_strtolower') ? mb_strtolower($str) : strtolower($str);
        $str = strtr($str, $translite);
        $str = preg_replace("~[^a-z0-9_-]+~u", '-', $str);

        return $str;
    }



    public static function onBeforeAdd(Entity\Event $event)
    {
        $result = new Entity\EventResult;
        $data = $event->getParameter("fields");

        $city= trim($data['CITY_CODE']);
        if(strlen($city)<=0 && isset($data['CITY'])) {
            $city_code = self::translite($data['CITY']);
            $result->modifyFields(array('CITY_CODE' => $city_code));
        }

        return $result;
    }
}