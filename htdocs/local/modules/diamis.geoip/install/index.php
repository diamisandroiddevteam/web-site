<?php
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config as Conf;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Base;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Application;


Loc::loadMessages(__FILE__);


Class diamis_geoip extends CModule
{
    var $MODULE_ID = "diamis.geoip";
    var $MODULE_NAME;
    var $MODULE_DIR;
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_DESCRIPTION;

    var $PARTNER_NAME;
    var $PARTNER_URI;

    function __construct()
    {
        $this->MODULE_PATH = str_replace('\\', '/', realpath(dirname(__FILE__)."/.."));

        $arModuleVersion = array();
        include($this->MODULE_DIR . '/install/version.php');

        $this->MODULE_NAME = Loc::getMessage('DIAMIS_GEOIP_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('DIAMIS_GEOIP_MODULE_DESCRIPTION');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

        $this->PARTNER_NAME = Loc::getMessage('DIAMIS_GEOIP_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('DIAMIS_GEOIP_PARTNER_URI');
    }


    // Проверяем поддрежку D7
    function isVersionD7()
    {
        global $APPLICATION;

        if($status = CheckVersion(ModuleManager::getVersion('main'), '14.00.00'))
        {
            $APPLICATION->ThrowException(Loc::getMessage('DIAMIS_GEOIP_ERROR_VERCION_D7'));
        }
        return $status;
    }


    function DoInstall()
    {
        global $USER;

        if($this->isVersionD7() && $USER->IsAdmin())
        {
            ModuleManager::registerModule($this->MODULE_ID);

            $this->InstallFiles();
//            $this->InstallEvents();
            $this->InstallDB();

//            Diamis\GeoIp\GeoBase::updateGeoBase();
        }
    }


    function DoUninstall()
    {
        global $USER;

        if($this->isVersionD7() && $USER->isAdmin())
        {
            $this->UnInstallDB();
//            $this->UnInstallEvents();
            $this->UnInstallFiles();


            ModuleManager::unRegisterModule($this->MODULE_ID);
        }
    }


    function InstallFiles()
    {
        CopyDirFiles($this->MODULE_DIR."/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFiles($this->MODULE_DIR."/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
        return true;
    }


    function InstallDB()
    {
        global $DB;

        Loader::includeModule($this->MODULE_ID);

        $ipTableName = Base::getInstance('Diamis\GeoIp\IpTable')->getDBTableName();
        $cityTableName = Base::getInstance('Diamis\GeoIp\CityTable')->getDBTableName();

        if(!Application::getConnection(Diamis\GeoIp\IpTable::getConnectionName())->isTableExists($ipTableName))
        {
            $DB->Query("CREATE TABLE IF NOT EXISTS `".$ipTableName."` 
            ( 
                `ID` INT(11) NOT NULL AUTO_INCREMENT, 
                `CREATE` DATETIME NOT NULL, 
                `FROM` bigint(20) NOT NULL, 
                `BEFORE` bigint(20) NOT NULL, 
                `IP` varchar(32) NOT NULL, 
                `CODE` varchar(2) NOT NULL, 
                `INDEX` int(10) NOT NULL, 
               PRIMARY KEY (`ID`, `FROM`,`BEFORE`) 
            );");
//            Base::getInstance('Diamis\GeoIp\IpTable')->createDbTable();
        }

        if(!Application::getConnection(Diamis\GeoIp\CityTable::getConnectionName())->isTableExists($cityTableName))
        {
            Base::getInstance('Diamis\GeoIp\CityTable')->createDbTable();
        }
    }

    function UnInstallDB()
    {
        Loader::includeModule($this->MODULE_ID);

        $ipTableName = Base::getInstance('Diamis\GeoIp\IpTable')->getDBTableName();
        $cityTableName = Base::getInstance('Diamis\GeoIp\CityTable')->getDBTableName();

        Application::getConnection(Diamis\GeoIp\IpTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $ipTableName);
        Application::getConnection(Diamis\GeoIp\CityTable::getConnectionName())->queryExecute('DROP TABLE IF EXISTS ' . $cityTableName);

        \Bitrix\Main\Config\Option::delete($this->MODULE_ID);
    }
}