/**
 * Created by Алексей Чернышов on 18.01.2018.
 */
import path from 'path';
import webpack from 'webpack';
import webpackConfig from './webpack.config';
import webpackDevServer from 'webpack-dev-server';


const isDevelopment = process.env.NODE_ENV !== "production";
const PORT = process.env.PORT || 3000;
const options = {
    contentBase: './bundle',
    hot: false,
    host: 'localhost'
};


const output = {
    path: path.resolve(__dirname, '../bundle')
};


function start()
{
    const config = webpackConfig.find(conf => conf.name === 'config');

    config.output.path = output.path;

    webpackDevServer.addDevServerEntrypoints(config, options);

    const compiler = webpack(config);
    const server = new webpackDevServer(compiler, options);

    server.listen(PORT, 'localhost', () => {
        console.log('dev server listening on port http://localhost:' + PORT);
    });
}

module.exports = start();