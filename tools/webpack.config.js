import path from 'path';
import webpack from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';


const TMP_PATH = '/htdocs/local/templates/tebemne';


let HtmlTemplates = [
    'index',
    'map',
    'ads-add',
    'ads-add-prop',
    'office-ads',
    'office-ads-moderation',
    'office-ads-arhiv',
    'office-ads-dell',
    'office-order',
    'office-sale',
    'office-message',
    'office-message-ads',
    'office-company',
    'office-company-gallery',
    'office-company-location',
    'office-company-location-add',
    'office-company-news',
    'office-company-news-add',
    'office-company-sale',
    'office-company-sale-add',
    'office-settings',
    'auto',
    'pay',
    'reg',
    'packages',
    'news',
    'news-detail',
    'search',
    'detail-ads',
    'ajax-ads-list'
];

let HtmlPlugin = [];
for(let i=0; i<HtmlTemplates.length; i++) {
    HtmlPlugin.push(
        new HtmlWebpackPlugin({
            filename: HtmlTemplates[i] + '.html',
            template: HtmlTemplates[i] + '.pug',
            inject: false
        })
    );
}




const config = {
    entry: [
        './entry.js',
        './styles/style.scss'
    ],
    name: 'config',
    devtool: 'inline-source-map',
    context: path.resolve(__dirname, "../src"),


    output: {
        path: path.resolve(__dirname, '..' + TMP_PATH),
        publicPath: '/',
        filename: 'js/scripts.js',
    },

    module: {
        rules: [
            {
                test: /\.pug$/,
                use: {
                    loader: "pug-loader",
                    options: {
                        name: '[name].html',
                        pretty: true,
                    }
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },

            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                        fallback: "style-loader",
                        use: [
                            {
                                loader: "css-loader"
                            },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: true,
                                    plugins: (loader) => [
                                        require('autoprefixer')()
                                    ]
                                }
                            }
                        ]
                    })
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    publicPath: '../',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true,
                                plugins: (loader) => [
                                    require('autoprefixer')()
                                ]
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true
                            }
                        }
                    ]
                })
            },
            {
                test: /\.(png|gif|jpg|jpeg|svg|cur)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: 'images/[name].[ext]',
                        limit: 100000
                    }
                },
            },
            {
                test: /\.(ttf|eot|woff(2)?)(\?.+)?$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[name].[ext]',
                        limit: 10000
                    }
                }
            }
        ]
    },

    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new ExtractTextPlugin({
            filename: "css/styles.css",
            allChunks: true
        })
    ],

    watch: true,
    watchOptions:{
        aggregateTimeout:100
    },

    node: {
        fs: "empty"
    },

    resolve: {
        alias: {

        }
    }
};

config.plugins.push(...HtmlPlugin);


export default [config];