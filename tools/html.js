import path from 'path';
import webpack from 'webpack';
import webpackConfig from './webpack.config';



const config = webpackConfig.find(conf => conf.name === 'config');

config.output.path = path.resolve(__dirname, '../bundle/');


function bundle() {
    return new Promise((resolve, reject) => {
        webpack(config).run((err, stats) => {
            if(err) {
                return reject(err);
            }

            return resolve();
        });
    });
}

export default bundle();