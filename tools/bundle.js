import webpack from 'webpack';
import webpackConfig from './webpack.config';
import ExtractTextPlugin from "extract-text-webpack-plugin";


const config = webpackConfig.find(conf => conf.name === 'config');

config.plugins = [
    new webpack.NamedModulesPlugin(),
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
    }),
    new ExtractTextPlugin({
        filename: "css/styles.css",
        allChunks: true
    })
];


function bundle() {
    return new Promise((resolve, reject) => {
        webpack(config).run((err, stats) => {
            if(err) {
                return reject(err);
            }

            return resolve();
        });
    });
}

export default bundle();