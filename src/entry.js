import 'slick-carousel';
import 'pgwslider';
import Blazy from 'blazy';


import './scripts/view';
import './scripts/select';
// import './_blocks/event';
import Dialog from './scripts/dialog/dialog';
import YMap from './scripts/location/location';

import Slider from './scripts/slider/slider';
import './scripts/animatedHeader';
import FormAdd from './_blocks/form/form.add';

import Link from './scripts/link';
import Input from './scripts/input';
import Button from './scripts/button';


import data from './scripts/location/data';






HTMLElement.prototype.addClass = function(x){
    var y = x.split(" "), z = this.className.split(" ");
    for(var i=0; i<y.length; i++){
        if( y[i].trim().length==0 ) continue;
        if( z.indexOf(y[i])>=0 ) continue;
        z.push(y[i]);
    }
    this.className = z.filter(function(_){return _.length>0}).join(" ");
    return this;
};
HTMLElement.prototype.removeClass = function(x){
    var y = x.split(" "), z = this.className.split(" ");
    for(var i=0, j; i<y.length; i++){
        if( y[i].trim().length==0 ) continue;
        while((j=z.indexOf(y[i])) >=0) z.splice(j, 1);
    }
    this.className = z.filter(function(_){return _.length>0}).join(" ");
    return this;
};
HTMLElement.prototype.hasClass = function(x){
    var y = this.className.split(" ");
    for(var i=0, j; i<y.length; i++){
        if( y[i].trim().length==0 ) continue;
        if(y[i]==x) return true;
    }
    return false;
};


document.addEventListener("click", (event) => {
    let target = event.target;

    while(target!==null)
    {
        if(!("tagName" in target)) break;
        if(target.tagName.toLowerCase()==='body') break;
        if(target.tagName.toLowerCase()==='a') {
            let link = new Link(event);
            link.init();
            break;
        }
        if(target.tagName.toLowerCase()==='input') {
            let input = new Input(event);
            input.init(target);
            break;
        }
        if(target.tagName.toLowerCase()==='textarea') {
            let parent = target.parentNode;
            if(parent.hasClass('field-error')) parent.removeClass('field-error');
            break;
        }
        target = target.parentNode;
    }

});


YMap.load()
    .then((res)=>{
        YMap.init();

        let locAdd = YMap.createMap('location-add');
        if( locAdd ) {
            locAdd.createSuggest('suggest');
        }


        let fieldMap = YMap.createMap('form-field-map');
        if(fieldMap) {
            fieldMap.createSuggest('form-field-input-map');
        }


        let pageMap = YMap.createMap('maps-page');
        if(pageMap) {
            pageMap.objectManager(data);
        }


        let prodMap = YMap.createMap('product-maps');
        if(prodMap) {
            prodMap.objectManager({
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "id": 0,
                        "name" : 'Объявление тестовое',
                        "image": '/images/ads-1.jpg',
                        "price": '66 200',
                        "text" : 'Душа мя озарена неземной радостью, как эти чудесные',
                        "link" : '/detail-ads.html',
                        "geometry": {
                            "type": "Point",
                            "coordinates": [59.910476, 30.316135]
                        },
                        "properties": {}
                    }
                ]
            });
        }

    });



$(function() {
    // ленивая загрузка изображений
    new Blazy({
        // ontainer:'.catalog',
        // success: function(element){
        //     console.log("Element loaded: ", element.nodeName);
        // }
    });

    let dialog = new Dialog();
    dialog.init();

    let slider = new Slider('slider-carousel');
    slider.init();

    let formAdd = new FormAdd();
    formAdd.init();

    // filter.init('filter');

    // Слайдер категории
    let ctgList = $('.ctg-list');
    if(ctgList)
    {
        ctgList.slick({
            infinite: false,
            slidesToShow: 7,
            slidesToScroll: 3,
            prevArrow: $('.ctg-scroll .scroll-arrow-left'),
            nextArrow: $('.ctg-scroll .scroll-arrow-right'),
            responsive: [
                {
                    breakpoint: 900,
                    settings: {
                        slidesToShow: 6,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        });
    }

    let reviewList = $('.review-list');
    if(reviewList)
    {
        reviewList.slick({
            infinite: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            prevArrow: $('.review-scroll .scroll-arrow-left'),
            nextArrow: $('.review-scroll .scroll-arrow-right'),
            responsive: [
                {
                    breakpoint: 1000,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
});

