import './location.scss';

import load from './load';
import tooltip from './tooltip';
import geocode from './geocode';


class YandexMap extends load
{
    constructor() {
        super();

        this.start = false;
        this.id = null;
        this.el = null;
        this.map = null;
        this.circle = null;
        this.placemarks = null;
    }


    init() {
        tooltip.init();

        window.addEventListener("resize", ()=>{ tooltip.hide(); });
        document.addEventListener("click", (e)=>{
            if(tooltip.point) {
                tooltip.hide();
                tooltip.show(e.pageX, e.pageY);
                tooltip.setPoint(null);
            }
        });

        this.start = true;
    }

    // Проверяем иницилизация выполнена или нет
    isStart() {
        if(!this.start) throw new Error('Is not function YandexMap::init();');
    }


    // Создаем Яндкес карту
    createMap(id, center=[55.751430, 37.618832], zoom=16) {
        this.isStart();

        this.id = id;
        this.el = document.getElementById(id);

        // Проверяем наличие DOM элемента
        if(!this.el) return false;

        this.el.style.minWidth = "320px";
        this.el.style.minHeight = "380px";


        // Проверяем подключен ли модуль
        // Яндекс Карт
        if(window.ymaps === undefined)
            throw new Error('The map API is NOT loaded yet');


        this.map = new ymaps.Map(id, {
                center: center,
                zoom: zoom,
                controls:['zoomControl','geolocationControl']
            });
        this.map.behaviors.disable('scrollZoom');
        this.map.events.add('actionbegin', ()=>{ tooltip.hide(); });

        return this;
    }


    // Размещаем маркер на карте
    objectManager(data) {
        this.map.geoObjects.removeAll();

        let boxList = document.getElementById('maps-list'),
            objectManager = new ymaps.ObjectManager({
                // Чтобы метки начали кластеризоваться, выставляем опцию.
                clusterize: true,
                // ObjectManager принимает те же опции, что и кластеризатор.
                gridSize: 32,
                clusterDisableClickZoom: true
            });

        let items = data.features;

        if(boxList) boxList.innerHTML = '';

        items.forEach((item, key)=> {

            if (boxList)
            {

                let ads = document.createElement('div');
                ads.className = 'ads ads-map';
                ads.innerHTML = `
<div class="ads-wrap ads-wrap-images">
    <div class="ads-images">
        <div class="ads-img-item">
            <img class="ads-img b-lazy b-loaded" src="${item.image}" alt="${item.name}" />
        </div>
    </div>
</div>
<div class="ads-wrap ads-wrap-info">
    <div class="ads-info">
        <a class="ads-title" href="${item.link}" title="${item.name}">${item.name}</a>
        <div class="ads-desc">${item.text}</div>
        <div class="ads-price">
            <div class="ads-price-num">${item.price}</div>
            <div class="ads-price-type">a</div>
        </div>
    </div>
</div>`;

                // событие при наведении
                ads.onmouseenter = (e) => {
                    objectManager.objects.setObjectOptions(item.id, {
                        preset: 'islands#greenIcon'
                    });
                    objectManager.clusters.setObjectOptions(item.id, {
                        preset: 'islands#greenClusterIcons'
                    });
                };

                // событие при снятии выделения
                ads.onmouseleave = (e) => {
                    objectManager.objects.setObjectOptions(item.id, {
                        preset: 'islands#blueIcon'
                    });
                    objectManager.clusters.setObjectOptions(item.id, {
                        preset: 'islands#blueClusterIcons'
                    });
                };

                // событие при клике на элемент
                ads.onclick = (e) => {
                    this.map.panTo(item.geometry.coordinates, {
                        // Задержка между перемещениями.
                        delay: 11500
                    });
                };

                boxList.appendChild(ads);
            }

            data.features[key].properties.hintContent = item.name;
            data.features[key].properties.clusterCaption = item.name;
            data.features[key].properties.balloonContentHeader = item.name;
            data.features[key].properties.balloonContentBody = `
<div class="ymap-widget-tooltip-content">
    <div class="ymap-widget-tooltip-image">
        <img src="${item.image}" alt="${item.name}">    
    </div>
    <div class="ymap-widget-tooltip-info">
        <div class="point-name">${item.text}</div>
        <div>
            <span>Цена: </span>
            <span>${item.price}</span>
            <span class="price">a</span>
        </div>
        <div>
            <a href="${item.url}" target="_blank" rel="noopener noreferrer" class="point-site">Подробнее</a>
        </div>
    </div>
</div>
`;
        });

        function onObjectEvent (e) {
            var objectId = e.get('objectId');
            if (e.get('type') == 'mouseenter') {
                // Метод setObjectOptions позволяет задавать опции объекта "на лету".
                objectManager.objects.setObjectOptions(objectId, {
                    preset: 'islands#greenIcon'
                });
            } else {
                objectManager.objects.setObjectOptions(objectId, {
                    preset: 'islands#blueIcon'
                });
            }
        }

        function onClusterEvent (e) {
            var objectId = e.get('objectId');
            if (e.get('type') == 'mouseenter') {
                objectManager.clusters.setClusterOptions(objectId, {
                    preset: 'islands#greenClusterIcons'
                });
            } else {
                objectManager.clusters.setClusterOptions(objectId, {
                    preset: 'islands#blueClusterIcons'
                });
            }
        }
        console.log(data);
        objectManager.add(data);
        objectManager.objects.events.add(['mouseenter', 'mouseleave'], onObjectEvent);
        objectManager.clusters.events.add(['mouseenter', 'mouseleave'], onClusterEvent);

        this.map.geoObjects.add(objectManager);
        this.map.setBounds(objectManager.getBounds(), { checkZoomRange: true });
    }


    createPlacemarks(points) {
        this.placemarks = new ymaps.GeoObjectCollection();

        points.forEach((p) => {
            let marker = new ymaps.Placemark([p.lat, p.lon]);

            marker.events.add('click', () => {
                tooltip.setPoint(p);
            });
            marker.events.add('touchstart', () => {
                tooltip.setPoint(p);
            });
            this.placemarks.add(marker);
        });


        if(this.map) {
            this.map.geoObjects.removeAll();
            this.map.geoObjects.add(this.placemarks);
            this.map.setBounds(this.placemarks.getBounds(), { checkZoomRange: true });
        }

        return this;
    }


    createSuggest(id='suggest') {
        let suggest = document.getElementById(id);

        if(suggest){
            geocode.init(id, this.map);
        }

        return this;
    }

}

/*
     // Создаем Яндкес карту
    createMap(id, points = [], zoom = 16, suggest = '')
    {


        this.map.geoObjects.add(this.placemarks);
        // this.map.setBounds(this.placemarks.getBounds())

        return this;

        // if(suggest.length>0) {
        //     let suggestView = this.suggestView(suggest);
        //
        //     suggestView.events.add('select', (event)=>{
        //         let item = event.get('item'),
        //             geocode = ymaps.geocode(item.value);
        //
        //         geocode.then((res)=>{
        //             map.geoObjects.add(res.geoObjects);
        //             // Масштабируем карту на область видимости коллекции.
        //             map.setBounds(res.geoObjects.getBounds());
        //
        //             console.log(res.geoObjects);
        //         });
        //     });
        // }


        // Сравним положение, вычисленное по ip пользователя и
        // положение, вычисленное средствами браузера.
        // let geolocation = ymaps.geolocation;
        // geolocation.get({
        //     provider: 'yandex',
        //     mapStateAutoApply: true
        // }).then((res) => {
        //     map.geoObjects.add(res.geoObjects);
        // });
        //
        // // Пометим положение, полученное через браузер.
        // // Если браузер не поддерживает эту функциональность, метка не будет добавлена на карту.
        // geolocation.get({
        //     provider: 'browser',
        //     mapStateAutoApply: true
        // }).then((res) => {
        //     map.geoObjects.add(res.geoObjects);
        // });
    }



    suggestView(idElement="suggest", options = {results: 1, offset: [-3, 3]}) {

        if(document.getElementById(idElement)) {
            return new ymaps.SuggestView(idElement, options);
        }
    }


    createCircle(center = [55.751430, 37.618832], radius = 1000) {
        this.circle = new ymaps.Circle([center, radius],{
            strokeColor: "#2083E3",
            strokeOpacity: 0.8,
            strokeWidth: 2
        });

        return this;
    }

    createPlacemarks(points) {
        const placemarks = new ymaps.GeoObjectCollection();

        points.forEach(p => {
            const marker = new ymaps.Placemark([p.lat, p.lon],{}, {});

            marker.events.add('click', () => {
                this.tooltip.setPoint(p);
            });

            placemarks.add(marker);
        });

        return placemarks;
    }
*/

export default new YandexMap();