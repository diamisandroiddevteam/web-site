class loadYMap
{
    constructor() {
        this.src = '//api-maps.yandex.ru/2.1/?lang=ru_RU';
        this.promise = null;
    }

    load() {
        this.promise = this.promise || new Promise((resolve) => {
            let el = document.createElement('script');
            el.type = 'text/javascript';
            el.src = this.src;
            el.onload = resolve;

            document.body.appendChild(el);
        }).then(()=>{
            return new Promise(resolve => ymaps.ready(resolve));
        });

        return this.promise;
    }
}

export default loadYMap;