
class tooltip
{
    constructor() {
        this.tooltip = false;
        this.point = null;
    }


    init() {
        this.tooltip = document.createElement('div');
        this.tooltip.id = 'ymap-widget-tooltip';
        this.tooltip.addEventListener("click", ()=>{ this.hide(); });
        document.body.appendChild(this.tooltip);

        return this;
    }


    getTooltip() {
        return this.tooltip;
    }


    setPoint(point = null) {
        this.point = point;
    }


    show(x, y) {
        let point = this.point;


        this.tooltip.innerHTML = `
<div class="ymap-widget-tooltip-content">
    <div class="ymap-widget-tooltip-image">
        <img src="${point.image}" alt="${point.name}">    
    </div>
    <div class="ymap-widget-tooltip-info">
        <div class="point-name">${point.name}</div>
        <div>
            <span>Цена: </span>
            <span>${point.price}</span>
            <span class="price">a</span>
        </div>
        <div>
            <span>Адрес: </span>
            <span>${point.address}</span>
        </div>
        <div>
            <a href="${point.url}" target="_blank" rel="noopener noreferrer" class="point-site">Подробнее</a>
        </div>
    </div>
</div>
`;

        let pos_left = x - this.tooltip.offsetWidth - 170;
        let pos_top  = y - this.tooltip.offsetHeight - 130;

        this.tooltip.style.left = pos_left + 'px';
        this.tooltip.style.top = pos_top + 'px';
        this.tooltip.addClass('show');
    }


    hide() {
        this.tooltip.removeClass('show');
    }
}

export default new tooltip();