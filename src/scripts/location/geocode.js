class geoCode
{
    constructor() {
        this.el = null;
        this.hint = null;
        this.error = null;
        this.suggestView = null;

        this.map = null;
    }


    init(id='suggest', map) {
        this.map = map;

        let _this = this;

        this.suggestView = new ymaps.SuggestView(id);
        this.suggestView.events.add('select', function(e){
            let val = e.get('item');
            _this.suggest(val.displayName);
        });
    }


    suggest(request='') {
        ymaps.geocode(request)
             .then(res => {
                 this.el = res.geoObjects.get(0);

                // Об оценке точности ответа геокодера можно прочитать тут:
                // https://tech.yandex.ru/maps/doc/geocoder/desc/reference/precision-docpage/
                if (this.el) {
                    switch (this.el.properties.get('metaDataProperty.GeocoderMetaData.precision'))
                    {
                        case 'exact': break;
                        case 'number':
                        case 'near':
                        case 'range':
                            this.error = 'Неточный адрес, требуется уточнение';
                            this.hint = 'Уточните номер дома';
                            break;
                        case 'street':
                            this.error = 'Неполный адрес, требуется уточнение';
                            this.hint = 'Уточните номер дома';
                            break;
                        case 'other':
                        default:
                            this.error = 'Неточный адрес, требуется уточнение';
                            this.hint = 'Уточните адрес';
                    }
                } else {
                    this.error = 'Адрес не найден';
                    this.hint = 'Уточните адрес';
                }

                if(this.error) {
                    console.log(this.error, this.hint);
                } else {
                    this.showResult(res);
                }
            });
    }

    showResult(res) {
        this.map.geoObjects.removeAll();

        let bounds =  res.geoObjects.get(0).properties.get('boundedBy');

        this.map.geoObjects.add(res.geoObjects.get(0));
        this.map.setBounds(bounds, { checkZoomRange: true });
        
        //TODO настроить указание точной метки
        let inputs = $(document).find('[name*=location]');

        if(inputs[0].name==='field[location][lat]') { inputs[0].value = bounds[0][0]; }
        if(inputs[1].name==='field[location][lng]') { inputs[1].value = bounds[0][1]; }
    }
}

export default new geoCode();