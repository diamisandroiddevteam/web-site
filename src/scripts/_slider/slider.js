'use strict';

class Slider
{
    constructor() {
        this.config = {
            'main'      : 'ds-slider',
            'current'   : 'ds-current',
            'list'      : 'ds-list',
            'listItems' : 'ds-list-items',
            'listItem'  : 'ds-list-item',
            'btnUp'     : 'ds-arrow-up',
            'btnDown'   : 'ds-arrow-down',
            'vertical'  : true
        };
        this.main = null;
        this.current = null;
        this.list = null;

        this.el = null;
        this.active = null;
        this.stop = false;

        this.start = 0;
        this.time  = 600;

        this.init();
    }

    init() {
        let config = this.config;

        this.main = document.querySelector('.' + config.main);
        if(this.main)
        {
            this.current = this.main.querySelector('.' + config.current);
            this.list = this.main.querySelector('.' + config.list);

            this.setup();
        }
    }

    setup() {
        let config = this.config,
            currentItems = this.main.querySelector('.' + config.current + '> div'),
            items = this.list.querySelector('.' + config.listItems),
            item = items.querySelectorAll('.' + config.listItem);


        let current = document.createElement('div');
        current.addClass('ds-current-items');

        for(let i=0; i<item.length; i++)
        {
            let img = item[i].querySelector('img'),
                currentItem = document.createElement('div'),
                currentItemSrc = document.createElement('img');

            currentItem.addClass('ds-current-item ds-current-item-' + i);
            currentItem.style.zIndex = 1;
            currentItem.style.opacity = 0;
            currentItem.style.display = 'none';
            if(i===0) {
                currentItem.style.zIndex = 2;
                currentItem.style.display = 'list-item';
                currentItem.style.opacity = 1;

                this.el = currentItem;
            }

            currentItemSrc.setAttribute('src', img.getAttribute('src'));

            currentItem.appendChild(currentItemSrc);
            current.appendChild(currentItem);

            item[i].addClass('ds-list-item-' + i);
            item[i].addEventListener("click", ()=>{ this.click(i); });
        }


        let arrowUp = this.main.querySelector('.' + config.btnUp),
            arrowDown = this.main.querySelector('.' + config.btnDown);

        arrowUp.addEventListener("click", ()=> {  this.animateScroll(1); });
        arrowDown.addEventListener("click", ()=> {  this.animateScroll(-1); });


        currentItems.parentNode.replaceChild(current, currentItems);
    }

    click(i) {
        this.start = Date.now();
        this.active = this.current.querySelector('.ds-current-item-'+i);

        if(this.active!==this.el && !this.stop) this.animateClick(i);
    }

    animateClick(i) {
        this.stop = true;

        $('.ds-list-item').removeClass('active');
        $('.ds-list-item-' + i).addClass('active');

        $(this.active).css({
            opacity: 1,
            display: 'list-item'
        });

        $(this.el).animate({opacity: 0, display: 'none', zIndex:1},200, false, ()=>{
            $(this.el).css('display', 'none');
            $(this.active).css({display: 'list-item', zIndex:2});
        });

        this.el = this.active;
        this.stop = false;
    }

    animateScroll(step) {
        let conf = this.config,
            items = this.list.querySelector('.' + conf.listItems),
            item  = this.list.querySelectorAll('.' + conf.listItem);

        let h = (item[0].offsetHeight + 30) * 2,
            t = items.offsetTop + (h * step);

        if(t<0 && t>items.offsetHeight*-1 && !this.stop)
        {
            this.stop = true;
            $(items).animate({
                top: t
            }, 300, false, ()=>{ this.stop = false; });
        }
    }
}

export default new Slider();