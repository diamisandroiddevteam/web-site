import Inputmask from 'inputmask';

// переменная содержит массив уже подключенных дом элементов
// во избежании повторного подключения
const currentInput = {};

class Input {
    constructor(event = null) {
        this.event = event;
        this.el = event.target;
        this.name = null;
        this.href = this.el.getAttribute('href');

    }

    init() {
        let container = this.el.parentNode,
            mask = this.el.getAttribute('data-mask'),
            name = this.el.getAttribute('name'),
            type = this.el.getAttribute('type');


        if(type==='radio') return false;


        if(container.hasClass('active') && !this.el.value.length) container.removeClass('active');
        else container.addClass('active');

        if(container.hasClass('field-error'))
            container.removeClass('field-error');

        $(this.el).on('blur', ()=>{
            if(!this.el.value.length) container.removeClass('active');
        });

        this.name = name;
        if(typeof currentInput[this.name]!=="undefined") return false;
        

        if(mask==='phone') this.maskPhone();
        if(mask==='email') this.maskEmail();
        if(mask==='int') this.maskInt();

    }


    toggleRadio(){
        let name = this.el.getAttribute('name'),
            type = this.el.getAttribute('type');

    }


    maskPhone() {
        let im = new Inputmask('+7 (999) 999-99-99');
        im.mask(this.el);

        currentInput[this.name] = true;
    }

    maskEmail() {
        Inputmask({
            mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
            greedy: false
        }).mask(this.el);

        currentInput[this.name] = true;
    }

    maskInt() {
        Inputmask({
            regex: "[0-9\ ]*",
        }).mask(this.el);

        currentInput[this.name] = true;
    }
}

export default Input;