'use strict';

class View
{
    constructor() {
        this.el = null;
        this.btnClass = 'btn-view';
        this.contenerClass = 'catalog-content';
        this.init();
    }

    init() {
        HTMLElement.prototype.addClass = function(x){
            var y = x.split(" "), z = this.className.split(" ");
            for(var i=0; i<y.length; i++){
                if( y[i].trim().length==0 ) continue;
                if( z.indexOf(y[i])>=0 ) continue;
                z.push(y[i]);
            }
            this.className = z.filter(function(_){return _.length>0}).join(" ");
            return this;
        };

        HTMLElement.prototype.removeClass = function(x){
            var y = x.split(" "), z = this.className.split(" ");
            for(var i=0, j; i<y.length; i++){
                if( y[i].trim().length==0 ) continue;
                while((j=z.indexOf(y[i])) >=0) z.splice(j, 1);
            }
            this.className = z.filter(function(_){return _.length>0}).join(" ");
            return this;
        };

        HTMLElement.prototype.hasClass = function(x){
            var y = this.className.split(" ");
            for(var i=0, j; i<y.length; i++){
                if( y[i].trim().length==0 ) continue;
                if(y[i]==x) return true;
            }
            return false;
        };

        document.addEventListener("click", (event) => {
            let target = event.target;

            while(target!==null) {
                if(!("tagName" in target)) break;
                if(target.tagName.toLowerCase()==="body") break;
                if(target.hasClass(this.btnClass)) {
                    this.el = target;
                    break;
                }
                target = target.parentNode;
            }

            if(this.el!==null) this.view();
            this.el = null;
        });
    }

    view() {
        let content = document.querySelector('.' + this.contenerClass);

        this.clearActive();
        this.el.addClass('active');

        if(this.el.hasClass('btn-view-list'))
        {
            content.removeClass('catalog-table');
            content.addClass('catalog-line');
        }
        else if(this.el.hasClass('btn-view-table'))
        {
            content.removeClass('catalog-line');
            content.addClass('catalog-table');
        }
    }

    clearActive() {
        let btn = document.querySelectorAll('.' + this.btnClass);
        for(let i=0; i<btn.length; i++) {
            btn[i].removeClass('active');
        }
    }
}


export default new View();