// API Documentation
// http://api.jqueryui.com/dialog/
import 'webpack-jquery-ui/dialog';
import './dialog.scss';
import Scroll from '../../_blocks/scroll/scroll';

class Dialog{
    constructor(){
        this.window = null;
    }


    init(){
        let btn = $('[data-dialog]');

        btn.on('click', (evt)=>{
            evt.preventDefault();

            let el = $(evt.currentTarget),
                link = el.attr('href'),
                type = el.data('dialog');

            $.ajax({
                url: link,
                data: {
                    ajax: 'y',
                    type: type
                },
                success: (data) => {
                    this.open(data);
                }
            });

        });
    }


    open(html) {
        let container = this.toggleDialog();

        if(!this.window) this.window = container;

        $('body').addClass('ui-window');

        $('<div/>').attr('id','dialog-close').appendTo(container);              // close
        $('<div/>').attr('id','dialog-body').html(html).appendTo(container);    // body
        $('<div/>').attr('id','dialog-footer').appendTo(container);             // footer

        this.resize(container);
        this.plagins();

        $('#dialog-container').click((evt)=>{
            // дополнительная проверка что клик пришелся на фон
            if($(evt.target).attr('id')==='dialog-container'){
                this.close(container);
            }
        });
        container.find('#dialog-close').click(()=>{ this.close(container); });

        this.autoPosition();
        // $(window).resize(()=>{ this.autoPosition(); });
    }


    // Вызываем дополнительные обработчики
    // После загрузки диалогового окна
    plagins(){
        // let scroll = new Scroll();
        // scroll.init();
    }


    resize(container) {
        let boxH = $(container).find('.window').height();

        $(container).find('#dialog-body').css({
            minHeight: boxH + 50 + 'px'
        });
        $(container).css({
            minHeight: boxH + 50 + 'px'
        });
    }



    toggleDialog() {
        let box = $('#dialog'),
            container = $('#dialog-container'),
            append = false;

        if(!container.length) { container = $('<div/>').attr('id', 'dialog-container'); append = true; }
        if(!box.length) { box = $('<div/>').attr('id','dialog'); }

        if(append) {
            container.appendTo('body');
        }

        box.html('');
        if(!box.hasClass('ui-dialog-content'))
        {
            box.dialog({
                autoOpen: false,
                modal: true,
                close: ()=>{ this.close(); }
            });
            box.appendTo(container);
        }

        return box;
    }



    toggleBackgorund() {
        let result = $('.ui-widget-overlay');

        if(!result.length) {
            result = $('<div/>')
                .addClass('ui-widget-overlay')
                .appendTo('body');
        }

        return result;
    }


    autoPosition(){
        let box = $(this.window).parent(),
            posX = $(document).outerWidth()/2 - $(box).outerWidth()/2,
            posY = $(document).scrollTop() + 80;

        this.resize();

        $(box).css('top', posY + 'px');
        $(box).css('left', posX + 'px');
    }

    close(dialog) {
        let box = dialog ? dialog : this.window;
        box.dialog("close");
        box.html("");
        $('body').removeClass('ui-window');
    }

}

export default Dialog;