//https://github.com/bqworks/slider-pro/
import 'slider-pro';
// import './slider.css';


class Slider
{
    constructor(id) {
        this.el = $('#'+id);
        this.up = null;
        this.down = null;
        this.slider = null;


        if(this.el)
        {
            this.up = this.el.find('.slider-arrow-up');
            this.down = this.el.find('.slider-arrow-down');
            this.init();
        }
    }

    init() {
        this.slider = this.el.sliderPro({
            arrows: true,
            autoplay: false,
            autoScaleLayers: false,
            width: '100%',
            height: 400,
            thumbnailArrows: false,
            thumbnailWidth: 170,
            thumbnailsPosition: 'right',
            breakpoints: {
                768: {
                    thumbnailsPosition: 'bottom',
                    thumbnailWidth: 150,
                    thumbnailHeight: 100
                }
            },
        });

        let width = $(window).width(),
            slider = this.el.data('sliderPro');

        if(width<768) {
            this.slider.sliderPro('thumbnailsPosition','bottom');
            $(window).resize(()=>{
                let w =  $(window).width();
                if(w>=768) this.slider.sliderPro('thumbnailsPosition','right');
            });
        }

        this.up.on('click', ()=>{
            slider.previousSlide();
        });

        this.down.on('click', ()=>{
            slider.nextSlide();
        });

    }
}

export default Slider;