import realty from '../vendor/filter-rialty';



class filter {
    constructor() {
        this.form = null;
        this.inputs = null;

        this.data = null;
        this.html = [];
    }

    init(formId) {

        this.form = document.getElementById(formId);
        if(!this.form) return false;

        this.inputs = this.form.querySelectorAll('input');

        for(let i=0; i<this.inputs.length; i++) {
            let type = this.inputs[i].getAttribute('type');

            switch(type) {
                case 'text':
                    this.inputs[i].addEventListener('change',(evt)=>{
                        console.log('text', evt, this);
                    });
                    break;
                case 'checkbox':
                    this.inputs[i].addEventListener('change', ()=> this.onChange(i));
                    break;
            }

        }

        this.load('/ajax-ads-list.html').then(data=>{
            this.parse(data);
        });
    }

    load(url) {
        return new Promise((res, fail)=>{
            const request = new XMLHttpRequest();
            request.open('GET', url, true);

            request.addEventListener('load', ()=>{

                request.status >= 200 && request.status < 400
                    ? res(realty)//res(request.responseText)
                    : fail(new Error(`Request Failed: ${request.statusText}`));
            });

            request.send();
        });
    }


    parse(data) {

        let result = data.result;
        for(let i=0; i<result.length; i++) {
            switch(result[i].type) {
                case 'numericRange':
                    break;
                case 'multiSelect':
                    this.tplSelect(result[i], true);
                    break;
                case 'select':
                    this.tplSelect(result[i]);
                    break;
                case 'slider':
                    this.tplSlider(result[i]);
                    break;
            }
        }

        this.html.forEach(node=>{
            let item = document.createElement('div');
            item.className = 'find-item';
            item.style = 'margin-top:35px;';
            item.appendChild(node);

            document.getElementById('filter-field').appendChild(item);
        });
    }

    tplSlider(data) {
        let slider = document.createElement('div'),
            sliderContent = document.createElement('div'),
            sliderLists = document.createElement('div');

        sliderContent.innerHTML = '<i class="select-icon"></i>' +
            '<div class="select-placeholder">' + data.title + '</div>' +
            '<div class="select-result"></div>';

        slider.className = 'select select-slider';
        sliderContent.className = 'select-content';
        sliderLists.className = 'select-lists';


        let wrap = document.createElement('div'),
            inputs = document.createElement('div');
        wrap.className = 'slc-slider-wrap';
        inputs.className = 'slc-input';

        let slcSlider = document.createElement('div');
        slcSlider.className = 'slc-slider';
        slcSlider.innerHTML = `<div id="${data.id}" class="slc-slider-container"></div>`;

        if(typeof data.inputs.from !== undefined) {
            let div = document.createElement('div'),
                input = document.createElement('input');

            div.className = 'slc-input-wrap';
            input.type = 'text';
            input.className = 'slc-slider-from';
            input.placeholder = data.current;
            input.value = data.currentValue.from;
            input.setAttribute('data-min', '0');

            div.appendChild(input);
            inputs.appendChild(div);
        }



        wrap.appendChild(inputs);
        wrap.appendChild(slcSlider);

        sliderLists.appendChild(wrap);

        slider.appendChild(sliderContent);
        slider.appendChild(sliderLists);

        this.html.push(slider);
        return slider;
    }


    tplSelect(data, multi = false) {
        let select = document.createElement('div'),
            selectContent = document.createElement('div'),
            selectLists = document.createElement('div'),
            selectCheck = document.createElement('div');

        if(multi) select.className = 'select select-multi';
        else select.className = 'select';

        selectContent.className = 'select-content';
        selectLists.className = 'select-lists';
        selectCheck.className = 'select-check';
        selectContent.innerHTML = '<i class="select-icon"></i>' +
            '<div class="select-placeholder">' + data.title + '</div>' +
            '<div class="select-result"></div>';


        let ul = document.createElement('ul');
        ul.className = 'select-lst-items';

        data.values.forEach((val)=>{
            let id = `${data.id}_${val.value}`,
                li = document.createElement('li'),
                input = document.createElement('input');

            id = id.replace('[','_');
            id = id.replace(']','');

            li.className = 'select-item';
            li.setAttribute('data-for', id);
            li.innerHTML = `<span>${val.name}</span>`;


            input.id = id;
            input.name = `${data.id}[${val.value}]`;
            input.type = 'checkbox';
            input.value = val.value;


            if(data.currentValue === val.value){
                // input.checked = true;
                // input.setAttribute('checked','checked');
            }

            ul.appendChild(li);
            selectCheck.appendChild(input);
        });

        let container = document.createElement('div');
        container.className = 'select-lst-container';
        container.appendChild(ul);
        selectLists.appendChild(container);


        select.appendChild(selectContent);
        select.appendChild(selectLists);
        select.appendChild(selectCheck);

        this.html.push(select);
        return select;
    }

    
    onChange(i){
        console.log(i);
    }


}



export default new filter();