'use strict';

class AnimatedHead
{
    constructor(selector='.globalnav')
    {
        this.doc = document.documentElement;
        this.body = document.querySelector('body');
        this.header = document.querySelector(selector);
        this.scroll = false;
        this.changeHeader = 160;

        this.init();
    }

    init() {

        this.scrollPage();
        window.addEventListener('scroll', ()=>{
            if( !this.scroll ) {
                this.scroll = true;
                this.scrollPage();
            }
        }, false);
    }

    scrollPage() {
        let sy = this.scrollY();

        if( sy >= this.changeHeader )
        {
            $(this.body).addClass('scroll');
            $(this.header).addClass('header-scroll');
        }
        else
        {
            $(this.body).removeClass('scroll');
            $(this.header).removeClass('header-scroll');
        }
        this.scroll = false;
    }

    scrollY() {
        return window.pageYOffset || this.doc.scrollTop;
    }
}

export default new AnimatedHead();