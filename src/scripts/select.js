'use strict';

import noUiSlider from 'nouislider';


class Select {
    constructor() {
        this.el = null;
        this.opt = null;
        this.sClassName = 'select';
        this.sValueClassName = 'select-result';
        this.sItemsClassName = 'select-item';
        this.sCloseClassName = 'select-icon';

        this.multi = false;
        this.scroll = false;

        this.uislider = {};
        this.nouislider = noUiSlider;

        this.init();
    }

    init() {

        document.addEventListener("click", (event) => {
            let target = event.target,
                clear = false;

            this.el = null;
            this.opt = null;

            while(target!==null)
            {
                if(!("tagName" in target)) break;
                if(target.tagName.toLowerCase()==="body") break;
                if(target.hasClass(this.sCloseClassName)) clear = true;
                if(target.hasClass(this.sItemsClassName)) this.opt = target;
                if(target.hasClass(this.sClassName)) {
                    this.el = target;
                    break;
                }
                target = target.parentNode;
            }

            if(clear) { this.clear(); return false; }
            if(this.el!==null && this.opt===null) this.open();
            if(this.el!==null && this.opt!==null) this.update();
            if(this.el===null) this.closeAll();
        });


        let selects = document.querySelectorAll("." + this.sClassName );
        for(let i=0; i<selects.length; i++) {
            let value = selects[i].querySelector("." + this.sValueClassName),
                active = selects[i].querySelector('input:checked');

            if(active !== null && active.value.length>0) {
                let id = active.id,
                    text = selects[i].querySelector('[data-for='+id+']').textContent;

                selects[i].addClass('select-active');
                value.innerHTML = text;
            }
        }

    }

    update() {
        if(this.el===null || this.opt===null) return false;

        let value = this.el.querySelector('.'+this.sValueClassName),
            label = this.opt.textContent.trim(),
            multiVal = 0;

        let placeholder = this.el.querySelector('.select-placeholder'),
            name = placeholder ? placeholder.innerHTML : '';


        this.multi = this.el.hasClass('select-multi');

        if(typeof value==="undefined") {
            this.el = null;
            this.close();
            return false;
        }


        let dataFor = this.opt.dataset.for,
            inputs = this.el.querySelectorAll('input');

        for(let i=0; i<inputs.length; i++)
        {
            if(inputs[i].id === dataFor)
            {
                if(!inputs[i].checked)
                {
                    inputs[i].checked = true;
                    inputs[i].setAttribute('checked','checked');
                    $(inputs[i]).change();

                    value.innerHTML = label;

                    if(this.el.hasClass('field-error')) this.el.removeClass('field-error');

                    this.el.addClass('select-active');
                    this.opt.addClass('select-active-item');
                }
                else
                {
                    inputs[i].checked = false;
                    inputs[i].removeAttribute('checked');
                    $(inputs[i]).change();

                    value.innerHTML = '';

                    this.opt.removeClass('select-active-item');
                }

                this.trueSelect(inputs[i]);
            }
            else if(this.multi===false)
            {
                inputs[i].checked = false;
                inputs[i].removeAttribute('checked');
                let item = this.el.querySelector('.' + this.sItemsClassName + '[data-for=' + inputs[i].getAttribute('id') + ']');
                if(item!==null) item.removeClass('select-active-item');
            }

            if(this.multi && inputs[i].checked)
            {
                multiVal += 1;
            }
        }

        if(multiVal > 0) {
            value.innerHTML = '<span>('+ multiVal + ')</span> ' + name;
        }

        if(value.innerHTML.length===0) this.el.removeClass('select-active');

        if(this.multi===false) this.close();
    }


    slider() {
        let value = this.el.querySelector('.'+this.sValueClassName);

        let placeholder = this.el.querySelector('.select-placeholder'),
            name = (placeholder ? placeholder.innerHTML : '');

        let container = this.el.querySelector('.slc-slider-container'),
            inputs = this.el.querySelectorAll('input'),
            id = container.getAttribute('id'),
            def = {
                start: [0],
                connect: true,
                range: {
                    min: 0,
                    max: 100
                }
            };

        if(this.el.hasClass('field-error')) this.el.removeClass('field-error');
        if(typeof value==="undefined") {
            this.el = null;
            this.close();
            return false;
        }

        if(typeof this.nouislider==="undefined") return false;

        if(!container.hasClass('noUi-target'))
        {
            let snapValues = [];

            for(let i=0; i<inputs.length; i++) {
                snapValues.push(inputs[i]);
                def.start[i] =  (inputs[i].value
                                    ? parseInt(inputs[i].value)
                                    : parseInt(inputs[i].dataset.start)
                                );
                if(i===0 && inputs.length>1) def.range.min = parseInt(inputs[i].dataset.start);
                if(i!==0 && i===inputs.length-1) def.range.max = parseInt(inputs[i].dataset.start);
            }

            if(snapValues.length===1) {
                def.connect = [true, false];
            }


            noUiSlider.create(container, def);


            // обновление инпут при скролинге
            container.noUiSlider.on('update', (values, handle) => {

                if(!this.el.hasClass('select-active')) this.el.addClass('select-active');

                let val = name.length<=0 ? '' : name + ': ';

                snapValues[handle].value = parseInt(values[handle]);


                if (snapValues.length === 2)
                {
                    val += 'от '+ parseInt(values[0]) + ' до ' + parseInt(values[1]);
                }
                else
                {
                    if(snapValues[handle].getAttribute('placeholder'))
                        val += snapValues[handle].getAttribute('placeholder') + ' ';
                    else
                        val += 'до ';

                    val +=  parseInt(values[0]);
                }

                value.innerHTML = val;
            });


            // ставим обработчки на отслеживание изменений
            snapValues.forEach((input, handle)=>{

                input.addEventListener('change', (event)=>{
                    let values = container.noUiSlider.get();


                    if(typeof values === "object") values[handle] = parseInt(event.target.value);
                    else values = parseInt(event.target.value);

                    container.noUiSlider.set(values);
                });

            });

        }
    }


    clear() {
        this.el.removeClass('select-active');

        let value = this.el.querySelector('.'+this.sValueClassName),
            inputs = this.el.querySelectorAll('input');

        for(let i=0; i<inputs.length; i++)
        {
            if(inputs[i].getAttribute('type') === "checkbox"){

                if(inputs[i].checked) $(inputs[i]).change();

                inputs[i].checked = false;
                inputs[i].removeAttribute('checked');

                let item = this.el.querySelector('.' + this.sItemsClassName + '[data-for=' + inputs[i].getAttribute('id') + ']');
                if(item!==null) item.removeClass('select-active-item');
            } else {
                inputs[i].value = '';
            };
        }
        value.innerHTML = '';
        this.close();
    }


    open() {
        if(this.el===null) return false;
        if(this.el.hasClass('disabled')) return false;

        this.closeAll();
        this.scroll = this.el.hasClass('select-slider');
        if(this.scroll) this.slider();

        this.el.addClass(!this.el.hasClass('dropdown') ? 'dropdown' : '');
    }

    close() {
        if(this.el===null) return false;
        this.el.removeClass('dropdown');
        this.el = null;
    }

    closeAll() {
        let selects = document.querySelectorAll("." + this.sClassName + ".dropdown");
        for(let i=0; i<selects.length; i++) {
            if(this.el===selects[i]) continue;
            selects[i].removeClass('dropdown');
        }
    }

    trueSelect(input) {
        let name = input.name,
            value = input.value;

        if(name==='field[type]')
        {
            let category = document.querySelector("[name*=category]:checked"),
                promise = new Promise((res, fail) => {

                    let request = new XMLHttpRequest(),
                        formData = new FormData();

                    formData.append('ajax', true);
                    formData.append('type', 'field');
                    formData.append('action', 'get');
                    formData.append('field[category]', category.value);
                    formData.append('field[type]' , value);

                    request.open('POST', window.location.href, true);
                    request.addEventListener('load', ()=>{
                        request.status >= 200 && request.status < 400
                            ? res(request.responseText)
                            : fail(new Error(`Request Failed: ${request.statusText}`));
                    });
                    request.send(formData);
                });

            promise.then((data)=>{
                let prop = document.getElementById('section-property');

                console.log(input);
                if(prop)
                {
                    let $arData = JSON.parse(data),
                        secMain = document.getElementById('section-main'),
                        secProp = document.getElementById('section-property'),
                        secCont = document.getElementById('section-contact');

                    secMain.className = '';
                    secProp.className = '';
                    secCont.className = '';

                    prop.innerHTML = '';
                    $arData.forEach(item => {
                        let group = document.createElement('div');
                        group.className = 'form-group';
                        group.innerHTML = item;
                        prop.appendChild(group);
                    });
                }
            });
        }
    }

}

export default new Select();