'use strict';

class Link {
    constructor(event = null) {
        this.event = event;
        this.el = event.target;
        this.href = this.el.getAttribute('href');
    }

    init() {
        if(this.el.hasClass('more')) this.more();
        if(this.el.hasClass('add-phone')) this.addPhone();
        if(this.el.hasClass('category-sub-btn')) this.categoryMore();
    }

    categoryMore(){
        let contener = this.el.parentNode,
            list = contener.querySelector('.category-sub-list');

        if(!list.hasClass('show')) {
            list.addClass('show');
            this.el.innerHTML = 'скрыть';
        } else {
            list.removeClass('show');
            this.el.innerHTML = 'показать еще';
        }
    }

    addPhone() {
        console.log('add');
    }

    // Каталог "отобразить еще"
    more() {
        this.event.preventDefault();
        let catalog = $('.catalog-content');

        catalog.addClass('loading');
        $.ajax({
            type: "GET",
            url: this.href,
            data: {},
            success: function (data) {
                $('.catalog-nav').remove();
                $(data).appendTo(catalog);
                setTimeout(function(){
                    catalog.removeClass('loading');
                },300);
            }
        });
    }
}

export default Link;