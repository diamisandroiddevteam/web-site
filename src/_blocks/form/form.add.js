import HtmlForm from '../html/htmlform';
import Dialog from '../../scripts/dialog/dialog';

import TestFields from './testFields';


class FormAdd extends HtmlForm
{
    constructor(){
        super();

        this.form = null;
        this.items = null;
        this.files = [];
        this.data = null;

        this.messsage = null;
        this.error = null;
    }



    init(form=null) {
        this.form = (form ? $(form) : $('[data-form]'));
        if(this.form.length) {
            this.start(this.form);
            this.form.on('submit', (event)=>{
                event.preventDefault();
                this.send(this.form);
            });
        }

    }


    start(form) {

        if(form.data('form')==='ads-add')
        {

            this.changeFiles(form);

            let formData = this.getFormData(form);

            $.ajax({
                url: window.location.href,
                type:'post',
                data: formData,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: (result) => {
                    if (result.status)
                    {
                        let fields = this.fields(form, result.result);
                        this.updateFormAdd(form, fields);
                    }

                }
            });
        }
    }


    /*
    * Обновляем веб-форму
    * добавления объявления
     */
    updateFormAdd(form, data) {
        let category = false,
            type = false,
            Section = {
                main: $(form).find('#section-main'),
                prop: $(form).find('#section-property')
            };

        Section.prop.html('');

        if(typeof data.main!=="undefined") {
            $.each(data.main, (i, item)=>{

                let htmlField;

                // Заменяем если элемент найден
                if(htmlField = $('.' + item.id)) {
                   htmlField.replaceWith( item.html );
                }


                if(item.data.id==='field[category]' && item.data.currentValue) category = true;
                if(item.data.id==='field[type]' && item.data.currentValue) type = true;
            });
        }

        if(typeof data.prop!=="undefined") {

            $('<div/>').addClass('form-group').html('<div class="title office-title">Параметры<div/>').appendTo(Section.prop);

            $.each(data.prop, (i, item)=>{

                let group = $('<div/>'),
                    field = $('<div/>'),
                    title = item.data.title;

                if(item.data.required)
                    title += '<span class="form-field--important"></span>';

                group.addClass('form-group');
                field.addClass('form-field');

                $('<div/>').addClass('form-field--label').html( title + ':').appendTo(field);
                $('<div/>').addClass('form-field--param').html( item.html ).appendTo(field);

                field.appendTo(group);
                group.appendTo(Section.prop);

            });
        }


        if(category && type)
        {
            $('#section-main').removeClass('section-disable');
            $('#section-contact').removeClass('section-disable');
            $('#section-save').removeClass('section-disable');
        }
        else
        {
            $('#section-main').addClass('section-disable');
            $('#section-contact').addClass('section-disable');
            $('#section-save').addClass('section-disable');
        }

    }


    changeFiles(container) {
        let box = container ? container : this.form;

        $(box).find('[type=file]').on('change', (evt)=>{
            let input = evt.target,
                image = $(input).parent().find('.field-file--preview');

            if(input.files && input.files[0]) {

                let reader = new FileReader();
                reader.onload = (e)=>{
                    $(image).css({
                        width: '102%',
                        height: '102%',
                        background: 'url('+e.target.result+') no-repeat',
                        backgroundSize: 'cover'
                    });
                };
                reader.readAsDataURL(input.files[0]);
            }
        });
    }


    change(container, callback=(el)=>{}) {

        let box = container ? container : this.form,
            items;

        items = box.find('[name]');
        items.on('change', (event)=> {

            let el = event.target;
            callback(el);

        });
    }

    clear(){}


    // Генерируем список полей
    // в виде DOM и групируем
    fields(form, fields) {

        let result = {};
        if(!fields) return result;

        for(let i=0; i<=fields.length; i++)
        {
            if(typeof fields[i]==="undefined") break;

            let field = this.getField(fields[i]);
            if(field)
            {
                if(typeof result[fields[i].group]==="undefined")
                    result[fields[i].group] = [];

                // Прикрепляем обработчик события при изменении
                if(fields[i].updatesForm) {
                    this.change(field.html, ()=>{ this.start(form); });
                }

                result[fields[i].group].push({
                    id: field.id,
                    data: fields[i],
                    html: field.html
                });
            }
        }
        return result;
    }



    getFormData(form) {
        let fd = new FormData;
        let data = $(form).serializeArray();
        let inputFiles = form.find('[type=file]');

        fd.append('ajax', 'y');

        if(inputFiles.length) {
            for(let i=0; i<inputFiles.length; i++) {

                let files = inputFiles[i].files;
                for(let j=0; j<files.length; j++) {

                    fd.append(inputFiles[i].name, files[j]);
                }
            }
        }

        for(let i=0; i<data.length; i++) {
            if(data[i].value) {
                fd.append(data[i].name, data[i].value);
            }
        }

        return fd;
    }


    required(field) {

    }


    send(form) {
        let action = $(form).attr('action') ? $(form).attr('action') : window.location.href,
            type = $(form).attr('method') ? $(form).attr('method') : 'post',
            data = this.getFormData(form);

        data.append('submit', 'send');
        $.ajax({
            url: action,
            type: type,
            data: data,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: (data) => {
                let status = data.status;
                if(!status) this.showError(data.result);
                else{
                    window.location.href = data.result.redirect;
                }
            }
        });
    }



    validation() { }



    showError(errors) {
        let stop = false;
        for(let i=0; i<errors.length; i++) {
            let input = $(errors[i].field);
            if(input.length)
            {
                let box = $(input).parents('.form-field--param'),
                    container = box.children();

                container.addClass('field-error');
                if(!stop && container.length) {
                    stop = true;
                    $('html, body').animate({
                        scrollTop: ($(container).offset().top - 100)
                    }, 800);
                }
            }

        }
    }


    showMessage() {
        Dialog.open('Объявление добавлено');
    }

}

export default FormAdd;