
export default {
    status: true,
    result: [
        {
            id: 'field[category]',
            title: 'Марка',
            updatesForm: true, // обновлять при изменении
            currentValue: 1161,
            type: 'select',
            values: [
                {value: "19652", name: "Иномарки"},
                {value: "19651", name: "Отечественные"},
                {value: "3431",  name: "AC"},
                {value: "1160",  name: "Acura"},
                {value: "1161",  name: "Alfa Romeo"},
                {value: "3432",  name: "Alpina"},
                {value: "3433",  name: "Aro"},
                {value: "3434",  name: "Asia"},
                {value: "1162",  name: "Aston Martin"},
                {value: "1163",  name: "Audi"},
                {value: "19928", name: "Bajaj"},
                {value: "9920",  name: "BAW"},
                {value: "1164",  name: "Bentley"},
                {value: "1165",  name: "BMW"},
                {value: "3435",  name: "Brilliance"}
            ]
        },
        {
            id: 'field[type]',
            title: 'Тип Объявления',
            inDisable: false,
            type: 'select',
            values: [
                {value: "1164",  name: "Bentley"},
                {value: "1165",  name: "BMW"},
                {value: "3435",  name: "Brilliance"}
            ]
        },
        {
            id: 'field[111]',
            title: 'Тип Кузова',
            type: 'multiselect',
            values: [
                {value: "869", name: "Седан"},
                {value: "872", name: "Хетчбэк"},
                {value: "870", name: "Универсал"},
                {value: "4804", name: "Внедорожник"},
                {value: "865", name: "Кабриолет"},
                {value: "866", name: "Купе"},
                {value: "867", name: "Лимузин"},
                {value: "4806", name: "Минивэн"},
                {value: "868", name: "Пикап"},
                {value: "871", name: "Фургон"},
                {value: "11695", name: "Микроавтобус"},
            ]
        },
        {
            id: 'field[222]',
            title: 'Тип Кузова',
            type: 'color',
            values: [
                {value: "#ffffff", name: "белый"},
                {value: "#e9e9e9", name: "серый"},
                {value: "#a6a6a6", name: "Универсал"},
                {value: "#000", name: "Внедорожник"},
                {value: "#974b00", name: "Кабриолет"},
                {value: "#ffeb78", name: "Купе"},
                {value: "#f1ddba", name: "Лимузин"},
                {value: "#fe0002", name: "Минивэн"},
                {value: "#ff7906", name: "Пикап"},
            ]
        }

    ]
};