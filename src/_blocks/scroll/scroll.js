class Scroll
{
    constructor(){
        this.el = null;
        this.active = 0;
    }

    init(){
        this.el = $('[data-scroll]');

        if(this.el.length) {
            this.el.css('overflow','hidden');
            this.generation();
        }
    }

    // Формируем стили
    generation()
    {
        let items = this.el.find('.'+this.el.data('scroll')),
            width = this.el.parents('#dialog') ? this.el.parents('#dialog').width() : items.width()-10,
            wrapRow = $('<div/>');

        wrapRow.css({
            width: (width + 20) * items.length,
            position: 'relative',
            overflow: 'hidden'
        });

        for(let i=0; i<items.length; i++)
        {
            if($(items[i]).hasClass('show')) {
                this.active = i;
                $(wrapRow).animate({'height': $(items[i]).height()+'px'});
                this.el.parents('#dialog')
                    ? this.el.parents('#dialog').css({
                        'min-height': $(items[i]).height() + 50 +'px',
                        'transition': 'all .3s',
                    })
                    : null;
            }

            $(items[i]).attr('data-scroll-key', i);
            $(items[i]).css({
                display: 'block',
                float: 'left',
                width: width + 'px',
                marginRight: '20px',
                position: 'relative'
            });

            $(items[i]).find('[data-scroll-target]').on('click', (e)=>{
                let idActive = this.el
                               .find('[data-scroll-id=' + $(items[i]).find('[data-scroll-target]').data('scroll-target') + ']')
                               .data('scroll-key');

                this.drive(this.el, idActive, e);
            });

            wrapRow.append(items[i]);
        }

        let clear = $('<div/>').css('clear','both');
        wrapRow.append(clear);

        this.el.innerHTML = '';
        this.el.append(wrapRow);
        this.drive(this.el, this.active);
    }



    drive(Element, idActive, evt=null)
    {
        if(evt) evt.preventDefault();

        let items = Element.find('.' + Element.data('scroll')),
            width = $(items[idActive]).width(),
            height = $(items[idActive]).height();


        $(Element).children().animate({
            'left': '-' + (width + 20) * idActive +'px',
            'height': height + 'px'
        },400);

        this.el.parents('#dialog') ? this.el.parents('#dialog').css({'min-height': height + 50 +'px'}) : null;

    }
}


export default Scroll;